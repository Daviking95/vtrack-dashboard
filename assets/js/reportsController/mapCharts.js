jQuery(document).ready(function () {

    var getToken = localStorage.getItem("x-auth-token");

    statesArray = [];

    filteredArray = [];

    $("#vmap").hide();
    $("#loader").show();

    getAllForms();

    // $("#vmap").vectorMap({map: 'nigeria_ng'});

    jQuery('#vmap').vectorMap({
        map: 'nigeria_ng',
        backgroundColor: '#fff',
        borderColor: '#818181',
        borderOpacity: 0.25,
        borderWidth: 1,
        color: '#E7EBFD',
        enableZoom: true,
        hoverColor: '#4D79F6',
        hoverOpacity: null,
        normalizeFunction: 'linear',
        scaleColors: ['#b6d6ff', '#005ace'],
        selectedColor: '#4D79F6',
        selectedRegions: null,
        showTooltip: true,
        onRegionOver: function (element, code, region) {
            // alert("Hey");
            console.log(element);
        },
        onRegionClick: function (element, code, region) {

            filteredArray = [];
            statesArray.forEach(element => {
                if (region.toLowerCase() === element) {
                    filteredArray.push(element);
                }
            });

            var message = 'You clicked "' +
                region +
                '" which total report of: ' +
                filteredArray.length;

            showAlert(message);
        }
    });

    function showAlert(message) {
        var SweetAlert = function () {};

        SweetAlert.prototype.init = function () {

            Swal.fire(message)
        }

        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
        $.SweetAlert.init()

    }

    function getAllForms() {

        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("x-auth-token", getToken);

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        fetch("https://peaceful-atoll-23751.herokuapp.com/" + "just-clock-285910.uc.r.appspot.com/api/form/all", requestOptions)
            .then(function (response) {
                if (response.status === 403) {
                    alert("Token expired. Login");
                    window.location.href = "index.php";
                }
                if (!response.ok) {
                    alert("Cannot login. Ensure your data is correct and you have internet connection");
                    throw new Error("HTTP status " + response.status);
                }

                return response.json();
            })
            .then(function (result) {
                result.details.forEach(element => {
                    // console.log(element.state);

                    if (element.firstApproval === true && element.secondApproval === true) {

                        statesArray.push(element.state);
                        // filterState(element.state);
                    }

                });

                $("#vmap").show();
                $("#loader").hide();
            })
            .catch(error => console.log('error', error));

    }

});