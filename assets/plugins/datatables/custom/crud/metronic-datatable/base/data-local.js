var DatatableDataForms = {
  init: function () {
    var e, a, i
    ;(e = JSON.parse(testTable)), (a = $('.m_datatable_2').mDatatable({
      data: { type: 'local', source: e, pageSize: 10 },
      layout: { theme: 'default', class: '', scroll: !1, footer: !1 },
      sortable: !0,
      pagination: !0,
      search: { input: $('#generalSearch') },
      columns: [
        { field: 'form_name', title: 'Form Data' },
          // { field: 'form_two', title: 'Form Two' },
          // { field: 'form_three', title: 'Form Three' },
          {
            field: 'user_name', title: 'Submitted By', template: function (t) {
              return ('<a href="view_single_user.php?user_email=' + t.user_email + '"  title="View User">' + t.user_email + '</a>')
            }
           },
          {
            field: 'input_date',
            title: 'Input Date',
            type: 'date',
            format: 'MM/DD/YYYY'
          },
        {
          field: 'Actions',
          width: 110,
          title: 'Actions',
          sortable: !1,
          overflow: 'visible',
          template: function (e, a, i) {
            return (
              '\t\t\t\t\t\t<div class="dropdown ' +
              (i.getPageSize() - a <= 4 ? 'dropup' : '') +
              '" style="display:none;">\t\t\t\t\t\t\t<a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" >                                <i class="la la-ellipsis-h"></i>                            </a>\t\t\t\t\t\t  \t<div class="dropdown-menu dropdown-menu-right">\t\t\t\t\t\t    \t<a class="dropdown-item" href="view_single_form.php?form_name=' + e.form_name + '"><i class="la la-eye"></i> View Form</a>\t\t\t\t\t\t    \t<a class="dropdown-item" href="#" id="'+e.form_name +'"><i class="la la-trash-o"></i> Delete Form</a>\t\t\t\t\t\t  \t</div>\t\t\t\t\t\t</div>\t\t\t\t\t\t<a href="view_single_form.php?form_name=' + e.form_name + '" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="View Form ">                            <i class="la la-eye"></i>                        </a>\t\t\t\t\t'
            )
          }
        }
      ]
    })), (i = a.getDataSourceQuery()), $('#m_form_status')
      .on('change', function () {
        a.search($(this).val(), 'Status')
      })
      .val(void 0 !== i.Status ? i.Status : ''), $('#m_form_type')
      .on('change', function () {
        a.search($(this).val(), 'Type')
      })
      .val(void 0 !== i.Type ? i.Type : ''), $(
      '#m_form_status, #m_form_type'
    ).selectpicker()
  }
}
jQuery(document).ready(function () {})

var DatatableDataFormsSingleUser = {
  init: function () {
    var e, a, i
      ; (e = JSON.parse(testTable)), (a = $('.m_datatable_user_forms').mDatatable({
        data: { type: 'local', source: e, pageSize: 10 },
        layout: { theme: 'default', class: '', scroll: !1, footer: !1 },
        sortable: !0,
        pagination: !0,
        search: { input: $('#singleUserFormSearch') },
        columns: [
          { field: 'form_one', title: 'Form One' },
          { field: 'form_two', title: 'Form Two' },
          { field: 'form_three', title: 'Form Three' },
          // { field: 'local_government', title: 'L.G.', width: 100 },
          // { field: 'amount', title: 'Amount', width: 100 },
          // { field: 'revenue_code', title: 'Revenue Code', width: 100 },
          {
            field: 'input_date',
            title: 'Input Date',
            type: 'date',
            format: 'MM/DD/YYYY'
          },
          {
          field: 'Actions',
          width: 110,
          title: 'Actions',
          sortable: !1,
          overflow: 'visible',
          template: function (e, a, i) {
            return (
              '\t\t\t\t\t\t</div>\t\t\t\t\t\t<a href="view_single_form.php?user_email=' + e.user_email + '" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="View ">                            <i class="la la-eye"></i>                        </a>\t\t\t\t\t'
            )
          }
        }
          // { field: 'gender', title: 'Gender' },
          // { field: 'phone', title: 'Payers Contact' }
        ]
      })), (i = a.getDataSourceQuery()), $('#m_form_status')
        .on('change', function () {
          a.search($(this).val(), 'Status')
        })
        .val(void 0 !== i.Status ? i.Status : ''), $('#m_form_type')
          .on('change', function () {
            a.search($(this).val(), 'Type')
          })
          .val(void 0 !== i.Type ? i.Type : ''), $(
            '#m_form_status, #m_form_type'
          ).selectpicker()
  }
}
jQuery(document).ready(function () { })


var DatatableDataUsers = {
  init: function () {
    var e, a, i;
    (e = JSON.parse(testTable)), (a = $('.m_datatable_agent').mDatatable({
      data: { type: 'local', source: e, pageSize: 10 },
      layout: { theme: 'default', class: '', scroll: !1, footer: !1 },
      sortable: !0,
      pagination: !0,
      search: { input: $('#generalSearch') },
      columns: [
        // { field: 'fuser_first_name', title: 'First Name' },
        // { field: 'user_last_name', title: 'Last Name' },
        {
            field: 'user_name', title: 'Username', template: function (t) {
              return ('<a href="view_single_user.php?user_email=' + t.user_email + '">'+ t.user_name +'</a>')
              }
        },
        { field: 'user_gender', title: 'Gender' },
        { field: 'user_phone_number', title: 'Phone', type: 'mobile', format: 'Mobile' },
        { field: 'user_email', title: 'Email', type: 'email' },
        // { field: 'user_age', title: 'Age' },
        // { field: 'user_address', title: 'Address' },
        { field: 'user_description', title: 'Description' },
        { field: 'user_location', title: 'Last Known Location' },
        {
          field: 'Actions',
          width: 110,
          title: 'Actions',
          sortable: !1,
          overflow: 'visible',
          template: function (e, a, i) {
            return (
              '\t\t\t\t\t\t<div class="dropdown ' +
              (i.getPageSize() - a <= 4 ? 'dropup' : '') +
              '">\t\t\t\t\t\t\t<a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown">                                <i class="la la-ellipsis-h"></i>                            </a>\t\t\t\t\t\t  \t<div class="dropdown-menu dropdown-menu-right">\t\t\t\t\t\t    \t<a class="dropdown-item" href="edit_single_user.php?user_email=' + e.user_email + '"><i class="la la-edit"></i> Edit User Details</a>\t\t\t\t\t\t    \t<a class="dropdown-item" href="view_single_user.php?user_email=' + e.user_email + '"><i class="la la-eye"></i> View User</a>\t\t\t\t\t\t    \t<a class="dropdown-item" href="view_single_agent_transaction.php?user_email=' + e.user_email + '" hidden><i class="la la-print"></i> View Agent Transactions</a>\t\t\t\t\t\t  \t</div>\t\t\t\t\t\t</div>\t\t\t\t\t\t<a href="view_single_user.php?user_email=' + e.user_email + '" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="View ">                            <i class="la la-eye"></i>                        </a>\t\t\t\t\t'
            )
          }
        }
      ]
    })), (i = a.getDataSourceQuery()), $('#m_form_status')
      .on('change', function () {
        a.search($(this).val(), 'Status')
      })
      .val(void 0 !== i.Status ? i.Status : ''), $('#m_form_type')
      .on('change', function () {
        a.search($(this).val(), 'Type')
      })
      .val(void 0 !== i.Type ? i.Type : ''), $(
      '#m_form_status, #m_form_type'
    ).selectpicker()
  }
}
jQuery(document).ready(function () {})