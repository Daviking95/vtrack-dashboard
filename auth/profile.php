<!DOCTYPE html>
<html lang="en">

<?php $header_title = 'Profile | VTrack Reporting Dasboard - A concept of KDI';
include '../customs/app_head.php'
?>

<body>

    <?php include '../customs/app_topbar.php'?>

    <div class="page-wrapper">
        <!-- Left Sidenav -->
        <?php include '../customs/app_sidenav.php'?>
        <!-- end left-sidenav-->

        <!-- Page Content-->
        <div class="page-content">

            <div class="container-fluid">
                <!-- Page-Title -->
                <?php $page_title = 'Profile';
include_once '../customs/app_page_title.php'?>
                <!-- end page title end breadcrumb -->

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">

                                <div class="cssload-thecube" id="loader" style="display: none">
                                    <div class="cssload-cube cssload-c1"></div>
                                    <div class="cssload-cube cssload-c2"></div>
                                    <div class="cssload-cube cssload-c4"></div>
                                    <div class="cssload-cube cssload-c3"></div>
                                </div>
                                <form id="form-horizontal-admin" class="form-horizontal form-wizard-wrapper">
                                    <fieldset>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label for="txtFirstName" class="col-lg-3 col-form-label">First
                                                        Name</label>
                                                    <div class="col-lg-9">
                                                        <input id="txtFirstName" name="txtFirstName" type="text"
                                                            class="form-control">
                                                    </div>
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-->
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label for="txtLastName" class="col-lg-3 col-form-label">Last
                                                        Name</label>
                                                    <div class="col-lg-9">
                                                        <input id="txtLastName" name="txtLastName" type="text"
                                                            class="form-control">
                                                    </div>
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-->

                                        </div>
                                        <!--end row-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label for="txtEmail" class="col-lg-3 col-form-label">Email</label>
                                                    <div class="col-lg-9">
                                                        <input id="txtEmail" name="txtEmail" type="email"
                                                            class="form-control">
                                                    </div>
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-->
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label for="txtPhoneNumber" class="col-lg-3 col-form-label">Phone
                                                        Number</label>
                                                    <div class="col-lg-9">
                                                        <input id="txtPhoneNumber" name="txtPhoneNumber" type="text"
                                                            class="form-control">
                                                    </div>
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-->

                                        </div>
                                        <!--end row-->

                                        <div class="row">
                                            <div class="col-md-4">
                                            </div>
                                            <div class="col-md-4">

                                                <button
                                                    class="btn btn-primary btn-round btn-block waves-effect waves-light"
                                                    type="button" id="updateAdmin">Update Profile<i
                                                        class="fas fa-sign-in-alt ml-1"></i></button>
                                            </div>
                                            <!--end col-->
                                            <div class="col-md-4">
                                            </div>
                                            <!--end col-->

                                        </div>
                                        <!--end row-->

                                    </fieldset>
                                    <!--end fieldset-->
                                </form>
                                <!--end form-->
                            </div>
                            <!--end card-body-->
                        </div>
                        <!--end card-->
                    </div>
                    <!--end col-->
                </div>
                <!--end row-->

            </div><!-- container -->

            <?php include_once '../customs/app_footer.php'?>
            <!--end footer-->
        </div>
        <!-- end page content -->
    </div>
    <!-- end page-wrapper -->

    <!-- jQuery  -->
    <?php include '../customs/app_js_files.php'?>

    <!-- Custom Charts Functions js -->
    <script src="../assets/js/adminController/updateCurrentAdmin.js"></script>

</body>

</html>