<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="../assets/js/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../assets/js/bootstrap.bundle.min.js"></script>
<script src="../assets/js/metisMenu.min.js"></script>
<script src="../assets/js/waves.min.js"></script>
<script src="../assets/js/jquery.slimscroll.min.js"></script>

<script src="../assets/plugins/moment/moment.js"></script>
<script src="../assets/plugins/apexcharts/apexcharts.min.js"></script>
<!-- <script src="../assets/pages/jquery.analytics_dashboard.init.js"></script> -->
<script src="http://cdn.datatables.net/plug-ins/1.10.15/dataRender/datetime.js"></script>

<script src="https://apexcharts.com/samples/assets/irregular-data-series.js"></script>
<script src="https://apexcharts.com/samples/assets/ohlc.js"></script>

<script src="../assets/plugins/vendors/base/vendors.bundle.js" type="text/javascript"></script>
<script src="../assets/plugins/base/scripts.bundle.js" type="text/javascript"></script>
<script src="../assets/plugins/metronic-datatable/base/data-local.js" type="text/javascript"></script>


<script src="../assets/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js"></script>
<script src="../assets/plugins/jvectormap/jquery-jvectormap-us-aea-en.js"></script>

<script src="../assets/plugins/vector-map/jquery.vmap.js"></script>
<script src="../assets/plugins/vector-map/maps/jquery.vmap.nigeria.js"></script>

<!-- <script src="../assets/pages/jquery.crm_dashboard.init.js"></script> -->

<!-- <script src="../assets/plugins/daterangepicker/daterangepicker.js"></script>
<script src="../assets/plugins/select2/select2.min.js"></script>
<script src="../assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<script src="../assets/plugins/timepicker/bootstrap-material-datetimepicker.js"></script>
<script src="../assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
<script src="../assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js"></script>
<script src="../assets/pages/jquery.forms-advanced.js"></script> -->

<!-- Chart JS -->
<script src="../assets/plugins/chartjs/chart.min.js"></script>

<!--Chartist Chart-->
<script src="../assets/plugins/chartist/js/chartist.min.js"></script>
<script src="../assets/plugins/chartist/js/chartist-plugin-tooltip.min.js"></script>

<!--Morris Chart-->
<script src="../assets/plugins/morris/morris.min.js"></script>
<script src="../assets/plugins/raphael/raphael.min.js"></script>

<!-- Sweet-Alert  -->
<script src="../assets/plugins/sweet-alert2/sweetalert2.min.js"></script>
<script src="../assets/pages/jquery.sweet-alert.init.js"></script>

<!-- Parsley js -->
<script src="../assets/plugins/parsleyjs/parsley.min.js"></script>
<script src="../assets/pages/jquery.validation.init.js"></script>

<script src="../assets/js/jquery.core.js"></script>
<script src="../assets/js/jquery.slimscroll.min.js"></script>
<script src="../assets/js/metisMenu.min.js"></script>

<script src="../assets/plugins/jquery-steps/jquery.steps.min.js"></script>
<script src="../assets/pages/jquery.form-wizard.init.js"></script>

<!-- Add Firebase products that you want to use -->
<script src="https://www.gstatic.com/firebasejs/7.17.1/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.17.1/firebase-storage.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.17.1/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.17.1/firebase-firestore.js"></script>

<script src="../assets/js/firebase_init.js"></script>

<script src="../assets/js/app.js"></script>

<script src="../assets/js/homeController/topbar.js"></script>

<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>

<script>
(function() {
    var getToken = localStorage.getItem("x-auth-token");
    if (getToken === null || getToken === "") {
        alert("Token expired. Login again");
        window.location.href = "../index.php";
    }
    var getRole = localStorage.getItem("role");
    console.log(getRole);
    if (getRole !== "approver") $('.only_for_approver').hide();
})(jQuery)

$('#logout').click(function() {
    alert("Logout Successful");
    localStorage.clear();
});
</script>