<!DOCTYPE html>
<html lang="en">

<?php $header_title = 'VTrack Reporting Dasboard - A concept of KDI';
include_once 'customs/index_head.php'
?>

<body class="account-body accountbg">

    <!-- Log In page -->
    <div class="row vh-100 ">
        <div class="col-12 align-self-center">
            <div class="auth-page">
                <div class="card auth-card shadow-lg">
                    <div class="card-body">
                        <div class="px-3">
                            <div class="auth-logo-box">
                                <a href="dashboard/home.php" class="logo logo-admin"><img
                                        src="assets/images/logo-sm.png" height="55" alt="logo" class="auth-logo"></a>
                            </div>
                            <!--end auth-logo-box-->

                            <div class="text-center auth-logo-text">
                                <h4 class="mt-0 mb-3 mt-5">Reset Password For VTrack</h4>
                                <p class="text-muted mb-0">Enter your Email and instructions will be sent to you!</p>
                            </div>
                            <!--end auth-logo-text-->


                            <form class="form-horizontal auth-form my-4" action="index.php">

                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <div class="input-group mb-3">
                                        <span class="auth-form-icon">
                                            <i class="dripicons-user"></i>
                                        </span>
                                        <input type="email" class="form-control" id="reset_email" parsley-type="email"
                                            placeholder="Enter email" required>
                                    </div>
                                </div>
                                <!--end form-group-->

                                <div class="form-group mb-0 row">
                                    <div class="col-12 mt-2">
                                        <button class="btn btn-primary btn-round btn-block waves-effect waves-light"
                                            type="button" id="reset_btn_div">Reset Password <i
                                                class="fas fa-sign-in-alt ml-1"></i></button>
                                    </div>
                                    <!--end col-->
                                </div>

                                <div class="cssload-thecube" id="loader" style="display: none">
                                    <div class="cssload-cube cssload-c1"></div>
                                    <div class="cssload-cube cssload-c2"></div>
                                    <div class="cssload-cube cssload-c4"></div>
                                    <div class="cssload-cube cssload-c3"></div>
                                </div>
                                <!--end form-group-->
                            </form>
                            <!--end form-->
                        </div>
                        <!--end /div-->

                        <div class="m-3 text-center text-muted">
                            <p class="">Have an account ? <a href="index.php"
                                    class="text-primary ml-2">Login</a></p>
                        </div>
                    </div>
                    <!--end card-body-->
                </div>
                <!--end card-->
                <!-- <div class="account-social text-center mt-4">
                        <h6 class="my-4">Or Login With</h6>
                        <ul class="list-inline mb-4">
                            <li class="list-inline-item">
                                <a href="" class="">
                                    <i class="fab fa-facebook-f facebook"></i>
                                </a>                                    
                            </li>
                            <li class="list-inline-item">
                                <a href="" class="">
                                    <i class="fab fa-twitter twitter"></i>
                                </a>                                    
                            </li>
                            <li class="list-inline-item">
                                <a href="" class="">
                                    <i class="fab fa-google google"></i>
                                </a>                                    
                            </li>
                        </ul>
                    </div> -->
                <!--end account-social-->
            </div>
            <!--end auth-page-->
        </div>
        <!--end col-->
    </div>
    <!--end row-->
    <!-- End Log In page -->

    <?php include_once 'customs/js_files.php' ?>

    <!-- Index Functions js -->
    <script src="assets/js/homeController/auth.js"></script>


</body>

</html>