(function ($) {

    var getToken = localStorage.getItem("x-auth-token");

    peopleVictimsArrayItem1 = [];
    peopleVictimsArrayItem2 = [];
    peopleVictimsArrayItem3 = [];
    peopleVictimsArrayItem4 = [];
    peopleVictimsArrayItem5 = [];
    peopleVictimsArrayItem6 = [];
    peopleVictimsArrayItem7 = [];
    peopleVictimsArrayItem8 = [];
    peopleVictimsArrayItem9 = [];
    peopleVictimsArrayItem10 = [];
    peopleVictimsArrayItem11 = [];
    peopleVictimsArrayItem12 = [];

    async function getAllFormsForViolenceVictimPeopleAndGenderInvolved() {


        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("x-auth-token", getToken);

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        fetch("https://peaceful-atoll-23751.herokuapp.com/" + "just-clock-285910.uc.r.appspot.com/api/form/all", requestOptions)
            .then(function (response) {
                if (response.status === 403) {
                    alert("Token expired. Login");
                    window.location.href = "index.php";
                }
                if (!response.ok) {
                    alert("Cannot login. Ensure your data is correct and you have internet connection");
                    throw new Error("HTTP status " + response.status);
                }

                return response.json();
            })
            .then(function (result) {

                var detailsArray = result.details;
                var incidentViolenceVictimsArray = [];

                for (let index = 0; index < detailsArray.length; index++) {
                    const element = detailsArray[index];

                    if (element.firstApproval === true && element.secondApproval === true) {
                        incidentViolenceVictimsArray.push(element.incidentViolenceVictims);
                    }

                }

                fillPeopleVictimsArrayWithResult(incidentViolenceVictimsArray);

                showingChart();

            })
            .catch(error => console.log('error', error));

    }

    function fillPeopleVictimsArrayWithResult(data) {

        peopleVictimsArrayItem1 = [];
        peopleVictimsArrayItem2 = [];
        peopleVictimsArrayItem3 = [];
        peopleVictimsArrayItem4 = [];
        peopleVictimsArrayItem5 = [];
        peopleVictimsArrayItem6 = [];
        peopleVictimsArrayItem7 = [];
        peopleVictimsArrayItem8 = [];
        peopleVictimsArrayItem9 = [];
        peopleVictimsArrayItem10 = [];
        peopleVictimsArrayItem11 = [];
        peopleVictimsArrayItem12 = [];

        data.reduce(function (a, b) {
                return a.concat(b);
            })
            .filter(function (element) {

                // console.log(element.violenceVictimsType.toLowerCase());

                let elementFinal = element.violenceVictimsType.toLowerCase();

                switch (elementFinal) {
                    case 'political party leader or supporter, candidate or candidate supporter':
                        peopleVictimsArrayItem1.push(element);

                        break;
                    case 'government / state actor':
                        peopleVictimsArrayItem2.push(element);
                        break;
                    case 'party agent':
                        peopleVictimsArrayItem3.push(element);
                        break;
                    case "election observer / monitor":
                        peopleVictimsArrayItem4.push(element);
                        break;

                    case "election worker (pos, apos, returning officers)":
                        peopleVictimsArrayItem5.push(element);
                        break;
                    case "voter(s)/ public":
                        peopleVictimsArrayItem6.push(element);
                        break;
                    case "campaign material (posters, billboards)":
                        peopleVictimsArrayItem7.push(element);
                        break;
                    case "election office, property, material (like ballots, trucks, warehouse, etc.)":
                        peopleVictimsArrayItem8.push(element);
                        break;
                    case "political party office or property (specify party)":
                        peopleVictimsArrayItem9.push(element);
                        break;
                    case "state (gov't) office or property":
                        peopleVictimsArrayItem10.push(element);
                        break;
                    case "private property/building":
                        peopleVictimsArrayItem11.push(element);
                        break;
                    case "others":
                        peopleVictimsArrayItem12.push(element);

                        break;

                    default:
                        break;
                }
            });


    }

    function showingChart() {

        var options = {
            chart: {
                height: 374,
                type: 'line',
                shadow: {
                    enabled: false,
                    color: '#bbb',
                    top: 3,
                    left: 2,
                    blur: 3,
                    opacity: 1
                },
            },
            stroke: {
                width: 5,
                curve: 'smooth'
            },
            series: [{
                name: 'Numbers',
                data: [peopleVictimsArrayItem1.length, peopleVictimsArrayItem2.length, peopleVictimsArrayItem3.length, peopleVictimsArrayItem4.length, peopleVictimsArrayItem5.length, peopleVictimsArrayItem6.length, peopleVictimsArrayItem7.length, peopleVictimsArrayItem8.length, peopleVictimsArrayItem9.length, peopleVictimsArrayItem10.length, peopleVictimsArrayItem11.length, peopleVictimsArrayItem12.length]
            }],
            xaxis: {
                type: 'text',
                categories: ["Political party leader or supporter, Candidate or candidate supporter", "Government / State actor", "Party agent", "Election observer / Monitor", "Election Worker (POs, APOs, Returning Officers)", "Voter(s)/ Public", "Campaign Material (Posters, Billboards)", "Election office, property, material (like ballots, trucks, warehouse, etc.)", "Political party office or property (specify party)", "State (Gov't) office or property", "Private property/building", "Other"],
                axisBorder: {
                    show: true,
                    color: '#bec7e0',
                },
                axisTicks: {
                    show: true,
                    color: '#bec7e0',
                },
            },
            title: {
                // text: 'Social Media',
                align: 'left',
                style: {
                    fontSize: "16px",
                    color: '#666'
                }
            },
            fill: {
                type: 'gradient',
                gradient: {
                    shade: 'dark',
                    gradientToColors: ['#43cea2'],
                    shadeIntensity: 1,
                    type: 'horizontal',
                    opacityFrom: 1,
                    opacityTo: 1,
                    stops: [0, 100, 100, 100]
                },
            },
            markers: {
                size: 4,
                opacity: 0.9,
                colors: ["#ffbc00"],
                strokeColor: "#fff",
                strokeWidth: 2,
                style: 'inverted', // full, hollow, inverted
                hover: {
                    size: 7,
                }
            },
            yaxis: {
                min: -10,
                max: 40,
                title: {
                    text: 'No. of Victims',
                },
            },
            grid: {
                row: {
                    colors: ['transparent', 'transparent'], // takes an array which will be repeated on columns
                    opacity: 0.2
                },
                borderColor: '#185a9d'
            },
            responsive: [{
                breakpoint: 600,
                options: {
                    chart: {
                        toolbar: {
                            show: false
                        }
                    },
                    legend: {
                        show: false
                    },
                }
            }]
        }

        var chart = new ApexCharts(
            document.querySelector("#apex_line1"),
            options
        );

        chart.render();
    }

    //initializing
    async function init() {
        "use strict";
        await getAllFormsForViolenceVictimPeopleAndGenderInvolved();
        // showingChart()
    }

    init();

})(jQuery)