<!DOCTYPE html>
<html lang="en">

<?php $header_title = 'Election Results | VTrack Reporting Dasboard - A concept of KDI';
include '../customs/app_head.php'
?>

<body>

    <?php include '../customs/app_topbar.php'?>

    <div class="page-wrapper">
        <!-- Left Sidenav -->
        <?php include '../customs/app_sidenav.php'?>
        <!-- end left-sidenav-->

        <!-- Page Content-->
        <div class="page-content">

            <div class="container-fluid">
                <!-- Page-Title -->
                <?php $page_title = 'Election Result Data';
include_once '../customs/app_page_title.php'?>
                <!-- end page title end breadcrumb -->

                <div class="row">
                    <div class="col-lg-12">
                        <!--begin::Portlet-->
                        <div
                            class="m-portlet m-portlet--mobile m-portlet--creative m-portlet--first m-portlet--bordered-semi">

                            <div class="m-portlet__body">
                                <!--begin: Datatable -->
                                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                                    <div class="row align-items-center">
                                        <div class="col-xl-8 order-2 order-xl-1">
                                            <div class="form-group m-form__group row align-items-center">

                                                <div class="col-md-6">
                                                    <div class="m-input-icon m-input-icon--left">
                                                        <input type="text" class="form-control m-input m-input--solid"
                                                            placeholder="Search..." id="allElectionReportSearch">
                                                        <span class="m-input-icon__icon m-input-icon__icon--left">
                                                            <span>
                                                                <i class="la la-search"></i>
                                                            </span>
                                                        </span>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                        <div class="col-xl-4 order-2 order-xl-1">
                                            <span class="input-group-append ml-2">
                                                <button class="btn btn-primary js--triggerAnimation" type="button"
                                                    id="add_result">Add Result
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <!-- Table -->
                                <div class="m_datatable_election_result" id="local_data"></div>
                                <!-- End Table -->
                            </div>
                        </div>
                        <!--end::Portlet-->
                    </div>
                </div>

                <div class="report-popup">
                    <div class="report-popup__content">
                        <div class="row">
                            <div class="col-lg-12"><i id="close-report" class="far fa-window-close"
                                    style="float: right;width:50px;height: 50px;font-size: 25px;color: red;"
                                    aria-hidden="true"></i></div>
                        </div>

                        <div class="row">

                            <div class="form-group col-lg-6 row">
                                <label for="txtTitle" class="col-lg-3 col-form-label">Title. Ensure title is unique</label>
                                <div class="col-lg-9">
                                    <input id="txtTitle" name="txtTitle" type="text" class="form-control" required>
                                </div>
                            </div>

                            <div class="form-group col-lg-6 row">
                                <label for="txtAccrVoters" class="col-lg-3 col-form-label">Accredited Voters</label>
                                <div class="col-lg-9">
                                    <input id="txtAccrVoters" name="txtAccrVoters" type="number" class="form-control" required>
                                </div>
                            </div>

                        </div>

                        <div class="row">

                            <div class="form-group col-lg-6 row">
                                <label for="txtNoOfPolParty" class="col-lg-3 col-form-label">No Of Political
                                    Parties</label>
                                <div class="col-lg-9">
                                    <input id="txtNoOfPolParty" name="txtNoOfPolParty" type="number"
                                        class="form-control" required>
                                </div>
                            </div>

                            <div class="form-group col-lg-6 row">
                                <label for="txtNoOfPollingUnits" class="col-lg-3 col-form-label">No Of Polling
                                    Units</label>
                                <div class="col-lg-9">
                                    <input id="txtNoOfPollingUnits" name="txtNoOfPollingUnits" type="number"
                                        class="form-control" required>
                                </div>
                            </div>

                        </div>

                        <div class="row">

                            <div class="form-group col-lg-6 row">
                                <label for="txtPopulation" class="col-lg-3 col-form-label">Est. Population Of The
                                    State</label>
                                <div class="col-lg-9">
                                    <input id="txtPopulation" name="txtPopulation" type="number" class="form-control" required>
                                </div>
                            </div>

                            <div class="form-group col-lg-6 row">
                                <label for="txtPvcCollected" class="col-lg-3 col-form-label">PVC Collected</label>
                                <div class="col-lg-9">
                                    <input id="txtPvcCollected" name="txtPvcCollected" type="number"
                                        class="form-control" required>
                                </div>
                            </div>

                        </div>

                        <div class="row">

                            <div class="form-group col-lg-6 row">
                                <label for="txtRegVoters" class="col-lg-3 col-form-label">Registered Voters</label>
                                <div class="col-lg-9">
                                    <input id="txtRegVoters" name="txtRegVoters" type="number" class="form-control" required>
                                </div>
                            </div>

                            <div class="form-group col-lg-6 row">
                                <label for="txtState" class="col-lg-3 col-form-label">State</label>
                                <div class="col-lg-9">
                                    <select id="txtState" name="txtState"
                                        class="select2 form-control mb-3 custom-select"
                                        style="width: 100%; height:36px;" data-placeholder="Choose" required>
                                        <option disabled selected>--Select State--</option>
                                        <option value="abia">Abia</option>
                                        <option value="adamawa">Adamawa</option>
                                        <option value="akwa Ibom">Akwa Ibom</option>
                                        <option value="anambra">Anambra</option>
                                        <option value="bauchi">Bauchi</option>
                                        <option value="bayelsa">Bayelsa</option>
                                        <option value="benue">Benue</option>
                                        <option value="borno">Borno</option>
                                        <option value="cross River">Cross River</option>
                                        <option value="delta">Delta</option>
                                        <option value="ebonyi">Ebonyi</option>
                                        <option value="edo">Edo</option>
                                        <option value="ekiti">Ekiti</option>
                                        <option value="enugu">Enugu</option>
                                        <option value="abuja">Federal Capital Territory</option>
                                        <option value="gombe">Gombe</option>
                                        <option value="imo">Imo</option>
                                        <option value="jigawa">Jigawa</option>
                                        <option value="kaduna">Kaduna</option>
                                        <option value="kano">Kano</option>
                                        <option value="katsina">Katsina</option>
                                        <option value="kebbi">Kebbi</option>
                                        <option value="kogi">Kogi</option>
                                        <option value="kwara">Kwara</option>
                                        <option value="lagos">Lagos</option>
                                        <option value="nasarawa">Nasarawa</option>
                                        <option value="niger">Niger</option>
                                        <option value="ogun">Ogun</option>
                                        <option value="ondo">Ondo</option>
                                        <option value="osun">Osun</option>
                                        <option value="oyo">Oyo</option>
                                        <option value="plateau">Plateau</option>
                                        <option value="rivers">Rivers</option>
                                        <option value="sokoto">Sokoto</option>
                                        <option value="taraba">Taraba</option>
                                        <option value="yobe">Yobe</option>
                                        <option value="zamfara">Zamfara</option>
                                    </select>
                                </div>
                            </div>

                        </div>

                        <div class="row">

                            <div class="form-group col-lg-6 row">
                                <label for="txtUncollectedPvc" class="col-lg-3 col-form-label">Uncollected PVC</label>
                                <div class="col-lg-9">
                                    <input id="txtUncollectedPvc" name="txtUncollectedPvc" type="number"
                                        class="form-control" required>
                                </div>
                            </div>

                            <div class="form-group col-lg-6 row">
                                <label for="txtVoterPerPolParty" class="col-lg-3 col-form-label">Voter Per Political
                                    Party</label>
                                <div class="col-lg-9">
                                    <input id="txtVoterPerPolParty" name="txtVoterPerPolParty" type="number"
                                        class="form-control" required>
                                </div>
                            </div>

                        </div>

                        <div class="row">

                            <div class="form-group col-lg-6 row">
                                <label for="txtVotersTurnout" class="col-lg-3 col-form-label">Voters Turnout</label>
                                <div class="col-lg-9">
                                    <input id="txtVotersTurnout" name="txtVotersTurnout" type="number"
                                        class="form-control" required>
                                </div>
                            </div>

                            <div class="form-group col-lg-6 row">
                                <label for="txtYear" class="col-lg-3 col-form-label">Year</label>
                                <div class="col-lg-9">
                                    <input id="txtYear" name="txtYear" type="number" class="form-control" required>
                                </div>
                            </div>

                        </div>

                        <div class="row">

                            <div class="form-group col-lg-6 row center">
                                <button class="btn btn-primary js--triggerAnimation" type="button"
                                    id="submit_result">SUBMIT
                                </button>
                            </div>


                            <div class="cssload-thecube" id="loader3" style="display: none">
                                            <div class="cssload-cube cssload-c1"></div>
                                            <div class="cssload-cube cssload-c2"></div>
                                            <div class="cssload-cube cssload-c4"></div>
                                            <div class="cssload-cube cssload-c3"></div>
                                        </div>
                        </div>

                    </div>

                    <div></div>
                </div>

            </div><!-- container -->

            <?php include_once '../customs/app_footer.php'?>
            <!--end footer-->
        </div>
        <!-- end page content -->
    </div>
    <!-- end page-wrapper -->

    <!-- jQuery  -->
    <?php include '../customs/app_js_files.php'?>

    <!-- Custom Charts Functions js -->
    <script src="../assets/js/homeController/electionResult.js"></script>

</body>

</html>