// (function ($) {

// 'use strict';

var getToken = localStorage.getItem("tempPass");

function loginUser() {

  $('#loader').show();
  $('#submit_btn_div').hide();

  var _email = $('#user_email').val();
  var _password = $('#user_password').val();

  var myHeaders = new Headers();
  myHeaders.append("Content-Type", "application/json");

  var raw = JSON.stringify({
    "email": _email,
    "password": _password
  });

  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow'
  };

  fetch("https://peaceful-atoll-23751.herokuapp.com/" + "just-clock-285910.uc.r.appspot.com/api/admin/auth", requestOptions)
    .then(function (response) {
      if (response.status === 403) {
        alert("Token expired. Login");
        $('#loader').hide();
        $('#submit_btn_div').show();
        window.location.href = "index.php";
      }
      if (!response.ok) {
        alert("Cannot login. Ensure your data is correct and you have internet connection");
        $('#loader').hide();
        $('#submit_btn_div').show();
        throw new Error("HTTP status " + response.status);
      }

      return response.json();
    })
    .then(function (result) {
      localStorage.setItem("x-auth-token", result.details.token);
      localStorage.setItem("role", result.details.role);

      // alert(result.details);
      alert("Login successful");
      window.location.href = getToken === null || getToken === "" ? "dashboard/home.php" : "change-password.php";
      $('#loader').hide();
      $('#submit_btn_div').show();


    })
    .catch(error => console.log('error', error));

}



$('#reset_btn_div').click(function () {

  $('#loader').show();
  $('#reset_btn_div').hide();

  var _email = $('#reset_email').val();

  var myHeaders = new Headers();
  myHeaders.append("Content-Type", "application/json");

  var raw = JSON.stringify({
    "email": _email
  });

  var requestOptions = {
    method: 'PATCH',
    headers: myHeaders,
    body: raw,
    redirect: 'follow'
  };

  fetch("https://peaceful-atoll-23751.herokuapp.com/" + "just-clock-285910.uc.r.appspot.com/api/admin/forgotPassword", requestOptions)
    .then(function (response) {
      if (response.status === 403) {
        alert("Token expired. Login");
        $('#loader').hide();
        $('#reset_btn_div').show();
        window.location.href = "index.php";
      }
      if (!response.ok) {
        alert("Cannot login. Ensure your data is correct and you have internet connection");
        $('#loader').hide();
        $('#reset_btn_div').show();
        throw new Error("HTTP status " + response.status);
      }

      return response.json();
    })
    .then(function (result) {
      // localStorage.setItem("x-auth-token", result.details.token);
      localStorage.setItem("tempPass", result.details);

      alert(`Please copy this new password and paste in the password field of login page : ${result.details}`);
      alert(result.message);
      window.location.href = "index.php";
      $('#loader').hide();
      $('#reset_btn_div').show();


    })
    .catch(error => console.log('error', error));

});

function init() {
  loginUser();
  ResetPassword();
  Waves.init();
}

// init();

// })(jQuery)