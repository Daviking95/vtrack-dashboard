/*
 Template: VTrack - Bootstrap 4 Admin Dashboard
 Author: VTrack
 File: Treeview
 */



$(function () {
	"use strict";

	// Default
	$('#jstree').jstree();
	
	//Check Box
	$('#jstree-checkbox').jstree({
		"checkbox" : {
			"keep_selected_style" : false
		  },
		  "plugins" : [ "checkbox" ]
	});
});