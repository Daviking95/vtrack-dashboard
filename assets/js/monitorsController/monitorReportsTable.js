(function ($) {
    var getToken = localStorage.getItem("x-auth-token");

    'use strict';

    function getAllForms() {

        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("x-auth-token", getToken);

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        fetch("https://peaceful-atoll-23751.herokuapp.com/" + "just-clock-285910.uc.r.appspot.com/api/monitor/all", requestOptions)
            .then(function (response) {
                if (response.status === 403) {
                    alert("Token expired. Login");
                    window.location.href = "index.php";
                }
                if (!response.ok) {
                    alert("Cannot login. Ensure your data is correct and you have internet connection");
                    throw new Error("HTTP status " + response.status);
                }

                return response.json();
            })
            .then(function (result) {

                testTable = JSON.stringify(result.details);

                DatatableDataForms();
                $("#csv_div").css("display", "block");

            })
            .catch(error => console.log('error', error));

    }

    function DatatableDataForms() {
        var e, a, i;
        (e = JSON.parse(testTable)), (a = $('.m_datatable_2').mDatatable({
            data: {
                type: 'local',
                source: e,
                pageSize: 10
            },
            layout: {
                theme: 'default',
                class: '',
                scroll: !1,
                footer: !1
            },
            sortable: !0,
            pagination: !0,
            search: {
                input: $('#generalSearch')
            },
            columns: [{
                    field: 'user_name',
                    title: 'Username'
                },
                {
                    field: 'user_first_name',
                    title: 'First name',
                    // template: function (t) {
                    //     return ('<a href="view_single_user.php?user_email=' + t.user_email + '"  title="View User">' + t.user_email + '</a>')
                    // }
                },
                {
                    field: 'user_last_name',
                    title: 'Last name'
                },
                {
                    field: 'user_monitor_code',
                    title: 'Monitor Code'
                },
                // {
                //     field: 'user_email',
                //     title: 'Email'
                // },
                {
                    field: 'user_phone_number',
                    title: 'Phone Number'
                },
                // {
                //     field: 'user_gender',
                //     title: 'Gender'
                // },
                {
                    field: 'Actions',
                    width: 110,
                    title: 'Actions',
                    sortable: !1,
                    overflow: 'visible',
                    template: function (e, a, i) {
                        return (
                            '\t\t\t\t\t\t<div class="dropdown ' +
                            (i.getPageSize() - a <= 4 ? 'dropup' : '') +
                            '" style="display:none;">\t\t\t\t\t\t\t<a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" >                                <i class="la la-ellipsis-h"></i>                            </a>\t\t\t\t\t\t  \t<div class="dropdown-menu dropdown-menu-right">\t\t\t\t\t\t    \t<a class="dropdown-item" href="add.php?monitor_code=' + e._id + '"><i class="la la-eye"></i> View Form</a>\t\t\t\t\t\t    \t<a class="dropdown-item" href="#" id="' + e._id + '"><i class="la la-trash-o"></i> Delete Form</a>\t\t\t\t\t\t  \t</div>\t\t\t\t\t\t</div>\t\t\t\t\t\t<a href="add.php?monitor_code=' + e._id + '" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="View Form ">                            <i class="la la-eye"></i>                        </a>\t\t\t\t\t'
                        )
                    }
                }
            ]
        })), i = a.getDataSourceQuery(), $("#m_form_status").on("change", function () {
            a.search($(this).val(), "firstApproval")
        }).val(void 0 !== i.firstApproval ? i.firstApproval : ""), $("#m_form_type").on("change", function () {
            a.search($(this).val(), "Type")
        }).val(void 0 !== i.Type ? i.Type : ""), $("#m_form_status, #m_form_type").selectpicker()
    }


    function init() {
        getAllForms();
        Waves.init();
    }

    init();

})(jQuery)