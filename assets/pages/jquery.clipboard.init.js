/**
 * Theme: VTrack Reporting Dasboard - A concept of KDI
 * Author: VTrack
 * Clipboard Js
 */


var clipboard = new ClipboardJS('.btn');

clipboard.on('success', function(e) {
    console.log(e);
});

clipboard.on('error', function(e) {
    console.log(e);
});