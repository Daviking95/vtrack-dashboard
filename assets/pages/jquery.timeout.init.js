/**
 * Theme: VTrack Reporting Dasboard - A concept of KDI
 * Author: VTrack
 * Timeout Js
 */


$.sessionTimeout({
  keepAliveUrl: '../pages/page-starter.html',
  logoutButton:'Logout',
  logoutUrl: '../index.php',
  redirUrl: '../authentication/auth-lock-screen.html',
  warnAfter: 3000,
  redirAfter: 30000,
  countdownBar: true
});
