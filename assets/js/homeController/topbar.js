(function ($) {
    var getToken = localStorage.getItem("x-auth-token");

    'use strict';

    function getAdminDetails() {

        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("x-auth-token", getToken);

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        fetch("https://peaceful-atoll-23751.herokuapp.com/" + "just-clock-285910.uc.r.appspot.com/api/admin/getCurrentAdmin", requestOptions)
            .then(function (response) {
                if (response.status === 403) {
                    alert("Token expired. Login");
                    window.location.href = "index.php";
                }
                if (!response.ok) {
                    alert("Cannot login. Ensure your data is correct and you have internet connection");
                    throw new Error("HTTP status " + response.status);
                }

                return response.json();
            })
            .then(function (result) {
                // localStorage.setItem("x-auth-token", result.details.token);
                // localStorage.setItem("role", result.details.role);

                $('#admin_name span').text(result.details.firstName).append('<i class="mdi mdi-chevron-down"></i>');
            })
            .catch(error => console.log('error', error));

    }

    function init() {
        getAdminDetails();
        Waves.init();
    }

    init();

})(jQuery)