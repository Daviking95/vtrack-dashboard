/**
 * Theme: VTrack Reporting Dasboard - A concept of KDI
 * Author: VTrack
 * Responsive-table Js
 */

 
$(function() {
  $('.table-responsive').responsiveTable({
      addDisplayAllBtn: 'btn btn-secondary'
  });
});