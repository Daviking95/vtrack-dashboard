(function ($) {
    var getToken = localStorage.getItem("x-auth-token");

    var getRole = localStorage.getItem("role");

    let AddAdminsConstants = {};

    'use strict';

    function changeAdminPassword() {

        AddAdminsConstants["password"] = $('#txtPassword').val();

        AddAdminsConstants["passwordConfirmation"] = $('#txtConfirmPassword').val();

        console.log(AddAdminsConstants);

        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("x-auth-token", getToken);

        var requestOptions = {
            method: 'PATCH',
            headers: myHeaders,
            body: JSON.stringify(AddAdminsConstants),
            redirect: 'follow'
        };

        $("#form-horizontal-admin").hide();
        $("#loader").show();

        fetch("https://peaceful-atoll-23751.herokuapp.com/" + `just-clock-285910.uc.r.appspot.com/api/admin/changePassword`, requestOptions)
            .then(function (response) {
                if (response.status === 403) {
                    alert("Token expired. Login");
                    window.location.href = "../index.php";
                }
                if (!response.ok) {
                    // console.log(response.json());
                    // alert(response.statusText);

                    response.json().then(function (data) {
                        alert(data.message);
                    });

                    $("#form-horizontal-admin").show();
                    $("#loader").hide();
                    throw new Error("HTTP status " + response.status);
                }

                $("#form-horizontal-admin").show();
                $("#loader").hide();

                return response.clone().json();
            })
            .then(function (result) {

                console.log(result);

                if (result.statusCode !== 200) {
                    alert(result.message);

                    $("#form-horizontal-admin").show();
                    $("#loader").hide();
                    return;
                }

                $("#form-horizontal-admin").show();
                $("#loader").hide();
                alert(result.message);
                window.location.href = "../index.php";

            })
            .catch(error => console.log('error', error));

    }

    $("#changeAdminPassword").click(function () {
        changeAdminPassword();

    });


})(jQuery)