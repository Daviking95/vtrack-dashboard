(function ($) {

    // Use this link to get the vector maps and more
    // https://gadm.org/download_country_v3.html

    // Styling Google Maps
    // https://mapstyle.withgoogle.com

    let statesWithViolenceArray = [];

    let edoCounts = 0;
    let ondoCounts = 0;

    var map_parameters;

    map_parameters = {
        // center: {
        //     lat: 9.072264,
        //     lng: 7.491302
        // },
        // zoom: 6,
        center: {
            lat: 6.6342,
            lng: 5.9304
        },
        zoom: 8,
        styles: [{
                "elementType": "geometry",
                "stylers": [{
                    "color": "#f5f5f5"
                }]
            },
            {
                "elementType": "labels.icon",
                "stylers": [{
                    "visibility": "off"
                }]
            },
            {
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#616161"
                }]
            },
            {
                "elementType": "labels.text.stroke",
                "stylers": [{
                    "color": "#f5f5f5"
                }]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry",
                "stylers": [{
                    "visibility": "off"
                }]
            },
            {
                "featureType": "administrative.land_parcel",
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#bdbdbd"
                }]
            },
            {
                "featureType": "administrative.province",
                "elementType": "geometry.fill",
                "stylers": [{
                    "visibility": "on"
                }]
            },
            {
                "featureType": "administrative.province",
                "elementType": "geometry.stroke",
                "stylers": [{
                    "visibility": "on"
                }]
            },
            {
                "featureType": "poi",
                "stylers": [{
                    "visibility": "off"
                }]
            },
            {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#eeeeee"
                }]
            },
            {
                "featureType": "poi",
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#757575"
                }]
            },
            {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#e5e5e5"
                }]
            },
            {
                "featureType": "poi.park",
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#9e9e9e"
                }]
            },
            {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#ffffff"
                }]
            },
            {
                "featureType": "road",
                "elementType": "labels.icon",
                "stylers": [{
                    "visibility": "off"
                }]
            },
            {
                "featureType": "road.arterial",
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#757575"
                }]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#dadada"
                }]
            },
            {
                "featureType": "road.highway",
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#616161"
                }]
            },
            {
                "featureType": "road.local",
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#9e9e9e"
                }]
            },
            {
                "featureType": "transit",
                "stylers": [{
                    "visibility": "off"
                }]
            },
            {
                "featureType": "transit.line",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#e5e5e5"
                }]
            },
            {
                "featureType": "transit.station",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#eeeeee"
                }]
            },
            {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#c9c9c9"
                }]
            },
            {
                "featureType": "water",
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#9e9e9e"
                }]
            }
        ],
        // mapTypeId: google.maps.MapTypeId.HYBRID
        // mapTypeId: 'satellite'
    };

    var map = new google.maps.Map(document.getElementById('map3'), map_parameters);

    function getAllForms() {

        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        // myHeaders.append("x-auth-token", getToken);

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        fetch("https://peaceful-atoll-23751.herokuapp.com/just-clock-285910.uc.r.appspot.com/api/form/all", requestOptions)
            .then(function (response) {
                if (response.status === 403) {
                    alert("Token expired. Login");
                    window.location.href = "index.php";
                }
                if (!response.ok) {
                    alert("Cannot login. Ensure your data is correct and you have internet connection");
                    throw new Error("HTTP status " + response.status);
                }

                return response.json();
            })
            .then(function (result) {

                result.details.forEach(element => {
                    // console.log(element);
                    if (element.firstApproval === true && element.secondApproval === true) {

                        if (element.state.toLowerCase() == "ondo" || element.state.toLowerCase() == "edo") {
                            statesWithViolenceArray.push(element);
                        }

                        if (element.state.toLowerCase() == "ondo"){
                            ondoCounts += 1;
                        }else if(element.state.toLowerCase() == "edo"){
                            edoCounts += 1;
                        }
                    }
                });

                // showMap(statesWithViolenceArray);

                drawPolygonOnMap();

                // initMap();

            })
            .catch(error => console.log('error', error));

    }

    function drawPolygonOnMap() {

        // var edoCoordinates = buildCoordinatesArrayFromString(EdoStateBoundaries);

        // console.log(ondoCounts);
        // console.log(edoCounts);

        var EdoPolygon = new google.maps.Polygon({
            paths: EdoStateBoundaries,
            strokeColor: getColorCode("Edo", edoCounts), // '#ff0204',
            strokeOpacity: 0.8,
            strokeWeight: 3,
            fillColor: getColorCode("Edo", edoCounts), // '#ff0204',
            fillOpacity: 0.35,
        });

        var OndoPolygon = new google.maps.Polygon({
            paths: OndoStateBoundaries,
            strokeColor: getColorCode("Ondo", ondoCounts), // '#56cb51',
            strokeOpacity: 0.8,
            strokeWeight: 3,
            fillColor: getColorCode("Ondo", ondoCounts), // '#56cb51',
            fillOpacity: 0.35,
        });

        EdoPolygon.setMap(map);

        OndoPolygon.setMap(map);

    }

    function getColorCode(name, counts){
        let color;

        if (counts >= 10 ) {
                color = "#fd0105";
        }else if (counts > 5 && counts <= 10) {
                color = "#ffbf01";
        }else if (counts >= 1 && counts <= 5) {
                color = "#0fc501";
        } else {
                color = "#fff"; 
        }

        return color;
    }

    async function getLatLong(state) {

        var stateObject = {};

        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        return new Promise((resolve, reject) => {
            fetch(`https://peaceful-atoll-23751.herokuapp.com/https://maps.googleapis.com/maps/api/geocode/json?address=${state}&key=AIzaSyDyE7hN2pWfF64NDqyXKz3Sm_lql5obcOs`, requestOptions)
                .then(function (response) {

                    return response.json();
                })
                .then(function (result) {

                    var latitude = result.results[0].geometry.location.lat;
                    var longitude = result.results[0].geometry.location.lng;

                    stateObject = {
                        state: state,
                        lat: latitude,
                        long: longitude
                    };

                    // console.log(stateObject);
                    resolve(stateObject);

                })
                .catch(error => console.log('error', error));
        })

    }

    function buildCoordinatesArrayFromString(MultiGeometryCoordinates) {
        var finalData = [];
        var grouped = MultiGeometryCoordinates.split("\n");

        grouped.forEach(function (item, i) {
            let a = item.trim().split(',');

            finalData.push({
                lng: parseFloat(a[0]),
                lat: parseFloat(a[1])
            });
        });

        return finalData;
    }

    function showMap(statesWithViolence) {

        // console.log(statesWithViolence);

        // map.setMapTypeId('hybrid');

        var position, marker, info;

        let contentString = "";

        for (let index = 0; index < statesWithViolence.length; index++) {

            const element = statesWithViolence[index];

            const markerColor = setColorForMarker(element.incidentViolence);

            // console.log(`Marker ${markerColor}`);

            var circle = {
                path: google.maps.SymbolPath.CIRCLE,
                fillColor: markerColor,
                fillOpacity: .9,
                scale: 5.5,
                strokeColor: 'white',
                strokeWeight: 1
            };

            // console.log(`Fill color ${circle['fillColor']}`);

            getLatLong(element.city).then(data => {

                circle['fillColor'] = setColorForMarker(element.incidentViolence);

                // console.log(`Circle ${JSON.stringify(circle)}`);
                position = {
                    position: {
                        lat: data.lat,
                        lng: data.long
                    },
                    map: map,
                    icon: circle,
                    title: data.state
                };

                marker = new google.maps.Marker(position);

                info = new google.maps.InfoWindow();
                marker.addListener('click', function () {
                    // alert(element.incidentTitle);
                    contentString =
                        '<div id="content" style="height: 550px; width: 350px; background-color: #333333; color: #fff; padding: 20px;">' +
                        '<div id="siteNotice">' +
                        "</div>" +
                        '<h1 id="firstHeading" >' + element.state.toUpperCase() + ' STATE</h1><br /><hr style="width: 80%; padding-left: 50px;"><b style="line-height: 2;">' + element.incidentTitle.toUpperCase() + '</b><hr style="width: 80%; padding-left: 50px;"><br />' +
                        '<div id="bodyContent">' + "<br /> <b>" + element.incidentDesc.toUpperCase() + "</b><br /><br /><br /> <i class='fa fa-map-marker' aria-hidden='true'></i> " + jsUcfirst(element.locationOfReport) + "<div style='line-height: 3;'><i class='fa fa-clock-o' aria-hidden='true'></i> " + element.timeOfReport + " " + dateFormat(element.dateAdded) + '</div></div>'
                    "</div>";

                    info.setContent(contentString);
                    info.open(map, this);

                });
            });

        }

        const dateFormat = (s) => {
            console.log(s);

            var now = new Date(s);

            var day = ("0" + now.getDate()).slice(-2);
            var month = ("0" + (now.getMonth() + 1)).slice(-2);

            var newDate = now.getFullYear() + "-" + (month) + "-" + (day);

            console.log(newDate);

            return newDate;
        }

        function marker_clicked(contentString) {
            info.setContent(contentString);
            info.open(map, this);
        }

        function setColorForMarker(incidentViolence) {
            let setColor = "white";

            // console.log(`Inci ${JSON.stringify(incidentViolence)}`);

            for (const key in incidentViolence) {
                if (incidentViolence.hasOwnProperty(key)) {
                    const element = incidentViolence[key];
                    // console.log(`${element.incidentViolencePosition}  ${element.violenceType}`);
                    // console.log(Object.values(element).indexOf("violenceType"));

                    switch (element.incidentViolencePosition) {
                        case 1:
                            setColor = "#EB1212";
                            break;
                        case 2:
                            setColor = "#A8413E";
                            break;
                        case 3:
                            setColor = "#8F1A07";
                            break;
                        case 4:
                            setColor = "#B0593A";
                            break;
                        case 5:
                            setColor = "#DB6810";
                            break;
                        case 6:
                            setColor = "#E6C90B";
                            break;
                        case 7:
                            setColor = "#3605FA";
                            break;
                        case 8:
                            setColor = "#B0593A";
                            break;
                        case 9:
                            setColor = "#88AFD1";
                            break;
                        case 10:
                            setColor = "#B0593A";
                            break;
                        case 11:
                            setColor = "#756F6F";
                            break;

                        default:
                            setColor = "white";
                            break;
                    }

                }
            }

            // console.log(`SetColor ${setColor}`);
            return setColor;
        }

    }

    function jsUcfirst(string) {
        if(string === null) return "";
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    function init() {
        getAllForms();
    }

    init();

    let OndoStateBoundaries = [{
            lng: 4.99680519,
            lat: 5.86319399
        },
        {
            lng: 4.99680519,
            lat: 5.862638
        },
        {
            lng: 4.99736118,
            lat: 5.862638
        },
        {
            lng: 4.99736118,
            lat: 5.86208391
        },
        {
            lng: 4.99763918,
            lat: 5.86208391
        },
        {
            lng: 4.99763918,
            lat: 5.86180592
        },
        {
            lng: 4.99791718,
            lat: 5.86180592
        },
        {
            lng: 4.99791718,
            lat: 5.86097193
        },
        {
            lng: 4.99819517,
            lat: 5.86097193
        },
        {
            lng: 4.99819517,
            lat: 5.86041594
        },
        {
            lng: 4.99875116,
            lat: 5.86041594
        },
        {
            lng: 4.99875116,
            lat: 5.85986185
        },
        {
            lng: 4.99930477,
            lat: 5.85986185
        },
        {
            lng: 4.99930477,
            lat: 5.85874987
        },
        {
            lng: 4.99847317,
            lat: 5.85874987
        },
        {
            lng: 4.99847317,
            lat: 5.85902786
        },
        {
            lng: 4.99819517,
            lat: 5.85902786
        },
        {
            lng: 4.99819517,
            lat: 5.85958385
        },
        {
            lng: 4.99791718,
            lat: 5.85958385
        },
        {
            lng: 4.99791718,
            lat: 5.85986185
        },
        {
            lng: 4.99763918,
            lat: 5.85986185
        },
        {
            lng: 4.99763918,
            lat: 5.86041594
        },
        {
            lng: 4.99736118,
            lat: 5.86041594
        },
        {
            lng: 4.99736118,
            lat: 5.86069393
        },
        {
            lng: 4.99708319,
            lat: 5.86069393
        },
        {
            lng: 4.99708319,
            lat: 5.86124992
        },
        {
            lng: 4.99680519,
            lat: 5.86124992
        },
        {
            lng: 4.99680519,
            lat: 5.86180592
        },
        {
            lng: 4.99652719,
            lat: 5.86180592
        },
        {
            lng: 4.99652719,
            lat: 5.86236
        },
        {
            lng: 4.99625111,
            lat: 5.86236
        },
        {
            lng: 4.99625111,
            lat: 5.862638
        },
        {
            lng: 4.99597311,
            lat: 5.862638
        },
        {
            lng: 4.99597311,
            lat: 5.86319399
        },
        {
            lng: 4.99680519,
            lat: 5.86319399
        },
        {
            lng: 4.99069405,
            lat: 5.87152719
        },
        {
            lng: 4.99069405,
            lat: 5.87125111
        },
        {
            lng: 4.99097204,
            lat: 5.87125111
        },
        {
            lng: 4.99097204,
            lat: 5.87069511
        },
        {
            lng: 4.99125004,
            lat: 5.87069511
        },
        {
            lng: 4.99125004,
            lat: 5.87041712
        },
        {
            lng: 4.99152899,
            lat: 5.87041712
        },
        {
            lng: 4.99152899,
            lat: 5.87013912
        },
        {
            lng: 4.99180698,
            lat: 5.87013912
        },
        {
            lng: 4.99180698,
            lat: 5.86986017
        },
        {
            lng: 4.99208498,
            lat: 5.86986017
        },
        {
            lng: 4.99208498,
            lat: 5.86958218
        },
        {
            lng: 4.99236012,
            lat: 5.86958218
        },
        {
            lng: 4.99236012,
            lat: 5.86902905
        },
        {
            lng: 4.99263811,
            lat: 5.86902905
        },
        {
            lng: 4.99263811,
            lat: 5.86875105
        },
        {
            lng: 4.99291706,
            lat: 5.86875105
        },
        {
            lng: 4.99291706,
            lat: 5.86791611
        },
        {
            lng: 4.99319506,
            lat: 5.86791611
        },
        {
            lng: 4.99319506,
            lat: 5.86763811
        },
        {
            lng: 4.99347305,
            lat: 5.86763811
        },
        {
            lng: 4.99347305,
            lat: 5.86736012
        },
        {
            lng: 4.99375105,
            lat: 5.86736012
        },
        {
            lng: 4.99375105,
            lat: 5.86708498
        },
        {
            lng: 4.99402905,
            lat: 5.86708498
        },
        {
            lng: 4.99402905,
            lat: 5.86597204
        },
        {
            lng: 4.99347305,
            lat: 5.86597204
        },
        {
            lng: 4.99347305,
            lat: 5.86625004
        },
        {
            lng: 4.99291706,
            lat: 5.86625004
        },
        {
            lng: 4.99291706,
            lat: 5.86652803
        },
        {
            lng: 4.99263811,
            lat: 5.86652803
        },
        {
            lng: 4.99263811,
            lat: 5.86708212
        },
        {
            lng: 4.99236012,
            lat: 5.86708498
        },
        {
            lng: 4.99236012,
            lat: 5.86736012
        },
        {
            lng: 4.99208498,
            lat: 5.86736012
        },
        {
            lng: 4.99208212,
            lat: 5.86791611
        },
        {
            lng: 4.99180698,
            lat: 5.86791611
        },
        {
            lng: 4.99180698,
            lat: 5.86875105
        },
        {
            lng: 4.99152899,
            lat: 5.86875105
        },
        {
            lng: 4.99152899,
            lat: 5.86930704
        },
        {
            lng: 4.99125004,
            lat: 5.86930704
        },
        {
            lng: 4.99125004,
            lat: 5.86986017
        },
        {
            lng: 4.99069405,
            lat: 5.86986017
        },
        {
            lng: 4.99069405,
            lat: 5.87041712
        },
        {
            lng: 4.99041605,
            lat: 5.87041712
        },
        {
            lng: 4.99041605,
            lat: 5.87069511
        },
        {
            lng: 4.99013805,
            lat: 5.87069511
        },
        {
            lng: 4.99013805,
            lat: 5.87097311
        },
        {
            lng: 4.98986006,
            lat: 5.87097311
        },
        {
            lng: 4.98986006,
            lat: 5.87152719
        },
        {
            lng: 4.99069405,
            lat: 5.87152719
        },
        {
            lng: 4.97958422,
            lat: 5.88764
        },
        {
            lng: 4.97958422,
            lat: 5.887362
        },
        {
            lng: 4.97986221,
            lat: 5.887362
        },
        {
            lng: 4.97986221,
            lat: 5.88680601
        },
        {
            lng: 4.98014021,
            lat: 5.88680601
        },
        {
            lng: 4.98014021,
            lat: 5.88652802
        },
        {
            lng: 4.98041821,
            lat: 5.88652802
        },
        {
            lng: 4.98041821,
            lat: 5.88625002
        },
        {
            lng: 4.98069382,
            lat: 5.88625002
        },
        {
            lng: 4.98069382,
            lat: 5.88569403
        },
        {
            lng: 4.98097181,
            lat: 5.88569403
        },
        {
            lng: 4.98097181,
            lat: 5.88541603
        },
        {
            lng: 4.98124981,
            lat: 5.88541603
        },
        {
            lng: 4.98124981,
            lat: 5.88513994
        },
        {
            lng: 4.98152781,
            lat: 5.88513994
        },
        {
            lng: 4.98152781,
            lat: 5.88486195
        },
        {
            lng: 4.9818058,
            lat: 5.88486195
        },
        {
            lng: 4.9818058,
            lat: 5.88458395
        },
        {
            lng: 4.9820838,
            lat: 5.88458395
        },
        {
            lng: 4.9820838,
            lat: 5.88430595
        },
        {
            lng: 4.98236179,
            lat: 5.88430595
        },
        {
            lng: 4.98236179,
            lat: 5.88402796
        },
        {
            lng: 4.98263979,
            lat: 5.88402796
        },
        {
            lng: 4.98263979,
            lat: 5.88374996
        },
        {
            lng: 4.98236179,
            lat: 5.88374996
        },
        {
            lng: 4.98236179,
            lat: 5.88319302
        },
        {
            lng: 4.98263979,
            lat: 5.88319302
        },
        {
            lng: 4.98263979,
            lat: 5.88291788
        },
        {
            lng: 4.98319387,
            lat: 5.88291502
        },
        {
            lng: 4.98319387,
            lat: 5.88208389
        },
        {
            lng: 4.98347187,
            lat: 5.88208389
        },
        {
            lng: 4.98347187,
            lat: 5.88180494
        },
        {
            lng: 4.98374987,
            lat: 5.88180494
        },
        {
            lng: 4.98374987,
            lat: 5.88152695
        },
        {
            lng: 4.98402786,
            lat: 5.88152695
        },
        {
            lng: 4.98402786,
            lat: 5.88124895
        },
        {
            lng: 4.98430586,
            lat: 5.88124895
        },
        {
            lng: 4.98430586,
            lat: 5.88097095
        },
        {
            lng: 4.98458385,
            lat: 5.88097095
        },
        {
            lng: 4.98458385,
            lat: 5.88069582
        },
        {
            lng: 4.98485994,
            lat: 5.88069582
        },
        {
            lng: 4.98486185,
            lat: 5.88041687
        },
        {
            lng: 4.98513794,
            lat: 5.88041687
        },
        {
            lng: 4.98513794,
            lat: 5.88013887
        },
        {
            lng: 4.98541594,
            lat: 5.88013887
        },
        {
            lng: 4.98541594,
            lat: 5.87986088
        },
        {
            lng: 4.98569393,
            lat: 5.87986088
        },
        {
            lng: 4.98569393,
            lat: 5.87930489
        },
        {
            lng: 4.98624992,
            lat: 5.87930489
        },
        {
            lng: 4.98624992,
            lat: 5.87902689
        },
        {
            lng: 4.98597193,
            lat: 5.87902689
        },
        {
            lng: 4.98597193,
            lat: 5.87874889
        },
        {
            lng: 4.98624992,
            lat: 5.87874889
        },
        {
            lng: 4.98624992,
            lat: 5.87847281
        },
        {
            lng: 4.98652792,
            lat: 5.87847281
        },
        {
            lng: 4.98652792,
            lat: 5.87819481
        },
        {
            lng: 4.98680592,
            lat: 5.87819481
        },
        {
            lng: 4.98680592,
            lat: 5.87736082
        },
        {
            lng: 4.98652792,
            lat: 5.87736082
        },
        {
            lng: 4.98652792,
            lat: 5.87680483
        },
        {
            lng: 4.98708391,
            lat: 5.87680483
        },
        {
            lng: 4.98708391,
            lat: 5.87652683
        },
        {
            lng: 4.987638,
            lat: 5.87652683
        },
        {
            lng: 4.987638,
            lat: 5.87569523
        },
        {
            lng: 4.98736,
            lat: 5.87569523
        },
        {
            lng: 4.98736,
            lat: 5.87486076
        },
        {
            lng: 4.98680592,
            lat: 5.87486076
        },
        {
            lng: 4.98680592,
            lat: 5.87513924
        },
        {
            lng: 4.98652792,
            lat: 5.87513924
        },
        {
            lng: 4.98652792,
            lat: 5.87541723
        },
        {
            lng: 4.98624992,
            lat: 5.87541723
        },
        {
            lng: 4.98624992,
            lat: 5.87569523
        },
        {
            lng: 4.98597193,
            lat: 5.87569523
        },
        {
            lng: 4.98597193,
            lat: 5.87597322
        },
        {
            lng: 4.98569393,
            lat: 5.87597322
        },
        {
            lng: 4.98569393,
            lat: 5.87624884
        },
        {
            lng: 4.98541594,
            lat: 5.87624884
        },
        {
            lng: 4.98541594,
            lat: 5.87652683
        },
        {
            lng: 4.98513794,
            lat: 5.87652683
        },
        {
            lng: 4.98513794,
            lat: 5.87708282
        },
        {
            lng: 4.98485994,
            lat: 5.87708282
        },
        {
            lng: 4.98485994,
            lat: 5.87736082
        },
        {
            lng: 4.98458385,
            lat: 5.87736082
        },
        {
            lng: 4.98458385,
            lat: 5.87763882
        },
        {
            lng: 4.98430586,
            lat: 5.87763882
        },
        {
            lng: 4.98430586,
            lat: 5.87791681
        },
        {
            lng: 4.98402786,
            lat: 5.87791681
        },
        {
            lng: 4.98402786,
            lat: 5.87819481
        },
        {
            lng: 4.98374987,
            lat: 5.87819481
        },
        {
            lng: 4.98374987,
            lat: 5.87847281
        },
        {
            lng: 4.98347187,
            lat: 5.87847281
        },
        {
            lng: 4.98347187,
            lat: 5.87874889
        },
        {
            lng: 4.98319387,
            lat: 5.87874889
        },
        {
            lng: 4.98319387,
            lat: 5.87902689
        },
        {
            lng: 4.98291779,
            lat: 5.87902689
        },
        {
            lng: 4.98291779,
            lat: 5.87930489
        },
        {
            lng: 4.98263979,
            lat: 5.87930489
        },
        {
            lng: 4.98263979,
            lat: 5.87958288
        },
        {
            lng: 4.98236179,
            lat: 5.87958288
        },
        {
            lng: 4.98236179,
            lat: 5.88013887
        },
        {
            lng: 4.9820838,
            lat: 5.88013887
        },
        {
            lng: 4.9820838,
            lat: 5.88041687
        },
        {
            lng: 4.9818058,
            lat: 5.88041687
        },
        {
            lng: 4.9818058,
            lat: 5.88097095
        },
        {
            lng: 4.98152781,
            lat: 5.88097095
        },
        {
            lng: 4.98152781,
            lat: 5.88152695
        },
        {
            lng: 4.98124981,
            lat: 5.88152695
        },
        {
            lng: 4.98124981,
            lat: 5.88180494
        },
        {
            lng: 4.98097181,
            lat: 5.88180494
        },
        {
            lng: 4.98097181,
            lat: 5.88236189
        },
        {
            lng: 4.98069382,
            lat: 5.88236189
        },
        {
            lng: 4.98069382,
            lat: 5.88291502
        },
        {
            lng: 4.98041821,
            lat: 5.88291502
        },
        {
            lng: 4.98041821,
            lat: 5.88347101
        },
        {
            lng: 4.98014021,
            lat: 5.88347101
        },
        {
            lng: 4.98014021,
            lat: 5.88402796
        },
        {
            lng: 4.97986221,
            lat: 5.88402796
        },
        {
            lng: 4.97986221,
            lat: 5.88430595
        },
        {
            lng: 4.97958422,
            lat: 5.88430595
        },
        {
            lng: 4.97958422,
            lat: 5.88458395
        },
        {
            lng: 4.97902679,
            lat: 5.88458395
        },
        {
            lng: 4.97902679,
            lat: 5.88486195
        },
        {
            lng: 4.9787488,
            lat: 5.88486195
        },
        {
            lng: 4.9787488,
            lat: 5.88513994
        },
        {
            lng: 4.9784708,
            lat: 5.88513994
        },
        {
            lng: 4.9784708,
            lat: 5.88541603
        },
        {
            lng: 4.97813177,
            lat: 5.88542509
        },
        {
            lng: 4.97819281,
            lat: 5.88597202
        },
        {
            lng: 4.97791815,
            lat: 5.88597202
        },
        {
            lng: 4.97791815,
            lat: 5.88652802
        },
        {
            lng: 4.9776392,
            lat: 5.88652802
        },
        {
            lng: 4.9776392,
            lat: 5.88708401
        },
        {
            lng: 4.97791815,
            lat: 5.88708401
        },
        {
            lng: 4.97791815,
            lat: 5.88764
        },
        {
            lng: 4.97958422,
            lat: 5.88764
        },
        {
            lng: 4.93763781,
            lat: 5.93624878
        },
        {
            lng: 4.93763781,
            lat: 5.93597078
        },
        {
            lng: 4.9379158,
            lat: 5.93597078
        },
        {
            lng: 4.9379158,
            lat: 5.93541813
        },
        {
            lng: 4.9381938,
            lat: 5.93541813
        },
        {
            lng: 4.9381938,
            lat: 5.93486118
        },
        {
            lng: 4.93847322,
            lat: 5.93486118
        },
        {
            lng: 4.93847322,
            lat: 5.93458319
        },
        {
            lng: 4.93875122,
            lat: 5.93458319
        },
        {
            lng: 4.93875122,
            lat: 5.93430519
        },
        {
            lng: 4.93902922,
            lat: 5.93430519
        },
        {
            lng: 4.93902922,
            lat: 5.9337492
        },
        {
            lng: 4.93930721,
            lat: 5.9337492
        },
        {
            lng: 4.93930721,
            lat: 5.9334712
        },
        {
            lng: 4.93958187,
            lat: 5.9334712
        },
        {
            lng: 4.93958187,
            lat: 5.93319511
        },
        {
            lng: 4.94013882,
            lat: 5.93319511
        },
        {
            lng: 4.94013882,
            lat: 5.93291712
        },
        {
            lng: 4.94069481,
            lat: 5.93291712
        },
        {
            lng: 4.94069481,
            lat: 5.93236113
        },
        {
            lng: 4.9412508,
            lat: 5.93236113
        },
        {
            lng: 4.9412508,
            lat: 5.93124914
        },
        {
            lng: 4.9415288,
            lat: 5.93124914
        },
        {
            lng: 4.9415288,
            lat: 5.93069506
        },
        {
            lng: 4.94069481,
            lat: 5.93069506
        },
        {
            lng: 4.94069481,
            lat: 5.93097115
        },
        {
            lng: 4.94041681,
            lat: 5.93097115
        },
        {
            lng: 4.94041681,
            lat: 5.93124914
        },
        {
            lng: 4.94013882,
            lat: 5.93124914
        },
        {
            lng: 4.94013882,
            lat: 5.93152714
        },
        {
            lng: 4.93986082,
            lat: 5.93152714
        },
        {
            lng: 4.93986082,
            lat: 5.93208313
        },
        {
            lng: 4.93958187,
            lat: 5.93208313
        },
        {
            lng: 4.93958187,
            lat: 5.93236113
        },
        {
            lng: 4.93930721,
            lat: 5.93236113
        },
        {
            lng: 4.93930387,
            lat: 5.93263912
        },
        {
            lng: 4.93902922,
            lat: 5.93263912
        },
        {
            lng: 4.93902922,
            lat: 5.93291712
        },
        {
            lng: 4.93875122,
            lat: 5.93291712
        },
        {
            lng: 4.93875122,
            lat: 5.93319511
        },
        {
            lng: 4.93847322,
            lat: 5.93319511
        },
        {
            lng: 4.93847322,
            lat: 5.9334712
        },
        {
            lng: 4.9381938,
            lat: 5.9334712
        },
        {
            lng: 4.9381938,
            lat: 5.93402719
        },
        {
            lng: 4.9379158,
            lat: 5.93402719
        },
        {
            lng: 4.9379158,
            lat: 5.93458319
        },
        {
            lng: 4.93763781,
            lat: 5.93458319
        },
        {
            lng: 4.93763781,
            lat: 5.93514013
        },
        {
            lng: 4.93735981,
            lat: 5.93514013
        },
        {
            lng: 4.93735981,
            lat: 5.93541813
        },
        {
            lng: 4.93708181,
            lat: 5.93541813
        },
        {
            lng: 4.93708181,
            lat: 5.93569279
        },
        {
            lng: 4.93652821,
            lat: 5.93569279
        },
        {
            lng: 4.93652821,
            lat: 5.93624878
        },
        {
            lng: 4.93763781,
            lat: 5.93624878
        },
        {
            lng: 4.99472618,
            lat: 6.31304121
        },
        {
            lng: 4.99423599,
            lat: 6.31161404
        },
        {
            lng: 4.9910388,
            lat: 6.29944277
        },
        {
            lng: 4.98315001,
            lat: 6.29703903
        },
        {
            lng: 4.97815895,
            lat: 6.28812695
        },
        {
            lng: 4.98265982,
            lat: 6.29520178
        },
        {
            lng: 4.98051977,
            lat: 6.29131603
        },
        {
            lng: 4.97857809,
            lat: 6.28558207
        },
        {
            lng: 4.98099899,
            lat: 6.2781558
        },
        {
            lng: 4.98573303,
            lat: 6.27092505
        },
        {
            lng: 4.99702978,
            lat: 6.2700429
        },
        {
            lng: 5.00134182,
            lat: 6.26535177
        },
        {
            lng: 5.00499821,
            lat: 6.2629838
        },
        {
            lng: 5.00930786,
            lat: 6.25829601
        },
        {
            lng: 5.01640081,
            lat: 6.25425482
        },
        {
            lng: 5.019382,
            lat: 6.2530489
        },
        {
            lng: 5.02830696,
            lat: 6.24805117
        },
        {
            lng: 5.03234291,
            lat: 6.24060106
        },
        {
            lng: 5.03870392,
            lat: 6.23426485
        },
        {
            lng: 5.04297113,
            lat: 6.22680998
        },
        {
            lng: 5.05021191,
            lat: 6.21768904
        },
        {
            lng: 5.05028677,
            lat: 6.2079978
        },
        {
            lng: 5.05501604,
            lat: 6.20053577
        },
        {
            lng: 5.07371902,
            lat: 6.20067692
        },
        {
            lng: 5.0801568,
            lat: 6.1989522
        },
        {
            lng: 5.08793402,
            lat: 6.19443703
        },
        {
            lng: 5.09250593,
            lat: 6.19158983
        },
        {
            lng: 5.10158777,
            lat: 6.18220711
        },
        {
            lng: 5.10675001,
            lat: 6.17289209
        },
        {
            lng: 5.10776711,
            lat: 6.16433907
        },
        {
            lng: 5.11317492,
            lat: 6.1561718
        },
        {
            lng: 5.12109518,
            lat: 6.14611721
        },
        {
            lng: 5.12131405,
            lat: 6.14542198
        },
        {
            lng: 5.12154913,
            lat: 6.14564705
        },
        {
            lng: 5.11909199,
            lat: 6.13646221
        },
        {
            lng: 5.11518097,
            lat: 6.12696314
        },
        {
            lng: 5.05018187,
            lat: 5.96913481
        },
        {
            lng: 5.00369215,
            lat: 5.85624981
        },
        {
            lng: 5.00347281,
            lat: 5.85624981
        },
        {
            lng: 5.00347281,
            lat: 5.8568058
        },
        {
            lng: 5.00319481,
            lat: 5.8568058
        },
        {
            lng: 5.00319481,
            lat: 5.85736179
        },
        {
            lng: 5.00263882,
            lat: 5.85736179
        },
        {
            lng: 5.00263882,
            lat: 5.85791779
        },
        {
            lng: 5.00236082,
            lat: 5.85791779
        },
        {
            lng: 5.00236082,
            lat: 5.85847187
        },
        {
            lng: 5.00208282,
            lat: 5.85847187
        },
        {
            lng: 5.00208282,
            lat: 5.85874987
        },
        {
            lng: 5.00180483,
            lat: 5.85874987
        },
        {
            lng: 5.00180483,
            lat: 5.85902786
        },
        {
            lng: 5.00124884,
            lat: 5.85902786
        },
        {
            lng: 5.00124884,
            lat: 5.86013985
        },
        {
            lng: 5.00097322,
            lat: 5.86013985
        },
        {
            lng: 5.00097322,
            lat: 5.86097193
        },
        {
            lng: 5.00013924,
            lat: 5.86097193
        },
        {
            lng: 4.99986076,
            lat: 5.86097193
        },
        {
            lng: 4.99986076,
            lat: 5.86124992
        },
        {
            lng: 4.99958277,
            lat: 5.86124992
        },
        {
            lng: 4.99958277,
            lat: 5.86152792
        },
        {
            lng: 4.99930477,
            lat: 5.86152792
        },
        {
            lng: 4.99930477,
            lat: 5.86208391
        },
        {
            lng: 4.99902678,
            lat: 5.86208391
        },
        {
            lng: 4.99902678,
            lat: 5.86236
        },
        {
            lng: 4.99875116,
            lat: 5.86236
        },
        {
            lng: 4.99875116,
            lat: 5.862638
        },
        {
            lng: 4.99847317,
            lat: 5.862638
        },
        {
            lng: 4.99847317,
            lat: 5.86319399
        },
        {
            lng: 4.99819517,
            lat: 5.86319399
        },
        {
            lng: 4.99819517,
            lat: 5.86347198
        },
        {
            lng: 4.99791718,
            lat: 5.86347198
        },
        {
            lng: 4.99791718,
            lat: 5.86374998
        },
        {
            lng: 4.99763918,
            lat: 5.86374998
        },
        {
            lng: 4.99763918,
            lat: 5.86430597
        },
        {
            lng: 4.99736118,
            lat: 5.86430597
        },
        {
            lng: 4.99736118,
            lat: 5.86513805
        },
        {
            lng: 4.99708319,
            lat: 5.86513805
        },
        {
            lng: 4.99708319,
            lat: 5.86597204
        },
        {
            lng: 4.99680519,
            lat: 5.86597204
        },
        {
            lng: 4.99680519,
            lat: 5.86652803
        },
        {
            lng: 4.99652719,
            lat: 5.86652803
        },
        {
            lng: 4.99652719,
            lat: 5.86708498
        },
        {
            lng: 4.99625111,
            lat: 5.86708498
        },
        {
            lng: 4.99625111,
            lat: 5.86763811
        },
        {
            lng: 4.99597311,
            lat: 5.86763811
        },
        {
            lng: 4.99597311,
            lat: 5.86791611
        },
        {
            lng: 4.99569511,
            lat: 5.86791611
        },
        {
            lng: 4.99569511,
            lat: 5.8681941
        },
        {
            lng: 4.99541712,
            lat: 5.8681941
        },
        {
            lng: 4.99541712,
            lat: 5.86847305
        },
        {
            lng: 4.99513912,
            lat: 5.86847305
        },
        {
            lng: 4.99513912,
            lat: 5.86875105
        },
        {
            lng: 4.99486113,
            lat: 5.86875105
        },
        {
            lng: 4.99486113,
            lat: 5.86902905
        },
        {
            lng: 4.99458313,
            lat: 5.86902905
        },
        {
            lng: 4.99458313,
            lat: 5.86930704
        },
        {
            lng: 4.99430513,
            lat: 5.86930704
        },
        {
            lng: 4.99430513,
            lat: 5.86958218
        },
        {
            lng: 4.99402905,
            lat: 5.86958218
        },
        {
            lng: 4.99402905,
            lat: 5.86986017
        },
        {
            lng: 4.99375105,
            lat: 5.86986017
        },
        {
            lng: 4.99375105,
            lat: 5.87041712
        },
        {
            lng: 4.99347305,
            lat: 5.87041712
        },
        {
            lng: 4.99347305,
            lat: 5.87069511
        },
        {
            lng: 4.99319506,
            lat: 5.87069511
        },
        {
            lng: 4.99319506,
            lat: 5.87097311
        },
        {
            lng: 4.99291706,
            lat: 5.87097311
        },
        {
            lng: 4.99291706,
            lat: 5.87125111
        },
        {
            lng: 4.99263811,
            lat: 5.87125111
        },
        {
            lng: 4.99263811,
            lat: 5.87152719
        },
        {
            lng: 4.99236012,
            lat: 5.87152719
        },
        {
            lng: 4.99236012,
            lat: 5.87180519
        },
        {
            lng: 4.99208498,
            lat: 5.87180519
        },
        {
            lng: 4.99208498,
            lat: 5.87208319
        },
        {
            lng: 4.99180698,
            lat: 5.87208319
        },
        {
            lng: 4.99180698,
            lat: 5.87236118
        },
        {
            lng: 4.99152899,
            lat: 5.87236118
        },
        {
            lng: 4.99152899,
            lat: 5.87291718
        },
        {
            lng: 4.99125004,
            lat: 5.87291718
        },
        {
            lng: 4.99125004,
            lat: 5.87319517
        },
        {
            lng: 4.99097204,
            lat: 5.87319517
        },
        {
            lng: 4.99097204,
            lat: 5.87375116
        },
        {
            lng: 4.99041605,
            lat: 5.87375116
        },
        {
            lng: 4.99041605,
            lat: 5.87430477
        },
        {
            lng: 4.99013805,
            lat: 5.87430477
        },
        {
            lng: 4.99013805,
            lat: 5.87486076
        },
        {
            lng: 4.98986006,
            lat: 5.87486076
        },
        {
            lng: 4.98986006,
            lat: 5.87513924
        },
        {
            lng: 4.98958397,
            lat: 5.87513924
        },
        {
            lng: 4.98958397,
            lat: 5.87569523
        },
        {
            lng: 4.98930597,
            lat: 5.87569523
        },
        {
            lng: 4.98930597,
            lat: 5.87624884
        },
        {
            lng: 4.98902798,
            lat: 5.87624884
        },
        {
            lng: 4.98902798,
            lat: 5.87680483
        },
        {
            lng: 4.98874998,
            lat: 5.87680483
        },
        {
            lng: 4.98874998,
            lat: 5.87736082
        },
        {
            lng: 4.98847198,
            lat: 5.87736082
        },
        {
            lng: 4.98847198,
            lat: 5.87763882
        },
        {
            lng: 4.98819399,
            lat: 5.87763882
        },
        {
            lng: 4.98819399,
            lat: 5.87819481
        },
        {
            lng: 4.98791599,
            lat: 5.87819481
        },
        {
            lng: 4.98791599,
            lat: 5.87874889
        },
        {
            lng: 4.987638,
            lat: 5.87874889
        },
        {
            lng: 4.987638,
            lat: 5.87902689
        },
        {
            lng: 4.98708391,
            lat: 5.87902689
        },
        {
            lng: 4.98708391,
            lat: 5.87930489
        },
        {
            lng: 4.98680592,
            lat: 5.87930489
        },
        {
            lng: 4.98680592,
            lat: 5.87958288
        },
        {
            lng: 4.98652792,
            lat: 5.87958288
        },
        {
            lng: 4.98652792,
            lat: 5.87986088
        },
        {
            lng: 4.98624992,
            lat: 5.87986088
        },
        {
            lng: 4.98624992,
            lat: 5.88041687
        },
        {
            lng: 4.98597193,
            lat: 5.88041687
        },
        {
            lng: 4.98597193,
            lat: 5.88069582
        },
        {
            lng: 4.98569393,
            lat: 5.88069582
        },
        {
            lng: 4.98569393,
            lat: 5.88097095
        },
        {
            lng: 4.98541594,
            lat: 5.88097095
        },
        {
            lng: 4.98541594,
            lat: 5.88124895
        },
        {
            lng: 4.98513794,
            lat: 5.88124895
        },
        {
            lng: 4.98513794,
            lat: 5.88152695
        },
        {
            lng: 4.98486185,
            lat: 5.88152695
        },
        {
            lng: 4.98485994,
            lat: 5.88236189
        },
        {
            lng: 4.98458385,
            lat: 5.88236189
        },
        {
            lng: 4.98458385,
            lat: 5.88263988
        },
        {
            lng: 4.98402786,
            lat: 5.88263988
        },
        {
            lng: 4.98402786,
            lat: 5.88291502
        },
        {
            lng: 4.98374987,
            lat: 5.88291788
        },
        {
            lng: 4.98374987,
            lat: 5.88319302
        },
        {
            lng: 4.98347187,
            lat: 5.88319302
        },
        {
            lng: 4.98347187,
            lat: 5.88402796
        },
        {
            lng: 4.98319387,
            lat: 5.88402796
        },
        {
            lng: 4.98319387,
            lat: 5.88430595
        },
        {
            lng: 4.98291779,
            lat: 5.88430595
        },
        {
            lng: 4.98291779,
            lat: 5.88513994
        },
        {
            lng: 4.98263979,
            lat: 5.88513994
        },
        {
            lng: 4.98263979,
            lat: 5.88569403
        },
        {
            lng: 4.98236179,
            lat: 5.88569403
        },
        {
            lng: 4.98236179,
            lat: 5.88597202
        },
        {
            lng: 4.9820838,
            lat: 5.88597202
        },
        {
            lng: 4.9820838,
            lat: 5.88652802
        },
        {
            lng: 4.9818058,
            lat: 5.88652802
        },
        {
            lng: 4.9818058,
            lat: 5.88680601
        },
        {
            lng: 4.98152781,
            lat: 5.88680601
        },
        {
            lng: 4.98152781,
            lat: 5.88708401
        },
        {
            lng: 4.98124981,
            lat: 5.88708401
        },
        {
            lng: 4.98124981,
            lat: 5.88764
        },
        {
            lng: 4.98097181,
            lat: 5.88764
        },
        {
            lng: 4.98097181,
            lat: 5.88791609
        },
        {
            lng: 4.98069382,
            lat: 5.88791609
        },
        {
            lng: 4.98069382,
            lat: 5.88819408
        },
        {
            lng: 4.98041821,
            lat: 5.88819408
        },
        {
            lng: 4.98041821,
            lat: 5.88875008
        },
        {
            lng: 4.98014021,
            lat: 5.88875008
        },
        {
            lng: 4.98014021,
            lat: 5.88902807
        },
        {
            lng: 4.97958422,
            lat: 5.88902807
        },
        {
            lng: 4.97958422,
            lat: 5.88930607
        },
        {
            lng: 4.97902679,
            lat: 5.88930607
        },
        {
            lng: 4.97902679,
            lat: 5.88958406
        },
        {
            lng: 4.9787488,
            lat: 5.88958406
        },
        {
            lng: 4.9787488,
            lat: 5.88986206
        },
        {
            lng: 4.97819281,
            lat: 5.88986206
        },
        {
            lng: 4.97819281,
            lat: 5.89041615
        },
        {
            lng: 4.97791815,
            lat: 5.89041615
        },
        {
            lng: 4.97791815,
            lat: 5.89097214
        },
        {
            lng: 4.9776392,
            lat: 5.89097214
        },
        {
            lng: 4.9776392,
            lat: 5.89125013
        },
        {
            lng: 4.9773612,
            lat: 5.89125013
        },
        {
            lng: 4.9773612,
            lat: 5.89152813
        },
        {
            lng: 4.97708321,
            lat: 5.89152813
        },
        {
            lng: 4.97708321,
            lat: 5.89180613
        },
        {
            lng: 4.97624922,
            lat: 5.89180613
        },
        {
            lng: 4.97624922,
            lat: 5.89152813
        },
        {
            lng: 4.97597122,
            lat: 5.89152813
        },
        {
            lng: 4.97597122,
            lat: 5.89125013
        },
        {
            lng: 4.97569513,
            lat: 5.89125013
        },
        {
            lng: 4.97569323,
            lat: 5.89069414
        },
        {
            lng: 4.97541714,
            lat: 5.89069414
        },
        {
            lng: 4.97541714,
            lat: 5.89097214
        },
        {
            lng: 4.97513914,
            lat: 5.89097214
        },
        {
            lng: 4.97513914,
            lat: 5.89125013
        },
        {
            lng: 4.97486115,
            lat: 5.89125013
        },
        {
            lng: 4.97486115,
            lat: 5.89152813
        },
        {
            lng: 4.97424793,
            lat: 5.89154005
        },
        {
            lng: 4.97430515,
            lat: 5.89180613
        },
        {
            lng: 4.97402716,
            lat: 5.89180613
        },
        {
            lng: 4.97402716,
            lat: 5.89208221
        },
        {
            lng: 4.9738059,
            lat: 5.89211178
        },
        {
            lng: 4.97369719,
            lat: 5.89232588
        },
        {
            lng: 4.97347307,
            lat: 5.89236021
        },
        {
            lng: 4.97348881,
            lat: 5.89265013
        },
        {
            lng: 4.97319508,
            lat: 5.89263821
        },
        {
            lng: 4.97319508,
            lat: 5.8931942
        },
        {
            lng: 4.97291708,
            lat: 5.8931942
        },
        {
            lng: 4.97295094,
            lat: 5.89349508
        },
        {
            lng: 4.97263908,
            lat: 5.89347219
        },
        {
            lng: 4.97263908,
            lat: 5.89375019
        },
        {
            lng: 4.97236109,
            lat: 5.89375019
        },
        {
            lng: 4.97241306,
            lat: 5.89433813
        },
        {
            lng: 4.97208309,
            lat: 5.89430714
        },
        {
            lng: 4.97208309,
            lat: 5.89458179
        },
        {
            lng: 4.9718051,
            lat: 5.89458179
        },
        {
            lng: 4.9718051,
            lat: 5.89485979
        },
        {
            lng: 4.9715271,
            lat: 5.89485979
        },
        {
            lng: 4.9715271,
            lat: 5.89541578
        },
        {
            lng: 4.97125101,
            lat: 5.89541578
        },
        {
            lng: 4.97125101,
            lat: 5.89569521
        },
        {
            lng: 4.97097301,
            lat: 5.89569521
        },
        {
            lng: 4.97097301,
            lat: 5.8962512
        },
        {
            lng: 4.97069502,
            lat: 5.8962512
        },
        {
            lng: 4.97069502,
            lat: 5.89680719
        },
        {
            lng: 4.97041702,
            lat: 5.89680719
        },
        {
            lng: 4.97041702,
            lat: 5.89708185
        },
        {
            lng: 4.97013903,
            lat: 5.89708185
        },
        {
            lng: 4.97013903,
            lat: 5.8973608
        },
        {
            lng: 4.96986103,
            lat: 5.8973608
        },
        {
            lng: 4.96986103,
            lat: 5.89791679
        },
        {
            lng: 4.96958303,
            lat: 5.89791679
        },
        {
            lng: 4.96958303,
            lat: 5.89819479
        },
        {
            lng: 4.96930504,
            lat: 5.89819479
        },
        {
            lng: 4.96930504,
            lat: 5.89847279
        },
        {
            lng: 4.96902895,
            lat: 5.89847279
        },
        {
            lng: 4.96902895,
            lat: 5.89902878
        },
        {
            lng: 4.96875095,
            lat: 5.89902878
        },
        {
            lng: 4.96875095,
            lat: 5.89930487
        },
        {
            lng: 4.96847296,
            lat: 5.89930677
        },
        {
            lng: 4.96847296,
            lat: 5.89958286
        },
        {
            lng: 4.96819496,
            lat: 5.89958286
        },
        {
            lng: 4.96819496,
            lat: 5.90013885
        },
        {
            lng: 4.96791697,
            lat: 5.90013885
        },
        {
            lng: 4.96791697,
            lat: 5.90041685
        },
        {
            lng: 4.96763897,
            lat: 5.90041685
        },
        {
            lng: 4.96763897,
            lat: 5.90097284
        },
        {
            lng: 4.96736097,
            lat: 5.90097284
        },
        {
            lng: 4.96736097,
            lat: 5.90124893
        },
        {
            lng: 4.96708298,
            lat: 5.90124893
        },
        {
            lng: 4.96708298,
            lat: 5.90180492
        },
        {
            lng: 4.96680403,
            lat: 5.90180492
        },
        {
            lng: 4.96680403,
            lat: 5.90263891
        },
        {
            lng: 4.96652889,
            lat: 5.90263891
        },
        {
            lng: 4.96652889,
            lat: 5.9031949
        },
        {
            lng: 4.9662509,
            lat: 5.9031949
        },
        {
            lng: 4.9662509,
            lat: 5.9034729
        },
        {
            lng: 4.9656949,
            lat: 5.9034729
        },
        {
            lng: 4.9656949,
            lat: 5.90430498
        },
        {
            lng: 4.96541595,
            lat: 5.90430498
        },
        {
            lng: 4.96541595,
            lat: 5.90486097
        },
        {
            lng: 4.96513796,
            lat: 5.90486097
        },
        {
            lng: 4.96513796,
            lat: 5.90541697
        },
        {
            lng: 4.96485996,
            lat: 5.90541697
        },
        {
            lng: 4.96485996,
            lat: 5.90569496
        },
        {
            lng: 4.96458197,
            lat: 5.90569496
        },
        {
            lng: 4.96458197,
            lat: 5.90624905
        },
        {
            lng: 4.96430683,
            lat: 5.90624905
        },
        {
            lng: 4.96430683,
            lat: 5.90680504
        },
        {
            lng: 4.96402788,
            lat: 5.90680504
        },
        {
            lng: 4.96408987,
            lat: 5.90740013
        },
        {
            lng: 4.96374989,
            lat: 5.90736103
        },
        {
            lng: 4.96374989,
            lat: 5.90793324
        },
        {
            lng: 4.96374989,
            lat: 5.90819597
        },
        {
            lng: 4.963552,
            lat: 5.90819597
        },
        {
            lng: 4.96347189,
            lat: 5.90819597
        },
        {
            lng: 4.96347189,
            lat: 5.90847111
        },
        {
            lng: 4.96319389,
            lat: 5.90847111
        },
        {
            lng: 4.96321917,
            lat: 5.90876484
        },
        {
            lng: 4.9629159,
            lat: 5.9087491
        },
        {
            lng: 4.9629159,
            lat: 5.9090271
        },
        {
            lng: 4.96236181,
            lat: 5.9090271
        },
        {
            lng: 4.96236181,
            lat: 5.9093051
        },
        {
            lng: 4.96208382,
            lat: 5.9093051
        },
        {
            lng: 4.96208382,
            lat: 5.90958405
        },
        {
            lng: 4.96180582,
            lat: 5.90958405
        },
        {
            lng: 4.96180582,
            lat: 5.91014004
        },
        {
            lng: 4.96152782,
            lat: 5.91014004
        },
        {
            lng: 4.96152782,
            lat: 5.91041803
        },
        {
            lng: 4.96124983,
            lat: 5.91041803
        },
        {
            lng: 4.96124983,
            lat: 5.91069317
        },
        {
            lng: 4.96097183,
            lat: 5.91069317
        },
        {
            lng: 4.96097183,
            lat: 5.91097212
        },
        {
            lng: 4.96069384,
            lat: 5.91097212
        },
        {
            lng: 4.96069384,
            lat: 5.91125011
        },
        {
            lng: 4.96041584,
            lat: 5.91125011
        },
        {
            lng: 4.96041584,
            lat: 5.91152811
        },
        {
            lng: 4.95986223,
            lat: 5.91152811
        },
        {
            lng: 4.95986223,
            lat: 5.91180611
        },
        {
            lng: 4.95930576,
            lat: 5.91180611
        },
        {
            lng: 4.95930576,
            lat: 5.9120841
        },
        {
            lng: 4.95902777,
            lat: 5.9120841
        },
        {
            lng: 4.95902777,
            lat: 5.9123621
        },
        {
            lng: 4.95847178,
            lat: 5.9123621
        },
        {
            lng: 4.95847178,
            lat: 5.91263819
        },
        {
            lng: 4.95791578,
            lat: 5.91264009
        },
        {
            lng: 4.95791578,
            lat: 5.91291618
        },
        {
            lng: 4.95763779,
            lat: 5.91291618
        },
        {
            lng: 4.95763779,
            lat: 5.91319418
        },
        {
            lng: 4.95736217,
            lat: 5.91319418
        },
        {
            lng: 4.95736217,
            lat: 5.91347218
        },
        {
            lng: 4.95708418,
            lat: 5.91347218
        },
        {
            lng: 4.95708418,
            lat: 5.91375017
        },
        {
            lng: 4.95652819,
            lat: 5.91375017
        },
        {
            lng: 4.95652819,
            lat: 5.91402817
        },
        {
            lng: 4.95625019,
            lat: 5.91402817
        },
        {
            lng: 4.95625019,
            lat: 5.91430616
        },
        {
            lng: 4.95597219,
            lat: 5.91430616
        },
        {
            lng: 4.95597219,
            lat: 5.91458416
        },
        {
            lng: 4.9556942,
            lat: 5.91458416
        },
        {
            lng: 4.9556942,
            lat: 5.91513777
        },
        {
            lng: 4.9554162,
            lat: 5.91513777
        },
        {
            lng: 4.9554162,
            lat: 5.91541576
        },
        {
            lng: 4.95513821,
            lat: 5.91541576
        },
        {
            lng: 4.95513821,
            lat: 5.91597223
        },
        {
            lng: 4.95486212,
            lat: 5.91597223
        },
        {
            lng: 4.95486212,
            lat: 5.91652822
        },
        {
            lng: 4.95458412,
            lat: 5.91652822
        },
        {
            lng: 4.95458412,
            lat: 5.91680622
        },
        {
            lng: 4.95402813,
            lat: 5.91680622
        },
        {
            lng: 4.95402813,
            lat: 5.91708422
        },
        {
            lng: 4.95375013,
            lat: 5.91708422
        },
        {
            lng: 4.95375013,
            lat: 5.91736221
        },
        {
            lng: 4.95347214,
            lat: 5.91736221
        },
        {
            lng: 4.95347214,
            lat: 5.91763783
        },
        {
            lng: 4.95319605,
            lat: 5.91763783
        },
        {
            lng: 4.95319605,
            lat: 5.91791582
        },
        {
            lng: 4.95291805,
            lat: 5.91791582
        },
        {
            lng: 4.95291805,
            lat: 5.91819382
        },
        {
            lng: 4.95236206,
            lat: 5.91819382
        },
        {
            lng: 4.95236206,
            lat: 5.91847181
        },
        {
            lng: 4.95208406,
            lat: 5.91847181
        },
        {
            lng: 4.95208406,
            lat: 5.91874981
        },
        {
            lng: 4.95180511,
            lat: 5.91874981
        },
        {
            lng: 4.95180511,
            lat: 5.91902781
        },
        {
            lng: 4.95124912,
            lat: 5.91902781
        },
        {
            lng: 4.95124912,
            lat: 5.9193058
        },
        {
            lng: 4.95097113,
            lat: 5.9193058
        },
        {
            lng: 4.95097113,
            lat: 5.9195838
        },
        {
            lng: 4.95069599,
            lat: 5.9195838
        },
        {
            lng: 4.95069599,
            lat: 5.92013788
        },
        {
            lng: 4.95041704,
            lat: 5.92013788
        },
        {
            lng: 4.95041704,
            lat: 5.92041588
        },
        {
            lng: 4.95013905,
            lat: 5.92041588
        },
        {
            lng: 4.95013905,
            lat: 5.92069387
        },
        {
            lng: 4.94986105,
            lat: 5.92069387
        },
        {
            lng: 4.94986105,
            lat: 5.92097187
        },
        {
            lng: 4.94958305,
            lat: 5.92097187
        },
        {
            lng: 4.94958305,
            lat: 5.92152882
        },
        {
            lng: 4.94930506,
            lat: 5.92152882
        },
        {
            lng: 4.94930506,
            lat: 5.92180395
        },
        {
            lng: 4.94902706,
            lat: 5.92180395
        },
        {
            lng: 4.94902706,
            lat: 5.92235994
        },
        {
            lng: 4.94874907,
            lat: 5.92235994
        },
        {
            lng: 4.94874907,
            lat: 5.92291594
        },
        {
            lng: 4.94847107,
            lat: 5.92291594
        },
        {
            lng: 4.94847107,
            lat: 5.92263794
        },
        {
            lng: 4.94791698,
            lat: 5.92263794
        },
        {
            lng: 4.94791698,
            lat: 5.92235994
        },
        {
            lng: 4.94736099,
            lat: 5.92235994
        },
        {
            lng: 4.94736099,
            lat: 5.92291594
        },
        {
            lng: 4.947083,
            lat: 5.92291594
        },
        {
            lng: 4.947083,
            lat: 5.92347288
        },
        {
            lng: 4.946805,
            lat: 5.92347288
        },
        {
            lng: 4.946805,
            lat: 5.92375088
        },
        {
            lng: 4.946527,
            lat: 5.92375088
        },
        {
            lng: 4.946527,
            lat: 5.92430401
        },
        {
            lng: 4.94624901,
            lat: 5.92430401
        },
        {
            lng: 4.94624901,
            lat: 5.92458296
        },
        {
            lng: 4.94597292,
            lat: 5.92458296
        },
        {
            lng: 4.94597292,
            lat: 5.92486095
        },
        {
            lng: 4.94569492,
            lat: 5.92486095
        },
        {
            lng: 4.94569492,
            lat: 5.92513895
        },
        {
            lng: 4.94541693,
            lat: 5.92513895
        },
        {
            lng: 4.94541693,
            lat: 5.92541695
        },
        {
            lng: 4.94513893,
            lat: 5.92541695
        },
        {
            lng: 4.94513893,
            lat: 5.92569494
        },
        {
            lng: 4.94486094,
            lat: 5.92569494
        },
        {
            lng: 4.94486094,
            lat: 5.92597294
        },
        {
            lng: 4.94458294,
            lat: 5.92597294
        },
        {
            lng: 4.94458294,
            lat: 5.92625093
        },
        {
            lng: 4.94430494,
            lat: 5.92625093
        },
        {
            lng: 4.94430494,
            lat: 5.92652893
        },
        {
            lng: 4.94402695,
            lat: 5.92652893
        },
        {
            lng: 4.94402695,
            lat: 5.927917
        },
        {
            lng: 4.94430494,
            lat: 5.927917
        },
        {
            lng: 4.94430494,
            lat: 5.928195
        },
        {
            lng: 4.94458294,
            lat: 5.928195
        },
        {
            lng: 4.94458294,
            lat: 5.92875099
        },
        {
            lng: 4.94486094,
            lat: 5.92875099
        },
        {
            lng: 4.94486094,
            lat: 5.92930508
        },
        {
            lng: 4.94513893,
            lat: 5.92930508
        },
        {
            lng: 4.94513893,
            lat: 5.92986107
        },
        {
            lng: 4.94486094,
            lat: 5.92986107
        },
        {
            lng: 4.94486094,
            lat: 5.93041706
        },
        {
            lng: 4.94458294,
            lat: 5.93041706
        },
        {
            lng: 4.94458294,
            lat: 5.93097115
        },
        {
            lng: 4.94430494,
            lat: 5.93097115
        },
        {
            lng: 4.94430494,
            lat: 5.93124914
        },
        {
            lng: 4.94402695,
            lat: 5.93124914
        },
        {
            lng: 4.94402695,
            lat: 5.93152714
        },
        {
            lng: 4.94375086,
            lat: 5.93152714
        },
        {
            lng: 4.94375086,
            lat: 5.93180513
        },
        {
            lng: 4.94347286,
            lat: 5.93180513
        },
        {
            lng: 4.94347286,
            lat: 5.93208313
        },
        {
            lng: 4.94319487,
            lat: 5.93208313
        },
        {
            lng: 4.94319487,
            lat: 5.9334712
        },
        {
            lng: 4.94263887,
            lat: 5.9334712
        },
        {
            lng: 4.94263887,
            lat: 5.9337492
        },
        {
            lng: 4.94236088,
            lat: 5.9337492
        },
        {
            lng: 4.94236088,
            lat: 5.93402719
        },
        {
            lng: 4.94208288,
            lat: 5.93402719
        },
        {
            lng: 4.94208288,
            lat: 5.93458319
        },
        {
            lng: 4.9412508,
            lat: 5.93458319
        },
        {
            lng: 4.9412508,
            lat: 5.93514013
        },
        {
            lng: 4.94097281,
            lat: 5.93514013
        },
        {
            lng: 4.94097281,
            lat: 5.93541813
        },
        {
            lng: 4.94069481,
            lat: 5.93541813
        },
        {
            lng: 4.94069481,
            lat: 5.93624878
        },
        {
            lng: 4.94041681,
            lat: 5.93624878
        },
        {
            lng: 4.94041681,
            lat: 5.9368062
        },
        {
            lng: 4.94013882,
            lat: 5.9368062
        },
        {
            lng: 4.94013882,
            lat: 5.9370842
        },
        {
            lng: 4.93958187,
            lat: 5.9370842
        },
        {
            lng: 4.93958187,
            lat: 5.93736219
        },
        {
            lng: 4.93708181,
            lat: 5.93736219
        },
        {
            lng: 4.93708181,
            lat: 5.9370842
        },
        {
            lng: 4.93652821,
            lat: 5.9370842
        },
        {
            lng: 4.93652821,
            lat: 5.9368062
        },
        {
            lng: 4.93625021,
            lat: 5.9368062
        },
        {
            lng: 4.93625021,
            lat: 5.93652678
        },
        {
            lng: 4.93569422,
            lat: 5.93652678
        },
        {
            lng: 4.93569422,
            lat: 5.9370842
        },
        {
            lng: 4.93513823,
            lat: 5.9370842
        },
        {
            lng: 4.93513823,
            lat: 5.93736219
        },
        {
            lng: 4.93486023,
            lat: 5.93736219
        },
        {
            lng: 4.93486023,
            lat: 5.93791819
        },
        {
            lng: 4.93541622,
            lat: 5.93791819
        },
        {
            lng: 4.93541622,
            lat: 5.9381938
        },
        {
            lng: 4.93597221,
            lat: 5.9381938
        },
        {
            lng: 4.93597221,
            lat: 5.93847179
        },
        {
            lng: 4.93625021,
            lat: 5.93847179
        },
        {
            lng: 4.93625021,
            lat: 5.93874979
        },
        {
            lng: 4.9368062,
            lat: 5.93874979
        },
        {
            lng: 4.9368062,
            lat: 5.94013977
        },
        {
            lng: 4.93652821,
            lat: 5.94013977
        },
        {
            lng: 4.93652821,
            lat: 5.94041586
        },
        {
            lng: 4.93625021,
            lat: 5.94041586
        },
        {
            lng: 4.93625021,
            lat: 5.94069386
        },
        {
            lng: 4.93597221,
            lat: 5.94069386
        },
        {
            lng: 4.93597221,
            lat: 5.94097185
        },
        {
            lng: 4.93513823,
            lat: 5.94097185
        },
        {
            lng: 4.93513823,
            lat: 5.94124985
        },
        {
            lng: 4.93486023,
            lat: 5.94124985
        },
        {
            lng: 4.93486023,
            lat: 5.94152784
        },
        {
            lng: 4.93430614,
            lat: 5.94152784
        },
        {
            lng: 4.93430614,
            lat: 5.94180584
        },
        {
            lng: 4.93402815,
            lat: 5.94180584
        },
        {
            lng: 4.93402815,
            lat: 5.94208384
        },
        {
            lng: 4.93375015,
            lat: 5.94208384
        },
        {
            lng: 4.93375015,
            lat: 5.94235992
        },
        {
            lng: 4.93347216,
            lat: 5.94236183
        },
        {
            lng: 4.93347216,
            lat: 5.94263792
        },
        {
            lng: 4.93319416,
            lat: 5.94263792
        },
        {
            lng: 4.93319416,
            lat: 5.94291592
        },
        {
            lng: 4.93291616,
            lat: 5.94291592
        },
        {
            lng: 4.93291616,
            lat: 5.94347191
        },
        {
            lng: 4.93264008,
            lat: 5.94347191
        },
        {
            lng: 4.93264008,
            lat: 5.9437499
        },
        {
            lng: 4.93236208,
            lat: 5.9437499
        },
        {
            lng: 4.93236208,
            lat: 5.9440279
        },
        {
            lng: 4.93208408,
            lat: 5.9440279
        },
        {
            lng: 4.93208408,
            lat: 5.9443059
        },
        {
            lng: 4.93180609,
            lat: 5.9443059
        },
        {
            lng: 4.93180609,
            lat: 5.94458389
        },
        {
            lng: 4.93152809,
            lat: 5.94458389
        },
        {
            lng: 4.93152809,
            lat: 5.94485998
        },
        {
            lng: 4.9312501,
            lat: 5.94485998
        },
        {
            lng: 4.9312501,
            lat: 5.94513798
        },
        {
            lng: 4.9309721,
            lat: 5.94513798
        },
        {
            lng: 4.9309721,
            lat: 5.94541597
        },
        {
            lng: 4.9306941,
            lat: 5.94541597
        },
        {
            lng: 4.9306941,
            lat: 5.94569397
        },
        {
            lng: 4.93041611,
            lat: 5.94569397
        },
        {
            lng: 4.93041611,
            lat: 5.94597197
        },
        {
            lng: 4.93014002,
            lat: 5.94597197
        },
        {
            lng: 4.93014002,
            lat: 5.94624996
        },
        {
            lng: 4.92986202,
            lat: 5.94624996
        },
        {
            lng: 4.92986202,
            lat: 5.94652796
        },
        {
            lng: 4.92958403,
            lat: 5.94652796
        },
        {
            lng: 4.92958403,
            lat: 5.94680595
        },
        {
            lng: 4.92930603,
            lat: 5.94680595
        },
        {
            lng: 4.92930603,
            lat: 5.94708395
        },
        {
            lng: 4.92902803,
            lat: 5.94708395
        },
        {
            lng: 4.92902803,
            lat: 5.94736004
        },
        {
            lng: 4.92875004,
            lat: 5.94736004
        },
        {
            lng: 4.92875004,
            lat: 5.94791603
        },
        {
            lng: 4.92847204,
            lat: 5.94791603
        },
        {
            lng: 4.92847204,
            lat: 5.94875097
        },
        {
            lng: 4.92736197,
            lat: 5.94875097
        },
        {
            lng: 4.92736197,
            lat: 5.94902897
        },
        {
            lng: 4.92708397,
            lat: 5.94902897
        },
        {
            lng: 4.92708397,
            lat: 5.94958496
        },
        {
            lng: 4.92680597,
            lat: 5.94958496
        },
        {
            lng: 4.92680597,
            lat: 5.9498601
        },
        {
            lng: 4.92652798,
            lat: 5.9498601
        },
        {
            lng: 4.92652798,
            lat: 5.95041704
        },
        {
            lng: 4.92624998,
            lat: 5.95041704
        },
        {
            lng: 4.92624998,
            lat: 5.95069504
        },
        {
            lng: 4.92597103,
            lat: 5.95069504
        },
        {
            lng: 4.92597103,
            lat: 5.95125103
        },
        {
            lng: 4.92569304,
            lat: 5.95125103
        },
        {
            lng: 4.92569304,
            lat: 5.95152617
        },
        {
            lng: 4.92541504,
            lat: 5.95152617
        },
        {
            lng: 4.92541504,
            lat: 5.95180511
        },
        {
            lng: 4.9251399,
            lat: 5.95180511
        },
        {
            lng: 4.9251399,
            lat: 5.95236111
        },
        {
            lng: 4.92486191,
            lat: 5.95236111
        },
        {
            lng: 4.92486191,
            lat: 5.9526391
        },
        {
            lng: 4.92458296,
            lat: 5.9526391
        },
        {
            lng: 4.92458296,
            lat: 5.9529171
        },
        {
            lng: 4.92402697,
            lat: 5.9529171
        },
        {
            lng: 4.92402697,
            lat: 5.9531951
        },
        {
            lng: 4.92374897,
            lat: 5.9531951
        },
        {
            lng: 4.92374897,
            lat: 5.95347309
        },
        {
            lng: 4.92347383,
            lat: 5.95347309
        },
        {
            lng: 4.92347383,
            lat: 5.95375109
        },
        {
            lng: 4.92319489,
            lat: 5.95375109
        },
        {
            lng: 4.92319489,
            lat: 5.95402718
        },
        {
            lng: 4.92291689,
            lat: 5.95402718
        },
        {
            lng: 4.92291689,
            lat: 5.95430517
        },
        {
            lng: 4.92263889,
            lat: 5.95430517
        },
        {
            lng: 4.92263889,
            lat: 5.95486116
        },
        {
            lng: 4.9223609,
            lat: 5.95486116
        },
        {
            lng: 4.9223609,
            lat: 5.95513916
        },
        {
            lng: 4.9220829,
            lat: 5.95513916
        },
        {
            lng: 4.9220829,
            lat: 5.95541716
        },
        {
            lng: 4.9218049,
            lat: 5.95541716
        },
        {
            lng: 4.9218049,
            lat: 5.95569515
        },
        {
            lng: 4.92152691,
            lat: 5.95569515
        },
        {
            lng: 4.92152691,
            lat: 5.95597315
        },
        {
            lng: 4.92097282,
            lat: 5.95597315
        },
        {
            lng: 4.92097282,
            lat: 5.95625114
        },
        {
            lng: 4.92069483,
            lat: 5.95625114
        },
        {
            lng: 4.92069483,
            lat: 5.95652723
        },
        {
            lng: 4.92041683,
            lat: 5.95652723
        },
        {
            lng: 4.92041683,
            lat: 5.95708323
        },
        {
            lng: 4.92013884,
            lat: 5.95708323
        },
        {
            lng: 4.92013884,
            lat: 5.95763922
        },
        {
            lng: 4.91986084,
            lat: 5.95763922
        },
        {
            lng: 4.91986084,
            lat: 5.95819521
        },
        {
            lng: 4.91930485,
            lat: 5.95819521
        },
        {
            lng: 4.91930485,
            lat: 5.95847321
        },
        {
            lng: 4.91902685,
            lat: 5.95847321
        },
        {
            lng: 4.91902685,
            lat: 5.95874882
        },
        {
            lng: 4.91847277,
            lat: 5.9587512
        },
        {
            lng: 4.91847277,
            lat: 5.95902681
        },
        {
            lng: 4.91819477,
            lat: 5.95902681
        },
        {
            lng: 4.91819477,
            lat: 5.95930481
        },
        {
            lng: 4.91791677,
            lat: 5.95930481
        },
        {
            lng: 4.91791677,
            lat: 5.95958281
        },
        {
            lng: 4.91763878,
            lat: 5.95958281
        },
        {
            lng: 4.91763878,
            lat: 5.9601388
        },
        {
            lng: 4.91736078,
            lat: 5.9601388
        },
        {
            lng: 4.91736078,
            lat: 5.96041679
        },
        {
            lng: 4.91708279,
            lat: 5.96041679
        },
        {
            lng: 4.91708279,
            lat: 5.96097088
        },
        {
            lng: 4.91680479,
            lat: 5.96097088
        },
        {
            lng: 4.91680479,
            lat: 5.96124887
        },
        {
            lng: 4.91652679,
            lat: 5.96124887
        },
        {
            lng: 4.91652679,
            lat: 5.96152687
        },
        {
            lng: 4.91625118,
            lat: 5.96152687
        },
        {
            lng: 4.91625118,
            lat: 5.96208286
        },
        {
            lng: 4.91597319,
            lat: 5.96208286
        },
        {
            lng: 4.91597319,
            lat: 5.96236086
        },
        {
            lng: 4.91541719,
            lat: 5.96236086
        },
        {
            lng: 4.91541719,
            lat: 5.96263981
        },
        {
            lng: 4.9151392,
            lat: 5.96263981
        },
        {
            lng: 4.9151392,
            lat: 5.9629178
        },
        {
            lng: 4.91458321,
            lat: 5.9629178
        },
        {
            lng: 4.91458321,
            lat: 5.96319294
        },
        {
            lng: 4.91430521,
            lat: 5.96319294
        },
        {
            lng: 4.91430521,
            lat: 5.96374893
        },
        {
            lng: 4.91402912,
            lat: 5.96374893
        },
        {
            lng: 4.91402912,
            lat: 5.96458387
        },
        {
            lng: 4.91375113,
            lat: 5.96458387
        },
        {
            lng: 4.91375113,
            lat: 5.96486187
        },
        {
            lng: 4.91347313,
            lat: 5.96486187
        },
        {
            lng: 4.91347313,
            lat: 5.96513987
        },
        {
            lng: 4.91319513,
            lat: 5.96513987
        },
        {
            lng: 4.91319513,
            lat: 5.96541595
        },
        {
            lng: 4.91291714,
            lat: 5.96541786
        },
        {
            lng: 4.91291714,
            lat: 5.96569395
        },
        {
            lng: 4.91263914,
            lat: 5.96569395
        },
        {
            lng: 4.91263914,
            lat: 5.96597195
        },
        {
            lng: 4.91180706,
            lat: 5.96597195
        },
        {
            lng: 4.91180706,
            lat: 5.96569395
        },
        {
            lng: 4.9120822,
            lat: 5.96569395
        },
        {
            lng: 4.9120822,
            lat: 5.96513987
        },
        {
            lng: 4.91236019,
            lat: 5.96513987
        },
        {
            lng: 4.91236019,
            lat: 5.96486187
        },
        {
            lng: 4.91263914,
            lat: 5.96486187
        },
        {
            lng: 4.91263914,
            lat: 5.96430588
        },
        {
            lng: 4.91291714,
            lat: 5.96430588
        },
        {
            lng: 4.91291714,
            lat: 5.96402788
        },
        {
            lng: 4.91319513,
            lat: 5.96402788
        },
        {
            lng: 4.91319513,
            lat: 5.96347094
        },
        {
            lng: 4.91347313,
            lat: 5.96347094
        },
        {
            lng: 4.91347313,
            lat: 5.96319294
        },
        {
            lng: 4.91375113,
            lat: 5.96319294
        },
        {
            lng: 4.91375113,
            lat: 5.9629178
        },
        {
            lng: 4.91402912,
            lat: 5.9629178
        },
        {
            lng: 4.91402912,
            lat: 5.96236086
        },
        {
            lng: 4.91430521,
            lat: 5.96236086
        },
        {
            lng: 4.91430521,
            lat: 5.96208286
        },
        {
            lng: 4.91458321,
            lat: 5.96208286
        },
        {
            lng: 4.91458321,
            lat: 5.96180487
        },
        {
            lng: 4.9148612,
            lat: 5.96180487
        },
        {
            lng: 4.9148612,
            lat: 5.96152687
        },
        {
            lng: 4.9151392,
            lat: 5.96152687
        },
        {
            lng: 4.9151392,
            lat: 5.96124887
        },
        {
            lng: 4.91541719,
            lat: 5.96124887
        },
        {
            lng: 4.91541719,
            lat: 5.96069479
        },
        {
            lng: 4.91569519,
            lat: 5.96069479
        },
        {
            lng: 4.91569519,
            lat: 5.96041679
        },
        {
            lng: 4.91597319,
            lat: 5.96041679
        },
        {
            lng: 4.91597319,
            lat: 5.9601388
        },
        {
            lng: 4.9162488,
            lat: 5.9601388
        },
        {
            lng: 4.91625118,
            lat: 5.95958281
        },
        {
            lng: 4.91652679,
            lat: 5.95958281
        },
        {
            lng: 4.91652679,
            lat: 5.95930481
        },
        {
            lng: 4.91680479,
            lat: 5.95930481
        },
        {
            lng: 4.91680479,
            lat: 5.95902681
        },
        {
            lng: 4.91708279,
            lat: 5.95902681
        },
        {
            lng: 4.91708279,
            lat: 5.95874882
        },
        {
            lng: 4.91736078,
            lat: 5.95874882
        },
        {
            lng: 4.91736078,
            lat: 5.95847321
        },
        {
            lng: 4.91763878,
            lat: 5.95847321
        },
        {
            lng: 4.91763878,
            lat: 5.95819521
        },
        {
            lng: 4.91791677,
            lat: 5.95819521
        },
        {
            lng: 4.91791677,
            lat: 5.95791721
        },
        {
            lng: 4.91819477,
            lat: 5.95791721
        },
        {
            lng: 4.91819477,
            lat: 5.95763922
        },
        {
            lng: 4.91847277,
            lat: 5.95763922
        },
        {
            lng: 4.91847277,
            lat: 5.95736122
        },
        {
            lng: 4.91874886,
            lat: 5.95736122
        },
        {
            lng: 4.91874886,
            lat: 5.95708323
        },
        {
            lng: 4.91902685,
            lat: 5.95708323
        },
        {
            lng: 4.91902685,
            lat: 5.95680523
        },
        {
            lng: 4.91930485,
            lat: 5.95680523
        },
        {
            lng: 4.91930485,
            lat: 5.95652723
        },
        {
            lng: 4.91958284,
            lat: 5.95652723
        },
        {
            lng: 4.91958284,
            lat: 5.95625114
        },
        {
            lng: 4.91986084,
            lat: 5.95625114
        },
        {
            lng: 4.91986084,
            lat: 5.95597315
        },
        {
            lng: 4.92013884,
            lat: 5.95597315
        },
        {
            lng: 4.92013884,
            lat: 5.95541716
        },
        {
            lng: 4.92041683,
            lat: 5.95541716
        },
        {
            lng: 4.92041683,
            lat: 5.95513916
        },
        {
            lng: 4.92069483,
            lat: 5.95513916
        },
        {
            lng: 4.92069483,
            lat: 5.95486116
        },
        {
            lng: 4.92097282,
            lat: 5.95486116
        },
        {
            lng: 4.92097282,
            lat: 5.95458317
        },
        {
            lng: 4.92124891,
            lat: 5.95458317
        },
        {
            lng: 4.92124891,
            lat: 5.95430517
        },
        {
            lng: 4.92152691,
            lat: 5.95430517
        },
        {
            lng: 4.92152691,
            lat: 5.95402718
        },
        {
            lng: 4.9218049,
            lat: 5.95402718
        },
        {
            lng: 4.9218049,
            lat: 5.95375109
        },
        {
            lng: 4.9220829,
            lat: 5.95375109
        },
        {
            lng: 4.9220829,
            lat: 5.95347309
        },
        {
            lng: 4.9223609,
            lat: 5.95347309
        },
        {
            lng: 4.9223609,
            lat: 5.9529171
        },
        {
            lng: 4.92263889,
            lat: 5.9529171
        },
        {
            lng: 4.92263889,
            lat: 5.9526391
        },
        {
            lng: 4.92291689,
            lat: 5.9526391
        },
        {
            lng: 4.92291689,
            lat: 5.95236111
        },
        {
            lng: 4.92319489,
            lat: 5.95236111
        },
        {
            lng: 4.92319489,
            lat: 5.95208311
        },
        {
            lng: 4.92347383,
            lat: 5.95208311
        },
        {
            lng: 4.92347383,
            lat: 5.95180511
        },
        {
            lng: 4.92374897,
            lat: 5.95180511
        },
        {
            lng: 4.92374897,
            lat: 5.95152617
        },
        {
            lng: 4.92402697,
            lat: 5.95152617
        },
        {
            lng: 4.92402697,
            lat: 5.95097303
        },
        {
            lng: 4.92430496,
            lat: 5.95097303
        },
        {
            lng: 4.92430496,
            lat: 5.95069504
        },
        {
            lng: 4.92458296,
            lat: 5.95069504
        },
        {
            lng: 4.92458296,
            lat: 5.95041704
        },
        {
            lng: 4.92486191,
            lat: 5.95041704
        },
        {
            lng: 4.92486191,
            lat: 5.9498601
        },
        {
            lng: 4.9251399,
            lat: 5.9498601
        },
        {
            lng: 4.9251399,
            lat: 5.94958496
        },
        {
            lng: 4.92541504,
            lat: 5.94958496
        },
        {
            lng: 4.92541504,
            lat: 5.94930696
        },
        {
            lng: 4.92569304,
            lat: 5.94930696
        },
        {
            lng: 4.92569304,
            lat: 5.94902897
        },
        {
            lng: 4.92597103,
            lat: 5.94902897
        },
        {
            lng: 4.92597103,
            lat: 5.94847202
        },
        {
            lng: 4.92624998,
            lat: 5.94847202
        },
        {
            lng: 4.92624998,
            lat: 5.94819403
        },
        {
            lng: 4.92652798,
            lat: 5.94819403
        },
        {
            lng: 4.92652798,
            lat: 5.94791603
        },
        {
            lng: 4.92680597,
            lat: 5.94791603
        },
        {
            lng: 4.92680597,
            lat: 5.94763803
        },
        {
            lng: 4.92708397,
            lat: 5.94763803
        },
        {
            lng: 4.92708397,
            lat: 5.94736004
        },
        {
            lng: 4.92736197,
            lat: 5.94736004
        },
        {
            lng: 4.92736197,
            lat: 5.94680595
        },
        {
            lng: 4.92763996,
            lat: 5.94680595
        },
        {
            lng: 4.92763996,
            lat: 5.94652796
        },
        {
            lng: 4.92791605,
            lat: 5.94652796
        },
        {
            lng: 4.92791605,
            lat: 5.94624996
        },
        {
            lng: 4.92819405,
            lat: 5.94624996
        },
        {
            lng: 4.92819405,
            lat: 5.94569397
        },
        {
            lng: 4.92847204,
            lat: 5.94569397
        },
        {
            lng: 4.92847204,
            lat: 5.94541597
        },
        {
            lng: 4.92875004,
            lat: 5.94541597
        },
        {
            lng: 4.92875004,
            lat: 5.94485998
        },
        {
            lng: 4.92902803,
            lat: 5.94485998
        },
        {
            lng: 4.92902803,
            lat: 5.94458389
        },
        {
            lng: 4.92930603,
            lat: 5.94458389
        },
        {
            lng: 4.92930603,
            lat: 5.9440279
        },
        {
            lng: 4.92958403,
            lat: 5.9440279
        },
        {
            lng: 4.92958403,
            lat: 5.94347191
        },
        {
            lng: 4.92930603,
            lat: 5.94347191
        },
        {
            lng: 4.92930603,
            lat: 5.94319391
        },
        {
            lng: 4.92902803,
            lat: 5.94319391
        },
        {
            lng: 4.92902803,
            lat: 5.9437499
        },
        {
            lng: 4.92875004,
            lat: 5.9437499
        },
        {
            lng: 4.92875004,
            lat: 5.9443059
        },
        {
            lng: 4.92847204,
            lat: 5.9443059
        },
        {
            lng: 4.92847204,
            lat: 5.94485998
        },
        {
            lng: 4.92819405,
            lat: 5.94485998
        },
        {
            lng: 4.92819405,
            lat: 5.94513798
        },
        {
            lng: 4.92791605,
            lat: 5.94513798
        },
        {
            lng: 4.92791605,
            lat: 5.94541597
        },
        {
            lng: 4.92763996,
            lat: 5.94541597
        },
        {
            lng: 4.92763996,
            lat: 5.94569397
        },
        {
            lng: 4.92736197,
            lat: 5.94569397
        },
        {
            lng: 4.92736197,
            lat: 5.94624996
        },
        {
            lng: 4.92708397,
            lat: 5.94624996
        },
        {
            lng: 4.92708397,
            lat: 5.94652796
        },
        {
            lng: 4.92680597,
            lat: 5.94652796
        },
        {
            lng: 4.92680597,
            lat: 5.94680595
        },
        {
            lng: 4.92652798,
            lat: 5.94680595
        },
        {
            lng: 4.92652798,
            lat: 5.94708395
        },
        {
            lng: 4.92624998,
            lat: 5.94708395
        },
        {
            lng: 4.92624998,
            lat: 5.94763803
        },
        {
            lng: 4.92597103,
            lat: 5.94763803
        },
        {
            lng: 4.92597103,
            lat: 5.94791603
        },
        {
            lng: 4.92569304,
            lat: 5.94791603
        },
        {
            lng: 4.92569304,
            lat: 5.94819403
        },
        {
            lng: 4.92541504,
            lat: 5.94819403
        },
        {
            lng: 4.92541504,
            lat: 5.94875097
        },
        {
            lng: 4.9251399,
            lat: 5.94875097
        },
        {
            lng: 4.9251399,
            lat: 5.94902897
        },
        {
            lng: 4.92486191,
            lat: 5.94902897
        },
        {
            lng: 4.92486191,
            lat: 5.94958496
        },
        {
            lng: 4.92458296,
            lat: 5.94958496
        },
        {
            lng: 4.92458296,
            lat: 5.9498601
        },
        {
            lng: 4.92430496,
            lat: 5.9498601
        },
        {
            lng: 4.92430496,
            lat: 5.95013809
        },
        {
            lng: 4.92402697,
            lat: 5.95013809
        },
        {
            lng: 4.92402697,
            lat: 5.95041704
        },
        {
            lng: 4.92374897,
            lat: 5.95041704
        },
        {
            lng: 4.92374897,
            lat: 5.95069504
        },
        {
            lng: 4.92347383,
            lat: 5.95069504
        },
        {
            lng: 4.92347383,
            lat: 5.95097303
        },
        {
            lng: 4.92319489,
            lat: 5.95097303
        },
        {
            lng: 4.92319489,
            lat: 5.95152617
        },
        {
            lng: 4.92291689,
            lat: 5.95152617
        },
        {
            lng: 4.92291689,
            lat: 5.95180511
        },
        {
            lng: 4.92263889,
            lat: 5.95180511
        },
        {
            lng: 4.92263889,
            lat: 5.95208311
        },
        {
            lng: 4.9223609,
            lat: 5.95208311
        },
        {
            lng: 4.9223609,
            lat: 5.95236111
        },
        {
            lng: 4.9220829,
            lat: 5.95236111
        },
        {
            lng: 4.9220829,
            lat: 5.9526391
        },
        {
            lng: 4.9218049,
            lat: 5.9526391
        },
        {
            lng: 4.9218049,
            lat: 5.9529171
        },
        {
            lng: 4.92152691,
            lat: 5.9529171
        },
        {
            lng: 4.92152691,
            lat: 5.9531951
        },
        {
            lng: 4.92124891,
            lat: 5.9531951
        },
        {
            lng: 4.92124891,
            lat: 5.95347309
        },
        {
            lng: 4.92097282,
            lat: 5.95347309
        },
        {
            lng: 4.92097282,
            lat: 5.95375109
        },
        {
            lng: 4.92069483,
            lat: 5.95375109
        },
        {
            lng: 4.92069483,
            lat: 5.95402718
        },
        {
            lng: 4.92041683,
            lat: 5.95402718
        },
        {
            lng: 4.92041683,
            lat: 5.95458317
        },
        {
            lng: 4.92013884,
            lat: 5.95458317
        },
        {
            lng: 4.92013884,
            lat: 5.95486116
        },
        {
            lng: 4.91986084,
            lat: 5.95486116
        },
        {
            lng: 4.91986084,
            lat: 5.95513916
        },
        {
            lng: 4.91958284,
            lat: 5.95513916
        },
        {
            lng: 4.91958284,
            lat: 5.95541716
        },
        {
            lng: 4.91930485,
            lat: 5.95541716
        },
        {
            lng: 4.91930485,
            lat: 5.95569515
        },
        {
            lng: 4.91902685,
            lat: 5.95569515
        },
        {
            lng: 4.91902685,
            lat: 5.95597315
        },
        {
            lng: 4.91874886,
            lat: 5.95597315
        },
        {
            lng: 4.91874886,
            lat: 5.95625114
        },
        {
            lng: 4.91847277,
            lat: 5.95625114
        },
        {
            lng: 4.91847277,
            lat: 5.95652723
        },
        {
            lng: 4.91819477,
            lat: 5.95652723
        },
        {
            lng: 4.91819477,
            lat: 5.95680523
        },
        {
            lng: 4.91791677,
            lat: 5.95680523
        },
        {
            lng: 4.91791677,
            lat: 5.95708323
        },
        {
            lng: 4.91763878,
            lat: 5.95708323
        },
        {
            lng: 4.91763878,
            lat: 5.95736122
        },
        {
            lng: 4.91736078,
            lat: 5.95736122
        },
        {
            lng: 4.91736078,
            lat: 5.95763922
        },
        {
            lng: 4.91708279,
            lat: 5.95763922
        },
        {
            lng: 4.91708279,
            lat: 5.95791721
        },
        {
            lng: 4.91680479,
            lat: 5.95791721
        },
        {
            lng: 4.91680479,
            lat: 5.95819521
        },
        {
            lng: 4.91652679,
            lat: 5.95819521
        },
        {
            lng: 4.91652679,
            lat: 5.95847321
        },
        {
            lng: 4.91625118,
            lat: 5.95847321
        },
        {
            lng: 4.91625118,
            lat: 5.95902681
        },
        {
            lng: 4.91597319,
            lat: 5.95902681
        },
        {
            lng: 4.91597319,
            lat: 5.95930481
        },
        {
            lng: 4.91569519,
            lat: 5.95930481
        },
        {
            lng: 4.91569519,
            lat: 5.9598608
        },
        {
            lng: 4.91541719,
            lat: 5.9598608
        },
        {
            lng: 4.91541719,
            lat: 5.9601388
        },
        {
            lng: 4.9151392,
            lat: 5.9601388
        },
        {
            lng: 4.9151392,
            lat: 5.96041679
        },
        {
            lng: 4.9148612,
            lat: 5.96041679
        },
        {
            lng: 4.9148612,
            lat: 5.96069479
        },
        {
            lng: 4.91458321,
            lat: 5.96069479
        },
        {
            lng: 4.91458321,
            lat: 5.96097088
        },
        {
            lng: 4.91430521,
            lat: 5.96097088
        },
        {
            lng: 4.91430521,
            lat: 5.96152687
        },
        {
            lng: 4.91402912,
            lat: 5.96152687
        },
        {
            lng: 4.91402912,
            lat: 5.96180487
        },
        {
            lng: 4.91375113,
            lat: 5.96180487
        },
        {
            lng: 4.91375113,
            lat: 5.96208286
        },
        {
            lng: 4.91347313,
            lat: 5.96208286
        },
        {
            lng: 4.91347313,
            lat: 5.96236086
        },
        {
            lng: 4.91319513,
            lat: 5.96236086
        },
        {
            lng: 4.91319513,
            lat: 5.96263981
        },
        {
            lng: 4.91291714,
            lat: 5.96263981
        },
        {
            lng: 4.91291714,
            lat: 5.9629178
        },
        {
            lng: 4.91263914,
            lat: 5.9629178
        },
        {
            lng: 4.91263914,
            lat: 5.96319294
        },
        {
            lng: 4.91236019,
            lat: 5.96319294
        },
        {
            lng: 4.91236019,
            lat: 5.96347094
        },
        {
            lng: 4.9120822,
            lat: 5.96347094
        },
        {
            lng: 4.9120822,
            lat: 5.96374893
        },
        {
            lng: 4.91180706,
            lat: 5.96374893
        },
        {
            lng: 4.91180706,
            lat: 5.96402788
        },
        {
            lng: 4.91152906,
            lat: 5.96402788
        },
        {
            lng: 4.91152906,
            lat: 5.96430588
        },
        {
            lng: 4.91125107,
            lat: 5.96430588
        },
        {
            lng: 4.91125107,
            lat: 5.96458387
        },
        {
            lng: 4.91097212,
            lat: 5.96458387
        },
        {
            lng: 4.91097212,
            lat: 5.96513987
        },
        {
            lng: 4.91069412,
            lat: 5.96513987
        },
        {
            lng: 4.91069412,
            lat: 5.96541595
        },
        {
            lng: 4.91041613,
            lat: 5.96541786
        },
        {
            lng: 4.91041613,
            lat: 5.96569395
        },
        {
            lng: 4.91013813,
            lat: 5.96569395
        },
        {
            lng: 4.91013813,
            lat: 5.96597195
        },
        {
            lng: 4.90986013,
            lat: 5.96597195
        },
        {
            lng: 4.90986013,
            lat: 5.96624994
        },
        {
            lng: 4.90958405,
            lat: 5.96624994
        },
        {
            lng: 4.90958405,
            lat: 5.96652794
        },
        {
            lng: 4.90930605,
            lat: 5.96652794
        },
        {
            lng: 4.90930605,
            lat: 5.96680593
        },
        {
            lng: 4.90902805,
            lat: 5.96680593
        },
        {
            lng: 4.90902805,
            lat: 5.96708393
        },
        {
            lng: 4.90875006,
            lat: 5.96708393
        },
        {
            lng: 4.90875006,
            lat: 5.96736193
        },
        {
            lng: 4.90847206,
            lat: 5.96736193
        },
        {
            lng: 4.90847206,
            lat: 5.96763992
        },
        {
            lng: 4.90819407,
            lat: 5.96763992
        },
        {
            lng: 4.90819407,
            lat: 5.96791601
        },
        {
            lng: 4.90791607,
            lat: 5.96791601
        },
        {
            lng: 4.90791607,
            lat: 5.96819401
        },
        {
            lng: 4.90763807,
            lat: 5.96819401
        },
        {
            lng: 4.90763807,
            lat: 5.968472
        },
        {
            lng: 4.90736008,
            lat: 5.968472
        },
        {
            lng: 4.90736008,
            lat: 5.96875
        },
        {
            lng: 4.90708399,
            lat: 5.96875
        },
        {
            lng: 4.90708399,
            lat: 5.969028
        },
        {
            lng: 4.90680599,
            lat: 5.969028
        },
        {
            lng: 4.90680599,
            lat: 5.96930599
        },
        {
            lng: 4.906528,
            lat: 5.96930599
        },
        {
            lng: 4.906528,
            lat: 5.96986198
        },
        {
            lng: 4.90625,
            lat: 5.96986198
        },
        {
            lng: 4.90625,
            lat: 5.97013807
        },
        {
            lng: 4.905972,
            lat: 5.97013807
        },
        {
            lng: 4.905972,
            lat: 5.97041607
        },
        {
            lng: 4.90569401,
            lat: 5.97041607
        },
        {
            lng: 4.90569401,
            lat: 5.97069407
        },
        {
            lng: 4.90541601,
            lat: 5.97069407
        },
        {
            lng: 4.90541601,
            lat: 5.97125006
        },
        {
            lng: 4.90513802,
            lat: 5.97125006
        },
        {
            lng: 4.90513802,
            lat: 5.97152805
        },
        {
            lng: 4.90486193,
            lat: 5.97152805
        },
        {
            lng: 4.90486193,
            lat: 5.97180605
        },
        {
            lng: 4.90458393,
            lat: 5.97180605
        },
        {
            lng: 4.90458393,
            lat: 5.97208405
        },
        {
            lng: 4.90430593,
            lat: 5.97208405
        },
        {
            lng: 4.90430593,
            lat: 5.97263813
        },
        {
            lng: 4.90402794,
            lat: 5.97263813
        },
        {
            lng: 4.90402794,
            lat: 5.97291613
        },
        {
            lng: 4.90374994,
            lat: 5.97291613
        },
        {
            lng: 4.90374994,
            lat: 5.97319412
        },
        {
            lng: 4.90347195,
            lat: 5.97319412
        },
        {
            lng: 4.90347195,
            lat: 5.97375011
        },
        {
            lng: 4.90319395,
            lat: 5.97375011
        },
        {
            lng: 4.90319395,
            lat: 5.97402811
        },
        {
            lng: 4.90263987,
            lat: 5.97402811
        },
        {
            lng: 4.90263987,
            lat: 5.97430611
        },
        {
            lng: 4.90236187,
            lat: 5.97430611
        },
        {
            lng: 4.90236187,
            lat: 5.9745822
        },
        {
            lng: 4.90208387,
            lat: 5.9745822
        },
        {
            lng: 4.90208387,
            lat: 5.97486019
        },
        {
            lng: 4.90180588,
            lat: 5.97486019
        },
        {
            lng: 4.90180588,
            lat: 5.97541618
        },
        {
            lng: 4.90152788,
            lat: 5.97541618
        },
        {
            lng: 4.90152788,
            lat: 5.97569418
        },
        {
            lng: 4.90124989,
            lat: 5.97569418
        },
        {
            lng: 4.90124989,
            lat: 5.97597218
        },
        {
            lng: 4.90097189,
            lat: 5.97597218
        },
        {
            lng: 4.90097189,
            lat: 5.97652912
        },
        {
            lng: 4.90069389,
            lat: 5.97652912
        },
        {
            lng: 4.90069389,
            lat: 5.97680712
        },
        {
            lng: 4.9004178,
            lat: 5.97680712
        },
        {
            lng: 4.9004178,
            lat: 5.97708178
        },
        {
            lng: 4.90013981,
            lat: 5.97708178
        },
        {
            lng: 4.90013981,
            lat: 5.97735977
        },
        {
            lng: 4.89986181,
            lat: 5.97735977
        },
        {
            lng: 4.89986181,
            lat: 5.97791719
        },
        {
            lng: 4.89958382,
            lat: 5.97791719
        },
        {
            lng: 4.89958382,
            lat: 5.97819519
        },
        {
            lng: 4.89930582,
            lat: 5.97819519
        },
        {
            lng: 4.89930582,
            lat: 5.97847319
        },
        {
            lng: 4.89902782,
            lat: 5.97847319
        },
        {
            lng: 4.89902782,
            lat: 5.97875118
        },
        {
            lng: 4.89874887,
            lat: 5.97875118
        },
        {
            lng: 4.89874887,
            lat: 5.97902918
        },
        {
            lng: 4.89847088,
            lat: 5.97902918
        },
        {
            lng: 4.89847088,
            lat: 5.97930717
        },
        {
            lng: 4.89819288,
            lat: 5.97930717
        },
        {
            lng: 4.89819288,
            lat: 5.97958279
        },
        {
            lng: 4.89791822,
            lat: 5.97958279
        },
        {
            lng: 4.89791822,
            lat: 5.97986078
        },
        {
            lng: 4.89764023,
            lat: 5.97986078
        },
        {
            lng: 4.89764023,
            lat: 5.98013878
        },
        {
            lng: 4.8973608,
            lat: 5.98013878
        },
        {
            lng: 4.8973608,
            lat: 5.98069477
        },
        {
            lng: 4.89708281,
            lat: 5.98069477
        },
        {
            lng: 4.89708281,
            lat: 5.98097277
        },
        {
            lng: 4.89680481,
            lat: 5.98097277
        },
        {
            lng: 4.89680481,
            lat: 5.98124886
        },
        {
            lng: 4.89652681,
            lat: 5.98124886
        },
        {
            lng: 4.89652681,
            lat: 5.98152685
        },
        {
            lng: 4.89624882,
            lat: 5.98152685
        },
        {
            lng: 4.89624882,
            lat: 5.98180485
        },
        {
            lng: 4.89597082,
            lat: 5.98180485
        },
        {
            lng: 4.89597082,
            lat: 5.98208284
        },
        {
            lng: 4.89569283,
            lat: 5.98208284
        },
        {
            lng: 4.89569283,
            lat: 5.98236084
        },
        {
            lng: 4.89541721,
            lat: 5.98236084
        },
        {
            lng: 4.89541721,
            lat: 5.98291683
        },
        {
            lng: 4.89513922,
            lat: 5.98291683
        },
        {
            lng: 4.89513922,
            lat: 5.98319483
        },
        {
            lng: 4.89486122,
            lat: 5.98319483
        },
        {
            lng: 4.89486122,
            lat: 5.98347282
        },
        {
            lng: 4.89458323,
            lat: 5.98347282
        },
        {
            lng: 4.89458323,
            lat: 5.98402691
        },
        {
            lng: 4.89430523,
            lat: 5.98402691
        },
        {
            lng: 4.89430523,
            lat: 5.9843049
        },
        {
            lng: 4.89402723,
            lat: 5.9843049
        },
        {
            lng: 4.89402723,
            lat: 5.9845829
        },
        {
            lng: 4.89375114,
            lat: 5.9845829
        },
        {
            lng: 4.89375114,
            lat: 5.9848609
        },
        {
            lng: 4.89347315,
            lat: 5.9848609
        },
        {
            lng: 4.89347315,
            lat: 5.98513889
        },
        {
            lng: 4.89319515,
            lat: 5.98513889
        },
        {
            lng: 4.89319515,
            lat: 5.98569489
        },
        {
            lng: 4.89291716,
            lat: 5.98569489
        },
        {
            lng: 4.89291716,
            lat: 5.98597288
        },
        {
            lng: 4.89263916,
            lat: 5.98597288
        },
        {
            lng: 4.89263916,
            lat: 5.98624897
        },
        {
            lng: 4.89236116,
            lat: 5.98624897
        },
        {
            lng: 4.89236116,
            lat: 5.98652697
        },
        {
            lng: 4.89208317,
            lat: 5.98652697
        },
        {
            lng: 4.89208317,
            lat: 5.98680496
        },
        {
            lng: 4.89180517,
            lat: 5.98680496
        },
        {
            lng: 4.89180517,
            lat: 5.98708296
        },
        {
            lng: 4.89125109,
            lat: 5.98708296
        },
        {
            lng: 4.89125109,
            lat: 5.98736095
        },
        {
            lng: 4.89097309,
            lat: 5.98736095
        },
        {
            lng: 4.89097309,
            lat: 5.98763895
        },
        {
            lng: 4.8906951,
            lat: 5.98763895
        },
        {
            lng: 4.8906951,
            lat: 5.98791695
        },
        {
            lng: 4.8904171,
            lat: 5.98791695
        },
        {
            lng: 4.8904171,
            lat: 5.9881959
        },
        {
            lng: 4.8901391,
            lat: 5.9881959
        },
        {
            lng: 4.8901391,
            lat: 5.98874903
        },
        {
            lng: 4.88986111,
            lat: 5.98874903
        },
        {
            lng: 4.88986111,
            lat: 5.98930502
        },
        {
            lng: 4.88958311,
            lat: 5.98930502
        },
        {
            lng: 4.88958311,
            lat: 5.98958302
        },
        {
            lng: 4.88930511,
            lat: 5.98958302
        },
        {
            lng: 4.88930511,
            lat: 5.98986197
        },
        {
            lng: 4.88902712,
            lat: 5.98986197
        },
        {
            lng: 4.88902712,
            lat: 5.99041796
        },
        {
            lng: 4.88875103,
            lat: 5.99041796
        },
        {
            lng: 4.88875103,
            lat: 5.99069309
        },
        {
            lng: 4.88847303,
            lat: 5.99069309
        },
        {
            lng: 4.88847303,
            lat: 5.99097109
        },
        {
            lng: 4.88791704,
            lat: 5.99097109
        },
        {
            lng: 4.88791704,
            lat: 5.99125004
        },
        {
            lng: 4.88763905,
            lat: 5.99125004
        },
        {
            lng: 4.88763905,
            lat: 5.99180603
        },
        {
            lng: 4.88736105,
            lat: 5.99180603
        },
        {
            lng: 4.88736105,
            lat: 5.99208403
        },
        {
            lng: 4.88708305,
            lat: 5.99208403
        },
        {
            lng: 4.88708305,
            lat: 5.99236202
        },
        {
            lng: 4.88680506,
            lat: 5.99236202
        },
        {
            lng: 4.88680506,
            lat: 5.99264002
        },
        {
            lng: 4.88652897,
            lat: 5.99264002
        },
        {
            lng: 4.88652897,
            lat: 5.99291611
        },
        {
            lng: 4.88625097,
            lat: 5.99291611
        },
        {
            lng: 4.88625097,
            lat: 5.9931941
        },
        {
            lng: 4.88597298,
            lat: 5.9931941
        },
        {
            lng: 4.88597298,
            lat: 5.9937501
        },
        {
            lng: 4.88569498,
            lat: 5.9937501
        },
        {
            lng: 4.88569498,
            lat: 5.99402809
        },
        {
            lng: 4.88541698,
            lat: 5.99402809
        },
        {
            lng: 4.88541698,
            lat: 5.99430609
        },
        {
            lng: 4.88513803,
            lat: 5.99430609
        },
        {
            lng: 4.88513803,
            lat: 5.99458408
        },
        {
            lng: 4.88458204,
            lat: 5.99458408
        },
        {
            lng: 4.88458204,
            lat: 5.99486208
        },
        {
            lng: 4.88430691,
            lat: 5.99486208
        },
        {
            lng: 4.88430691,
            lat: 5.99514008
        },
        {
            lng: 4.88402891,
            lat: 5.99514008
        },
        {
            lng: 4.88402891,
            lat: 5.99541616
        },
        {
            lng: 4.88374996,
            lat: 5.99541616
        },
        {
            lng: 4.88374996,
            lat: 5.99569416
        },
        {
            lng: 4.88319397,
            lat: 5.99569416
        },
        {
            lng: 4.88319397,
            lat: 5.99597216
        },
        {
            lng: 4.88291597,
            lat: 5.99597216
        },
        {
            lng: 4.88291597,
            lat: 5.99625015
        },
        {
            lng: 4.88263798,
            lat: 5.99625015
        },
        {
            lng: 4.88263798,
            lat: 5.99652815
        },
        {
            lng: 4.88235998,
            lat: 5.99652815
        },
        {
            lng: 4.88235998,
            lat: 5.99680614
        },
        {
            lng: 4.88208389,
            lat: 5.99680614
        },
        {
            lng: 4.88208389,
            lat: 5.99736214
        },
        {
            lng: 4.8818059,
            lat: 5.99736214
        },
        {
            lng: 4.8818059,
            lat: 5.99763823
        },
        {
            lng: 4.8815279,
            lat: 5.99763823
        },
        {
            lng: 4.8815279,
            lat: 5.99791622
        },
        {
            lng: 4.8812499,
            lat: 5.99791622
        },
        {
            lng: 4.8812499,
            lat: 5.99819422
        },
        {
            lng: 4.88097191,
            lat: 5.99819422
        },
        {
            lng: 4.88097191,
            lat: 5.99875021
        },
        {
            lng: 4.88069391,
            lat: 5.99875021
        },
        {
            lng: 4.88069391,
            lat: 5.99902821
        },
        {
            lng: 4.88041592,
            lat: 5.99902821
        },
        {
            lng: 4.88041592,
            lat: 5.9993062
        },
        {
            lng: 4.87986183,
            lat: 5.9993062
        },
        {
            lng: 4.87985992,
            lat: 5.9995842
        },
        {
            lng: 4.87958384,
            lat: 5.9995842
        },
        {
            lng: 4.87958384,
            lat: 5.99985981
        },
        {
            lng: 4.87930584,
            lat: 5.99985981
        },
        {
            lng: 4.87930584,
            lat: 6.00013781
        },
        {
            lng: 4.87930584,
            lat: 6.0004158
        },
        {
            lng: 4.87902784,
            lat: 6.0004158
        },
        {
            lng: 4.87902784,
            lat: 6.00097179
        },
        {
            lng: 4.87819386,
            lat: 6.00097179
        },
        {
            lng: 4.87819386,
            lat: 6.00124979
        },
        {
            lng: 4.87791586,
            lat: 6.00124979
        },
        {
            lng: 4.87791586,
            lat: 6.00180578
        },
        {
            lng: 4.87763786,
            lat: 6.00180578
        },
        {
            lng: 4.87763786,
            lat: 6.00208187
        },
        {
            lng: 4.87736177,
            lat: 6.00208187
        },
        {
            lng: 4.87736177,
            lat: 6.00235987
        },
        {
            lng: 4.87708378,
            lat: 6.00235987
        },
        {
            lng: 4.87708378,
            lat: 6.00263786
        },
        {
            lng: 4.87652779,
            lat: 6.00263786
        },
        {
            lng: 4.87652779,
            lat: 6.00291586
        },
        {
            lng: 4.87624979,
            lat: 6.00291586
        },
        {
            lng: 4.87624979,
            lat: 6.00319386
        },
        {
            lng: 4.87597179,
            lat: 6.00319386
        },
        {
            lng: 4.87597179,
            lat: 6.00347281
        },
        {
            lng: 4.8756938,
            lat: 6.00347281
        },
        {
            lng: 4.8756938,
            lat: 6.0040288
        },
        {
            lng: 4.87514019,
            lat: 6.0040288
        },
        {
            lng: 4.87514019,
            lat: 6.00430393
        },
        {
            lng: 4.8745842,
            lat: 6.00430393
        },
        {
            lng: 4.8745842,
            lat: 6.00458193
        },
        {
            lng: 4.8743062,
            lat: 6.00458193
        },
        {
            lng: 4.8743062,
            lat: 6.00486088
        },
        {
            lng: 4.87402821,
            lat: 6.00486088
        },
        {
            lng: 4.87402821,
            lat: 6.00513887
        },
        {
            lng: 4.87375021,
            lat: 6.00513887
        },
        {
            lng: 4.87375021,
            lat: 6.00625086
        },
        {
            lng: 4.87347221,
            lat: 6.00625086
        },
        {
            lng: 4.87347221,
            lat: 6.00652885
        },
        {
            lng: 4.87319613,
            lat: 6.00652885
        },
        {
            lng: 4.87319422,
            lat: 6.00680494
        },
        {
            lng: 4.87291813,
            lat: 6.00680494
        },
        {
            lng: 4.87291813,
            lat: 6.00708294
        },
        {
            lng: 4.87264013,
            lat: 6.00708294
        },
        {
            lng: 4.87264013,
            lat: 6.00736094
        },
        {
            lng: 4.87236214,
            lat: 6.00736094
        },
        {
            lng: 4.87236214,
            lat: 6.00791693
        },
        {
            lng: 4.87208414,
            lat: 6.00791693
        },
        {
            lng: 4.87208414,
            lat: 6.00819492
        },
        {
            lng: 4.87180614,
            lat: 6.00819492
        },
        {
            lng: 4.87180614,
            lat: 6.00847292
        },
        {
            lng: 4.87152719,
            lat: 6.00847292
        },
        {
            lng: 4.87152719,
            lat: 6.00875092
        },
        {
            lng: 4.8709712,
            lat: 6.00875092
        },
        {
            lng: 4.8709712,
            lat: 6.00902891
        },
        {
            lng: 4.87069607,
            lat: 6.00902891
        },
        {
            lng: 4.87069607,
            lat: 6.009583
        },
        {
            lng: 4.87013912,
            lat: 6.009583
        },
        {
            lng: 4.87013912,
            lat: 6.01013899
        },
        {
            lng: 4.86986113,
            lat: 6.01013899
        },
        {
            lng: 4.86986113,
            lat: 6.01041698
        },
        {
            lng: 4.86958313,
            lat: 6.01041698
        },
        {
            lng: 4.86958313,
            lat: 6.01069498
        },
        {
            lng: 4.86930513,
            lat: 6.01069498
        },
        {
            lng: 4.86930513,
            lat: 6.01097107
        },
        {
            lng: 4.86902714,
            lat: 6.01097107
        },
        {
            lng: 4.86902714,
            lat: 6.01124907
        },
        {
            lng: 4.86874914,
            lat: 6.01124907
        },
        {
            lng: 4.86874914,
            lat: 6.01152706
        },
        {
            lng: 4.86847115,
            lat: 6.01152706
        },
        {
            lng: 4.86847115,
            lat: 6.01180506
        },
        {
            lng: 4.86819506,
            lat: 6.01180506
        },
        {
            lng: 4.86819506,
            lat: 6.01236105
        },
        {
            lng: 4.86736107,
            lat: 6.01236105
        },
        {
            lng: 4.86736107,
            lat: 6.01291704
        },
        {
            lng: 4.86708307,
            lat: 6.01291704
        },
        {
            lng: 4.86708307,
            lat: 6.01319504
        },
        {
            lng: 4.86680508,
            lat: 6.01319504
        },
        {
            lng: 4.86680508,
            lat: 6.01374912
        },
        {
            lng: 4.86624908,
            lat: 6.01374912
        },
        {
            lng: 4.86624908,
            lat: 6.01402712
        },
        {
            lng: 4.86597109,
            lat: 6.01402712
        },
        {
            lng: 4.86597109,
            lat: 6.01430511
        },
        {
            lng: 4.865695,
            lat: 6.01430511
        },
        {
            lng: 4.865695,
            lat: 6.01458311
        },
        {
            lng: 4.86513901,
            lat: 6.01458311
        },
        {
            lng: 4.86513901,
            lat: 6.01486111
        },
        {
            lng: 4.86458302,
            lat: 6.01486111
        },
        {
            lng: 4.86458302,
            lat: 6.0151391
        },
        {
            lng: 4.86402893,
            lat: 6.0151391
        },
        {
            lng: 4.86402893,
            lat: 6.0154171
        },
        {
            lng: 4.86375093,
            lat: 6.0154171
        },
        {
            lng: 4.86375093,
            lat: 6.01569605
        },
        {
            lng: 4.86347294,
            lat: 6.01569605
        },
        {
            lng: 4.86347294,
            lat: 6.01597118
        },
        {
            lng: 4.86319494,
            lat: 6.01597118
        },
        {
            lng: 4.86319494,
            lat: 6.01652718
        },
        {
            lng: 4.86291695,
            lat: 6.01652718
        },
        {
            lng: 4.86291695,
            lat: 6.01680517
        },
        {
            lng: 4.86263895,
            lat: 6.01680517
        },
        {
            lng: 4.86263895,
            lat: 6.01708412
        },
        {
            lng: 4.86236095,
            lat: 6.01708412
        },
        {
            lng: 4.86236095,
            lat: 6.01736212
        },
        {
            lng: 4.86208296,
            lat: 6.01736212
        },
        {
            lng: 4.86208296,
            lat: 6.01764011
        },
        {
            lng: 4.86180496,
            lat: 6.01764011
        },
        {
            lng: 4.86180496,
            lat: 6.01791811
        },
        {
            lng: 4.86152887,
            lat: 6.01791811
        },
        {
            lng: 4.86152887,
            lat: 6.01819611
        },
        {
            lng: 4.86125088,
            lat: 6.01819611
        },
        {
            lng: 4.86125088,
            lat: 6.01875019
        },
        {
            lng: 4.86069489,
            lat: 6.01875019
        },
        {
            lng: 4.86069489,
            lat: 6.01902819
        },
        {
            lng: 4.86041689,
            lat: 6.01902819
        },
        {
            lng: 4.86041689,
            lat: 6.01930618
        },
        {
            lng: 4.8598609,
            lat: 6.01930618
        },
        {
            lng: 4.8598609,
            lat: 6.01958418
        },
        {
            lng: 4.8595829,
            lat: 6.01958418
        },
        {
            lng: 4.8595829,
            lat: 6.01986217
        },
        {
            lng: 4.85930395,
            lat: 6.01986217
        },
        {
            lng: 4.85930395,
            lat: 6.02014017
        },
        {
            lng: 4.85875082,
            lat: 6.02014017
        },
        {
            lng: 4.85875082,
            lat: 6.02041578
        },
        {
            lng: 4.85847282,
            lat: 6.02041578
        },
        {
            lng: 4.85847282,
            lat: 6.02069378
        },
        {
            lng: 4.85791588,
            lat: 6.02069378
        },
        {
            lng: 4.85791588,
            lat: 6.02097178
        },
        {
            lng: 4.85763788,
            lat: 6.02097178
        },
        {
            lng: 4.85763788,
            lat: 6.02124977
        },
        {
            lng: 4.85735989,
            lat: 6.02124977
        },
        {
            lng: 4.85735989,
            lat: 6.02152777
        },
        {
            lng: 4.85708189,
            lat: 6.02152777
        },
        {
            lng: 4.85708189,
            lat: 6.02180576
        },
        {
            lng: 4.85680723,
            lat: 6.02180576
        },
        {
            lng: 4.85680389,
            lat: 6.02208424
        },
        {
            lng: 4.85652781,
            lat: 6.02208424
        },
        {
            lng: 4.85652781,
            lat: 6.02263784
        },
        {
            lng: 4.85624981,
            lat: 6.02263784
        },
        {
            lng: 4.85624981,
            lat: 6.02291584
        },
        {
            lng: 4.85597181,
            lat: 6.02291584
        },
        {
            lng: 4.85597181,
            lat: 6.02319384
        },
        {
            lng: 4.85569382,
            lat: 6.02319384
        },
        {
            lng: 4.85569382,
            lat: 6.02347183
        },
        {
            lng: 4.85541582,
            lat: 6.02347183
        },
        {
            lng: 4.85541582,
            lat: 6.02374983
        },
        {
            lng: 4.85513783,
            lat: 6.02374983
        },
        {
            lng: 4.85513783,
            lat: 6.02430582
        },
        {
            lng: 4.85458422,
            lat: 6.02430582
        },
        {
            lng: 4.85458422,
            lat: 6.02458382
        },
        {
            lng: 4.85430622,
            lat: 6.02458382
        },
        {
            lng: 4.85430622,
            lat: 6.02486181
        },
        {
            lng: 4.85402822,
            lat: 6.02486181
        },
        {
            lng: 4.85402822,
            lat: 6.0251379
        },
        {
            lng: 4.85375023,
            lat: 6.0251379
        },
        {
            lng: 4.85375023,
            lat: 6.02569389
        },
        {
            lng: 4.85347223,
            lat: 6.02569389
        },
        {
            lng: 4.85347223,
            lat: 6.02624989
        },
        {
            lng: 4.85291576,
            lat: 6.02624989
        },
        {
            lng: 4.85291576,
            lat: 6.02652788
        },
        {
            lng: 4.85263777,
            lat: 6.02652788
        },
        {
            lng: 4.85263777,
            lat: 6.02680588
        },
        {
            lng: 4.85236216,
            lat: 6.02680588
        },
        {
            lng: 4.85236216,
            lat: 6.02708387
        },
        {
            lng: 4.85208416,
            lat: 6.02708387
        },
        {
            lng: 4.85208416,
            lat: 6.02763796
        },
        {
            lng: 4.85152817,
            lat: 6.02763796
        },
        {
            lng: 4.85152817,
            lat: 6.02791595
        },
        {
            lng: 4.85125017,
            lat: 6.02791595
        },
        {
            lng: 4.85125017,
            lat: 6.02819395
        },
        {
            lng: 4.85097218,
            lat: 6.02819395
        },
        {
            lng: 4.85097218,
            lat: 6.02847195
        },
        {
            lng: 4.85069418,
            lat: 6.02847195
        },
        {
            lng: 4.85069418,
            lat: 6.02930689
        },
        {
            lng: 4.85041618,
            lat: 6.02930689
        },
        {
            lng: 4.85041618,
            lat: 6.02958202
        },
        {
            lng: 4.85014009,
            lat: 6.02958202
        },
        {
            lng: 4.85013819,
            lat: 6.03013802
        },
        {
            lng: 4.8498621,
            lat: 6.03013802
        },
        {
            lng: 4.8498621,
            lat: 6.03041601
        },
        {
            lng: 4.8495841,
            lat: 6.03041601
        },
        {
            lng: 4.8495841,
            lat: 6.03069496
        },
        {
            lng: 4.84902811,
            lat: 6.03069496
        },
        {
            lng: 4.84902811,
            lat: 6.03097296
        },
        {
            lng: 4.84875011,
            lat: 6.03097296
        },
        {
            lng: 4.84875011,
            lat: 6.03125095
        },
        {
            lng: 4.84847212,
            lat: 6.03125095
        },
        {
            lng: 4.84847212,
            lat: 6.03152895
        },
        {
            lng: 4.84819412,
            lat: 6.03152895
        },
        {
            lng: 4.84819412,
            lat: 6.03180408
        },
        {
            lng: 4.84791613,
            lat: 6.03180408
        },
        {
            lng: 4.84791613,
            lat: 6.03236103
        },
        {
            lng: 4.84764004,
            lat: 6.03236103
        },
        {
            lng: 4.84764004,
            lat: 6.03263903
        },
        {
            lng: 4.84736204,
            lat: 6.03263903
        },
        {
            lng: 4.84736204,
            lat: 6.03291702
        },
        {
            lng: 4.84680605,
            lat: 6.03291702
        },
        {
            lng: 4.84680605,
            lat: 6.03319502
        },
        {
            lng: 4.84652805,
            lat: 6.03319502
        },
        {
            lng: 4.84652805,
            lat: 6.03347301
        },
        {
            lng: 4.84625006,
            lat: 6.03347301
        },
        {
            lng: 4.84625006,
            lat: 6.03375101
        },
        {
            lng: 4.84597206,
            lat: 6.03375101
        },
        {
            lng: 4.84597206,
            lat: 6.0340271
        },
        {
            lng: 4.84569311,
            lat: 6.0340271
        },
        {
            lng: 4.84569311,
            lat: 6.0343051
        },
        {
            lng: 4.84541512,
            lat: 6.0343051
        },
        {
            lng: 4.84541512,
            lat: 6.03458309
        },
        {
            lng: 4.84513998,
            lat: 6.03458309
        },
        {
            lng: 4.84513998,
            lat: 6.03486109
        },
        {
            lng: 4.84486198,
            lat: 6.03486109
        },
        {
            lng: 4.84486198,
            lat: 6.03513908
        },
        {
            lng: 4.84458399,
            lat: 6.03513908
        },
        {
            lng: 4.84458399,
            lat: 6.03541708
        },
        {
            lng: 4.84430504,
            lat: 6.03541708
        },
        {
            lng: 4.84430504,
            lat: 6.03569508
        },
        {
            lng: 4.84402704,
            lat: 6.03569508
        },
        {
            lng: 4.84402704,
            lat: 6.03597307
        },
        {
            lng: 4.84347391,
            lat: 6.03597307
        },
        {
            lng: 4.84347105,
            lat: 6.03625107
        },
        {
            lng: 4.84319592,
            lat: 6.03625107
        },
        {
            lng: 4.84319592,
            lat: 6.03652716
        },
        {
            lng: 4.84291697,
            lat: 6.03652716
        },
        {
            lng: 4.84291697,
            lat: 6.03680515
        },
        {
            lng: 4.84263897,
            lat: 6.03680515
        },
        {
            lng: 4.84263897,
            lat: 6.03708315
        },
        {
            lng: 4.84236097,
            lat: 6.03708315
        },
        {
            lng: 4.84236097,
            lat: 6.03736115
        },
        {
            lng: 4.84208298,
            lat: 6.03736115
        },
        {
            lng: 4.84208298,
            lat: 6.03763914
        },
        {
            lng: 4.84180498,
            lat: 6.03763914
        },
        {
            lng: 4.84180498,
            lat: 6.03791714
        },
        {
            lng: 4.84124899,
            lat: 6.03791714
        },
        {
            lng: 4.84124899,
            lat: 6.03819513
        },
        {
            lng: 4.8406949,
            lat: 6.03819513
        },
        {
            lng: 4.8406949,
            lat: 6.03847313
        },
        {
            lng: 4.84041691,
            lat: 6.03847313
        },
        {
            lng: 4.84041691,
            lat: 6.03930521
        },
        {
            lng: 4.83930492,
            lat: 6.03930521
        },
        {
            lng: 4.83930492,
            lat: 6.03958321
        },
        {
            lng: 4.83902693,
            lat: 6.03958321
        },
        {
            lng: 4.83902693,
            lat: 6.0398612
        },
        {
            lng: 4.83874893,
            lat: 6.0398612
        },
        {
            lng: 4.83874893,
            lat: 6.0401392
        },
        {
            lng: 4.83847284,
            lat: 6.0401392
        },
        {
            lng: 4.83847284,
            lat: 6.04041719
        },
        {
            lng: 4.83819485,
            lat: 6.04041719
        },
        {
            lng: 4.83819485,
            lat: 6.04069281
        },
        {
            lng: 4.83763885,
            lat: 6.04069281
        },
        {
            lng: 4.83763885,
            lat: 6.0409708
        },
        {
            lng: 4.83736086,
            lat: 6.0409708
        },
        {
            lng: 4.83736086,
            lat: 6.04152679
        },
        {
            lng: 4.83708286,
            lat: 6.04152679
        },
        {
            lng: 4.83708286,
            lat: 6.04180479
        },
        {
            lng: 4.83680487,
            lat: 6.04180479
        },
        {
            lng: 4.83680487,
            lat: 6.04236078
        },
        {
            lng: 4.83624887,
            lat: 6.04236078
        },
        {
            lng: 4.83624887,
            lat: 6.04263878
        },
        {
            lng: 4.83597279,
            lat: 6.04263878
        },
        {
            lng: 4.83597279,
            lat: 6.04319286
        },
        {
            lng: 4.83569479,
            lat: 6.04319286
        },
        {
            lng: 4.83569479,
            lat: 6.04347086
        },
        {
            lng: 4.8351388,
            lat: 6.04347086
        },
        {
            lng: 4.8351388,
            lat: 6.0443058
        },
        {
            lng: 4.8348608,
            lat: 6.0443058
        },
        {
            lng: 4.8348608,
            lat: 6.04495287
        },
        {
            lng: 4.8348608,
            lat: 6.04513979
        },
        {
            lng: 4.83458281,
            lat: 6.04513979
        },
        {
            lng: 4.83458281,
            lat: 6.04541779
        },
        {
            lng: 4.8343482,
            lat: 6.04541779
        },
        {
            lng: 4.83347321,
            lat: 6.04541779
        },
        {
            lng: 4.83347321,
            lat: 6.04569387
        },
        {
            lng: 4.83319521,
            lat: 6.04569387
        },
        {
            lng: 4.83319521,
            lat: 6.04624987
        },
        {
            lng: 4.83291721,
            lat: 6.04624987
        },
        {
            lng: 4.83291721,
            lat: 6.04652786
        },
        {
            lng: 4.83319521,
            lat: 6.04652786
        },
        {
            lng: 4.83319521,
            lat: 6.04680586
        },
        {
            lng: 4.83291721,
            lat: 6.04680586
        },
        {
            lng: 4.83291721,
            lat: 6.04736185
        },
        {
            lng: 4.83263922,
            lat: 6.04736185
        },
        {
            lng: 4.83263922,
            lat: 6.04763985
        },
        {
            lng: 4.83208179,
            lat: 6.04763985
        },
        {
            lng: 4.83208179,
            lat: 6.04791784
        },
        {
            lng: 4.83180714,
            lat: 6.04791784
        },
        {
            lng: 4.83180714,
            lat: 6.04847193
        },
        {
            lng: 4.83152914,
            lat: 6.04847193
        },
        {
            lng: 4.83152914,
            lat: 6.04902792
        },
        {
            lng: 4.83097315,
            lat: 6.04902792
        },
        {
            lng: 4.83097315,
            lat: 6.04930592
        },
        {
            lng: 4.8304162,
            lat: 6.04930592
        },
        {
            lng: 4.8304162,
            lat: 6.04958391
        },
        {
            lng: 4.83013821,
            lat: 6.04958391
        },
        {
            lng: 4.83013821,
            lat: 6.050138
        },
        {
            lng: 4.82986021,
            lat: 6.050138
        },
        {
            lng: 4.82986021,
            lat: 6.05041599
        },
        {
            lng: 4.82958221,
            lat: 6.05041599
        },
        {
            lng: 4.82958221,
            lat: 6.05069399
        },
        {
            lng: 4.82902813,
            lat: 6.05069399
        },
        {
            lng: 4.82902813,
            lat: 6.05124998
        },
        {
            lng: 4.82847214,
            lat: 6.05124998
        },
        {
            lng: 4.82847214,
            lat: 6.05180597
        },
        {
            lng: 4.82819414,
            lat: 6.05180597
        },
        {
            lng: 4.82819414,
            lat: 6.05208397
        },
        {
            lng: 4.82763815,
            lat: 6.05208397
        },
        {
            lng: 4.82763815,
            lat: 6.05236006
        },
        {
            lng: 4.82736015,
            lat: 6.05236006
        },
        {
            lng: 4.82736015,
            lat: 6.05263805
        },
        {
            lng: 4.82708406,
            lat: 6.05263805
        },
        {
            lng: 4.82708216,
            lat: 6.05291605
        },
        {
            lng: 4.82680607,
            lat: 6.05291605
        },
        {
            lng: 4.82680607,
            lat: 6.05347204
        },
        {
            lng: 4.82652807,
            lat: 6.05347204
        },
        {
            lng: 4.82652807,
            lat: 6.05375004
        },
        {
            lng: 4.82625008,
            lat: 6.05375004
        },
        {
            lng: 4.82625008,
            lat: 6.05402803
        },
        {
            lng: 4.82597208,
            lat: 6.05402803
        },
        {
            lng: 4.82597208,
            lat: 6.05430603
        },
        {
            lng: 4.82569408,
            lat: 6.05430603
        },
        {
            lng: 4.82569408,
            lat: 6.05458403
        },
        {
            lng: 4.824862,
            lat: 6.05458403
        },
        {
            lng: 4.824862,
            lat: 6.05486012
        },
        {
            lng: 4.82458401,
            lat: 6.05486012
        },
        {
            lng: 4.82458401,
            lat: 6.05513811
        },
        {
            lng: 4.82402802,
            lat: 6.05513811
        },
        {
            lng: 4.82402802,
            lat: 6.05541611
        },
        {
            lng: 4.82375002,
            lat: 6.05541611
        },
        {
            lng: 4.82375002,
            lat: 6.0556941
        },
        {
            lng: 4.82347202,
            lat: 6.0556941
        },
        {
            lng: 4.82347202,
            lat: 6.0559721
        },
        {
            lng: 4.82319403,
            lat: 6.0559721
        },
        {
            lng: 4.82319403,
            lat: 6.0562501
        },
        {
            lng: 4.82347202,
            lat: 6.0562501
        },
        {
            lng: 4.82347202,
            lat: 6.05652905
        },
        {
            lng: 4.82291603,
            lat: 6.05652905
        },
        {
            lng: 4.82291603,
            lat: 6.05680704
        },
        {
            lng: 4.82263994,
            lat: 6.05680704
        },
        {
            lng: 4.82263994,
            lat: 6.05736017
        },
        {
            lng: 4.82236195,
            lat: 6.05736017
        },
        {
            lng: 4.82236195,
            lat: 6.05763817
        },
        {
            lng: 4.82208395,
            lat: 6.05763817
        },
        {
            lng: 4.82208395,
            lat: 6.05791712
        },
        {
            lng: 4.82124996,
            lat: 6.05791712
        },
        {
            lng: 4.82124996,
            lat: 6.05819511
        },
        {
            lng: 4.82097197,
            lat: 6.05819511
        },
        {
            lng: 4.82097197,
            lat: 6.05930519
        },
        {
            lng: 4.82069397,
            lat: 6.05930519
        },
        {
            lng: 4.82069397,
            lat: 6.06013918
        },
        {
            lng: 4.82041788,
            lat: 6.06013918
        },
        {
            lng: 4.82041597,
            lat: 6.06041718
        },
        {
            lng: 4.82013988,
            lat: 6.06041718
        },
        {
            lng: 4.82013988,
            lat: 6.06069517
        },
        {
            lng: 4.8190279,
            lat: 6.06069517
        },
        {
            lng: 4.8190279,
            lat: 6.06097317
        },
        {
            lng: 4.8187499,
            lat: 6.06097317
        },
        {
            lng: 4.8187499,
            lat: 6.06152678
        },
        {
            lng: 4.81847095,
            lat: 6.06152678
        },
        {
            lng: 4.81847095,
            lat: 6.06208277
        },
        {
            lng: 4.81819296,
            lat: 6.06208277
        },
        {
            lng: 4.81819296,
            lat: 6.06236076
        },
        {
            lng: 4.81763983,
            lat: 6.06236076
        },
        {
            lng: 4.81763983,
            lat: 6.06208277
        },
        {
            lng: 4.81680489,
            lat: 6.06213522
        },
        {
            lng: 4.81680489,
            lat: 6.06236076
        },
        {
            lng: 4.81652689,
            lat: 6.06241608
        },
        {
            lng: 4.81652689,
            lat: 6.06263924
        },
        {
            lng: 4.81636095,
            lat: 6.06263924
        },
        {
            lng: 4.8159709,
            lat: 6.06263924
        },
        {
            lng: 4.8159709,
            lat: 6.06302881
        },
        {
            lng: 4.8159709,
            lat: 6.06319523
        },
        {
            lng: 4.8156929,
            lat: 6.06325197
        },
        {
            lng: 4.8156929,
            lat: 6.06402683
        },
        {
            lng: 4.81541681,
            lat: 6.06402683
        },
        {
            lng: 4.81541681,
            lat: 6.06430483
        },
        {
            lng: 4.81486082,
            lat: 6.06430483
        },
        {
            lng: 4.81486082,
            lat: 6.06458282
        },
        {
            lng: 4.81430483,
            lat: 6.06464481
        },
        {
            lng: 4.81430483,
            lat: 6.06513882
        },
        {
            lng: 4.81402683,
            lat: 6.06513882
        },
        {
            lng: 4.81402683,
            lat: 6.06569481
        },
        {
            lng: 4.81374884,
            lat: 6.06569481
        },
        {
            lng: 4.81374884,
            lat: 6.06597281
        },
        {
            lng: 4.81347322,
            lat: 6.06597281
        },
        {
            lng: 4.81347322,
            lat: 6.06624889
        },
        {
            lng: 4.81291723,
            lat: 6.06624889
        },
        {
            lng: 4.81291723,
            lat: 6.06652689
        },
        {
            lng: 4.81243181,
            lat: 6.06659794
        },
        {
            lng: 4.81229305,
            lat: 6.06680489
        },
        {
            lng: 4.81180477,
            lat: 6.06680489
        },
        {
            lng: 4.81173897,
            lat: 6.06736088
        },
        {
            lng: 4.81152678,
            lat: 6.06736088
        },
        {
            lng: 4.81152678,
            lat: 6.06750679
        },
        {
            lng: 4.81152678,
            lat: 6.06791687
        },
        {
            lng: 4.81119919,
            lat: 6.06792688
        },
        {
            lng: 4.81125116,
            lat: 6.06819487
        },
        {
            lng: 4.81091595,
            lat: 6.06820011
        },
        {
            lng: 4.81097317,
            lat: 6.06874895
        },
        {
            lng: 4.81069517,
            lat: 6.06874895
        },
        {
            lng: 4.81069517,
            lat: 6.06902695
        },
        {
            lng: 4.81041718,
            lat: 6.06902695
        },
        {
            lng: 4.81041718,
            lat: 6.06930494
        },
        {
            lng: 4.81013918,
            lat: 6.06930494
        },
        {
            lng: 4.81013918,
            lat: 6.06958294
        },
        {
            lng: 4.80986118,
            lat: 6.06958294
        },
        {
            lng: 4.80986118,
            lat: 6.06986094
        },
        {
            lng: 4.80958319,
            lat: 6.06986094
        },
        {
            lng: 4.80958319,
            lat: 6.07013988
        },
        {
            lng: 4.80930519,
            lat: 6.07013988
        },
        {
            lng: 4.80930519,
            lat: 6.07041502
        },
        {
            lng: 4.80902719,
            lat: 6.07041788
        },
        {
            lng: 4.80902719,
            lat: 6.07069302
        },
        {
            lng: 4.80875111,
            lat: 6.07069302
        },
        {
            lng: 4.80875111,
            lat: 6.07097101
        },
        {
            lng: 4.80847311,
            lat: 6.07097101
        },
        {
            lng: 4.80847311,
            lat: 6.07124901
        },
        {
            lng: 4.80819511,
            lat: 6.07124901
        },
        {
            lng: 4.80819511,
            lat: 6.07152796
        },
        {
            lng: 4.80791712,
            lat: 6.07152796
        },
        {
            lng: 4.80791712,
            lat: 6.07208395
        },
        {
            lng: 4.80736113,
            lat: 6.07208395
        },
        {
            lng: 4.80736113,
            lat: 6.07236195
        },
        {
            lng: 4.80680513,
            lat: 6.07236195
        },
        {
            lng: 4.80680513,
            lat: 6.07263994
        },
        {
            lng: 4.80652714,
            lat: 6.07263994
        },
        {
            lng: 4.80652714,
            lat: 6.07291603
        },
        {
            lng: 4.80625105,
            lat: 6.07291603
        },
        {
            lng: 4.80625105,
            lat: 6.07347202
        },
        {
            lng: 4.80597305,
            lat: 6.07347202
        },
        {
            lng: 4.80597305,
            lat: 6.07375002
        },
        {
            lng: 4.80569506,
            lat: 6.07375002
        },
        {
            lng: 4.80569506,
            lat: 6.07402802
        },
        {
            lng: 4.80513906,
            lat: 6.07402802
        },
        {
            lng: 4.80513906,
            lat: 6.07430601
        },
        {
            lng: 4.80486012,
            lat: 6.07430601
        },
        {
            lng: 4.80486012,
            lat: 6.07458401
        },
        {
            lng: 4.80458212,
            lat: 6.07458401
        },
        {
            lng: 4.80458212,
            lat: 6.07514
        },
        {
            lng: 4.80430698,
            lat: 6.07514
        },
        {
            lng: 4.80430698,
            lat: 6.07541609
        },
        {
            lng: 4.80402899,
            lat: 6.07541609
        },
        {
            lng: 4.80402899,
            lat: 6.07569408
        },
        {
            lng: 4.80340004,
            lat: 6.07569408
        },
        {
            lng: 4.80319405,
            lat: 6.07569408
        },
        {
            lng: 4.80319405,
            lat: 6.07590103
        },
        {
            lng: 4.80319405,
            lat: 6.07625008
        },
        {
            lng: 4.80291605,
            lat: 6.07625008
        },
        {
            lng: 4.80291605,
            lat: 6.07652807
        },
        {
            lng: 4.80257416,
            lat: 6.07652807
        },
        {
            lng: 4.80236006,
            lat: 6.07652807
        },
        {
            lng: 4.80236006,
            lat: 6.07680607
        },
        {
            lng: 4.80180597,
            lat: 6.07680607
        },
        {
            lng: 4.80180597,
            lat: 6.07708406
        },
        {
            lng: 4.80152798,
            lat: 6.07708406
        },
        {
            lng: 4.80152798,
            lat: 6.07736206
        },
        {
            lng: 4.80097198,
            lat: 6.07736206
        },
        {
            lng: 4.80097198,
            lat: 6.07764006
        },
        {
            lng: 4.80041599,
            lat: 6.07764006
        },
        {
            lng: 4.80041599,
            lat: 6.07791615
        },
        {
            lng: 4.79986,
            lat: 6.07791615
        },
        {
            lng: 4.79986,
            lat: 6.07902813
        },
        {
            lng: 4.79958391,
            lat: 6.07902813
        },
        {
            lng: 4.79958391,
            lat: 6.07958412
        },
        {
            lng: 4.79930592,
            lat: 6.07958412
        },
        {
            lng: 4.79930592,
            lat: 6.07986021
        },
        {
            lng: 4.79847193,
            lat: 6.07986021
        },
        {
            lng: 4.79847193,
            lat: 6.08013821
        },
        {
            lng: 4.79819393,
            lat: 6.08013821
        },
        {
            lng: 4.79819393,
            lat: 6.0804162
        },
        {
            lng: 4.79791594,
            lat: 6.0804162
        },
        {
            lng: 4.79791594,
            lat: 6.08097219
        },
        {
            lng: 4.79763794,
            lat: 6.08097219
        },
        {
            lng: 4.79763794,
            lat: 6.08125019
        },
        {
            lng: 4.79736185,
            lat: 6.08125019
        },
        {
            lng: 4.79735994,
            lat: 6.08152819
        },
        {
            lng: 4.79708385,
            lat: 6.08152819
        },
        {
            lng: 4.79708385,
            lat: 6.08180618
        },
        {
            lng: 4.79680586,
            lat: 6.08180618
        },
        {
            lng: 4.79680586,
            lat: 6.08208179
        },
        {
            lng: 4.79652786,
            lat: 6.08208179
        },
        {
            lng: 4.79652786,
            lat: 6.08235979
        },
        {
            lng: 4.79624987,
            lat: 6.08235979
        },
        {
            lng: 4.79624987,
            lat: 6.08263779
        },
        {
            lng: 4.79597187,
            lat: 6.08263779
        },
        {
            lng: 4.79597187,
            lat: 6.08291578
        },
        {
            lng: 4.79569387,
            lat: 6.08291578
        },
        {
            lng: 4.79569387,
            lat: 6.08319378
        },
        {
            lng: 4.79513979,
            lat: 6.08319378
        },
        {
            lng: 4.79513979,
            lat: 6.08347178
        },
        {
            lng: 4.79486179,
            lat: 6.08347178
        },
        {
            lng: 4.79486179,
            lat: 6.0837512
        },
        {
            lng: 4.7945838,
            lat: 6.0837512
        },
        {
            lng: 4.7945838,
            lat: 6.0840292
        },
        {
            lng: 4.7943058,
            lat: 6.0840292
        },
        {
            lng: 4.7943058,
            lat: 6.08430719
        },
        {
            lng: 4.79402781,
            lat: 6.08430719
        },
        {
            lng: 4.79402781,
            lat: 6.08485985
        },
        {
            lng: 4.79393721,
            lat: 6.08485985
        },
        {
            lng: 4.79374981,
            lat: 6.08485985
        },
        {
            lng: 4.79369307,
            lat: 6.0851388
        },
        {
            lng: 4.79361486,
            lat: 6.0851388
        },
        {
            lng: 4.79291821,
            lat: 6.0851388
        },
        {
            lng: 4.79294109,
            lat: 6.08572197
        },
        {
            lng: 4.79264021,
            lat: 6.08569479
        },
        {
            lng: 4.79264021,
            lat: 6.08598423
        },
        {
            lng: 4.79264021,
            lat: 6.08625078
        },
        {
            lng: 4.79236221,
            lat: 6.08625078
        },
        {
            lng: 4.79236221,
            lat: 6.08652878
        },
        {
            lng: 4.79208422,
            lat: 6.08652878
        },
        {
            lng: 4.79208422,
            lat: 6.08680487
        },
        {
            lng: 4.79168892,
            lat: 6.08680677
        },
        {
            lng: 4.79152822,
            lat: 6.08680677
        },
        {
            lng: 4.79152822,
            lat: 6.08694601
        },
        {
            lng: 4.79152822,
            lat: 6.08708286
        },
        {
            lng: 4.79136801,
            lat: 6.08708286
        },
        {
            lng: 4.7909708,
            lat: 6.08708286
        },
        {
            lng: 4.79100609,
            lat: 6.08739901
        },
        {
            lng: 4.79069281,
            lat: 6.08736086
        },
        {
            lng: 4.79070711,
            lat: 6.08765507
        },
        {
            lng: 4.79041815,
            lat: 6.08763885
        },
        {
            lng: 4.79041815,
            lat: 6.08791685
        },
        {
            lng: 4.79014015,
            lat: 6.08791685
        },
        {
            lng: 4.79014015,
            lat: 6.08819485
        },
        {
            lng: 4.7898612,
            lat: 6.08819485
        },
        {
            lng: 4.7898612,
            lat: 6.08847284
        },
        {
            lng: 4.78976297,
            lat: 6.08847284
        },
        {
            lng: 4.78958321,
            lat: 6.08847284
        },
        {
            lng: 4.78952408,
            lat: 6.08867979
        },
        {
            lng: 4.78930521,
            lat: 6.08881712
        },
        {
            lng: 4.78930521,
            lat: 6.08902884
        },
        {
            lng: 4.78902721,
            lat: 6.08902884
        },
        {
            lng: 4.78902721,
            lat: 6.08910894
        },
        {
            lng: 4.78902721,
            lat: 6.08930492
        },
        {
            lng: 4.78874922,
            lat: 6.08930492
        },
        {
            lng: 4.78874922,
            lat: 6.08958292
        },
        {
            lng: 4.78847122,
            lat: 6.08958292
        },
        {
            lng: 4.78847122,
            lat: 6.09041691
        },
        {
            lng: 4.78819513,
            lat: 6.09041691
        },
        {
            lng: 4.78819513,
            lat: 6.0906949
        },
        {
            lng: 4.78791714,
            lat: 6.0906949
        },
        {
            lng: 4.78791714,
            lat: 6.0909729
        },
        {
            lng: 4.78708315,
            lat: 6.0909729
        },
        {
            lng: 4.78708315,
            lat: 6.09124899
        },
        {
            lng: 4.78680515,
            lat: 6.09124899
        },
        {
            lng: 4.78680515,
            lat: 6.09180498
        },
        {
            lng: 4.78624916,
            lat: 6.09180498
        },
        {
            lng: 4.78624916,
            lat: 6.09208298
        },
        {
            lng: 4.78597116,
            lat: 6.09208298
        },
        {
            lng: 4.78597116,
            lat: 6.09236097
        },
        {
            lng: 4.78569508,
            lat: 6.09236097
        },
        {
            lng: 4.78569508,
            lat: 6.09319496
        },
        {
            lng: 4.78486109,
            lat: 6.09319496
        },
        {
            lng: 4.78486109,
            lat: 6.09374905
        },
        {
            lng: 4.7843051,
            lat: 6.09374905
        },
        {
            lng: 4.7843051,
            lat: 6.09347296
        },
        {
            lng: 4.7840271,
            lat: 6.09347296
        },
        {
            lng: 4.7840271,
            lat: 6.09402704
        },
        {
            lng: 4.78342295,
            lat: 6.09403181
        },
        {
            lng: 4.78347301,
            lat: 6.09458303
        },
        {
            lng: 4.78319502,
            lat: 6.09458303
        },
        {
            lng: 4.78319502,
            lat: 6.09486103
        },
        {
            lng: 4.78291702,
            lat: 6.09486103
        },
        {
            lng: 4.78291702,
            lat: 6.09513903
        },
        {
            lng: 4.78263903,
            lat: 6.09513903
        },
        {
            lng: 4.78263903,
            lat: 6.09541702
        },
        {
            lng: 4.78236103,
            lat: 6.09541702
        },
        {
            lng: 4.78236103,
            lat: 6.09569502
        },
        {
            lng: 4.78208303,
            lat: 6.09569502
        },
        {
            lng: 4.78208303,
            lat: 6.0965271
        },
        {
            lng: 4.78180504,
            lat: 6.0965271
        },
        {
            lng: 4.78180504,
            lat: 6.0968051
        },
        {
            lng: 4.78152895,
            lat: 6.0968051
        },
        {
            lng: 4.78152895,
            lat: 6.09708309
        },
        {
            lng: 4.78125095,
            lat: 6.09708309
        },
        {
            lng: 4.78125095,
            lat: 6.09736204
        },
        {
            lng: 4.78069496,
            lat: 6.09736204
        },
        {
            lng: 4.78069496,
            lat: 6.09791803
        },
        {
            lng: 4.78041697,
            lat: 6.09791803
        },
        {
            lng: 4.78041697,
            lat: 6.09847116
        },
        {
            lng: 4.78013897,
            lat: 6.09847116
        },
        {
            lng: 4.78013897,
            lat: 6.09875011
        },
        {
            lng: 4.77986097,
            lat: 6.09875011
        },
        {
            lng: 4.77986097,
            lat: 6.09930611
        },
        {
            lng: 4.77958298,
            lat: 6.09930611
        },
        {
            lng: 4.77958298,
            lat: 6.0998621
        },
        {
            lng: 4.77930498,
            lat: 6.0998621
        },
        {
            lng: 4.77930498,
            lat: 6.10013819
        },
        {
            lng: 4.7784729,
            lat: 6.10013819
        },
        {
            lng: 4.7784729,
            lat: 6.0998621
        },
        {
            lng: 4.77763796,
            lat: 6.0998621
        },
        {
            lng: 4.77763796,
            lat: 6.10013819
        },
        {
            lng: 4.77735996,
            lat: 6.10014009
        },
        {
            lng: 4.77735996,
            lat: 6.10023308
        },
        {
            lng: 4.77735996,
            lat: 6.10097218
        },
        {
            lng: 4.77708197,
            lat: 6.10097218
        },
        {
            lng: 4.77708197,
            lat: 6.10180616
        },
        {
            lng: 4.77624989,
            lat: 6.10180616
        },
        {
            lng: 4.77624989,
            lat: 6.10208416
        },
        {
            lng: 4.77584696,
            lat: 6.10208416
        },
        {
            lng: 4.77569389,
            lat: 6.10208416
        },
        {
            lng: 4.77569389,
            lat: 6.10227108
        },
        {
            lng: 4.77569389,
            lat: 6.10291576
        },
        {
            lng: 4.7754159,
            lat: 6.10291576
        },
        {
            lng: 4.7754159,
            lat: 6.10319424
        },
        {
            lng: 4.77493811,
            lat: 6.10319424
        },
        {
            lng: 4.77458382,
            lat: 6.10319424
        },
        {
            lng: 4.77457809,
            lat: 6.10352707
        },
        {
            lng: 4.77402782,
            lat: 6.10347223
        },
        {
            lng: 4.77402782,
            lat: 6.10375023
        },
        {
            lng: 4.77347183,
            lat: 6.10375023
        },
        {
            lng: 4.77347183,
            lat: 6.10402822
        },
        {
            lng: 4.77319384,
            lat: 6.10402822
        },
        {
            lng: 4.77319384,
            lat: 6.10430622
        },
        {
            lng: 4.77291584,
            lat: 6.10430622
        },
        {
            lng: 4.77291584,
            lat: 6.10458422
        },
        {
            lng: 4.77263784,
            lat: 6.10458422
        },
        {
            lng: 4.77263784,
            lat: 6.10513783
        },
        {
            lng: 4.77236223,
            lat: 6.10513783
        },
        {
            lng: 4.77236223,
            lat: 6.10541582
        },
        {
            lng: 4.77180576,
            lat: 6.10541582
        },
        {
            lng: 4.77180576,
            lat: 6.10569382
        },
        {
            lng: 4.77152777,
            lat: 6.10569382
        },
        {
            lng: 4.77152777,
            lat: 6.10597181
        },
        {
            lng: 4.77097178,
            lat: 6.10597181
        },
        {
            lng: 4.77097178,
            lat: 6.10624981
        },
        {
            lng: 4.77069378,
            lat: 6.10624981
        },
        {
            lng: 4.77069378,
            lat: 6.1068058
        },
        {
            lng: 4.77013779,
            lat: 6.1068058
        },
        {
            lng: 4.77013779,
            lat: 6.1070838
        },
        {
            lng: 4.76958418,
            lat: 6.1070838
        },
        {
            lng: 4.76958418,
            lat: 6.10736179
        },
        {
            lng: 4.76902819,
            lat: 6.10736179
        },
        {
            lng: 4.76902819,
            lat: 6.10763788
        },
        {
            lng: 4.76875019,
            lat: 6.10763788
        },
        {
            lng: 4.76875019,
            lat: 6.10791588
        },
        {
            lng: 4.7681942,
            lat: 6.10791588
        },
        {
            lng: 4.7681942,
            lat: 6.10819387
        },
        {
            lng: 4.7679162,
            lat: 6.10819387
        },
        {
            lng: 4.7679162,
            lat: 6.10847187
        },
        {
            lng: 4.76736212,
            lat: 6.10847187
        },
        {
            lng: 4.76736212,
            lat: 6.10874987
        },
        {
            lng: 4.76680613,
            lat: 6.10874987
        },
        {
            lng: 4.76680613,
            lat: 6.10902786
        },
        {
            lng: 4.76652813,
            lat: 6.10902786
        },
        {
            lng: 4.76652813,
            lat: 6.10930586
        },
        {
            lng: 4.76625013,
            lat: 6.10930586
        },
        {
            lng: 4.76625013,
            lat: 6.10958195
        },
        {
            lng: 4.76597214,
            lat: 6.10958195
        },
        {
            lng: 4.76597214,
            lat: 6.10985994
        },
        {
            lng: 4.76569414,
            lat: 6.10985994
        },
        {
            lng: 4.76569414,
            lat: 6.11180401
        },
        {
            lng: 4.76514006,
            lat: 6.11180401
        },
        {
            lng: 4.76514006,
            lat: 6.112082
        },
        {
            lng: 4.76402712,
            lat: 6.112082
        },
        {
            lng: 4.76402712,
            lat: 6.11236095
        },
        {
            lng: 4.76374912,
            lat: 6.11236095
        },
        {
            lng: 4.76374912,
            lat: 6.11263895
        },
        {
            lng: 4.76319599,
            lat: 6.11263895
        },
        {
            lng: 4.76319599,
            lat: 6.11291695
        },
        {
            lng: 4.762918,
            lat: 6.11291695
        },
        {
            lng: 4.762918,
            lat: 6.11319494
        },
        {
            lng: 4.76180506,
            lat: 6.11319494
        },
        {
            lng: 4.76180506,
            lat: 6.11347294
        },
        {
            lng: 4.76152706,
            lat: 6.11347294
        },
        {
            lng: 4.76152706,
            lat: 6.11402893
        },
        {
            lng: 4.76097107,
            lat: 6.11402893
        },
        {
            lng: 4.76097107,
            lat: 6.11430502
        },
        {
            lng: 4.76069498,
            lat: 6.11430502
        },
        {
            lng: 4.76069498,
            lat: 6.11458302
        },
        {
            lng: 4.76041698,
            lat: 6.11458302
        },
        {
            lng: 4.76041698,
            lat: 6.11486101
        },
        {
            lng: 4.76013899,
            lat: 6.11486101
        },
        {
            lng: 4.76013899,
            lat: 6.11513901
        },
        {
            lng: 4.75986099,
            lat: 6.11513901
        },
        {
            lng: 4.75986099,
            lat: 6.115417
        },
        {
            lng: 4.759027,
            lat: 6.115417
        },
        {
            lng: 4.759027,
            lat: 6.115695
        },
        {
            lng: 4.75874901,
            lat: 6.115695
        },
        {
            lng: 4.75874901,
            lat: 6.11652708
        },
        {
            lng: 4.75819492,
            lat: 6.11652899
        },
        {
            lng: 4.75819492,
            lat: 6.11680508
        },
        {
            lng: 4.75791693,
            lat: 6.11680508
        },
        {
            lng: 4.75791693,
            lat: 6.11708307
        },
        {
            lng: 4.75708294,
            lat: 6.11708307
        },
        {
            lng: 4.75708294,
            lat: 6.11763906
        },
        {
            lng: 4.75680494,
            lat: 6.11763906
        },
        {
            lng: 4.75680494,
            lat: 6.11791706
        },
        {
            lng: 4.75624895,
            lat: 6.11791706
        },
        {
            lng: 4.75624895,
            lat: 6.11819506
        },
        {
            lng: 4.75597286,
            lat: 6.11819506
        },
        {
            lng: 4.75597286,
            lat: 6.11875105
        },
        {
            lng: 4.75541687,
            lat: 6.11875105
        },
        {
            lng: 4.75541687,
            lat: 6.11986113
        },
        {
            lng: 4.75569487,
            lat: 6.11986113
        },
        {
            lng: 4.75569487,
            lat: 6.12069511
        },
        {
            lng: 4.75541687,
            lat: 6.12069511
        },
        {
            lng: 4.75541687,
            lat: 6.1209712
        },
        {
            lng: 4.7540288,
            lat: 6.1209712
        },
        {
            lng: 4.7540288,
            lat: 6.1212492
        },
        {
            lng: 4.75347281,
            lat: 6.1212492
        },
        {
            lng: 4.75347281,
            lat: 6.12152719
        },
        {
            lng: 4.75319481,
            lat: 6.12152719
        },
        {
            lng: 4.75319481,
            lat: 6.12180519
        },
        {
            lng: 4.75291681,
            lat: 6.12180519
        },
        {
            lng: 4.75291681,
            lat: 6.12208319
        },
        {
            lng: 4.75236082,
            lat: 6.12208319
        },
        {
            lng: 4.75236082,
            lat: 6.12236118
        },
        {
            lng: 4.75208282,
            lat: 6.12236118
        },
        {
            lng: 4.75208282,
            lat: 6.12208319
        },
        {
            lng: 4.75180721,
            lat: 6.12208319
        },
        {
            lng: 4.75180721,
            lat: 6.12236118
        },
        {
            lng: 4.75152922,
            lat: 6.12236118
        },
        {
            lng: 4.75152922,
            lat: 6.12263918
        },
        {
            lng: 4.75125122,
            lat: 6.12263918
        },
        {
            lng: 4.75125122,
            lat: 6.12291718
        },
        {
            lng: 4.75069523,
            lat: 6.12291718
        },
        {
            lng: 4.75069523,
            lat: 6.12319279
        },
        {
            lng: 4.7504158,
            lat: 6.12319613
        },
        {
            lng: 4.7504158,
            lat: 6.12347078
        },
        {
            lng: 4.75013781,
            lat: 6.12347078
        },
        {
            lng: 4.75013781,
            lat: 6.12374878
        },
        {
            lng: 4.74985981,
            lat: 6.12374878
        },
        {
            lng: 4.74985981,
            lat: 6.12402678
        },
        {
            lng: 4.74958181,
            lat: 6.12402678
        },
        {
            lng: 4.74958181,
            lat: 6.12430477
        },
        {
            lng: 4.74930716,
            lat: 6.12430477
        },
        {
            lng: 4.74930716,
            lat: 6.1245842
        },
        {
            lng: 4.74875021,
            lat: 6.1245842
        },
        {
            lng: 4.74875021,
            lat: 6.12486219
        },
        {
            lng: 4.74819422,
            lat: 6.12486219
        },
        {
            lng: 4.74819422,
            lat: 6.12514019
        },
        {
            lng: 4.74763823,
            lat: 6.12514019
        },
        {
            lng: 4.74763823,
            lat: 6.12541819
        },
        {
            lng: 4.74708223,
            lat: 6.12541819
        },
        {
            lng: 4.74708223,
            lat: 6.12569284
        },
        {
            lng: 4.74680614,
            lat: 6.12569284
        },
        {
            lng: 4.74680614,
            lat: 6.12597179
        },
        {
            lng: 4.74652815,
            lat: 6.12597179
        },
        {
            lng: 4.74652815,
            lat: 6.12624979
        },
        {
            lng: 4.74625015,
            lat: 6.12624979
        },
        {
            lng: 4.74625015,
            lat: 6.12652779
        },
        {
            lng: 4.74597216,
            lat: 6.12652779
        },
        {
            lng: 4.74597216,
            lat: 6.12708378
        },
        {
            lng: 4.74569416,
            lat: 6.12708378
        },
        {
            lng: 4.74569416,
            lat: 6.12736177
        },
        {
            lng: 4.74541616,
            lat: 6.12736177
        },
        {
            lng: 4.74541616,
            lat: 6.12763977
        },
        {
            lng: 4.74486208,
            lat: 6.12763977
        },
        {
            lng: 4.74486208,
            lat: 6.12791777
        },
        {
            lng: 4.74458408,
            lat: 6.12791777
        },
        {
            lng: 4.74458408,
            lat: 6.12819386
        },
        {
            lng: 4.74430609,
            lat: 6.12819386
        },
        {
            lng: 4.74430609,
            lat: 6.12847185
        },
        {
            lng: 4.7437501,
            lat: 6.12847185
        },
        {
            lng: 4.7437501,
            lat: 6.12874985
        },
        {
            lng: 4.7434721,
            lat: 6.12874985
        },
        {
            lng: 4.7434721,
            lat: 6.12902784
        },
        {
            lng: 4.74291611,
            lat: 6.12902784
        },
        {
            lng: 4.74291611,
            lat: 6.12930584
        },
        {
            lng: 4.74264002,
            lat: 6.12930584
        },
        {
            lng: 4.74264002,
            lat: 6.12958384
        },
        {
            lng: 4.74236202,
            lat: 6.12958384
        },
        {
            lng: 4.74236202,
            lat: 6.12986183
        },
        {
            lng: 4.74208403,
            lat: 6.12986183
        },
        {
            lng: 4.74208403,
            lat: 6.13013792
        },
        {
            lng: 4.74180603,
            lat: 6.13013792
        },
        {
            lng: 4.74180603,
            lat: 6.13041592
        },
        {
            lng: 4.74152803,
            lat: 6.13041592
        },
        {
            lng: 4.74152803,
            lat: 6.1315279
        },
        {
            lng: 4.74125004,
            lat: 6.1315279
        },
        {
            lng: 4.74125004,
            lat: 6.1318059
        },
        {
            lng: 4.74097204,
            lat: 6.1318059
        },
        {
            lng: 4.74097204,
            lat: 6.1315279
        },
        {
            lng: 4.73986197,
            lat: 6.1315279
        },
        {
            lng: 4.73986197,
            lat: 6.1318059
        },
        {
            lng: 4.73930597,
            lat: 6.1318059
        },
        {
            lng: 4.73930597,
            lat: 6.13208389
        },
        {
            lng: 4.73902798,
            lat: 6.13208389
        },
        {
            lng: 4.73902798,
            lat: 6.13235998
        },
        {
            lng: 4.73847198,
            lat: 6.13235998
        },
        {
            lng: 4.73847198,
            lat: 6.13263798
        },
        {
            lng: 4.73819304,
            lat: 6.13263798
        },
        {
            lng: 4.73819304,
            lat: 6.13319397
        },
        {
            lng: 4.73791504,
            lat: 6.13319397
        },
        {
            lng: 4.73791504,
            lat: 6.13513803
        },
        {
            lng: 4.7376399,
            lat: 6.13513803
        },
        {
            lng: 4.7376399,
            lat: 6.13541603
        },
        {
            lng: 4.73736191,
            lat: 6.13541603
        },
        {
            lng: 4.73736191,
            lat: 6.13569403
        },
        {
            lng: 4.73708391,
            lat: 6.13569403
        },
        {
            lng: 4.73708391,
            lat: 6.13541603
        },
        {
            lng: 4.73680496,
            lat: 6.13541603
        },
        {
            lng: 4.73680496,
            lat: 6.13569403
        },
        {
            lng: 4.73652697,
            lat: 6.13569403
        },
        {
            lng: 4.73652697,
            lat: 6.13597202
        },
        {
            lng: 4.73569584,
            lat: 6.13597202
        },
        {
            lng: 4.73569584,
            lat: 6.13625002
        },
        {
            lng: 4.73541689,
            lat: 6.13625002
        },
        {
            lng: 4.73541689,
            lat: 6.13652802
        },
        {
            lng: 4.73513889,
            lat: 6.13652802
        },
        {
            lng: 4.73513889,
            lat: 6.13680696
        },
        {
            lng: 4.7345829,
            lat: 6.13680696
        },
        {
            lng: 4.7345829,
            lat: 6.13708496
        },
        {
            lng: 4.73402691,
            lat: 6.13708496
        },
        {
            lng: 4.73402691,
            lat: 6.1373601
        },
        {
            lng: 4.73347282,
            lat: 6.1373601
        },
        {
            lng: 4.73347282,
            lat: 6.13763809
        },
        {
            lng: 4.73291683,
            lat: 6.13763809
        },
        {
            lng: 4.73291683,
            lat: 6.13819504
        },
        {
            lng: 4.73263884,
            lat: 6.13819504
        },
        {
            lng: 4.73263884,
            lat: 6.13847303
        },
        {
            lng: 4.73236084,
            lat: 6.13847303
        },
        {
            lng: 4.73236084,
            lat: 6.13875103
        },
        {
            lng: 4.73208284,
            lat: 6.13875103
        },
        {
            lng: 4.73208284,
            lat: 6.13930416
        },
        {
            lng: 4.73097277,
            lat: 6.13930416
        },
        {
            lng: 4.73097277,
            lat: 6.13958311
        },
        {
            lng: 4.73041677,
            lat: 6.13958311
        },
        {
            lng: 4.73041677,
            lat: 6.13986111
        },
        {
            lng: 4.72958279,
            lat: 6.13986111
        },
        {
            lng: 4.72958279,
            lat: 6.1401391
        },
        {
            lng: 4.72902679,
            lat: 6.1401391
        },
        {
            lng: 4.72902679,
            lat: 6.1404171
        },
        {
            lng: 4.72847319,
            lat: 6.1404171
        },
        {
            lng: 4.72847319,
            lat: 6.1406951
        },
        {
            lng: 4.72819519,
            lat: 6.1406951
        },
        {
            lng: 4.72819519,
            lat: 6.14097309
        },
        {
            lng: 4.72791719,
            lat: 6.14097309
        },
        {
            lng: 4.72791719,
            lat: 6.14125109
        },
        {
            lng: 4.7276392,
            lat: 6.14125109
        },
        {
            lng: 4.7276392,
            lat: 6.14152718
        },
        {
            lng: 4.7273612,
            lat: 6.14152718
        },
        {
            lng: 4.7273612,
            lat: 6.14180517
        },
        {
            lng: 4.72708321,
            lat: 6.14180517
        },
        {
            lng: 4.72708321,
            lat: 6.14208317
        },
        {
            lng: 4.72680521,
            lat: 6.14208317
        },
        {
            lng: 4.72680521,
            lat: 6.14236116
        },
        {
            lng: 4.72652721,
            lat: 6.14236116
        },
        {
            lng: 4.72652721,
            lat: 6.14263916
        },
        {
            lng: 4.72625113,
            lat: 6.14263916
        },
        {
            lng: 4.72625113,
            lat: 6.14291716
        },
        {
            lng: 4.72597313,
            lat: 6.14291716
        },
        {
            lng: 4.72597313,
            lat: 6.14319515
        },
        {
            lng: 4.72569513,
            lat: 6.14319515
        },
        {
            lng: 4.72569513,
            lat: 6.14347315
        },
        {
            lng: 4.72541714,
            lat: 6.14347315
        },
        {
            lng: 4.72541714,
            lat: 6.14375114
        },
        {
            lng: 4.72486115,
            lat: 6.14375114
        },
        {
            lng: 4.72486115,
            lat: 6.14402723
        },
        {
            lng: 4.7245822,
            lat: 6.14402723
        },
        {
            lng: 4.7245822,
            lat: 6.14430523
        },
        {
            lng: 4.72402906,
            lat: 6.14430523
        },
        {
            lng: 4.72402906,
            lat: 6.14458323
        },
        {
            lng: 4.72375107,
            lat: 6.14458323
        },
        {
            lng: 4.72375107,
            lat: 6.14486122
        },
        {
            lng: 4.72347307,
            lat: 6.14486122
        },
        {
            lng: 4.72347307,
            lat: 6.14513922
        },
        {
            lng: 4.72291613,
            lat: 6.14513922
        },
        {
            lng: 4.72291613,
            lat: 6.14541721
        },
        {
            lng: 4.722085,
            lat: 6.14541721
        },
        {
            lng: 4.722085,
            lat: 6.14569521
        },
        {
            lng: 4.72180605,
            lat: 6.14569521
        },
        {
            lng: 4.72180605,
            lat: 6.14597321
        },
        {
            lng: 4.72152805,
            lat: 6.14597321
        },
        {
            lng: 4.72152805,
            lat: 6.14624882
        },
        {
            lng: 4.72125006,
            lat: 6.1462512
        },
        {
            lng: 4.72125006,
            lat: 6.14652681
        },
        {
            lng: 4.72069407,
            lat: 6.14652681
        },
        {
            lng: 4.72069407,
            lat: 6.14680481
        },
        {
            lng: 4.72013807,
            lat: 6.14680481
        },
        {
            lng: 4.72013807,
            lat: 6.1473608
        },
        {
            lng: 4.71958399,
            lat: 6.1473608
        },
        {
            lng: 4.71958399,
            lat: 6.1476388
        },
        {
            lng: 4.71930599,
            lat: 6.1476388
        },
        {
            lng: 4.71930599,
            lat: 6.14791679
        },
        {
            lng: 4.719028,
            lat: 6.14791679
        },
        {
            lng: 4.719028,
            lat: 6.14819479
        },
        {
            lng: 4.71875,
            lat: 6.14819479
        },
        {
            lng: 4.71875,
            lat: 6.14847279
        },
        {
            lng: 4.718472,
            lat: 6.14847279
        },
        {
            lng: 4.718472,
            lat: 6.14874887
        },
        {
            lng: 4.71791601,
            lat: 6.14874887
        },
        {
            lng: 4.71791601,
            lat: 6.14902687
        },
        {
            lng: 4.71736002,
            lat: 6.14902687
        },
        {
            lng: 4.71736002,
            lat: 6.14930487
        },
        {
            lng: 4.71708393,
            lat: 6.14930487
        },
        {
            lng: 4.71708393,
            lat: 6.14986086
        },
        {
            lng: 4.71680593,
            lat: 6.14986086
        },
        {
            lng: 4.71680593,
            lat: 6.15013885
        },
        {
            lng: 4.71652794,
            lat: 6.15013885
        },
        {
            lng: 4.71652794,
            lat: 6.1504178
        },
        {
            lng: 4.71597195,
            lat: 6.1504178
        },
        {
            lng: 4.71597195,
            lat: 6.15069294
        },
        {
            lng: 4.71513987,
            lat: 6.15069294
        },
        {
            lng: 4.71513987,
            lat: 6.15097094
        },
        {
            lng: 4.71486187,
            lat: 6.15097094
        },
        {
            lng: 4.71486187,
            lat: 6.15124893
        },
        {
            lng: 4.71458387,
            lat: 6.15124893
        },
        {
            lng: 4.71458387,
            lat: 6.15152693
        },
        {
            lng: 4.71402788,
            lat: 6.15152693
        },
        {
            lng: 4.71402788,
            lat: 6.15180588
        },
        {
            lng: 4.71374989,
            lat: 6.15180588
        },
        {
            lng: 4.71374989,
            lat: 6.15208387
        },
        {
            lng: 4.71319389,
            lat: 6.15208387
        },
        {
            lng: 4.71319389,
            lat: 6.15236187
        },
        {
            lng: 4.71236181,
            lat: 6.15236187
        },
        {
            lng: 4.71236181,
            lat: 6.15263987
        },
        {
            lng: 4.71208382,
            lat: 6.15263987
        },
        {
            lng: 4.71208382,
            lat: 6.152915
        },
        {
            lng: 4.71124983,
            lat: 6.15291786
        },
        {
            lng: 4.71124983,
            lat: 6.15347195
        },
        {
            lng: 4.71069288,
            lat: 6.15347195
        },
        {
            lng: 4.71069288,
            lat: 6.15402794
        },
        {
            lng: 4.71041822,
            lat: 6.15402794
        },
        {
            lng: 4.71041822,
            lat: 6.15430593
        },
        {
            lng: 4.71014023,
            lat: 6.15430593
        },
        {
            lng: 4.71014023,
            lat: 6.15458393
        },
        {
            lng: 4.70986223,
            lat: 6.15458393
        },
        {
            lng: 4.70986223,
            lat: 6.15486193
        },
        {
            lng: 4.70958281,
            lat: 6.15486193
        },
        {
            lng: 4.70958281,
            lat: 6.15513992
        },
        {
            lng: 4.70902681,
            lat: 6.15513992
        },
        {
            lng: 4.70902681,
            lat: 6.15541601
        },
        {
            lng: 4.70874882,
            lat: 6.15541601
        },
        {
            lng: 4.70874882,
            lat: 6.15569401
        },
        {
            lng: 4.70819283,
            lat: 6.15569401
        },
        {
            lng: 4.70819283,
            lat: 6.155972
        },
        {
            lng: 4.70791721,
            lat: 6.155972
        },
        {
            lng: 4.70791721,
            lat: 6.15625
        },
        {
            lng: 4.70763922,
            lat: 6.15625
        },
        {
            lng: 4.70763922,
            lat: 6.156528
        },
        {
            lng: 4.70708323,
            lat: 6.156528
        },
        {
            lng: 4.70708323,
            lat: 6.15680599
        },
        {
            lng: 4.70680523,
            lat: 6.15680599
        },
        {
            lng: 4.70680523,
            lat: 6.15736198
        },
        {
            lng: 4.70652723,
            lat: 6.15736198
        },
        {
            lng: 4.70652723,
            lat: 6.15763998
        },
        {
            lng: 4.70624924,
            lat: 6.15763998
        },
        {
            lng: 4.70624924,
            lat: 6.15791607
        },
        {
            lng: 4.70597315,
            lat: 6.15791607
        },
        {
            lng: 4.70597315,
            lat: 6.15819407
        },
        {
            lng: 4.70569515,
            lat: 6.15819407
        },
        {
            lng: 4.70569515,
            lat: 6.15847206
        },
        {
            lng: 4.70541716,
            lat: 6.15847206
        },
        {
            lng: 4.70541716,
            lat: 6.15875006
        },
        {
            lng: 4.70513916,
            lat: 6.15875006
        },
        {
            lng: 4.70513916,
            lat: 6.15930605
        },
        {
            lng: 4.70486116,
            lat: 6.15930605
        },
        {
            lng: 4.70486116,
            lat: 6.16013813
        },
        {
            lng: 4.70458317,
            lat: 6.16013813
        },
        {
            lng: 4.70458317,
            lat: 6.16069412
        },
        {
            lng: 4.70430517,
            lat: 6.16069412
        },
        {
            lng: 4.70430517,
            lat: 6.16097212
        },
        {
            lng: 4.70402718,
            lat: 6.16097212
        },
        {
            lng: 4.70402718,
            lat: 6.16125011
        },
        {
            lng: 4.70375109,
            lat: 6.16125011
        },
        {
            lng: 4.70375109,
            lat: 6.16152811
        },
        {
            lng: 4.70347309,
            lat: 6.16152811
        },
        {
            lng: 4.70347309,
            lat: 6.16180611
        },
        {
            lng: 4.7031951,
            lat: 6.16180611
        },
        {
            lng: 4.7031951,
            lat: 6.1620822
        },
        {
            lng: 4.7026391,
            lat: 6.1620822
        },
        {
            lng: 4.7026391,
            lat: 6.16236019
        },
        {
            lng: 4.70236111,
            lat: 6.16236019
        },
        {
            lng: 4.70236111,
            lat: 6.16263819
        },
        {
            lng: 4.70180511,
            lat: 6.16263819
        },
        {
            lng: 4.70180511,
            lat: 6.16291618
        },
        {
            lng: 4.70152712,
            lat: 6.16291618
        },
        {
            lng: 4.70152712,
            lat: 6.16319418
        },
        {
            lng: 4.70125103,
            lat: 6.16319418
        },
        {
            lng: 4.70125103,
            lat: 6.16375017
        },
        {
            lng: 4.70097303,
            lat: 6.16375017
        },
        {
            lng: 4.70097303,
            lat: 6.16402912
        },
        {
            lng: 4.70069504,
            lat: 6.16402912
        },
        {
            lng: 4.70069504,
            lat: 6.16430712
        },
        {
            lng: 4.70041704,
            lat: 6.16430712
        },
        {
            lng: 4.70041704,
            lat: 6.16458178
        },
        {
            lng: 4.70013905,
            lat: 6.16458178
        },
        {
            lng: 4.70013905,
            lat: 6.16485977
        },
        {
            lng: 4.69958305,
            lat: 6.16485977
        },
        {
            lng: 4.69958305,
            lat: 6.16513777
        },
        {
            lng: 4.69902897,
            lat: 6.16513777
        },
        {
            lng: 4.69902897,
            lat: 6.16541719
        },
        {
            lng: 4.69847298,
            lat: 6.16541719
        },
        {
            lng: 4.69847298,
            lat: 6.16569519
        },
        {
            lng: 4.69791698,
            lat: 6.16569519
        },
        {
            lng: 4.69791698,
            lat: 6.16597319
        },
        {
            lng: 4.69763899,
            lat: 6.16597319
        },
        {
            lng: 4.69763899,
            lat: 6.16652918
        },
        {
            lng: 4.69736004,
            lat: 6.16652918
        },
        {
            lng: 4.69736004,
            lat: 6.16680717
        },
        {
            lng: 4.69708204,
            lat: 6.16680717
        },
        {
            lng: 4.69708204,
            lat: 6.16708279
        },
        {
            lng: 4.69680405,
            lat: 6.16708279
        },
        {
            lng: 4.69680405,
            lat: 6.16736078
        },
        {
            lng: 4.69625092,
            lat: 6.16736078
        },
        {
            lng: 4.69625092,
            lat: 6.16763878
        },
        {
            lng: 4.69569397,
            lat: 6.16763878
        },
        {
            lng: 4.69569397,
            lat: 6.16819477
        },
        {
            lng: 4.69541597,
            lat: 6.16819477
        },
        {
            lng: 4.69541597,
            lat: 6.16847277
        },
        {
            lng: 4.69513798,
            lat: 6.16847277
        },
        {
            lng: 4.69513798,
            lat: 6.16875076
        },
        {
            lng: 4.69458389,
            lat: 6.16875076
        },
        {
            lng: 4.69458389,
            lat: 6.16902685
        },
        {
            lng: 4.6943059,
            lat: 6.16902685
        },
        {
            lng: 4.6943059,
            lat: 6.17013884
        },
        {
            lng: 4.6940279,
            lat: 6.17013884
        },
        {
            lng: 4.6940279,
            lat: 6.17069483
        },
        {
            lng: 4.6937499,
            lat: 6.17069483
        },
        {
            lng: 4.6937499,
            lat: 6.17097282
        },
        {
            lng: 4.69347191,
            lat: 6.17097282
        },
        {
            lng: 4.69347191,
            lat: 6.17069483
        },
        {
            lng: 4.69263792,
            lat: 6.17069483
        },
        {
            lng: 4.69263792,
            lat: 6.17041683
        },
        {
            lng: 4.69152784,
            lat: 6.17041683
        },
        {
            lng: 4.69152784,
            lat: 6.17097282
        },
        {
            lng: 4.69124985,
            lat: 6.17097282
        },
        {
            lng: 4.69124985,
            lat: 6.17124891
        },
        {
            lng: 4.69069386,
            lat: 6.17124891
        },
        {
            lng: 4.69069386,
            lat: 6.17152691
        },
        {
            lng: 4.69041586,
            lat: 6.17152691
        },
        {
            lng: 4.69041586,
            lat: 6.1720829
        },
        {
            lng: 4.69013786,
            lat: 6.1720829
        },
        {
            lng: 4.69013786,
            lat: 6.1723609
        },
        {
            lng: 4.68986177,
            lat: 6.1723609
        },
        {
            lng: 4.68986177,
            lat: 6.17263889
        },
        {
            lng: 4.68958378,
            lat: 6.17263889
        },
        {
            lng: 4.68958378,
            lat: 6.17291689
        },
        {
            lng: 4.68930578,
            lat: 6.17291689
        },
        {
            lng: 4.68930578,
            lat: 6.17319489
        },
        {
            lng: 4.68902779,
            lat: 6.17319489
        },
        {
            lng: 4.68902779,
            lat: 6.17347288
        },
        {
            lng: 4.68874979,
            lat: 6.17347288
        },
        {
            lng: 4.68874979,
            lat: 6.17374897
        },
        {
            lng: 4.6879158,
            lat: 6.17374897
        },
        {
            lng: 4.6879158,
            lat: 6.17402697
        },
        {
            lng: 4.68736219,
            lat: 6.17402697
        },
        {
            lng: 4.68736219,
            lat: 6.17430496
        },
        {
            lng: 4.6870842,
            lat: 6.17430496
        },
        {
            lng: 4.6870842,
            lat: 6.17458296
        },
        {
            lng: 4.6868062,
            lat: 6.17458296
        },
        {
            lng: 4.6868062,
            lat: 6.17486095
        },
        {
            lng: 4.68625021,
            lat: 6.17486095
        },
        {
            lng: 4.68625021,
            lat: 6.17513895
        },
        {
            lng: 4.68597221,
            lat: 6.17513895
        },
        {
            lng: 4.68597221,
            lat: 6.17541695
        },
        {
            lng: 4.68541813,
            lat: 6.17541695
        },
        {
            lng: 4.68541813,
            lat: 6.17569494
        },
        {
            lng: 4.68514013,
            lat: 6.17569494
        },
        {
            lng: 4.68514013,
            lat: 6.17597103
        },
        {
            lng: 4.68486214,
            lat: 6.17597294
        },
        {
            lng: 4.68486214,
            lat: 6.17624903
        },
        {
            lng: 4.68458414,
            lat: 6.17624903
        },
        {
            lng: 4.68458414,
            lat: 6.17652702
        },
        {
            lng: 4.68430614,
            lat: 6.17652702
        },
        {
            lng: 4.68430614,
            lat: 6.17680502
        },
        {
            lng: 4.6837492,
            lat: 6.17680502
        },
        {
            lng: 4.6837492,
            lat: 6.17708302
        },
        {
            lng: 4.68319607,
            lat: 6.17708302
        },
        {
            lng: 4.68319607,
            lat: 6.17736101
        },
        {
            lng: 4.68291807,
            lat: 6.17736101
        },
        {
            lng: 4.68291807,
            lat: 6.17791796
        },
        {
            lng: 4.68264008,
            lat: 6.17791796
        },
        {
            lng: 4.68264008,
            lat: 6.17819595
        },
        {
            lng: 4.68236113,
            lat: 6.17819595
        },
        {
            lng: 4.68236113,
            lat: 6.17847109
        },
        {
            lng: 4.68180513,
            lat: 6.17847109
        },
        {
            lng: 4.68180513,
            lat: 6.17874908
        },
        {
            lng: 4.68124914,
            lat: 6.17874908
        },
        {
            lng: 4.68124914,
            lat: 6.17902803
        },
        {
            lng: 4.68097115,
            lat: 6.17902803
        },
        {
            lng: 4.68097115,
            lat: 6.17930603
        },
        {
            lng: 4.68041706,
            lat: 6.17930603
        },
        {
            lng: 4.68041706,
            lat: 6.17958403
        },
        {
            lng: 4.68013906,
            lat: 6.17958403
        },
        {
            lng: 4.68013906,
            lat: 6.17986202
        },
        {
            lng: 4.67986107,
            lat: 6.17986202
        },
        {
            lng: 4.67986107,
            lat: 6.18014002
        },
        {
            lng: 4.67958307,
            lat: 6.18014002
        },
        {
            lng: 4.67958307,
            lat: 6.18041611
        },
        {
            lng: 4.67930508,
            lat: 6.18041611
        },
        {
            lng: 4.67930508,
            lat: 6.1806941
        },
        {
            lng: 4.67902708,
            lat: 6.1806941
        },
        {
            lng: 4.67902708,
            lat: 6.1809721
        },
        {
            lng: 4.67874908,
            lat: 6.1809721
        },
        {
            lng: 4.67874908,
            lat: 6.1812501
        },
        {
            lng: 4.67847109,
            lat: 6.1812501
        },
        {
            lng: 4.67847109,
            lat: 6.18152809
        },
        {
            lng: 4.678195,
            lat: 6.18152809
        },
        {
            lng: 4.678195,
            lat: 6.18180609
        },
        {
            lng: 4.677917,
            lat: 6.18180609
        },
        {
            lng: 4.677917,
            lat: 6.18236208
        },
        {
            lng: 4.67763901,
            lat: 6.18236208
        },
        {
            lng: 4.67763901,
            lat: 6.18264008
        },
        {
            lng: 4.67736101,
            lat: 6.18264008
        },
        {
            lng: 4.67736101,
            lat: 6.18291616
        },
        {
            lng: 4.67708302,
            lat: 6.18291616
        },
        {
            lng: 4.67708302,
            lat: 6.18319416
        },
        {
            lng: 4.67680502,
            lat: 6.18319416
        },
        {
            lng: 4.67680502,
            lat: 6.18347216
        },
        {
            lng: 4.67652702,
            lat: 6.18347216
        },
        {
            lng: 4.67652702,
            lat: 6.18375015
        },
        {
            lng: 4.67624903,
            lat: 6.18375015
        },
        {
            lng: 4.67624903,
            lat: 6.18430614
        },
        {
            lng: 4.67597294,
            lat: 6.18430614
        },
        {
            lng: 4.67597294,
            lat: 6.18486214
        },
        {
            lng: 4.67569494,
            lat: 6.18486214
        },
        {
            lng: 4.67569494,
            lat: 6.18513823
        },
        {
            lng: 4.67541695,
            lat: 6.18513823
        },
        {
            lng: 4.67541695,
            lat: 6.18541622
        },
        {
            lng: 4.67458296,
            lat: 6.18541622
        },
        {
            lng: 4.67458296,
            lat: 6.18569422
        },
        {
            lng: 4.67430496,
            lat: 6.18569422
        },
        {
            lng: 4.67430496,
            lat: 6.18597221
        },
        {
            lng: 4.67402887,
            lat: 6.18597221
        },
        {
            lng: 4.67402887,
            lat: 6.18625021
        },
        {
            lng: 4.67347288,
            lat: 6.18625021
        },
        {
            lng: 4.67347288,
            lat: 6.18652821
        },
        {
            lng: 4.67319489,
            lat: 6.18652821
        },
        {
            lng: 4.67319489,
            lat: 6.1868062
        },
        {
            lng: 4.67263889,
            lat: 6.1868062
        },
        {
            lng: 4.67263889,
            lat: 6.1870842
        },
        {
            lng: 4.6723609,
            lat: 6.1870842
        },
        {
            lng: 4.6723609,
            lat: 6.18736219
        },
        {
            lng: 4.67152882,
            lat: 6.18736219
        },
        {
            lng: 4.67152882,
            lat: 6.18763781
        },
        {
            lng: 4.67125082,
            lat: 6.18763781
        },
        {
            lng: 4.67125082,
            lat: 6.1879158
        },
        {
            lng: 4.67097282,
            lat: 6.1879158
        },
        {
            lng: 4.67097282,
            lat: 6.1881938
        },
        {
            lng: 4.67069483,
            lat: 6.1881938
        },
        {
            lng: 4.67069483,
            lat: 6.18874979
        },
        {
            lng: 4.67013788,
            lat: 6.18874979
        },
        {
            lng: 4.67013788,
            lat: 6.18902779
        },
        {
            lng: 4.66985989,
            lat: 6.18902779
        },
        {
            lng: 4.66985989,
            lat: 6.18930578
        },
        {
            lng: 4.66958189,
            lat: 6.18930578
        },
        {
            lng: 4.66958189,
            lat: 6.18985987
        },
        {
            lng: 4.66930723,
            lat: 6.18985987
        },
        {
            lng: 4.66930389,
            lat: 6.19013786
        },
        {
            lng: 4.66874981,
            lat: 6.19013786
        },
        {
            lng: 4.66874981,
            lat: 6.19041586
        },
        {
            lng: 4.66847181,
            lat: 6.19041586
        },
        {
            lng: 4.66847181,
            lat: 6.19097185
        },
        {
            lng: 4.66819382,
            lat: 6.19097185
        },
        {
            lng: 4.66819382,
            lat: 6.1912508
        },
        {
            lng: 4.66763783,
            lat: 6.1912508
        },
        {
            lng: 4.66763783,
            lat: 6.1915288
        },
        {
            lng: 4.66735983,
            lat: 6.1915288
        },
        {
            lng: 4.66735983,
            lat: 6.19180393
        },
        {
            lng: 4.66708183,
            lat: 6.19180393
        },
        {
            lng: 4.66708183,
            lat: 6.19208193
        },
        {
            lng: 4.66680622,
            lat: 6.19208193
        },
        {
            lng: 4.66680622,
            lat: 6.19235992
        },
        {
            lng: 4.66625023,
            lat: 6.19235992
        },
        {
            lng: 4.66625023,
            lat: 6.19263887
        },
        {
            lng: 4.66569424,
            lat: 6.19263887
        },
        {
            lng: 4.66569424,
            lat: 6.19291687
        },
        {
            lng: 4.66513777,
            lat: 6.19291687
        },
        {
            lng: 4.66513777,
            lat: 6.19319487
        },
        {
            lng: 4.66458416,
            lat: 6.19319487
        },
        {
            lng: 4.66458416,
            lat: 6.19347286
        },
        {
            lng: 4.66402817,
            lat: 6.19347286
        },
        {
            lng: 4.66402817,
            lat: 6.19375086
        },
        {
            lng: 4.66375017,
            lat: 6.19375086
        },
        {
            lng: 4.66375017,
            lat: 6.19402885
        },
        {
            lng: 4.66347218,
            lat: 6.19402885
        },
        {
            lng: 4.66347218,
            lat: 6.19430494
        },
        {
            lng: 4.66319418,
            lat: 6.19430494
        },
        {
            lng: 4.66319418,
            lat: 6.19458294
        },
        {
            lng: 4.66291618,
            lat: 6.19458294
        },
        {
            lng: 4.66291618,
            lat: 6.19486094
        },
        {
            lng: 4.66264009,
            lat: 6.19486094
        },
        {
            lng: 4.66263819,
            lat: 6.19513893
        },
        {
            lng: 4.6620841,
            lat: 6.19513893
        },
        {
            lng: 4.6620841,
            lat: 6.19541693
        },
        {
            lng: 4.66180611,
            lat: 6.19541693
        },
        {
            lng: 4.66180611,
            lat: 6.19569492
        },
        {
            lng: 4.66152811,
            lat: 6.19569492
        },
        {
            lng: 4.66152811,
            lat: 6.19597292
        },
        {
            lng: 4.66125011,
            lat: 6.19597292
        },
        {
            lng: 4.66125011,
            lat: 6.19625092
        },
        {
            lng: 4.66097212,
            lat: 6.19625092
        },
        {
            lng: 4.66097212,
            lat: 6.19652891
        },
        {
            lng: 4.66069412,
            lat: 6.19652891
        },
        {
            lng: 4.66069412,
            lat: 6.196805
        },
        {
            lng: 4.66041613,
            lat: 6.196805
        },
        {
            lng: 4.66041613,
            lat: 6.197083
        },
        {
            lng: 4.66014004,
            lat: 6.197083
        },
        {
            lng: 4.66014004,
            lat: 6.19736099
        },
        {
            lng: 4.65986204,
            lat: 6.19736099
        },
        {
            lng: 4.65986204,
            lat: 6.19763899
        },
        {
            lng: 4.65958405,
            lat: 6.19763899
        },
        {
            lng: 4.65958405,
            lat: 6.19791698
        },
        {
            lng: 4.65930605,
            lat: 6.19791698
        },
        {
            lng: 4.65930605,
            lat: 6.19847298
        },
        {
            lng: 4.65902805,
            lat: 6.19847298
        },
        {
            lng: 4.65902805,
            lat: 6.19874907
        },
        {
            lng: 4.65875006,
            lat: 6.19874907
        },
        {
            lng: 4.65875006,
            lat: 6.19902706
        },
        {
            lng: 4.65847206,
            lat: 6.19902706
        },
        {
            lng: 4.65847206,
            lat: 6.19958305
        },
        {
            lng: 4.65819407,
            lat: 6.19958305
        },
        {
            lng: 4.65819407,
            lat: 6.19986105
        },
        {
            lng: 4.65763998,
            lat: 6.19986105
        },
        {
            lng: 4.65763998,
            lat: 6.20013905
        },
        {
            lng: 4.65708399,
            lat: 6.20013905
        },
        {
            lng: 4.65708399,
            lat: 6.20041704
        },
        {
            lng: 4.65680599,
            lat: 6.20041704
        },
        {
            lng: 4.65680599,
            lat: 6.20069504
        },
        {
            lng: 4.65652704,
            lat: 6.20069504
        },
        {
            lng: 4.65652704,
            lat: 6.20097113
        },
        {
            lng: 4.65624905,
            lat: 6.20097113
        },
        {
            lng: 4.65624905,
            lat: 6.20124912
        },
        {
            lng: 4.65597105,
            lat: 6.20124912
        },
        {
            lng: 4.65597105,
            lat: 6.20152712
        },
        {
            lng: 4.65541792,
            lat: 6.20152712
        },
        {
            lng: 4.65541792,
            lat: 6.20180511
        },
        {
            lng: 4.65513897,
            lat: 6.20180511
        },
        {
            lng: 4.65513897,
            lat: 6.20208311
        },
        {
            lng: 4.65486097,
            lat: 6.20208311
        },
        {
            lng: 4.65486097,
            lat: 6.20236111
        },
        {
            lng: 4.65458298,
            lat: 6.20236111
        },
        {
            lng: 4.65458298,
            lat: 6.2029171
        },
        {
            lng: 4.65430498,
            lat: 6.2029171
        },
        {
            lng: 4.65430498,
            lat: 6.2031951
        },
        {
            lng: 4.65402699,
            lat: 6.2031951
        },
        {
            lng: 4.65402699,
            lat: 6.20347118
        },
        {
            lng: 4.65374899,
            lat: 6.20347118
        },
        {
            lng: 4.65374899,
            lat: 6.20402718
        },
        {
            lng: 4.6531949,
            lat: 6.20402718
        },
        {
            lng: 4.6531949,
            lat: 6.20430517
        },
        {
            lng: 4.65291691,
            lat: 6.20430517
        },
        {
            lng: 4.65291691,
            lat: 6.20458317
        },
        {
            lng: 4.65236092,
            lat: 6.20458317
        },
        {
            lng: 4.65236092,
            lat: 6.20486212
        },
        {
            lng: 4.65208292,
            lat: 6.20486212
        },
        {
            lng: 4.65208292,
            lat: 6.20514011
        },
        {
            lng: 4.65180492,
            lat: 6.20514011
        },
        {
            lng: 4.65180492,
            lat: 6.20541811
        },
        {
            lng: 4.65152693,
            lat: 6.20541811
        },
        {
            lng: 4.65152693,
            lat: 6.20569611
        },
        {
            lng: 4.65124893,
            lat: 6.20569611
        },
        {
            lng: 4.65124893,
            lat: 6.20541811
        },
        {
            lng: 4.65097284,
            lat: 6.20541811
        },
        {
            lng: 4.65097284,
            lat: 6.20569611
        },
        {
            lng: 4.65041685,
            lat: 6.20569611
        },
        {
            lng: 4.65041685,
            lat: 6.20597076
        },
        {
            lng: 4.64986086,
            lat: 6.20597076
        },
        {
            lng: 4.64986086,
            lat: 6.20652819
        },
        {
            lng: 4.64930487,
            lat: 6.20652819
        },
        {
            lng: 4.64930487,
            lat: 6.20680618
        },
        {
            lng: 4.64847279,
            lat: 6.20680618
        },
        {
            lng: 4.64847279,
            lat: 6.20708418
        },
        {
            lng: 4.64791679,
            lat: 6.20708418
        },
        {
            lng: 4.64791679,
            lat: 6.20736217
        },
        {
            lng: 4.6476388,
            lat: 6.20736217
        },
        {
            lng: 4.6476388,
            lat: 6.20764017
        },
        {
            lng: 4.64708281,
            lat: 6.20764017
        },
        {
            lng: 4.64708281,
            lat: 6.20791817
        },
        {
            lng: 4.64680481,
            lat: 6.20791817
        },
        {
            lng: 4.64680481,
            lat: 6.20819378
        },
        {
            lng: 4.6462512,
            lat: 6.20819378
        },
        {
            lng: 4.6462512,
            lat: 6.20847178
        },
        {
            lng: 4.64597321,
            lat: 6.20847178
        },
        {
            lng: 4.64597321,
            lat: 6.20874977
        },
        {
            lng: 4.64569521,
            lat: 6.20874977
        },
        {
            lng: 4.64569521,
            lat: 6.20902777
        },
        {
            lng: 4.64541721,
            lat: 6.20902777
        },
        {
            lng: 4.64541721,
            lat: 6.20930576
        },
        {
            lng: 4.64486122,
            lat: 6.20930576
        },
        {
            lng: 4.64486122,
            lat: 6.20958424
        },
        {
            lng: 4.64458323,
            lat: 6.20958424
        },
        {
            lng: 4.64458323,
            lat: 6.20986223
        },
        {
            lng: 4.64430714,
            lat: 6.20986223
        },
        {
            lng: 4.64430714,
            lat: 6.21013784
        },
        {
            lng: 4.64402914,
            lat: 6.21013784
        },
        {
            lng: 4.64402914,
            lat: 6.21041584
        },
        {
            lng: 4.64347315,
            lat: 6.21041584
        },
        {
            lng: 4.64347315,
            lat: 6.21069384
        },
        {
            lng: 4.64319515,
            lat: 6.21069384
        },
        {
            lng: 4.64319515,
            lat: 6.21097183
        },
        {
            lng: 4.6429162,
            lat: 6.21097183
        },
        {
            lng: 4.6429162,
            lat: 6.21124983
        },
        {
            lng: 4.64236021,
            lat: 6.21124983
        },
        {
            lng: 4.64236021,
            lat: 6.21152782
        },
        {
            lng: 4.64208221,
            lat: 6.21152782
        },
        {
            lng: 4.64208221,
            lat: 6.21180582
        },
        {
            lng: 4.64152813,
            lat: 6.21180582
        },
        {
            lng: 4.64152813,
            lat: 6.21208382
        },
        {
            lng: 4.64125013,
            lat: 6.21208382
        },
        {
            lng: 4.64125013,
            lat: 6.21236181
        },
        {
            lng: 4.64069414,
            lat: 6.21236181
        },
        {
            lng: 4.64069414,
            lat: 6.2126379
        },
        {
            lng: 4.64041615,
            lat: 6.2126379
        },
        {
            lng: 4.64041615,
            lat: 6.2129159
        },
        {
            lng: 4.64013815,
            lat: 6.2129159
        },
        {
            lng: 4.64013815,
            lat: 6.21319389
        },
        {
            lng: 4.63986015,
            lat: 6.21319389
        },
        {
            lng: 4.63986015,
            lat: 6.21347189
        },
        {
            lng: 4.63930607,
            lat: 6.21347189
        },
        {
            lng: 4.63930607,
            lat: 6.21374989
        },
        {
            lng: 4.63902807,
            lat: 6.21374989
        },
        {
            lng: 4.63902807,
            lat: 6.21402788
        },
        {
            lng: 4.63875008,
            lat: 6.21402788
        },
        {
            lng: 4.63875008,
            lat: 6.21430588
        },
        {
            lng: 4.63819408,
            lat: 6.21430588
        },
        {
            lng: 4.63819408,
            lat: 6.21458387
        },
        {
            lng: 4.63791609,
            lat: 6.21458387
        },
        {
            lng: 4.63791609,
            lat: 6.21485996
        },
        {
            lng: 4.6373601,
            lat: 6.21485996
        },
        {
            lng: 4.6373601,
            lat: 6.21513796
        },
        {
            lng: 4.63708401,
            lat: 6.21513796
        },
        {
            lng: 4.63708401,
            lat: 6.21541595
        },
        {
            lng: 4.63680601,
            lat: 6.21541595
        },
        {
            lng: 4.63680601,
            lat: 6.21569395
        },
        {
            lng: 4.63652802,
            lat: 6.21569395
        },
        {
            lng: 4.63652802,
            lat: 6.21597195
        },
        {
            lng: 4.63597202,
            lat: 6.21597195
        },
        {
            lng: 4.63597202,
            lat: 6.21624994
        },
        {
            lng: 4.63569403,
            lat: 6.21624994
        },
        {
            lng: 4.63569403,
            lat: 6.21652794
        },
        {
            lng: 4.63541603,
            lat: 6.21652794
        },
        {
            lng: 4.63541603,
            lat: 6.21680593
        },
        {
            lng: 4.63513994,
            lat: 6.21680593
        },
        {
            lng: 4.63513994,
            lat: 6.21708488
        },
        {
            lng: 4.63486195,
            lat: 6.21708488
        },
        {
            lng: 4.63486195,
            lat: 6.21736002
        },
        {
            lng: 4.63402796,
            lat: 6.21736002
        },
        {
            lng: 4.63402796,
            lat: 6.21763802
        },
        {
            lng: 4.63347197,
            lat: 6.21763802
        },
        {
            lng: 4.63347197,
            lat: 6.21819401
        },
        {
            lng: 4.63291788,
            lat: 6.21819401
        },
        {
            lng: 4.63291597,
            lat: 6.21847296
        },
        {
            lng: 4.63236189,
            lat: 6.21847296
        },
        {
            lng: 4.63236189,
            lat: 6.21875095
        },
        {
            lng: 4.63208389,
            lat: 6.21875095
        },
        {
            lng: 4.63208389,
            lat: 6.21902895
        },
        {
            lng: 4.6318059,
            lat: 6.21902895
        },
        {
            lng: 4.6318059,
            lat: 6.21930408
        },
        {
            lng: 4.6315279,
            lat: 6.21930408
        },
        {
            lng: 4.6315279,
            lat: 6.21986103
        },
        {
            lng: 4.6312499,
            lat: 6.21986103
        },
        {
            lng: 4.6312499,
            lat: 6.22013903
        },
        {
            lng: 4.63069296,
            lat: 6.22013903
        },
        {
            lng: 4.63069296,
            lat: 6.22041702
        },
        {
            lng: 4.63041782,
            lat: 6.22041702
        },
        {
            lng: 4.63041782,
            lat: 6.22069502
        },
        {
            lng: 4.63013983,
            lat: 6.22069502
        },
        {
            lng: 4.63013983,
            lat: 6.22097301
        },
        {
            lng: 4.62986183,
            lat: 6.22097301
        },
        {
            lng: 4.62986183,
            lat: 6.22125101
        },
        {
            lng: 4.62958384,
            lat: 6.22125101
        },
        {
            lng: 4.62958384,
            lat: 6.2215271
        },
        {
            lng: 4.62930489,
            lat: 6.2215271
        },
        {
            lng: 4.62930489,
            lat: 6.2218051
        },
        {
            lng: 4.62902689,
            lat: 6.2218051
        },
        {
            lng: 4.62902689,
            lat: 6.22208309
        },
        {
            lng: 4.62874889,
            lat: 6.22208309
        },
        {
            lng: 4.62874889,
            lat: 6.22263908
        },
        {
            lng: 4.6281929,
            lat: 6.22263908
        },
        {
            lng: 4.6281929,
            lat: 6.22291708
        },
        {
            lng: 4.62791681,
            lat: 6.22291708
        },
        {
            lng: 4.62791681,
            lat: 6.22319508
        },
        {
            lng: 4.62736082,
            lat: 6.22319508
        },
        {
            lng: 4.62736082,
            lat: 6.22347307
        },
        {
            lng: 4.62708282,
            lat: 6.22347307
        },
        {
            lng: 4.62708282,
            lat: 6.22430515
        },
        {
            lng: 4.62680483,
            lat: 6.22430515
        },
        {
            lng: 4.62680483,
            lat: 6.22486115
        },
        {
            lng: 4.62597322,
            lat: 6.22486115
        },
        {
            lng: 4.62597322,
            lat: 6.22513914
        },
        {
            lng: 4.62569523,
            lat: 6.22513914
        },
        {
            lng: 4.62569523,
            lat: 6.22541714
        },
        {
            lng: 4.62513924,
            lat: 6.22541714
        },
        {
            lng: 4.62513924,
            lat: 6.22569513
        },
        {
            lng: 4.62486076,
            lat: 6.22569513
        },
        {
            lng: 4.62486076,
            lat: 6.22597313
        },
        {
            lng: 4.62458277,
            lat: 6.22597313
        },
        {
            lng: 4.62458277,
            lat: 6.22625113
        },
        {
            lng: 4.62430477,
            lat: 6.22625113
        },
        {
            lng: 4.62430477,
            lat: 6.22652721
        },
        {
            lng: 4.62402678,
            lat: 6.22652721
        },
        {
            lng: 4.62402678,
            lat: 6.22708321
        },
        {
            lng: 4.62375116,
            lat: 6.22708321
        },
        {
            lng: 4.62375116,
            lat: 6.2273612
        },
        {
            lng: 4.62347317,
            lat: 6.2273612
        },
        {
            lng: 4.62347317,
            lat: 6.2276392
        },
        {
            lng: 4.62291718,
            lat: 6.2276392
        },
        {
            lng: 4.62291718,
            lat: 6.22791719
        },
        {
            lng: 4.62263918,
            lat: 6.22791719
        },
        {
            lng: 4.62263918,
            lat: 6.22819519
        },
        {
            lng: 4.62236118,
            lat: 6.22819519
        },
        {
            lng: 4.62236118,
            lat: 6.2284708
        },
        {
            lng: 4.62263918,
            lat: 6.2284708
        },
        {
            lng: 4.62263918,
            lat: 6.2287488
        },
        {
            lng: 4.62236118,
            lat: 6.2287488
        },
        {
            lng: 4.62236118,
            lat: 6.22902679
        },
        {
            lng: 4.62208319,
            lat: 6.22902679
        },
        {
            lng: 4.62208319,
            lat: 6.22930479
        },
        {
            lng: 4.62152719,
            lat: 6.22930479
        },
        {
            lng: 4.62152719,
            lat: 6.22958279
        },
        {
            lng: 4.62097311,
            lat: 6.22958279
        },
        {
            lng: 4.62097311,
            lat: 6.22986078
        },
        {
            lng: 4.62069511,
            lat: 6.22986078
        },
        {
            lng: 4.62069511,
            lat: 6.23013878
        },
        {
            lng: 4.62013912,
            lat: 6.23013878
        },
        {
            lng: 4.62013912,
            lat: 6.23041677
        },
        {
            lng: 4.61986113,
            lat: 6.23041677
        },
        {
            lng: 4.61986113,
            lat: 6.23097086
        },
        {
            lng: 4.61958313,
            lat: 6.23097086
        },
        {
            lng: 4.61958313,
            lat: 6.23124886
        },
        {
            lng: 4.61930513,
            lat: 6.23124886
        },
        {
            lng: 4.61930513,
            lat: 6.23152685
        },
        {
            lng: 4.61902714,
            lat: 6.23152685
        },
        {
            lng: 4.61902714,
            lat: 6.23180485
        },
        {
            lng: 4.61847305,
            lat: 6.23180485
        },
        {
            lng: 4.61847305,
            lat: 6.2320838
        },
        {
            lng: 4.61819506,
            lat: 6.2320838
        },
        {
            lng: 4.61819506,
            lat: 6.23236179
        },
        {
            lng: 4.61791706,
            lat: 6.23236179
        },
        {
            lng: 4.61791706,
            lat: 6.23319292
        },
        {
            lng: 4.61763906,
            lat: 6.23319292
        },
        {
            lng: 4.61763906,
            lat: 6.23347187
        },
        {
            lng: 4.61791706,
            lat: 6.23347187
        },
        {
            lng: 4.61791706,
            lat: 6.23374987
        },
        {
            lng: 4.61763906,
            lat: 6.23374987
        },
        {
            lng: 4.61763906,
            lat: 6.23416424
        },
        {
            lng: 4.61763906,
            lat: 6.23430586
        },
        {
            lng: 4.61791706,
            lat: 6.23430586
        },
        {
            lng: 4.61791706,
            lat: 6.23458385
        },
        {
            lng: 4.61819506,
            lat: 6.23458385
        },
        {
            lng: 4.61819506,
            lat: 6.23513985
        },
        {
            lng: 4.61847305,
            lat: 6.23513985
        },
        {
            lng: 4.61847305,
            lat: 6.23652792
        },
        {
            lng: 4.61763906,
            lat: 6.23652792
        },
        {
            lng: 4.61763906,
            lat: 6.23624992
        },
        {
            lng: 4.61736107,
            lat: 6.23624992
        },
        {
            lng: 4.61736107,
            lat: 6.23569393
        },
        {
            lng: 4.61680412,
            lat: 6.23569393
        },
        {
            lng: 4.61680412,
            lat: 6.23541784
        },
        {
            lng: 4.61625099,
            lat: 6.23541784
        },
        {
            lng: 4.61621189,
            lat: 6.23514414
        },
        {
            lng: 4.61569405,
            lat: 6.23513985
        },
        {
            lng: 4.61569405,
            lat: 6.23486185
        },
        {
            lng: 4.61541605,
            lat: 6.23486185
        },
        {
            lng: 4.61541605,
            lat: 6.23430586
        },
        {
            lng: 4.61513805,
            lat: 6.23430586
        },
        {
            lng: 4.61513805,
            lat: 6.23402786
        },
        {
            lng: 4.61486006,
            lat: 6.23402786
        },
        {
            lng: 4.61486006,
            lat: 6.23458385
        },
        {
            lng: 4.61430597,
            lat: 6.23458385
        },
        {
            lng: 4.61430597,
            lat: 6.23486185
        },
        {
            lng: 4.61402798,
            lat: 6.23486185
        },
        {
            lng: 4.61402798,
            lat: 6.23513985
        },
        {
            lng: 4.61374998,
            lat: 6.23513985
        },
        {
            lng: 4.61374998,
            lat: 6.23541784
        },
        {
            lng: 4.61347198,
            lat: 6.23541784
        },
        {
            lng: 4.61347198,
            lat: 6.23597193
        },
        {
            lng: 4.61319399,
            lat: 6.23597193
        },
        {
            lng: 4.61319399,
            lat: 6.23624992
        },
        {
            lng: 4.612638,
            lat: 6.23624992
        },
        {
            lng: 4.612638,
            lat: 6.23652792
        },
        {
            lng: 4.61236,
            lat: 6.23652792
        },
        {
            lng: 4.61236,
            lat: 6.23680592
        },
        {
            lng: 4.61208391,
            lat: 6.23680592
        },
        {
            lng: 4.61208391,
            lat: 6.23708391
        },
        {
            lng: 4.61152792,
            lat: 6.23708391
        },
        {
            lng: 4.61152792,
            lat: 6.23736191
        },
        {
            lng: 4.61069393,
            lat: 6.23736191
        },
        {
            lng: 4.61069393,
            lat: 6.2376399
        },
        {
            lng: 4.60958385,
            lat: 6.2376399
        },
        {
            lng: 4.60958385,
            lat: 6.23819399
        },
        {
            lng: 4.60930586,
            lat: 6.23819399
        },
        {
            lng: 4.60930586,
            lat: 6.23847198
        },
        {
            lng: 4.60874987,
            lat: 6.23847198
        },
        {
            lng: 4.60874987,
            lat: 6.23874998
        },
        {
            lng: 4.60847187,
            lat: 6.23874998
        },
        {
            lng: 4.60847187,
            lat: 6.23902798
        },
        {
            lng: 4.60791588,
            lat: 6.23902798
        },
        {
            lng: 4.60791588,
            lat: 6.23930597
        },
        {
            lng: 4.60736179,
            lat: 6.23930597
        },
        {
            lng: 4.60736179,
            lat: 6.23958397
        },
        {
            lng: 4.6070838,
            lat: 6.23958397
        },
        {
            lng: 4.6070838,
            lat: 6.24013805
        },
        {
            lng: 4.60652781,
            lat: 6.24013805
        },
        {
            lng: 4.60652781,
            lat: 6.24041605
        },
        {
            lng: 4.60624981,
            lat: 6.24041605
        },
        {
            lng: 4.60624981,
            lat: 6.24069405
        },
        {
            lng: 4.60597181,
            lat: 6.24069405
        },
        {
            lng: 4.60597181,
            lat: 6.24097204
        },
        {
            lng: 4.60569382,
            lat: 6.24097204
        },
        {
            lng: 4.60569382,
            lat: 6.24125004
        },
        {
            lng: 4.60541821,
            lat: 6.24125004
        },
        {
            lng: 4.60541821,
            lat: 6.24152803
        },
        {
            lng: 4.60514021,
            lat: 6.24152803
        },
        {
            lng: 4.60514021,
            lat: 6.24180603
        },
        {
            lng: 4.60458422,
            lat: 6.24180603
        },
        {
            lng: 4.60458422,
            lat: 6.24208403
        },
        {
            lng: 4.60402822,
            lat: 6.24208403
        },
        {
            lng: 4.60402822,
            lat: 6.24236012
        },
        {
            lng: 4.60375023,
            lat: 6.24236012
        },
        {
            lng: 4.60375023,
            lat: 6.24263811
        },
        {
            lng: 4.6034708,
            lat: 6.24263811
        },
        {
            lng: 4.6034708,
            lat: 6.24291611
        },
        {
            lng: 4.60319281,
            lat: 6.24291611
        },
        {
            lng: 4.60319281,
            lat: 6.2431941
        },
        {
            lng: 4.60264015,
            lat: 6.2431941
        },
        {
            lng: 4.60264015,
            lat: 6.2434721
        },
        {
            lng: 4.60236216,
            lat: 6.2434721
        },
        {
            lng: 4.60236216,
            lat: 6.24402809
        },
        {
            lng: 4.60208321,
            lat: 6.24402809
        },
        {
            lng: 4.60208321,
            lat: 6.24430704
        },
        {
            lng: 4.60180521,
            lat: 6.24430704
        },
        {
            lng: 4.60180521,
            lat: 6.24458218
        },
        {
            lng: 4.60152721,
            lat: 6.24458218
        },
        {
            lng: 4.60152721,
            lat: 6.24486017
        },
        {
            lng: 4.60124922,
            lat: 6.24486017
        },
        {
            lng: 4.60124922,
            lat: 6.24513817
        },
        {
            lng: 4.60013914,
            lat: 6.24513817
        },
        {
            lng: 4.60013914,
            lat: 6.24541616
        },
        {
            lng: 4.59986115,
            lat: 6.24541616
        },
        {
            lng: 4.59986115,
            lat: 6.24569511
        },
        {
            lng: 4.59958315,
            lat: 6.24569511
        },
        {
            lng: 4.59958315,
            lat: 6.24597311
        },
        {
            lng: 4.59930515,
            lat: 6.24597311
        },
        {
            lng: 4.59930515,
            lat: 6.24625111
        },
        {
            lng: 4.59874916,
            lat: 6.24625111
        },
        {
            lng: 4.59874916,
            lat: 6.2465291
        },
        {
            lng: 4.59847116,
            lat: 6.2465291
        },
        {
            lng: 4.59847116,
            lat: 6.24708319
        },
        {
            lng: 4.59819508,
            lat: 6.24708319
        },
        {
            lng: 4.59819508,
            lat: 6.24736118
        },
        {
            lng: 4.59736109,
            lat: 6.24736118
        },
        {
            lng: 4.59736109,
            lat: 6.24791718
        },
        {
            lng: 4.59708309,
            lat: 6.24791718
        },
        {
            lng: 4.59708309,
            lat: 6.24819517
        },
        {
            lng: 4.5968051,
            lat: 6.24819517
        },
        {
            lng: 4.5968051,
            lat: 6.24847317
        },
        {
            lng: 4.59597301,
            lat: 6.24847317
        },
        {
            lng: 4.59597301,
            lat: 6.24875116
        },
        {
            lng: 4.59541702,
            lat: 6.24875116
        },
        {
            lng: 4.59541702,
            lat: 6.24902678
        },
        {
            lng: 4.59458303,
            lat: 6.24902678
        },
        {
            lng: 4.59458303,
            lat: 6.24958277
        },
        {
            lng: 4.59430504,
            lat: 6.24958277
        },
        {
            lng: 4.59430504,
            lat: 6.24986076
        },
        {
            lng: 4.59402895,
            lat: 6.24986076
        },
        {
            lng: 4.59402895,
            lat: 6.25013924
        },
        {
            lng: 4.59375095,
            lat: 6.25013924
        },
        {
            lng: 4.59375095,
            lat: 6.25041723
        },
        {
            lng: 4.59347296,
            lat: 6.25041723
        },
        {
            lng: 4.59347296,
            lat: 6.25069523
        },
        {
            lng: 4.59319496,
            lat: 6.25069523
        },
        {
            lng: 4.59319496,
            lat: 6.25097322
        },
        {
            lng: 4.59236097,
            lat: 6.25097322
        },
        {
            lng: 4.59236097,
            lat: 6.25124884
        },
        {
            lng: 4.59208298,
            lat: 6.25124884
        },
        {
            lng: 4.59208298,
            lat: 6.25152683
        },
        {
            lng: 4.59180498,
            lat: 6.25152683
        },
        {
            lng: 4.59180498,
            lat: 6.25180483
        },
        {
            lng: 4.59152889,
            lat: 6.25180483
        },
        {
            lng: 4.59152889,
            lat: 6.25208282
        },
        {
            lng: 4.5909729,
            lat: 6.25208282
        },
        {
            lng: 4.5909729,
            lat: 6.25236082
        },
        {
            lng: 4.5906949,
            lat: 6.25236082
        },
        {
            lng: 4.5906949,
            lat: 6.25263882
        },
        {
            lng: 4.59041691,
            lat: 6.25263882
        },
        {
            lng: 4.59041691,
            lat: 6.25319481
        },
        {
            lng: 4.59013891,
            lat: 6.25319481
        },
        {
            lng: 4.59013891,
            lat: 6.25347281
        },
        {
            lng: 4.58985996,
            lat: 6.25347281
        },
        {
            lng: 4.58985996,
            lat: 6.25374889
        },
        {
            lng: 4.58958197,
            lat: 6.25374889
        },
        {
            lng: 4.58958197,
            lat: 6.25402689
        },
        {
            lng: 4.58875084,
            lat: 6.25402689
        },
        {
            lng: 4.58875084,
            lat: 6.25430489
        },
        {
            lng: 4.58819389,
            lat: 6.25430489
        },
        {
            lng: 4.58819389,
            lat: 6.25458288
        },
        {
            lng: 4.5879159,
            lat: 6.25458288
        },
        {
            lng: 4.5879159,
            lat: 6.25486088
        },
        {
            lng: 4.58735991,
            lat: 6.25486088
        },
        {
            lng: 4.58735991,
            lat: 6.25513887
        },
        {
            lng: 4.58708191,
            lat: 6.25513887
        },
        {
            lng: 4.58708191,
            lat: 6.25541687
        },
        {
            lng: 4.58680582,
            lat: 6.25541687
        },
        {
            lng: 4.58680582,
            lat: 6.25569487
        },
        {
            lng: 4.58652782,
            lat: 6.25569487
        },
        {
            lng: 4.58652782,
            lat: 6.25597286
        },
        {
            lng: 4.58624983,
            lat: 6.25597286
        },
        {
            lng: 4.58624983,
            lat: 6.25624895
        },
        {
            lng: 4.58541584,
            lat: 6.25624895
        },
        {
            lng: 4.58541584,
            lat: 6.25652695
        },
        {
            lng: 4.58513784,
            lat: 6.25652695
        },
        {
            lng: 4.58513784,
            lat: 6.25680494
        },
        {
            lng: 4.58486223,
            lat: 6.25680494
        },
        {
            lng: 4.58486223,
            lat: 6.25708294
        },
        {
            lng: 4.58430576,
            lat: 6.25708294
        },
        {
            lng: 4.58430576,
            lat: 6.25763893
        },
        {
            lng: 4.58374977,
            lat: 6.25763893
        },
        {
            lng: 4.58374977,
            lat: 6.25819302
        },
        {
            lng: 4.58319378,
            lat: 6.25819302
        },
        {
            lng: 4.58319378,
            lat: 6.25847101
        },
        {
            lng: 4.58263779,
            lat: 6.25847101
        },
        {
            lng: 4.58263779,
            lat: 6.25874901
        },
        {
            lng: 4.58208418,
            lat: 6.25874901
        },
        {
            lng: 4.58208418,
            lat: 6.259027
        },
        {
            lng: 4.58180618,
            lat: 6.259027
        },
        {
            lng: 4.58180618,
            lat: 6.25930595
        },
        {
            lng: 4.58125019,
            lat: 6.25930595
        },
        {
            lng: 4.58125019,
            lat: 6.25986195
        },
        {
            lng: 4.58097219,
            lat: 6.25986195
        },
        {
            lng: 4.58097219,
            lat: 6.26013994
        },
        {
            lng: 4.5804162,
            lat: 6.26013994
        },
        {
            lng: 4.5804162,
            lat: 6.26041508
        },
        {
            lng: 4.58013821,
            lat: 6.26041508
        },
        {
            lng: 4.58013821,
            lat: 6.26069403
        },
        {
            lng: 4.57986212,
            lat: 6.26069403
        },
        {
            lng: 4.57986212,
            lat: 6.26097202
        },
        {
            lng: 4.57930613,
            lat: 6.26097202
        },
        {
            lng: 4.57930613,
            lat: 6.26152802
        },
        {
            lng: 4.57875013,
            lat: 6.26152802
        },
        {
            lng: 4.57875013,
            lat: 6.26180601
        },
        {
            lng: 4.57819414,
            lat: 6.26180601
        },
        {
            lng: 4.57819414,
            lat: 6.26208401
        },
        {
            lng: 4.57736206,
            lat: 6.26208401
        },
        {
            lng: 4.57736206,
            lat: 6.262362
        },
        {
            lng: 4.57708406,
            lat: 6.262362
        },
        {
            lng: 4.57708406,
            lat: 6.26319408
        },
        {
            lng: 4.57680607,
            lat: 6.26319408
        },
        {
            lng: 4.57680607,
            lat: 6.26347208
        },
        {
            lng: 4.57652807,
            lat: 6.26347208
        },
        {
            lng: 4.57652807,
            lat: 6.26375008
        },
        {
            lng: 4.57597113,
            lat: 6.26375008
        },
        {
            lng: 4.57597113,
            lat: 6.26430607
        },
        {
            lng: 4.57569599,
            lat: 6.26430607
        },
        {
            lng: 4.57569599,
            lat: 6.26458406
        },
        {
            lng: 4.575418,
            lat: 6.26458406
        },
        {
            lng: 4.575418,
            lat: 6.26486206
        },
        {
            lng: 4.57458305,
            lat: 6.26486206
        },
        {
            lng: 4.57458305,
            lat: 6.26514006
        },
        {
            lng: 4.57430506,
            lat: 6.26514006
        },
        {
            lng: 4.57430506,
            lat: 6.26541615
        },
        {
            lng: 4.57374907,
            lat: 6.26541615
        },
        {
            lng: 4.57374907,
            lat: 6.26569414
        },
        {
            lng: 4.57347107,
            lat: 6.26569414
        },
        {
            lng: 4.57347107,
            lat: 6.26597214
        },
        {
            lng: 4.57319498,
            lat: 6.26597214
        },
        {
            lng: 4.57319498,
            lat: 6.26625013
        },
        {
            lng: 4.57291698,
            lat: 6.26625013
        },
        {
            lng: 4.57291698,
            lat: 6.26652813
        },
        {
            lng: 4.57263899,
            lat: 6.26652813
        },
        {
            lng: 4.57263899,
            lat: 6.26680613
        },
        {
            lng: 4.572083,
            lat: 6.26680613
        },
        {
            lng: 4.572083,
            lat: 6.26708412
        },
        {
            lng: 4.571805,
            lat: 6.26708412
        },
        {
            lng: 4.571805,
            lat: 6.26736212
        },
        {
            lng: 4.571527,
            lat: 6.26736212
        },
        {
            lng: 4.571527,
            lat: 6.26763821
        },
        {
            lng: 4.57097292,
            lat: 6.26763821
        },
        {
            lng: 4.57097292,
            lat: 6.2679162
        },
        {
            lng: 4.57041693,
            lat: 6.2679162
        },
        {
            lng: 4.57041693,
            lat: 6.2681942
        },
        {
            lng: 4.57013893,
            lat: 6.2681942
        },
        {
            lng: 4.57013893,
            lat: 6.26847219
        },
        {
            lng: 4.56986094,
            lat: 6.26847219
        },
        {
            lng: 4.56986094,
            lat: 6.26875019
        },
        {
            lng: 4.56958294,
            lat: 6.26875019
        },
        {
            lng: 4.56958294,
            lat: 6.26902819
        },
        {
            lng: 4.56930494,
            lat: 6.26902819
        },
        {
            lng: 4.56930494,
            lat: 6.26985979
        },
        {
            lng: 4.56902695,
            lat: 6.26985979
        },
        {
            lng: 4.56902695,
            lat: 6.27013779
        },
        {
            lng: 4.56930494,
            lat: 6.27013779
        },
        {
            lng: 4.56930494,
            lat: 6.27069378
        },
        {
            lng: 4.56902695,
            lat: 6.27069378
        },
        {
            lng: 4.56902695,
            lat: 6.27097178
        },
        {
            lng: 4.56847286,
            lat: 6.27097178
        },
        {
            lng: 4.56847286,
            lat: 6.27124977
        },
        {
            lng: 4.56819487,
            lat: 6.27124977
        },
        {
            lng: 4.56819487,
            lat: 6.27097178
        },
        {
            lng: 4.56791687,
            lat: 6.27097178
        },
        {
            lng: 4.56791687,
            lat: 6.27069378
        },
        {
            lng: 4.56708288,
            lat: 6.27069378
        },
        {
            lng: 4.56708288,
            lat: 6.27097178
        },
        {
            lng: 4.56680489,
            lat: 6.27097178
        },
        {
            lng: 4.56680489,
            lat: 6.27124977
        },
        {
            lng: 4.5662508,
            lat: 6.27124977
        },
        {
            lng: 4.5662508,
            lat: 6.2715292
        },
        {
            lng: 4.56569481,
            lat: 6.2715292
        },
        {
            lng: 4.56569481,
            lat: 6.27180719
        },
        {
            lng: 4.56541681,
            lat: 6.27180719
        },
        {
            lng: 4.56541681,
            lat: 6.27208185
        },
        {
            lng: 4.56486082,
            lat: 6.27208185
        },
        {
            lng: 4.56486082,
            lat: 6.27235985
        },
        {
            lng: 4.56458282,
            lat: 6.27235985
        },
        {
            lng: 4.56458282,
            lat: 6.27263784
        },
        {
            lng: 4.56375122,
            lat: 6.27263784
        },
        {
            lng: 4.56375122,
            lat: 6.27291679
        },
        {
            lng: 4.56347322,
            lat: 6.27291679
        },
        {
            lng: 4.56347322,
            lat: 6.27319479
        },
        {
            lng: 4.56263781,
            lat: 6.27319479
        },
        {
            lng: 4.56263781,
            lat: 6.27347279
        },
        {
            lng: 4.56235981,
            lat: 6.27347279
        },
        {
            lng: 4.56235981,
            lat: 6.27402878
        },
        {
            lng: 4.56208181,
            lat: 6.27402878
        },
        {
            lng: 4.56208181,
            lat: 6.27430487
        },
        {
            lng: 4.56180716,
            lat: 6.27430677
        },
        {
            lng: 4.56180716,
            lat: 6.27458286
        },
        {
            lng: 4.56097221,
            lat: 6.27458286
        },
        {
            lng: 4.56097221,
            lat: 6.27486086
        },
        {
            lng: 4.56069422,
            lat: 6.27486086
        },
        {
            lng: 4.56069422,
            lat: 6.27513885
        },
        {
            lng: 4.56013823,
            lat: 6.27513885
        },
        {
            lng: 4.56013823,
            lat: 6.27541685
        },
        {
            lng: 4.55986023,
            lat: 6.27541685
        },
        {
            lng: 4.55986023,
            lat: 6.27569485
        },
        {
            lng: 4.55902815,
            lat: 6.27569485
        },
        {
            lng: 4.55902815,
            lat: 6.27597284
        },
        {
            lng: 4.55875015,
            lat: 6.27597284
        },
        {
            lng: 4.55875015,
            lat: 6.27625084
        },
        {
            lng: 4.55819416,
            lat: 6.27625084
        },
        {
            lng: 4.55819416,
            lat: 6.27652884
        },
        {
            lng: 4.55791616,
            lat: 6.27652884
        },
        {
            lng: 4.55791616,
            lat: 6.27680492
        },
        {
            lng: 4.55708408,
            lat: 6.27680492
        },
        {
            lng: 4.55708408,
            lat: 6.27708292
        },
        {
            lng: 4.55652809,
            lat: 6.27708292
        },
        {
            lng: 4.55652809,
            lat: 6.27736092
        },
        {
            lng: 4.5559721,
            lat: 6.27736092
        },
        {
            lng: 4.5559721,
            lat: 6.27763891
        },
        {
            lng: 4.5556941,
            lat: 6.27763891
        },
        {
            lng: 4.5556941,
            lat: 6.27791691
        },
        {
            lng: 4.55458403,
            lat: 6.27791691
        },
        {
            lng: 4.55458403,
            lat: 6.27763891
        },
        {
            lng: 4.55430603,
            lat: 6.27763891
        },
        {
            lng: 4.55430603,
            lat: 6.27791691
        },
        {
            lng: 4.55402803,
            lat: 6.27791691
        },
        {
            lng: 4.55402803,
            lat: 6.27874899
        },
        {
            lng: 4.55347204,
            lat: 6.27874899
        },
        {
            lng: 4.55347204,
            lat: 6.27902699
        },
        {
            lng: 4.55291605,
            lat: 6.27902699
        },
        {
            lng: 4.55291605,
            lat: 6.27930498
        },
        {
            lng: 4.55208397,
            lat: 6.27930498
        },
        {
            lng: 4.55208397,
            lat: 6.28013897
        },
        {
            lng: 4.55152798,
            lat: 6.28013897
        },
        {
            lng: 4.55152798,
            lat: 6.28041697
        },
        {
            lng: 4.55124998,
            lat: 6.28041697
        },
        {
            lng: 4.55124998,
            lat: 6.28069496
        },
        {
            lng: 4.55069399,
            lat: 6.28069496
        },
        {
            lng: 4.55069399,
            lat: 6.28097105
        },
        {
            lng: 4.5501399,
            lat: 6.28097296
        },
        {
            lng: 4.5501399,
            lat: 6.28152704
        },
        {
            lng: 4.54930592,
            lat: 6.28152704
        },
        {
            lng: 4.54930592,
            lat: 6.28180504
        },
        {
            lng: 4.54874897,
            lat: 6.28180504
        },
        {
            lng: 4.54874897,
            lat: 6.28208303
        },
        {
            lng: 4.54847097,
            lat: 6.28208303
        },
        {
            lng: 4.54847097,
            lat: 6.28263903
        },
        {
            lng: 4.54819298,
            lat: 6.28263903
        },
        {
            lng: 4.54819298,
            lat: 6.28291702
        },
        {
            lng: 4.54791784,
            lat: 6.28291702
        },
        {
            lng: 4.54791784,
            lat: 6.28319502
        },
        {
            lng: 4.5470829,
            lat: 6.28319502
        },
        {
            lng: 4.5470829,
            lat: 6.28347111
        },
        {
            lng: 4.5468049,
            lat: 6.28347111
        },
        {
            lng: 4.5468049,
            lat: 6.2837491
        },
        {
            lng: 4.54652691,
            lat: 6.2837491
        },
        {
            lng: 4.54652691,
            lat: 6.2840271
        },
        {
            lng: 4.54597282,
            lat: 6.2840271
        },
        {
            lng: 4.54597282,
            lat: 6.2843051
        },
        {
            lng: 4.54541683,
            lat: 6.2843051
        },
        {
            lng: 4.54541683,
            lat: 6.28486109
        },
        {
            lng: 4.54513884,
            lat: 6.28486109
        },
        {
            lng: 4.54513884,
            lat: 6.28514004
        },
        {
            lng: 4.54458284,
            lat: 6.28514004
        },
        {
            lng: 4.54458284,
            lat: 6.28541803
        },
        {
            lng: 4.54402685,
            lat: 6.28541803
        },
        {
            lng: 4.54402685,
            lat: 6.28569603
        },
        {
            lng: 4.54374886,
            lat: 6.28569603
        },
        {
            lng: 4.54374886,
            lat: 6.28624916
        },
        {
            lng: 4.54347277,
            lat: 6.28624916
        },
        {
            lng: 4.54347277,
            lat: 6.28652811
        },
        {
            lng: 4.54319477,
            lat: 6.28652811
        },
        {
            lng: 4.54319477,
            lat: 6.28680611
        },
        {
            lng: 4.54291677,
            lat: 6.28680611
        },
        {
            lng: 4.54291677,
            lat: 6.2873621
        },
        {
            lng: 4.54236078,
            lat: 6.2873621
        },
        {
            lng: 4.54236078,
            lat: 6.28764009
        },
        {
            lng: 4.54180479,
            lat: 6.28764009
        },
        {
            lng: 4.54180479,
            lat: 6.28791809
        },
        {
            lng: 4.54152679,
            lat: 6.28791809
        },
        {
            lng: 4.54152679,
            lat: 6.28819418
        },
        {
            lng: 4.54097319,
            lat: 6.28819418
        },
        {
            lng: 4.54097319,
            lat: 6.28847218
        },
        {
            lng: 4.54041719,
            lat: 6.28847218
        },
        {
            lng: 4.54041719,
            lat: 6.28958416
        },
        {
            lng: 4.5401392,
            lat: 6.28958416
        },
        {
            lng: 4.5401392,
            lat: 6.29013777
        },
        {
            lng: 4.5398612,
            lat: 6.29013777
        },
        {
            lng: 4.5398612,
            lat: 6.29041576
        },
        {
            lng: 4.53958321,
            lat: 6.29041576
        },
        {
            lng: 4.53958321,
            lat: 6.29069424
        },
        {
            lng: 4.53930521,
            lat: 6.29069424
        },
        {
            lng: 4.53930521,
            lat: 6.29125023
        },
        {
            lng: 4.53902721,
            lat: 6.29125023
        },
        {
            lng: 4.53902721,
            lat: 6.29152822
        },
        {
            lng: 4.53847313,
            lat: 6.29152822
        },
        {
            lng: 4.53847313,
            lat: 6.29180622
        },
        {
            lng: 4.53791714,
            lat: 6.29180622
        },
        {
            lng: 4.53791714,
            lat: 6.29291582
        },
        {
            lng: 4.53819513,
            lat: 6.29291582
        },
        {
            lng: 4.53819513,
            lat: 6.2945838
        },
        {
            lng: 4.53791714,
            lat: 6.2945838
        },
        {
            lng: 4.53791714,
            lat: 6.29486179
        },
        {
            lng: 4.53763914,
            lat: 6.29486179
        },
        {
            lng: 4.53763914,
            lat: 6.29513788
        },
        {
            lng: 4.53708315,
            lat: 6.29513788
        },
        {
            lng: 4.53708315,
            lat: 6.29541588
        },
        {
            lng: 4.53652906,
            lat: 6.29541588
        },
        {
            lng: 4.53652906,
            lat: 6.29569387
        },
        {
            lng: 4.53597307,
            lat: 6.29569387
        },
        {
            lng: 4.53597307,
            lat: 6.29541588
        },
        {
            lng: 4.53513813,
            lat: 6.29541588
        },
        {
            lng: 4.53513813,
            lat: 6.29569387
        },
        {
            lng: 4.53486013,
            lat: 6.29569387
        },
        {
            lng: 4.53486013,
            lat: 6.29597187
        },
        {
            lng: 4.53291607,
            lat: 6.29597187
        },
        {
            lng: 4.53291607,
            lat: 6.29624987
        },
        {
            lng: 4.53263807,
            lat: 6.29624987
        },
        {
            lng: 4.53263807,
            lat: 6.29652786
        },
        {
            lng: 4.53236008,
            lat: 6.29652786
        },
        {
            lng: 4.53236008,
            lat: 6.29680586
        },
        {
            lng: 4.531528,
            lat: 6.29680586
        },
        {
            lng: 4.531528,
            lat: 6.29708385
        },
        {
            lng: 4.530972,
            lat: 6.29708385
        },
        {
            lng: 4.530972,
            lat: 6.29735994
        },
        {
            lng: 4.53069401,
            lat: 6.29735994
        },
        {
            lng: 4.53069401,
            lat: 6.29763794
        },
        {
            lng: 4.53013802,
            lat: 6.29763794
        },
        {
            lng: 4.53013802,
            lat: 6.29791594
        },
        {
            lng: 4.52986002,
            lat: 6.29791594
        },
        {
            lng: 4.52986002,
            lat: 6.29819393
        },
        {
            lng: 4.52930593,
            lat: 6.29819393
        },
        {
            lng: 4.52930593,
            lat: 6.29847193
        },
        {
            lng: 4.52902794,
            lat: 6.29847193
        },
        {
            lng: 4.52902794,
            lat: 6.29875088
        },
        {
            lng: 4.52874994,
            lat: 6.29875088
        },
        {
            lng: 4.52874994,
            lat: 6.29902887
        },
        {
            lng: 4.52847195,
            lat: 6.29902887
        },
        {
            lng: 4.52847195,
            lat: 6.29930401
        },
        {
            lng: 4.52791595,
            lat: 6.29930401
        },
        {
            lng: 4.52791595,
            lat: 6.299582
        },
        {
            lng: 4.52763796,
            lat: 6.299582
        },
        {
            lng: 4.52763796,
            lat: 6.29986
        },
        {
            lng: 4.52736187,
            lat: 6.29986
        },
        {
            lng: 4.52736187,
            lat: 6.30013895
        },
        {
            lng: 4.52708387,
            lat: 6.30013895
        },
        {
            lng: 4.52708387,
            lat: 6.30041695
        },
        {
            lng: 4.52680588,
            lat: 6.30041695
        },
        {
            lng: 4.52680588,
            lat: 6.30069494
        },
        {
            lng: 4.52624989,
            lat: 6.30069494
        },
        {
            lng: 4.52624989,
            lat: 6.30097294
        },
        {
            lng: 4.52430582,
            lat: 6.30097294
        },
        {
            lng: 4.52430582,
            lat: 6.30125093
        },
        {
            lng: 4.52402782,
            lat: 6.30125093
        },
        {
            lng: 4.52402782,
            lat: 6.30180502
        },
        {
            lng: 4.52374983,
            lat: 6.30180502
        },
        {
            lng: 4.52374983,
            lat: 6.30208302
        },
        {
            lng: 4.52347183,
            lat: 6.30208302
        },
        {
            lng: 4.52347183,
            lat: 6.30236101
        },
        {
            lng: 4.52319288,
            lat: 6.30236101
        },
        {
            lng: 4.52319288,
            lat: 6.30263901
        },
        {
            lng: 4.52152681,
            lat: 6.30263901
        },
        {
            lng: 4.52152681,
            lat: 6.302917
        },
        {
            lng: 4.52097082,
            lat: 6.302917
        },
        {
            lng: 4.52097082,
            lat: 6.303195
        },
        {
            lng: 4.52069283,
            lat: 6.303195
        },
        {
            lng: 4.52069283,
            lat: 6.303473
        },
        {
            lng: 4.52041817,
            lat: 6.303473
        },
        {
            lng: 4.52041817,
            lat: 6.30402708
        },
        {
            lng: 4.52013922,
            lat: 6.30402899
        },
        {
            lng: 4.52013922,
            lat: 6.30430508
        },
        {
            lng: 4.51874924,
            lat: 6.30430508
        },
        {
            lng: 4.51874924,
            lat: 6.30458307
        },
        {
            lng: 4.51791716,
            lat: 6.30458307
        },
        {
            lng: 4.51791716,
            lat: 6.30486107
        },
        {
            lng: 4.51736116,
            lat: 6.30486107
        },
        {
            lng: 4.51736116,
            lat: 6.30513906
        },
        {
            lng: 4.51708317,
            lat: 6.30513906
        },
        {
            lng: 4.51708317,
            lat: 6.30541706
        },
        {
            lng: 4.51652718,
            lat: 6.30541706
        },
        {
            lng: 4.51652718,
            lat: 6.30569506
        },
        {
            lng: 4.51597309,
            lat: 6.30569506
        },
        {
            lng: 4.51597309,
            lat: 6.30597305
        },
        {
            lng: 4.5156951,
            lat: 6.30597305
        },
        {
            lng: 4.5156951,
            lat: 6.30625105
        },
        {
            lng: 4.5151391,
            lat: 6.30625105
        },
        {
            lng: 4.5151391,
            lat: 6.30652714
        },
        {
            lng: 4.51486111,
            lat: 6.30652714
        },
        {
            lng: 4.51486111,
            lat: 6.30680513
        },
        {
            lng: 4.51458311,
            lat: 6.30680513
        },
        {
            lng: 4.51458311,
            lat: 6.30708313
        },
        {
            lng: 4.51402712,
            lat: 6.30708313
        },
        {
            lng: 4.51402712,
            lat: 6.30736113
        },
        {
            lng: 4.51319504,
            lat: 6.30736113
        },
        {
            lng: 4.51319504,
            lat: 6.30763912
        },
        {
            lng: 4.51291704,
            lat: 6.30763912
        },
        {
            lng: 4.51291704,
            lat: 6.30791712
        },
        {
            lng: 4.51236105,
            lat: 6.30791712
        },
        {
            lng: 4.51236105,
            lat: 6.30819511
        },
        {
            lng: 4.51180506,
            lat: 6.30819511
        },
        {
            lng: 4.51180506,
            lat: 6.3084712
        },
        {
            lng: 4.51125097,
            lat: 6.3084712
        },
        {
            lng: 4.51125097,
            lat: 6.3087492
        },
        {
            lng: 4.51069498,
            lat: 6.3087492
        },
        {
            lng: 4.51069498,
            lat: 6.30902719
        },
        {
            lng: 4.51041698,
            lat: 6.30902719
        },
        {
            lng: 4.51041698,
            lat: 6.30930519
        },
        {
            lng: 4.51013899,
            lat: 6.30930519
        },
        {
            lng: 4.51013899,
            lat: 6.30958319
        },
        {
            lng: 4.50958204,
            lat: 6.30958319
        },
        {
            lng: 4.50958204,
            lat: 6.30986118
        },
        {
            lng: 4.50902891,
            lat: 6.30986118
        },
        {
            lng: 4.50902891,
            lat: 6.31013918
        },
        {
            lng: 4.50875092,
            lat: 6.31013918
        },
        {
            lng: 4.50875092,
            lat: 6.31041718
        },
        {
            lng: 4.50847292,
            lat: 6.31041718
        },
        {
            lng: 4.50847292,
            lat: 6.31069279
        },
        {
            lng: 4.50791597,
            lat: 6.31069517
        },
        {
            lng: 4.50791597,
            lat: 6.31097078
        },
        {
            lng: 4.50763798,
            lat: 6.31097078
        },
        {
            lng: 4.50763798,
            lat: 6.31124878
        },
        {
            lng: 4.50735998,
            lat: 6.31124878
        },
        {
            lng: 4.50735998,
            lat: 6.31152678
        },
        {
            lng: 4.50708199,
            lat: 6.31152678
        },
        {
            lng: 4.50708199,
            lat: 6.31180477
        },
        {
            lng: 4.50680685,
            lat: 6.31180477
        },
        {
            lng: 4.50680685,
            lat: 6.31208277
        },
        {
            lng: 4.5062499,
            lat: 6.31208277
        },
        {
            lng: 4.5062499,
            lat: 6.31236219
        },
        {
            lng: 4.50597191,
            lat: 6.31236219
        },
        {
            lng: 4.50597191,
            lat: 6.31264019
        },
        {
            lng: 4.50569391,
            lat: 6.31264019
        },
        {
            lng: 4.50569391,
            lat: 6.31291819
        },
        {
            lng: 4.50541592,
            lat: 6.31291819
        },
        {
            lng: 4.50541592,
            lat: 6.31319284
        },
        {
            lng: 4.50513792,
            lat: 6.31319284
        },
        {
            lng: 4.50513792,
            lat: 6.31374979
        },
        {
            lng: 4.50541592,
            lat: 6.31374979
        },
        {
            lng: 4.50541592,
            lat: 6.31458378
        },
        {
            lng: 4.50458384,
            lat: 6.31458378
        },
        {
            lng: 4.50458384,
            lat: 6.31430578
        },
        {
            lng: 4.50430584,
            lat: 6.31430578
        },
        {
            lng: 4.50430584,
            lat: 6.31402779
        },
        {
            lng: 4.50319386,
            lat: 6.31402779
        },
        {
            lng: 4.50319386,
            lat: 6.31430578
        },
        {
            lng: 4.50291586,
            lat: 6.31430578
        },
        {
            lng: 4.50291586,
            lat: 6.31458378
        },
        {
            lng: 4.50263786,
            lat: 6.31458378
        },
        {
            lng: 4.50263786,
            lat: 6.31569386
        },
        {
            lng: 4.50208378,
            lat: 6.31569386
        },
        {
            lng: 4.50208378,
            lat: 6.31597185
        },
        {
            lng: 4.50180578,
            lat: 6.31597185
        },
        {
            lng: 4.50180578,
            lat: 6.31569386
        },
        {
            lng: 4.50124979,
            lat: 6.31569386
        },
        {
            lng: 4.50124979,
            lat: 6.31541777
        },
        {
            lng: 4.5006938,
            lat: 6.31541777
        },
        {
            lng: 4.5006938,
            lat: 6.31569386
        },
        {
            lng: 4.5004158,
            lat: 6.31569386
        },
        {
            lng: 4.5004158,
            lat: 6.31597185
        },
        {
            lng: 4.50013781,
            lat: 6.31597185
        },
        {
            lng: 4.50013781,
            lat: 6.31652784
        },
        {
            lng: 4.49986219,
            lat: 6.31652784
        },
        {
            lng: 4.49986219,
            lat: 6.31708384
        },
        {
            lng: 4.4995842,
            lat: 6.31708384
        },
        {
            lng: 4.4995842,
            lat: 6.31736183
        },
        {
            lng: 4.49875021,
            lat: 6.31736183
        },
        {
            lng: 4.49875021,
            lat: 6.31763983
        },
        {
            lng: 4.49847221,
            lat: 6.31763983
        },
        {
            lng: 4.49847221,
            lat: 6.31791592
        },
        {
            lng: 4.49819422,
            lat: 6.31791592
        },
        {
            lng: 4.49819422,
            lat: 6.31819391
        },
        {
            lng: 4.49764013,
            lat: 6.31819391
        },
        {
            lng: 4.49764013,
            lat: 6.31847191
        },
        {
            lng: 4.49736214,
            lat: 6.31847191
        },
        {
            lng: 4.49736214,
            lat: 6.3187499
        },
        {
            lng: 4.49708414,
            lat: 6.3187499
        },
        {
            lng: 4.49708414,
            lat: 6.3190279
        },
        {
            lng: 4.49680614,
            lat: 6.3190279
        },
        {
            lng: 4.49680614,
            lat: 6.3193059
        },
        {
            lng: 4.49652815,
            lat: 6.3193059
        },
        {
            lng: 4.49652815,
            lat: 6.32013798
        },
        {
            lng: 4.49625015,
            lat: 6.32013798
        },
        {
            lng: 4.49625015,
            lat: 6.32124996
        },
        {
            lng: 4.49541807,
            lat: 6.32124996
        },
        {
            lng: 4.49541807,
            lat: 6.32152796
        },
        {
            lng: 4.49458313,
            lat: 6.32152796
        },
        {
            lng: 4.49458313,
            lat: 6.32180595
        },
        {
            lng: 4.49402714,
            lat: 6.32180595
        },
        {
            lng: 4.49402714,
            lat: 6.32195377
        },
        {
            lng: 4.49598503,
            lat: 6.32203579
        },
        {
            lng: 4.50681305,
            lat: 6.32248783
        },
        {
            lng: 4.51811981,
            lat: 6.3229599
        },
        {
            lng: 4.54200983,
            lat: 6.31540203
        },
        {
            lng: 4.55866623,
            lat: 6.31742287
        },
        {
            lng: 4.56262684,
            lat: 6.319664
        },
        {
            lng: 4.5720439,
            lat: 6.34534121
        },
        {
            lng: 4.56863022,
            lat: 6.36270285
        },
        {
            lng: 4.55718899,
            lat: 6.36912584
        },
        {
            lng: 4.53866386,
            lat: 6.380054
        },
        {
            lng: 4.51528883,
            lat: 6.39083385
        },
        {
            lng: 4.50681305,
            lat: 6.3907032
        },
        {
            lng: 4.49404287,
            lat: 6.39050579
        },
        {
            lng: 4.47020102,
            lat: 6.3867588
        },
        {
            lng: 4.45438385,
            lat: 6.39371777
        },
        {
            lng: 4.46357107,
            lat: 6.40509701
        },
        {
            lng: 4.47574377,
            lat: 6.41549921
        },
        {
            lng: 4.491714,
            lat: 6.41799688
        },
        {
            lng: 4.50681305,
            lat: 6.41743517
        },
        {
            lng: 4.51455784,
            lat: 6.41714716
        },
        {
            lng: 4.53251314,
            lat: 6.42814398
        },
        {
            lng: 4.54504585,
            lat: 6.44661617
        },
        {
            lng: 4.55897093,
            lat: 6.4512248
        },
        {
            lng: 4.56525278,
            lat: 6.45411682
        },
        {
            lng: 4.58446598,
            lat: 6.45724821
        },
        {
            lng: 4.59231997,
            lat: 6.47188091
        },
        {
            lng: 4.59233284,
            lat: 6.4725728
        },
        {
            lng: 4.59602118,
            lat: 6.48658323
        },
        {
            lng: 4.59028006,
            lat: 6.50282907
        },
        {
            lng: 4.58560801,
            lat: 6.51375103
        },
        {
            lng: 4.58240509,
            lat: 6.54425907
        },
        {
            lng: 4.58635283,
            lat: 6.56010818
        },
        {
            lng: 4.58643007,
            lat: 6.57925701
        },
        {
            lng: 4.585742,
            lat: 6.59380293
        },
        {
            lng: 4.58595896,
            lat: 6.60717916
        },
        {
            lng: 4.58264208,
            lat: 6.61646414
        },
        {
            lng: 4.572227,
            lat: 6.62909985
        },
        {
            lng: 4.56196404,
            lat: 6.62871504
        },
        {
            lng: 4.5595212,
            lat: 6.62862396
        },
        {
            lng: 4.54369402,
            lat: 6.62081718
        },
        {
            lng: 4.53359318,
            lat: 6.60991621
        },
        {
            lng: 4.53023815,
            lat: 6.6023612
        },
        {
            lng: 4.52617884,
            lat: 6.59412384
        },
        {
            lng: 4.51701498,
            lat: 6.58412981
        },
        {
            lng: 4.50681305,
            lat: 6.57743979
        },
        {
            lng: 4.50021505,
            lat: 6.57311296
        },
        {
            lng: 4.49411392,
            lat: 6.56698895
        },
        {
            lng: 4.48832989,
            lat: 6.56616402
        },
        {
            lng: 4.48419714,
            lat: 6.56761789
        },
        {
            lng: 4.47981024,
            lat: 6.56769323
        },
        {
            lng: 4.47980309,
            lat: 6.56723309
        },
        {
            lng: 4.47168684,
            lat: 6.565063
        },
        {
            lng: 4.46452904,
            lat: 6.5649538
        },
        {
            lng: 4.45695305,
            lat: 6.5676198
        },
        {
            lng: 4.44495821,
            lat: 6.56828785
        },
        {
            lng: 4.43135118,
            lat: 6.56921101
        },
        {
            lng: 4.4205389,
            lat: 6.571702
        },
        {
            lng: 4.40560102,
            lat: 6.57588005
        },
        {
            lng: 4.39890814,
            lat: 6.57599401
        },
        {
            lng: 4.38693285,
            lat: 6.57804298
        },
        {
            lng: 4.36960697,
            lat: 6.59171677
        },
        {
            lng: 4.36047888,
            lat: 6.59833384
        },
        {
            lng: 4.35442781,
            lat: 6.60974121
        },
        {
            lng: 4.35313797,
            lat: 6.61816883
        },
        {
            lng: 4.35234022,
            lat: 6.62338781
        },
        {
            lng: 4.35575008,
            lat: 6.63440323
        },
        {
            lng: 4.35511589,
            lat: 6.65240908
        },
        {
            lng: 4.35393095,
            lat: 6.66488695
        },
        {
            lng: 4.35775805,
            lat: 6.67312813
        },
        {
            lng: 4.36325979,
            lat: 6.68502998
        },
        {
            lng: 4.37699413,
            lat: 6.70625114
        },
        {
            lng: 4.38387394,
            lat: 6.7178998
        },
        {
            lng: 4.39682007,
            lat: 6.73313618
        },
        {
            lng: 4.40208006,
            lat: 6.74435091
        },
        {
            lng: 4.41403198,
            lat: 6.7554512
        },
        {
            lng: 4.42922211,
            lat: 6.76672506
        },
        {
            lng: 4.4433012,
            lat: 6.78078985
        },
        {
            lng: 4.46065187,
            lat: 6.79733419
        },
        {
            lng: 4.47463703,
            lat: 6.80563307
        },
        {
            lng: 4.49391794,
            lat: 6.81291723
        },
        {
            lng: 4.50681305,
            lat: 6.82419014
        },
        {
            lng: 4.50867081,
            lat: 6.82581377
        },
        {
            lng: 4.52080011,
            lat: 6.83345222
        },
        {
            lng: 4.53186607,
            lat: 6.84687614
        },
        {
            lng: 4.53742409,
            lat: 6.85567284
        },
        {
            lng: 4.53849983,
            lat: 6.85737419
        },
        {
            lng: 4.54157495,
            lat: 6.87623978
        },
        {
            lng: 4.54910517,
            lat: 6.89941216
        },
        {
            lng: 4.55132294,
            lat: 6.92244482
        },
        {
            lng: 4.55076599,
            lat: 6.93099022
        },
        {
            lng: 4.55264711,
            lat: 6.93303394
        },
        {
            lng: 4.55562115,
            lat: 6.94567204
        },
        {
            lng: 4.55969191,
            lat: 6.9545989
        },
        {
            lng: 4.55820084,
            lat: 6.97677279
        },
        {
            lng: 4.56109285,
            lat: 6.98834181
        },
        {
            lng: 4.56254005,
            lat: 6.98823214
        },
        {
            lng: 4.57330704,
            lat: 6.99727821
        },
        {
            lng: 4.58256483,
            lat: 6.99873495
        },
        {
            lng: 4.59490585,
            lat: 7.00521421
        },
        {
            lng: 4.59532595,
            lat: 7.00548792
        },
        {
            lng: 4.60423994,
            lat: 7.01128578
        },
        {
            lng: 4.61179495,
            lat: 7.02176809
        },
        {
            lng: 4.61943483,
            lat: 7.03732491
        },
        {
            lng: 4.62093401,
            lat: 7.04445219
        },
        {
            lng: 4.62296009,
            lat: 7.05549002
        },
        {
            lng: 4.62830496,
            lat: 7.07200909
        },
        {
            lng: 4.63900518,
            lat: 7.09143591
        },
        {
            lng: 4.6507411,
            lat: 7.10323286
        },
        {
            lng: 4.67042303,
            lat: 7.12112379
        },
        {
            lng: 4.68373394,
            lat: 7.13058519
        },
        {
            lng: 4.69258785,
            lat: 7.13550997
        },
        {
            lng: 4.69285488,
            lat: 7.13781214
        },
        {
            lng: 4.69543505,
            lat: 7.14030504
        },
        {
            lng: 4.70824099,
            lat: 7.14700794
        },
        {
            lng: 4.71108103,
            lat: 7.14747095
        },
        {
            lng: 4.72490788,
            lat: 7.14972305
        },
        {
            lng: 4.74564886,
            lat: 7.14729309
        },
        {
            lng: 4.76147699,
            lat: 7.14102602
        },
        {
            lng: 4.76298618,
            lat: 7.13430882
        },
        {
            lng: 4.76962709,
            lat: 7.13096619
        },
        {
            lng: 4.78619003,
            lat: 7.12722492
        },
        {
            lng: 4.80577183,
            lat: 7.11028004
        },
        {
            lng: 4.81989384,
            lat: 7.09827423
        },
        {
            lng: 4.83317423,
            lat: 7.0913558
        },
        {
            lng: 4.8499918,
            lat: 7.08922482
        },
        {
            lng: 4.86248589,
            lat: 7.09085703
        },
        {
            lng: 4.86935806,
            lat: 7.09825802
        },
        {
            lng: 4.87070894,
            lat: 7.0997138
        },
        {
            lng: 4.87274218,
            lat: 7.11121511
        },
        {
            lng: 4.87084579,
            lat: 7.12255096
        },
        {
            lng: 4.86901093,
            lat: 7.13757992
        },
        {
            lng: 4.87056303,
            lat: 7.14793396
        },
        {
            lng: 4.87556219,
            lat: 7.17161179
        },
        {
            lng: 4.87295389,
            lat: 7.18778181
        },
        {
            lng: 4.87157297,
            lat: 7.19636297
        },
        {
            lng: 4.86950588,
            lat: 7.21139383
        },
        {
            lng: 4.86805391,
            lat: 7.23587179
        },
        {
            lng: 4.87363291,
            lat: 7.25261879
        },
        {
            lng: 4.87745285,
            lat: 7.26039791
        },
        {
            lng: 4.87729597,
            lat: 7.26501322
        },
        {
            lng: 4.87804794,
            lat: 7.28299522
        },
        {
            lng: 4.87978697,
            lat: 7.2905798
        },
        {
            lng: 4.88058186,
            lat: 7.29238796
        },
        {
            lng: 4.88361788,
            lat: 7.29928017
        },
        {
            lng: 4.89330196,
            lat: 7.34133387
        },
        {
            lng: 4.90683985,
            lat: 7.35056019
        },
        {
            lng: 4.91402197,
            lat: 7.35205317
        },
        {
            lng: 4.94188023,
            lat: 7.36149979
        },
        {
            lng: 4.94906807,
            lat: 7.36345291
        },
        {
            lng: 4.97063303,
            lat: 7.36931276
        },
        {
            lng: 4.99237394,
            lat: 7.37171078
        },
        {
            lng: 5.00145197,
            lat: 7.37617111
        },
        {
            lng: 4.9992938,
            lat: 7.39997005
        },
        {
            lng: 4.998703,
            lat: 7.40110016
        },
        {
            lng: 4.99148703,
            lat: 7.41490507
        },
        {
            lng: 5.00094986,
            lat: 7.41678286
        },
        {
            lng: 5.01767015,
            lat: 7.42272711
        },
        {
            lng: 5.04301023,
            lat: 7.4336009
        },
        {
            lng: 5.06501913,
            lat: 7.43830013
        },
        {
            lng: 5.0965991,
            lat: 7.4352231
        },
        {
            lng: 5.10900402,
            lat: 7.436759
        },
        {
            lng: 5.11788416,
            lat: 7.43786001
        },
        {
            lng: 5.14113998,
            lat: 7.43400192
        },
        {
            lng: 5.16030502,
            lat: 7.43413782
        },
        {
            lng: 5.17518091,
            lat: 7.43189621
        },
        {
            lng: 5.17757893,
            lat: 7.4315362
        },
        {
            lng: 5.19073391,
            lat: 7.43131018
        },
        {
            lng: 5.20597887,
            lat: 7.43174314
        },
        {
            lng: 5.23321295,
            lat: 7.43104982
        },
        {
            lng: 5.25146389,
            lat: 7.43189096
        },
        {
            lng: 5.26951885,
            lat: 7.43481302
        },
        {
            lng: 5.28361607,
            lat: 7.43549681
        },
        {
            lng: 5.29517317,
            lat: 7.43645191
        },
        {
            lng: 5.29773521,
            lat: 7.43779182
        },
        {
            lng: 5.30328512,
            lat: 7.43629885
        },
        {
            lng: 5.31424999,
            lat: 7.43104982
        },
        {
            lng: 5.32827997,
            lat: 7.42781401
        },
        {
            lng: 5.33257198,
            lat: 7.42174196
        },
        {
            lng: 5.33365011,
            lat: 7.41711092
        },
        {
            lng: 5.34430885,
            lat: 7.4051609
        },
        {
            lng: 5.35125589,
            lat: 7.39212513
        },
        {
            lng: 5.34974003,
            lat: 7.38407707
        },
        {
            lng: 5.35507584,
            lat: 7.37129784
        },
        {
            lng: 5.36251307,
            lat: 7.35986614
        },
        {
            lng: 5.36797285,
            lat: 7.3406229
        },
        {
            lng: 5.36544085,
            lat: 7.32682514
        },
        {
            lng: 5.37325382,
            lat: 7.31008291
        },
        {
            lng: 5.3812418,
            lat: 7.29344511
        },
        {
            lng: 5.38139105,
            lat: 7.28825283
        },
        {
            lng: 5.38200808,
            lat: 7.28641176
        },
        {
            lng: 5.38298798,
            lat: 7.28347778
        },
        {
            lng: 5.39977884,
            lat: 7.27987099
        },
        {
            lng: 5.41977119,
            lat: 7.27399302
        },
        {
            lng: 5.44513607,
            lat: 7.28624821
        },
        {
            lng: 5.45827723,
            lat: 7.29511786
        },
        {
            lng: 5.45913887,
            lat: 7.29569912
        },
        {
            lng: 5.46891212,
            lat: 7.30037785
        },
        {
            lng: 5.49390221,
            lat: 7.31817579
        },
        {
            lng: 5.50568295,
            lat: 7.32763004
        },
        {
            lng: 5.50910378,
            lat: 7.33037615
        },
        {
            lng: 5.51830101,
            lat: 7.34221411
        },
        {
            lng: 5.53480577,
            lat: 7.3636198
        },
        {
            lng: 5.54645014,
            lat: 7.38418388
        },
        {
            lng: 5.55827713,
            lat: 7.40174723
        },
        {
            lng: 5.56856918,
            lat: 7.42441082
        },
        {
            lng: 5.57816696,
            lat: 7.44685602
        },
        {
            lng: 5.58584499,
            lat: 7.46495008
        },
        {
            lng: 5.59100723,
            lat: 7.48447084
        },
        {
            lng: 5.59152889,
            lat: 7.48822689
        },
        {
            lng: 5.59257507,
            lat: 7.49574804
        },
        {
            lng: 5.59515285,
            lat: 7.51254511
        },
        {
            lng: 5.59748888,
            lat: 7.52582598
        },
        {
            lng: 5.59871292,
            lat: 7.53278589
        },
        {
            lng: 5.60115385,
            lat: 7.54104996
        },
        {
            lng: 5.60767221,
            lat: 7.55870199
        },
        {
            lng: 5.60837889,
            lat: 7.5643568
        },
        {
            lng: 5.61032104,
            lat: 7.57988214
        },
        {
            lng: 5.61334515,
            lat: 7.59551716
        },
        {
            lng: 5.61789703,
            lat: 7.60582113
        },
        {
            lng: 5.62366295,
            lat: 7.61979723
        },
        {
            lng: 5.6316781,
            lat: 7.63004208
        },
        {
            lng: 5.64243793,
            lat: 7.63885498
        },
        {
            lng: 5.64901781,
            lat: 7.645895
        },
        {
            lng: 5.66006708,
            lat: 7.65816402
        },
        {
            lng: 5.66378498,
            lat: 7.66240978
        },
        {
            lng: 5.67041492,
            lat: 7.66998577
        },
        {
            lng: 5.68481922,
            lat: 7.67550611
        },
        {
            lng: 5.69345188,
            lat: 7.68112612
        },
        {
            lng: 5.7056551,
            lat: 7.69337606
        },
        {
            lng: 5.71046114,
            lat: 7.70506191
        },
        {
            lng: 5.7154851,
            lat: 7.71604681
        },
        {
            lng: 5.71934986,
            lat: 7.72659492
        },
        {
            lng: 5.72956085,
            lat: 7.72988081
        },
        {
            lng: 5.73770809,
            lat: 7.73412514
        },
        {
            lng: 5.73858786,
            lat: 7.73134184
        },
        {
            lng: 5.74946404,
            lat: 7.73277092
        },
        {
            lng: 5.75541306,
            lat: 7.72943878
        },
        {
            lng: 5.76526499,
            lat: 7.72488785
        },
        {
            lng: 5.7688508,
            lat: 7.71813679
        },
        {
            lng: 5.77361393,
            lat: 7.71275091
        },
        {
            lng: 5.78452206,
            lat: 7.71625423
        },
        {
            lng: 5.78685093,
            lat: 7.71760082
        },
        {
            lng: 5.79252195,
            lat: 7.72557783
        },
        {
            lng: 5.79498482,
            lat: 7.73522615
        },
        {
            lng: 5.80319309,
            lat: 7.74315977
        },
        {
            lng: 5.80981302,
            lat: 7.75273705
        },
        {
            lng: 5.81142521,
            lat: 7.75614595
        },
        {
            lng: 5.81701279,
            lat: 7.75538301
        },
        {
            lng: 5.82697105,
            lat: 7.75728798
        },
        {
            lng: 5.83554697,
            lat: 7.75944901
        },
        {
            lng: 5.84251022,
            lat: 7.76163721
        },
        {
            lng: 5.85084915,
            lat: 7.76334095
        },
        {
            lng: 5.85940695,
            lat: 7.76434898
        },
        {
            lng: 5.86819315,
            lat: 7.76512098
        },
        {
            lng: 5.87656498,
            lat: 7.76890182
        },
        {
            lng: 5.88605499,
            lat: 7.77035379
        },
        {
            lng: 5.89806414,
            lat: 7.7706089
        },
        {
            lng: 5.90935898,
            lat: 7.76949501
        },
        {
            lng: 5.92040396,
            lat: 7.76723194
        },
        {
            lng: 5.92657804,
            lat: 7.76343489
        },
        {
            lng: 5.92277002,
            lat: 7.75634718
        },
        {
            lng: 5.9219718,
            lat: 7.74990082
        },
        {
            lng: 5.92022419,
            lat: 7.74162579
        },
        {
            lng: 5.91927099,
            lat: 7.72549391
        },
        {
            lng: 5.91591597,
            lat: 7.71793604
        },
        {
            lng: 5.91551304,
            lat: 7.70710087
        },
        {
            lng: 5.91511393,
            lat: 7.6967268
        },
        {
            lng: 5.92642117,
            lat: 7.68200111
        },
        {
            lng: 5.93004513,
            lat: 7.67637396
        },
        {
            lng: 5.93182707,
            lat: 7.67360306
        },
        {
            lng: 5.94228411,
            lat: 7.66350412
        },
        {
            lng: 5.94812298,
            lat: 7.65325499
        },
        {
            lng: 5.95972586,
            lat: 7.64267492
        },
        {
            lng: 5.9729681,
            lat: 7.63345289
        },
        {
            lng: 5.98027706,
            lat: 7.62848282
        },
        {
            lng: 5.99028015,
            lat: 7.61885309
        },
        {
            lng: 6.00231791,
            lat: 7.60733223
        },
        {
            lng: 6.01187897,
            lat: 7.59818411
        },
        {
            lng: 6.01983976,
            lat: 7.59066677
        },
        {
            lng: 6.0246129,
            lat: 7.58596992
        },
        {
            lng: 6.02251101,
            lat: 7.58439016
        },
        {
            lng: 6.0283761,
            lat: 7.57342291
        },
        {
            lng: 6.02643824,
            lat: 7.57025099
        },
        {
            lng: 6.02240181,
            lat: 7.56339979
        },
        {
            lng: 6.01743412,
            lat: 7.55587196
        },
        {
            lng: 6.0115881,
            lat: 7.55135679
        },
        {
            lng: 6.00550508,
            lat: 7.54638386
        },
        {
            lng: 6.00211811,
            lat: 7.53675413
        },
        {
            lng: 5.99785614,
            lat: 7.53013611
        },
        {
            lng: 5.99385405,
            lat: 7.52535915
        },
        {
            lng: 5.99218321,
            lat: 7.52192783
        },
        {
            lng: 5.99012518,
            lat: 7.51894999
        },
        {
            lng: 5.98887396,
            lat: 7.517138
        },
        {
            lng: 5.98559904,
            lat: 7.51442623
        },
        {
            lng: 5.98256302,
            lat: 7.51240206
        },
        {
            lng: 5.97769022,
            lat: 7.51086998
        },
        {
            lng: 5.9744029,
            lat: 7.50746679
        },
        {
            lng: 5.96955299,
            lat: 7.50731707
        },
        {
            lng: 5.96240187,
            lat: 7.50766993
        },
        {
            lng: 5.95500803,
            lat: 7.50733519
        },
        {
            lng: 5.95134497,
            lat: 7.50924301
        },
        {
            lng: 5.94927406,
            lat: 7.50973988
        },
        {
            lng: 5.94600487,
            lat: 7.50748777
        },
        {
            lng: 5.94336224,
            lat: 7.50107384
        },
        {
            lng: 5.93796492,
            lat: 7.49562979
        },
        {
            lng: 5.93115902,
            lat: 7.48882389
        },
        {
            lng: 5.92643213,
            lat: 7.48198414
        },
        {
            lng: 5.9241991,
            lat: 7.472332
        },
        {
            lng: 5.92353296,
            lat: 7.4596529
        },
        {
            lng: 5.92821503,
            lat: 7.44919395
        },
        {
            lng: 5.93000984,
            lat: 7.44616318
        },
        {
            lng: 5.93369389,
            lat: 7.44540882
        },
        {
            lng: 5.93868494,
            lat: 7.44001579
        },
        {
            lng: 5.94213295,
            lat: 7.43903589
        },
        {
            lng: 5.94743395,
            lat: 7.43848515
        },
        {
            lng: 5.9518199,
            lat: 7.43840981
        },
        {
            lng: 5.95780706,
            lat: 7.43738413
        },
        {
            lng: 5.96258211,
            lat: 7.43292093
        },
        {
            lng: 5.96174097,
            lat: 7.42370701
        },
        {
            lng: 5.95766592,
            lat: 7.41431713
        },
        {
            lng: 5.95298195,
            lat: 7.41024399
        },
        {
            lng: 5.94887686,
            lat: 7.39901018
        },
        {
            lng: 5.94844198,
            lat: 7.38632917
        },
        {
            lng: 5.95037699,
            lat: 7.37752914
        },
        {
            lng: 5.95574903,
            lat: 7.36705685
        },
        {
            lng: 5.95949984,
            lat: 7.3561492
        },
        {
            lng: 5.96095705,
            lat: 7.34620523
        },
        {
            lng: 5.96781015,
            lat: 7.34170389
        },
        {
            lng: 5.96644211,
            lat: 7.33985996
        },
        {
            lng: 5.96072912,
            lat: 7.33213615
        },
        {
            lng: 5.95824909,
            lat: 7.32156515
        },
        {
            lng: 5.95481682,
            lat: 7.30916691
        },
        {
            lng: 5.952806,
            lat: 7.2990489
        },
        {
            lng: 5.9499259,
            lat: 7.29217815
        },
        {
            lng: 5.94472694,
            lat: 7.28465223
        },
        {
            lng: 5.93497896,
            lat: 7.28159094
        },
        {
            lng: 5.93264294,
            lat: 7.2797842
        },
        {
            lng: 5.92543221,
            lat: 7.27644682
        },
        {
            lng: 5.92004395,
            lat: 7.27169323
        },
        {
            lng: 5.91586208,
            lat: 7.26991987
        },
        {
            lng: 5.91005182,
            lat: 7.26771116
        },
        {
            lng: 5.90347004,
            lat: 7.26044083
        },
        {
            lng: 5.89670801,
            lat: 7.25617218
        },
        {
            lng: 5.89224291,
            lat: 7.25140381
        },
        {
            lng: 5.89060879,
            lat: 7.24930096
        },
        {
            lng: 5.88941479,
            lat: 7.2477622
        },
        {
            lng: 5.88540602,
            lat: 7.2425251
        },
        {
            lng: 5.88163996,
            lat: 7.23820591
        },
        {
            lng: 5.87781096,
            lat: 7.22973394
        },
        {
            lng: 5.875453,
            lat: 7.22654581
        },
        {
            lng: 5.87398481,
            lat: 7.22149515
        },
        {
            lng: 5.87480307,
            lat: 7.21502018
        },
        {
            lng: 5.87400818,
            lat: 7.20857477
        },
        {
            lng: 5.87833309,
            lat: 7.20481014
        },
        {
            lng: 5.8763938,
            lat: 7.19907522
        },
        {
            lng: 5.87215185,
            lat: 7.19384193
        },
        {
            lng: 5.86884499,
            lat: 7.18905401
        },
        {
            lng: 5.86504793,
            lat: 7.18265915
        },
        {
            lng: 5.86356211,
            lat: 7.17645502
        },
        {
            lng: 5.86233187,
            lat: 7.17163181
        },
        {
            lng: 5.85988379,
            lat: 7.16313791
        },
        {
            lng: 5.85606194,
            lat: 7.15512705
        },
        {
            lng: 5.8517952,
            lat: 7.14828014
        },
        {
            lng: 5.84729624,
            lat: 7.14120579
        },
        {
            lng: 5.84392786,
            lat: 7.13272476
        },
        {
            lng: 5.83963823,
            lat: 7.12449312
        },
        {
            lng: 5.83788109,
            lat: 7.11575699
        },
        {
            lng: 5.83520508,
            lat: 7.1072669
        },
        {
            lng: 5.83232021,
            lat: 7.10016489
        },
        {
            lng: 5.83068085,
            lat: 7.09880686
        },
        {
            lng: 5.82693005,
            lat: 7.09518099
        },
        {
            lng: 5.82427216,
            lat: 7.08784389
        },
        {
            lng: 5.82440901,
            lat: 7.08207321
        },
        {
            lng: 5.82808113,
            lat: 7.08062887
        },
        {
            lng: 5.82936382,
            lat: 7.07437706
        },
        {
            lng: 5.82652378,
            lat: 7.07004213
        },
        {
            lng: 5.82337189,
            lat: 7.06063604
        },
        {
            lng: 5.81737614,
            lat: 7.06097078
        },
        {
            lng: 5.8173852,
            lat: 7.0616622
        },
        {
            lng: 5.80851412,
            lat: 7.055583
        },
        {
            lng: 5.80382395,
            lat: 7.05105019
        },
        {
            lng: 5.79894018,
            lat: 7.04882479
        },
        {
            lng: 5.79329014,
            lat: 7.0420022
        },
        {
            lng: 5.78717184,
            lat: 7.03495216
        },
        {
            lng: 5.78597403,
            lat: 7.03220606
        },
        {
            lng: 5.7845149,
            lat: 7.02761602
        },
        {
            lng: 5.77888298,
            lat: 7.021945
        },
        {
            lng: 5.77870798,
            lat: 7.01110601
        },
        {
            lng: 5.77535677,
            lat: 7.00378084
        },
        {
            lng: 5.77359581,
            lat: 6.99481201
        },
        {
            lng: 5.77093077,
            lat: 6.98701477
        },
        {
            lng: 5.76963711,
            lat: 6.97826815
        },
        {
            lng: 5.76971197,
            lat: 6.96857882
        },
        {
            lng: 5.77404499,
            lat: 6.96527481
        },
        {
            lng: 5.77329683,
            lat: 6.96182585
        },
        {
            lng: 5.77206898,
            lat: 6.95723391
        },
        {
            lng: 5.76890516,
            lat: 6.94713688
        },
        {
            lng: 5.77276182,
            lat: 6.94291878
        },
        {
            lng: 5.77639389,
            lat: 6.93916702
        },
        {
            lng: 5.78321791,
            lat: 6.93282223
        },
        {
            lng: 5.78796577,
            lat: 6.92651081
        },
        {
            lng: 5.79061604,
            lat: 6.91908312
        },
        {
            lng: 5.79052496,
            lat: 6.9133172
        },
        {
            lng: 5.79029322,
            lat: 6.91332197
        },
        {
            lng: 5.78677607,
            lat: 6.90992117
        },
        {
            lng: 5.78513288,
            lat: 6.90833521
        },
        {
            lng: 5.7849021,
            lat: 6.90833998
        },
        {
            lng: 5.78097105,
            lat: 6.90794277
        },
        {
            lng: 5.77630615,
            lat: 6.9050231
        },
        {
            lng: 5.77612591,
            lat: 6.89372301
        },
        {
            lng: 5.77854109,
            lat: 6.88606977
        },
        {
            lng: 5.77611399,
            lat: 6.87872887
        },
        {
            lng: 5.7711072,
            lat: 6.88319778
        },
        {
            lng: 5.7568531,
            lat: 6.88689995
        },
        {
            lng: 5.75286198,
            lat: 6.88281584
        },
        {
            lng: 5.74755383,
            lat: 6.86860323
        },
        {
            lng: 5.74755001,
            lat: 6.86837292
        },
        {
            lng: 5.74497414,
            lat: 6.85180616
        },
        {
            lng: 5.74434519,
            lat: 6.84143591
        },
        {
            lng: 5.74161291,
            lat: 6.82948399
        },
        {
            lng: 5.74050522,
            lat: 6.81796694
        },
        {
            lng: 5.73749018,
            lat: 6.80279493
        },
        {
            lng: 5.73288822,
            lat: 6.78949213
        },
        {
            lng: 5.72572184,
            lat: 6.77461815
        },
        {
            lng: 5.72196913,
            lat: 6.76871681
        },
        {
            lng: 5.72122288,
            lat: 6.76754379
        },
        {
            lng: 5.71616411,
            lat: 6.75447989
        },
        {
            lng: 5.7130599,
            lat: 6.7480731
        },
        {
            lng: 5.69239712,
            lat: 6.74104214
        },
        {
            lng: 5.67941713,
            lat: 6.73780298
        },
        {
            lng: 5.66984892,
            lat: 6.73150778
        },
        {
            lng: 5.6567812,
            lat: 6.73726797
        },
        {
            lng: 5.64482498,
            lat: 6.75454283
        },
        {
            lng: 5.63753986,
            lat: 6.76112604
        },
        {
            lng: 5.63016987,
            lat: 6.76217413
        },
        {
            lng: 5.61587,
            lat: 6.76311111
        },
        {
            lng: 5.60048819,
            lat: 6.75414515
        },
        {
            lng: 5.58871698,
            lat: 6.74004078
        },
        {
            lng: 5.57983494,
            lat: 6.73327208
        },
        {
            lng: 5.57312489,
            lat: 6.73246288
        },
        {
            lng: 5.56820917,
            lat: 6.72816515
        },
        {
            lng: 5.56160593,
            lat: 6.71951103
        },
        {
            lng: 5.55501986,
            lat: 6.71200991
        },
        {
            lng: 5.54300594,
            lat: 6.71152306
        },
        {
            lng: 5.53903198,
            lat: 6.72266293
        },
        {
            lng: 5.53070116,
            lat: 6.73595524
        },
        {
            lng: 5.52907419,
            lat: 6.74959278
        },
        {
            lng: 5.52559614,
            lat: 6.76303577
        },
        {
            lng: 5.51634312,
            lat: 6.77611113
        },
        {
            lng: 5.51309013,
            lat: 6.78931618
        },
        {
            lng: 5.50910378,
            lat: 6.79976606
        },
        {
            lng: 5.5140481,
            lat: 6.8056798
        },
        {
            lng: 5.52340984,
            lat: 6.81359577
        },
        {
            lng: 5.53369808,
            lat: 6.82172585
        },
        {
            lng: 5.53961086,
            lat: 6.83039093
        },
        {
            lng: 5.54155016,
            lat: 6.8361249
        },
        {
            lng: 5.5378809,
            lat: 6.8419528
        },
        {
            lng: 5.53479719,
            lat: 6.84685278
        },
        {
            lng: 5.5291791,
            lat: 6.85640717
        },
        {
            lng: 5.51915979,
            lat: 6.86511087
        },
        {
            lng: 5.51346397,
            lat: 6.86982393
        },
        {
            lng: 5.49966621,
            lat: 6.87328911
        },
        {
            lng: 5.48700094,
            lat: 6.87535095
        },
        {
            lng: 5.47472095,
            lat: 6.87256193
        },
        {
            lng: 5.45207119,
            lat: 6.8711009
        },
        {
            lng: 5.43379879,
            lat: 6.86910677
        },
        {
            lng: 5.42507696,
            lat: 6.86925507
        },
        {
            lng: 5.4146409,
            lat: 6.86943197
        },
        {
            lng: 5.393682,
            lat: 6.87255716
        },
        {
            lng: 5.37127781,
            lat: 6.87201691
        },
        {
            lng: 5.34913921,
            lat: 6.87354898
        },
        {
            lng: 5.3264699,
            lat: 6.87093401
        },
        {
            lng: 5.30967808,
            lat: 6.87468386
        },
        {
            lng: 5.29932117,
            lat: 6.87670517
        },
        {
            lng: 5.2829051,
            lat: 6.87513924
        },
        {
            lng: 5.26251316,
            lat: 6.8706398
        },
        {
            lng: 5.24475193,
            lat: 6.87163591
        },
        {
            lng: 5.22821712,
            lat: 6.87699223
        },
        {
            lng: 5.20313501,
            lat: 6.86796188
        },
        {
            lng: 5.19262981,
            lat: 6.86075783
        },
        {
            lng: 5.18526888,
            lat: 6.84819508
        },
        {
            lng: 5.18604422,
            lat: 6.83895397
        },
        {
            lng: 5.18660021,
            lat: 6.83040905
        },
        {
            lng: 5.18722391,
            lat: 6.82624483
        },
        {
            lng: 5.18383694,
            lat: 6.8166132
        },
        {
            lng: 5.1736269,
            lat: 6.81332684
        },
        {
            lng: 5.16842413,
            lat: 6.80557299
        },
        {
            lng: 5.16127205,
            lat: 6.79162312
        },
        {
            lng: 5.15809679,
            lat: 6.78083277
        },
        {
            lng: 5.15752697,
            lat: 6.77415323
        },
        {
            lng: 5.15559387,
            lat: 6.76887894
        },
        {
            lng: 5.15350723,
            lat: 6.76822281
        },
        {
            lng: 5.13890696,
            lat: 6.764781
        },
        {
            lng: 5.12768221,
            lat: 6.75597477
        },
        {
            lng: 5.12408495,
            lat: 6.74773121
        },
        {
            lng: 5.12048721,
            lat: 6.73925686
        },
        {
            lng: 5.11417294,
            lat: 6.73428917
        },
        {
            lng: 5.10434818,
            lat: 6.72637987
        },
        {
            lng: 5.09679985,
            lat: 6.71824121
        },
        {
            lng: 5.09213018,
            lat: 6.71320915
        },
        {
            lng: 5.08426189,
            lat: 6.69765615
        },
        {
            lng: 5.08383417,
            lat: 6.68543577
        },
        {
            lng: 5.0822649,
            lat: 6.6741581
        },
        {
            lng: 5.08403015,
            lat: 6.66882277
        },
        {
            lng: 5.08207417,
            lat: 6.66216612
        },
        {
            lng: 5.08576202,
            lat: 6.66187191
        },
        {
            lng: 5.07549906,
            lat: 6.65535498
        },
        {
            lng: 5.07280922,
            lat: 6.64594316
        },
        {
            lng: 5.07542992,
            lat: 6.63667107
        },
        {
            lng: 5.07456207,
            lat: 6.62584305
        },
        {
            lng: 5.07227802,
            lat: 6.61296082
        },
        {
            lng: 5.07276106,
            lat: 6.60003424
        },
        {
            lng: 5.07985592,
            lat: 6.59622288
        },
        {
            lng: 5.08605289,
            lat: 6.59381008
        },
        {
            lng: 5.09268284,
            lat: 6.58977604
        },
        {
            lng: 5.10037804,
            lat: 6.58018494
        },
        {
            lng: 5.10316277,
            lat: 6.56675577
        },
        {
            lng: 5.11109686,
            lat: 6.55762577
        },
        {
            lng: 5.11517811,
            lat: 6.55293989
        },
        {
            lng: 5.12118578,
            lat: 6.53899622
        },
        {
            lng: 5.12449598,
            lat: 6.52948284
        },
        {
            lng: 5.12127304,
            lat: 6.5156951
        },
        {
            lng: 5.11873817,
            lat: 6.50166416
        },
        {
            lng: 5.11126709,
            lat: 6.48218393
        },
        {
            lng: 5.1053009,
            lat: 6.47005701
        },
        {
            lng: 5.09472799,
            lat: 6.45870304
        },
        {
            lng: 5.08607578,
            lat: 6.45192909
        },
        {
            lng: 5.07714891,
            lat: 6.44239283
        },
        {
            lng: 5.07525492,
            lat: 6.42512178
        },
        {
            lng: 5.07357693,
            lat: 6.4069252
        },
        {
            lng: 5.06805277,
            lat: 6.39364004
        },
        {
            lng: 5.06347895,
            lat: 6.3819499
        },
        {
            lng: 5.05464506,
            lat: 6.37818098
        },
        {
            lng: 5.03440809,
            lat: 6.36883593
        },
        {
            lng: 5.02498388,
            lat: 6.37153482
        },
        {
            lng: 5.01592398,
            lat: 6.36799812
        },
        {
            lng: 5.01347017,
            lat: 6.35904121
        },
        {
            lng: 5.0065279,
            lat: 6.358006
        },
        {
            lng: 4.99919605,
            lat: 6.34705877
        },
        {
            lng: 4.99805784,
            lat: 6.33392715
        },
        {
            lng: 4.99714422,
            lat: 6.32009888
        },
        {
            lng: 4.99472618,
            lat: 6.31304121
        }
    ];

    let EdoStateBoundaries = [{
            lng: 5.88308096,
            lat: 5.78829479
        },
        {
            lng: 5.88140821,
            lat: 5.78987694
        },
        {
            lng: 5.86892509,
            lat: 5.80323887
        },
        {
            lng: 5.87421703,
            lat: 5.8165288
        },
        {
            lng: 5.88196421,
            lat: 5.82447195
        },
        {
            lng: 5.89481783,
            lat: 5.84847498
        },
        {
            lng: 5.90183592,
            lat: 5.8684268
        },
        {
            lng: 5.89420319,
            lat: 5.88193798
        },
        {
            lng: 5.86923504,
            lat: 5.8941288
        },
        {
            lng: 5.84685612,
            lat: 5.90950584
        },
        {
            lng: 5.84433794,
            lat: 5.91093302
        },
        {
            lng: 5.83109093,
            lat: 5.91992617
        },
        {
            lng: 5.82482576,
            lat: 5.93225908
        },
        {
            lng: 5.81331587,
            lat: 5.94860506
        },
        {
            lng: 5.79881382,
            lat: 5.96569395
        },
        {
            lng: 5.7764039,
            lat: 5.97899485
        },
        {
            lng: 5.7571702,
            lat: 5.98901081
        },
        {
            lng: 5.73612881,
            lat: 6.00136709
        },
        {
            lng: 5.72330713,
            lat: 6.00804377
        },
        {
            lng: 5.72289181,
            lat: 6.00822878
        },
        {
            lng: 5.69878101,
            lat: 6.01907587
        },
        {
            lng: 5.68063879,
            lat: 6.02515221
        },
        {
            lng: 5.6721468,
            lat: 6.02829504
        },
        {
            lng: 5.66333103,
            lat: 6.03997993
        },
        {
            lng: 5.65374994,
            lat: 6.04706287
        },
        {
            lng: 5.64571285,
            lat: 6.0499692
        },
        {
            lng: 5.63875103,
            lat: 6.04755116
        },
        {
            lng: 5.62923288,
            lat: 6.04448318
        },
        {
            lng: 5.61666393,
            lat: 6.03800821
        },
        {
            lng: 5.59764194,
            lat: 6.03256321
        },
        {
            lng: 5.58600187,
            lat: 6.02653313
        },
        {
            lng: 5.56303978,
            lat: 6.02000284
        },
        {
            lng: 5.55703688,
            lat: 6.01420307
        },
        {
            lng: 5.54591894,
            lat: 6.00345421
        },
        {
            lng: 5.54348993,
            lat: 5.99588299
        },
        {
            lng: 5.53309917,
            lat: 5.98152685
        },
        {
            lng: 5.50457287,
            lat: 5.98801088
        },
        {
            lng: 5.4838419,
            lat: 6.00520515
        },
        {
            lng: 5.46791792,
            lat: 6.02000999
        },
        {
            lng: 5.44560099,
            lat: 6.02477598
        },
        {
            lng: 5.40647507,
            lat: 6.01805878
        },
        {
            lng: 5.40236998,
            lat: 6.01663923
        },
        {
            lng: 5.39440203,
            lat: 6.01388121
        },
        {
            lng: 5.36559391,
            lat: 6.01714087
        },
        {
            lng: 5.34633493,
            lat: 6.02554417
        },
        {
            lng: 5.33032417,
            lat: 6.02050877
        },
        {
            lng: 5.320086,
            lat: 6.01560879
        },
        {
            lng: 5.31912708,
            lat: 5.99901485
        },
        {
            lng: 5.32721615,
            lat: 5.98526716
        },
        {
            lng: 5.33349514,
            lat: 5.97362518
        },
        {
            lng: 5.32513714,
            lat: 5.95646286
        },
        {
            lng: 5.30271292,
            lat: 5.94023609
        },
        {
            lng: 5.2893548,
            lat: 5.92800617
        },
        {
            lng: 5.26877213,
            lat: 5.91151619
        },
        {
            lng: 5.25895786,
            lat: 5.90430117
        },
        {
            lng: 5.23216009,
            lat: 5.90314293
        },
        {
            lng: 5.23178577,
            lat: 5.92298889
        },
        {
            lng: 5.23141384,
            lat: 5.94306612
        },
        {
            lng: 5.22397995,
            lat: 5.96880007
        },
        {
            lng: 5.22076893,
            lat: 5.98431206
        },
        {
            lng: 5.2256732,
            lat: 6.00222397
        },
        {
            lng: 5.2283721,
            lat: 6.01209879
        },
        {
            lng: 5.23215103,
            lat: 6.03164291
        },
        {
            lng: 5.23238087,
            lat: 6.03860521
        },
        {
            lng: 5.23259497,
            lat: 6.0450139
        },
        {
            lng: 5.22975111,
            lat: 6.05475283
        },
        {
            lng: 5.21493387,
            lat: 6.06654119
        },
        {
            lng: 5.20294285,
            lat: 6.0817399
        },
        {
            lng: 5.11518097,
            lat: 6.12696314
        },
        {
            lng: 5.11909199,
            lat: 6.13646221
        },
        {
            lng: 5.12154913,
            lat: 6.14564705
        },
        {
            lng: 5.12131405,
            lat: 6.14542198
        },
        {
            lng: 5.12109518,
            lat: 6.14611721
        },
        {
            lng: 5.11317492,
            lat: 6.1561718
        },
        {
            lng: 5.10776711,
            lat: 6.16433907
        },
        {
            lng: 5.10675001,
            lat: 6.17289209
        },
        {
            lng: 5.10158777,
            lat: 6.18220711
        },
        {
            lng: 5.09250593,
            lat: 6.19158983
        },
        {
            lng: 5.08793402,
            lat: 6.19443703
        },
        {
            lng: 5.0801568,
            lat: 6.1989522
        },
        {
            lng: 5.07371902,
            lat: 6.20067692
        },
        {
            lng: 5.05501604,
            lat: 6.20053577
        },
        {
            lng: 5.05028677,
            lat: 6.2079978
        },
        {
            lng: 5.05021191,
            lat: 6.21768904
        },
        {
            lng: 5.04297113,
            lat: 6.22680998
        },
        {
            lng: 5.03870392,
            lat: 6.23426485
        },
        {
            lng: 5.03234291,
            lat: 6.24060106
        },
        {
            lng: 5.02830696,
            lat: 6.24805117
        },
        {
            lng: 5.019382,
            lat: 6.2530489
        },
        {
            lng: 5.01640081,
            lat: 6.25425482
        },
        {
            lng: 5.00930786,
            lat: 6.25829601
        },
        {
            lng: 5.00499821,
            lat: 6.2629838
        },
        {
            lng: 5.00134182,
            lat: 6.26535177
        },
        {
            lng: 4.99702978,
            lat: 6.2700429
        },
        {
            lng: 4.98573303,
            lat: 6.27092505
        },
        {
            lng: 4.98099899,
            lat: 6.2781558
        },
        {
            lng: 4.97857809,
            lat: 6.28558207
        },
        {
            lng: 4.98051977,
            lat: 6.29131603
        },
        {
            lng: 4.98265982,
            lat: 6.29520178
        },
        {
            lng: 4.97815895,
            lat: 6.28812695
        },
        {
            lng: 4.98315001,
            lat: 6.29703903
        },
        {
            lng: 4.9910388,
            lat: 6.29944277
        },
        {
            lng: 4.99423599,
            lat: 6.31161404
        },
        {
            lng: 4.99472618,
            lat: 6.31304121
        },
        {
            lng: 4.99714422,
            lat: 6.32009888
        },
        {
            lng: 4.99805784,
            lat: 6.33392715
        },
        {
            lng: 4.99919605,
            lat: 6.34705877
        },
        {
            lng: 5.0065279,
            lat: 6.358006
        },
        {
            lng: 5.01347017,
            lat: 6.35904121
        },
        {
            lng: 5.01592398,
            lat: 6.36799812
        },
        {
            lng: 5.02498388,
            lat: 6.37153482
        },
        {
            lng: 5.03440809,
            lat: 6.36883593
        },
        {
            lng: 5.05464506,
            lat: 6.37818098
        },
        {
            lng: 5.06347895,
            lat: 6.3819499
        },
        {
            lng: 5.06805277,
            lat: 6.39364004
        },
        {
            lng: 5.07357693,
            lat: 6.4069252
        },
        {
            lng: 5.07525492,
            lat: 6.42512178
        },
        {
            lng: 5.07714891,
            lat: 6.44239283
        },
        {
            lng: 5.08607578,
            lat: 6.45192909
        },
        {
            lng: 5.09472799,
            lat: 6.45870304
        },
        {
            lng: 5.1053009,
            lat: 6.47005701
        },
        {
            lng: 5.11126709,
            lat: 6.48218393
        },
        {
            lng: 5.11873817,
            lat: 6.50166416
        },
        {
            lng: 5.12127304,
            lat: 6.5156951
        },
        {
            lng: 5.12449598,
            lat: 6.52948284
        },
        {
            lng: 5.12118578,
            lat: 6.53899622
        },
        {
            lng: 5.11517811,
            lat: 6.55293989
        },
        {
            lng: 5.11109686,
            lat: 6.55762577
        },
        {
            lng: 5.10316277,
            lat: 6.56675577
        },
        {
            lng: 5.10037804,
            lat: 6.58018494
        },
        {
            lng: 5.09268284,
            lat: 6.58977604
        },
        {
            lng: 5.08605289,
            lat: 6.59381008
        },
        {
            lng: 5.07985592,
            lat: 6.59622288
        },
        {
            lng: 5.07276106,
            lat: 6.60003424
        },
        {
            lng: 5.07227802,
            lat: 6.61296082
        },
        {
            lng: 5.07456207,
            lat: 6.62584305
        },
        {
            lng: 5.07542992,
            lat: 6.63667107
        },
        {
            lng: 5.07280922,
            lat: 6.64594316
        },
        {
            lng: 5.07549906,
            lat: 6.65535498
        },
        {
            lng: 5.08576202,
            lat: 6.66187191
        },
        {
            lng: 5.08207417,
            lat: 6.66216612
        },
        {
            lng: 5.08403015,
            lat: 6.66882277
        },
        {
            lng: 5.0822649,
            lat: 6.6741581
        },
        {
            lng: 5.08383417,
            lat: 6.68543577
        },
        {
            lng: 5.08426189,
            lat: 6.69765615
        },
        {
            lng: 5.09213018,
            lat: 6.71320915
        },
        {
            lng: 5.09679985,
            lat: 6.71824121
        },
        {
            lng: 5.10434818,
            lat: 6.72637987
        },
        {
            lng: 5.11417294,
            lat: 6.73428917
        },
        {
            lng: 5.12048721,
            lat: 6.73925686
        },
        {
            lng: 5.12408495,
            lat: 6.74773121
        },
        {
            lng: 5.12768221,
            lat: 6.75597477
        },
        {
            lng: 5.13890696,
            lat: 6.764781
        },
        {
            lng: 5.15350723,
            lat: 6.76822281
        },
        {
            lng: 5.15559387,
            lat: 6.76887894
        },
        {
            lng: 5.15752697,
            lat: 6.77415323
        },
        {
            lng: 5.15809679,
            lat: 6.78083277
        },
        {
            lng: 5.16127205,
            lat: 6.79162312
        },
        {
            lng: 5.16842413,
            lat: 6.80557299
        },
        {
            lng: 5.1736269,
            lat: 6.81332684
        },
        {
            lng: 5.18383694,
            lat: 6.8166132
        },
        {
            lng: 5.18722391,
            lat: 6.82624483
        },
        {
            lng: 5.18660021,
            lat: 6.83040905
        },
        {
            lng: 5.18604422,
            lat: 6.83895397
        },
        {
            lng: 5.18526888,
            lat: 6.84819508
        },
        {
            lng: 5.19262981,
            lat: 6.86075783
        },
        {
            lng: 5.20313501,
            lat: 6.86796188
        },
        {
            lng: 5.22821712,
            lat: 6.87699223
        },
        {
            lng: 5.24475193,
            lat: 6.87163591
        },
        {
            lng: 5.26251316,
            lat: 6.8706398
        },
        {
            lng: 5.2829051,
            lat: 6.87513924
        },
        {
            lng: 5.29932117,
            lat: 6.87670517
        },
        {
            lng: 5.30967808,
            lat: 6.87468386
        },
        {
            lng: 5.3264699,
            lat: 6.87093401
        },
        {
            lng: 5.34913921,
            lat: 6.87354898
        },
        {
            lng: 5.37127781,
            lat: 6.87201691
        },
        {
            lng: 5.393682,
            lat: 6.87255716
        },
        {
            lng: 5.4146409,
            lat: 6.86943197
        },
        {
            lng: 5.42507696,
            lat: 6.86925507
        },
        {
            lng: 5.43379879,
            lat: 6.86910677
        },
        {
            lng: 5.45207119,
            lat: 6.8711009
        },
        {
            lng: 5.47472095,
            lat: 6.87256193
        },
        {
            lng: 5.48700094,
            lat: 6.87535095
        },
        {
            lng: 5.49966621,
            lat: 6.87328911
        },
        {
            lng: 5.51346397,
            lat: 6.86982393
        },
        {
            lng: 5.51915979,
            lat: 6.86511087
        },
        {
            lng: 5.5291791,
            lat: 6.85640717
        },
        {
            lng: 5.53479719,
            lat: 6.84685278
        },
        {
            lng: 5.5378809,
            lat: 6.8419528
        },
        {
            lng: 5.54155016,
            lat: 6.8361249
        },
        {
            lng: 5.53961086,
            lat: 6.83039093
        },
        {
            lng: 5.53369808,
            lat: 6.82172585
        },
        {
            lng: 5.52340984,
            lat: 6.81359577
        },
        {
            lng: 5.5140481,
            lat: 6.8056798
        },
        {
            lng: 5.50910378,
            lat: 6.79976606
        },
        {
            lng: 5.51309013,
            lat: 6.78931618
        },
        {
            lng: 5.51634312,
            lat: 6.77611113
        },
        {
            lng: 5.52559614,
            lat: 6.76303577
        },
        {
            lng: 5.52907419,
            lat: 6.74959278
        },
        {
            lng: 5.53070116,
            lat: 6.73595524
        },
        {
            lng: 5.53903198,
            lat: 6.72266293
        },
        {
            lng: 5.54300594,
            lat: 6.71152306
        },
        {
            lng: 5.55501986,
            lat: 6.71200991
        },
        {
            lng: 5.56160593,
            lat: 6.71951103
        },
        {
            lng: 5.56820917,
            lat: 6.72816515
        },
        {
            lng: 5.57312489,
            lat: 6.73246288
        },
        {
            lng: 5.57983494,
            lat: 6.73327208
        },
        {
            lng: 5.58871698,
            lat: 6.74004078
        },
        {
            lng: 5.60048819,
            lat: 6.75414515
        },
        {
            lng: 5.61587,
            lat: 6.76311111
        },
        {
            lng: 5.63016987,
            lat: 6.76217413
        },
        {
            lng: 5.63753986,
            lat: 6.76112604
        },
        {
            lng: 5.64482498,
            lat: 6.75454283
        },
        {
            lng: 5.6567812,
            lat: 6.73726797
        },
        {
            lng: 5.66984892,
            lat: 6.73150778
        },
        {
            lng: 5.67941713,
            lat: 6.73780298
        },
        {
            lng: 5.69239712,
            lat: 6.74104214
        },
        {
            lng: 5.7130599,
            lat: 6.7480731
        },
        {
            lng: 5.71616411,
            lat: 6.75447989
        },
        {
            lng: 5.72122288,
            lat: 6.76754379
        },
        {
            lng: 5.72196913,
            lat: 6.76871681
        },
        {
            lng: 5.72572184,
            lat: 6.77461815
        },
        {
            lng: 5.73288822,
            lat: 6.78949213
        },
        {
            lng: 5.73749018,
            lat: 6.80279493
        },
        {
            lng: 5.74050522,
            lat: 6.81796694
        },
        {
            lng: 5.74161291,
            lat: 6.82948399
        },
        {
            lng: 5.74434519,
            lat: 6.84143591
        },
        {
            lng: 5.74497414,
            lat: 6.85180616
        },
        {
            lng: 5.74755001,
            lat: 6.86837292
        },
        {
            lng: 5.74755383,
            lat: 6.86860323
        },
        {
            lng: 5.75286198,
            lat: 6.88281584
        },
        {
            lng: 5.7568531,
            lat: 6.88689995
        },
        {
            lng: 5.7711072,
            lat: 6.88319778
        },
        {
            lng: 5.77611399,
            lat: 6.87872887
        },
        {
            lng: 5.77854109,
            lat: 6.88606977
        },
        {
            lng: 5.77612591,
            lat: 6.89372301
        },
        {
            lng: 5.77630615,
            lat: 6.9050231
        },
        {
            lng: 5.78097105,
            lat: 6.90794277
        },
        {
            lng: 5.7849021,
            lat: 6.90833998
        },
        {
            lng: 5.78513288,
            lat: 6.90833521
        },
        {
            lng: 5.78677607,
            lat: 6.90992117
        },
        {
            lng: 5.79029322,
            lat: 6.91332197
        },
        {
            lng: 5.79052496,
            lat: 6.9133172
        },
        {
            lng: 5.79061604,
            lat: 6.91908312
        },
        {
            lng: 5.78796577,
            lat: 6.92651081
        },
        {
            lng: 5.78321791,
            lat: 6.93282223
        },
        {
            lng: 5.77639389,
            lat: 6.93916702
        },
        {
            lng: 5.77276182,
            lat: 6.94291878
        },
        {
            lng: 5.76890516,
            lat: 6.94713688
        },
        {
            lng: 5.77206898,
            lat: 6.95723391
        },
        {
            lng: 5.77329683,
            lat: 6.96182585
        },
        {
            lng: 5.77404499,
            lat: 6.96527481
        },
        {
            lng: 5.76971197,
            lat: 6.96857882
        },
        {
            lng: 5.76963711,
            lat: 6.97826815
        },
        {
            lng: 5.77093077,
            lat: 6.98701477
        },
        {
            lng: 5.77359581,
            lat: 6.99481201
        },
        {
            lng: 5.77535677,
            lat: 7.00378084
        },
        {
            lng: 5.77870798,
            lat: 7.01110601
        },
        {
            lng: 5.77888298,
            lat: 7.021945
        },
        {
            lng: 5.7845149,
            lat: 7.02761602
        },
        {
            lng: 5.78597403,
            lat: 7.03220606
        },
        {
            lng: 5.78717184,
            lat: 7.03495216
        },
        {
            lng: 5.79329014,
            lat: 7.0420022
        },
        {
            lng: 5.79894018,
            lat: 7.04882479
        },
        {
            lng: 5.80382395,
            lat: 7.05105019
        },
        {
            lng: 5.80851412,
            lat: 7.055583
        },
        {
            lng: 5.8173852,
            lat: 7.0616622
        },
        {
            lng: 5.81737614,
            lat: 7.06097078
        },
        {
            lng: 5.82337189,
            lat: 7.06063604
        },
        {
            lng: 5.82652378,
            lat: 7.07004213
        },
        {
            lng: 5.82936382,
            lat: 7.07437706
        },
        {
            lng: 5.82808113,
            lat: 7.08062887
        },
        {
            lng: 5.82440901,
            lat: 7.08207321
        },
        {
            lng: 5.82427216,
            lat: 7.08784389
        },
        {
            lng: 5.82693005,
            lat: 7.09518099
        },
        {
            lng: 5.83068085,
            lat: 7.09880686
        },
        {
            lng: 5.83232021,
            lat: 7.10016489
        },
        {
            lng: 5.83520508,
            lat: 7.1072669
        },
        {
            lng: 5.83788109,
            lat: 7.11575699
        },
        {
            lng: 5.83963823,
            lat: 7.12449312
        },
        {
            lng: 5.84392786,
            lat: 7.13272476
        },
        {
            lng: 5.84729624,
            lat: 7.14120579
        },
        {
            lng: 5.8517952,
            lat: 7.14828014
        },
        {
            lng: 5.85606194,
            lat: 7.15512705
        },
        {
            lng: 5.85988379,
            lat: 7.16313791
        },
        {
            lng: 5.86233187,
            lat: 7.17163181
        },
        {
            lng: 5.86356211,
            lat: 7.17645502
        },
        {
            lng: 5.86504793,
            lat: 7.18265915
        },
        {
            lng: 5.86884499,
            lat: 7.18905401
        },
        {
            lng: 5.87215185,
            lat: 7.19384193
        },
        {
            lng: 5.8763938,
            lat: 7.19907522
        },
        {
            lng: 5.87833309,
            lat: 7.20481014
        },
        {
            lng: 5.87400818,
            lat: 7.20857477
        },
        {
            lng: 5.87480307,
            lat: 7.21502018
        },
        {
            lng: 5.87398481,
            lat: 7.22149515
        },
        {
            lng: 5.875453,
            lat: 7.22654581
        },
        {
            lng: 5.87781096,
            lat: 7.22973394
        },
        {
            lng: 5.88163996,
            lat: 7.23820591
        },
        {
            lng: 5.88540602,
            lat: 7.2425251
        },
        {
            lng: 5.88941479,
            lat: 7.2477622
        },
        {
            lng: 5.89060879,
            lat: 7.24930096
        },
        {
            lng: 5.89224291,
            lat: 7.25140381
        },
        {
            lng: 5.89670801,
            lat: 7.25617218
        },
        {
            lng: 5.90347004,
            lat: 7.26044083
        },
        {
            lng: 5.91005182,
            lat: 7.26771116
        },
        {
            lng: 5.91586208,
            lat: 7.26991987
        },
        {
            lng: 5.92004395,
            lat: 7.27169323
        },
        {
            lng: 5.92543221,
            lat: 7.27644682
        },
        {
            lng: 5.93264294,
            lat: 7.2797842
        },
        {
            lng: 5.93497896,
            lat: 7.28159094
        },
        {
            lng: 5.94472694,
            lat: 7.28465223
        },
        {
            lng: 5.9499259,
            lat: 7.29217815
        },
        {
            lng: 5.952806,
            lat: 7.2990489
        },
        {
            lng: 5.95481682,
            lat: 7.30916691
        },
        {
            lng: 5.95824909,
            lat: 7.32156515
        },
        {
            lng: 5.96072912,
            lat: 7.33213615
        },
        {
            lng: 5.96644211,
            lat: 7.33985996
        },
        {
            lng: 5.96781015,
            lat: 7.34170389
        },
        {
            lng: 5.96095705,
            lat: 7.34620523
        },
        {
            lng: 5.95949984,
            lat: 7.3561492
        },
        {
            lng: 5.95574903,
            lat: 7.36705685
        },
        {
            lng: 5.95037699,
            lat: 7.37752914
        },
        {
            lng: 5.94844198,
            lat: 7.38632917
        },
        {
            lng: 5.94887686,
            lat: 7.39901018
        },
        {
            lng: 5.95298195,
            lat: 7.41024399
        },
        {
            lng: 5.95766592,
            lat: 7.41431713
        },
        {
            lng: 5.96174097,
            lat: 7.42370701
        },
        {
            lng: 5.96258211,
            lat: 7.43292093
        },
        {
            lng: 5.95780706,
            lat: 7.43738413
        },
        {
            lng: 5.9518199,
            lat: 7.43840981
        },
        {
            lng: 5.94743395,
            lat: 7.43848515
        },
        {
            lng: 5.94213295,
            lat: 7.43903589
        },
        {
            lng: 5.93868494,
            lat: 7.44001579
        },
        {
            lng: 5.93369389,
            lat: 7.44540882
        },
        {
            lng: 5.93000984,
            lat: 7.44616318
        },
        {
            lng: 5.92821503,
            lat: 7.44919395
        },
        {
            lng: 5.92353296,
            lat: 7.4596529
        },
        {
            lng: 5.9241991,
            lat: 7.472332
        },
        {
            lng: 5.92643213,
            lat: 7.48198414
        },
        {
            lng: 5.93115902,
            lat: 7.48882389
        },
        {
            lng: 5.93796492,
            lat: 7.49562979
        },
        {
            lng: 5.94336224,
            lat: 7.50107384
        },
        {
            lng: 5.94600487,
            lat: 7.50748777
        },
        {
            lng: 5.94927406,
            lat: 7.50973988
        },
        {
            lng: 5.95134497,
            lat: 7.50924301
        },
        {
            lng: 5.95500803,
            lat: 7.50733519
        },
        {
            lng: 5.96240187,
            lat: 7.50766993
        },
        {
            lng: 5.96955299,
            lat: 7.50731707
        },
        {
            lng: 5.9744029,
            lat: 7.50746679
        },
        {
            lng: 5.97769022,
            lat: 7.51086998
        },
        {
            lng: 5.98256302,
            lat: 7.51240206
        },
        {
            lng: 5.98559904,
            lat: 7.51442623
        },
        {
            lng: 5.98887396,
            lat: 7.517138
        },
        {
            lng: 5.99012518,
            lat: 7.51894999
        },
        {
            lng: 5.99218321,
            lat: 7.52192783
        },
        {
            lng: 5.99385405,
            lat: 7.52535915
        },
        {
            lng: 5.99785614,
            lat: 7.53013611
        },
        {
            lng: 6.00211811,
            lat: 7.53675413
        },
        {
            lng: 6.00550508,
            lat: 7.54638386
        },
        {
            lng: 6.0115881,
            lat: 7.55135679
        },
        {
            lng: 6.01743412,
            lat: 7.55587196
        },
        {
            lng: 6.02240181,
            lat: 7.56339979
        },
        {
            lng: 6.02643824,
            lat: 7.57025099
        },
        {
            lng: 6.0283761,
            lat: 7.57342291
        },
        {
            lng: 6.02923298,
            lat: 7.57181883
        },
        {
            lng: 6.04086781,
            lat: 7.56308508
        },
        {
            lng: 6.05081797,
            lat: 7.5645299
        },
        {
            lng: 6.05499411,
            lat: 7.56561279
        },
        {
            lng: 6.06241512,
            lat: 7.56779099
        },
        {
            lng: 6.0691309,
            lat: 7.56906319
        },
        {
            lng: 6.08035088,
            lat: 7.56333399
        },
        {
            lng: 6.08463383,
            lat: 7.5568018
        },
        {
            lng: 6.08502388,
            lat: 7.55241203
        },
        {
            lng: 6.07930088,
            lat: 7.53916788
        },
        {
            lng: 6.07879019,
            lat: 7.53798485
        },
        {
            lng: 6.08623791,
            lat: 7.52747583
        },
        {
            lng: 6.10237694,
            lat: 7.52604723
        },
        {
            lng: 6.11280203,
            lat: 7.52817583
        },
        {
            lng: 6.11768103,
            lat: 7.53016996
        },
        {
            lng: 6.12139893,
            lat: 7.5314889
        },
        {
            lng: 6.12589788,
            lat: 7.52426195
        },
        {
            lng: 6.12580919,
            lat: 7.51872587
        },
        {
            lng: 6.12322187,
            lat: 7.51577282
        },
        {
            lng: 6.12158298,
            lat: 7.51181984
        },
        {
            lng: 6.11865902,
            lat: 7.504776
        },
        {
            lng: 6.1155982,
            lat: 7.50113583
        },
        {
            lng: 6.11032009,
            lat: 7.48876715
        },
        {
            lng: 6.10663414,
            lat: 7.47498989
        },
        {
            lng: 6.10676193,
            lat: 7.46852779
        },
        {
            lng: 6.10658693,
            lat: 7.45768881
        },
        {
            lng: 6.10968208,
            lat: 7.44909906
        },
        {
            lng: 6.11116314,
            lat: 7.44076777
        },
        {
            lng: 6.12190723,
            lat: 7.43412685
        },
        {
            lng: 6.123281,
            lat: 7.43340921
        },
        {
            lng: 6.12850714,
            lat: 7.42824697
        },
        {
            lng: 6.13631105,
            lat: 7.4253459
        },
        {
            lng: 6.14579678,
            lat: 7.42656708
        },
        {
            lng: 6.15474892,
            lat: 7.42341518
        },
        {
            lng: 6.16300011,
            lat: 7.4195838
        },
        {
            lng: 6.16609001,
            lat: 7.42506695
        },
        {
            lng: 6.18095922,
            lat: 7.43081284
        },
        {
            lng: 6.18956804,
            lat: 7.43505001
        },
        {
            lng: 6.19839478,
            lat: 7.43835878
        },
        {
            lng: 6.21719122,
            lat: 7.4442668
        },
        {
            lng: 6.22484922,
            lat: 7.44667578
        },
        {
            lng: 6.23372412,
            lat: 7.45298195
        },
        {
            lng: 6.23408413,
            lat: 7.45311213
        },
        {
            lng: 6.24347782,
            lat: 7.45650816
        },
        {
            lng: 6.25048685,
            lat: 7.46169281
        },
        {
            lng: 6.258883,
            lat: 7.46685791
        },
        {
            lng: 6.26413584,
            lat: 7.46330881
        },
        {
            lng: 6.266119,
            lat: 7.45750618
        },
        {
            lng: 6.27562094,
            lat: 7.44534683
        },
        {
            lng: 6.279881,
            lat: 7.43743181
        },
        {
            lng: 6.28419495,
            lat: 7.43297577
        },
        {
            lng: 6.29460382,
            lat: 7.43418217
        },
        {
            lng: 6.29824114,
            lat: 7.43065786
        },
        {
            lng: 6.30463409,
            lat: 7.42616606
        },
        {
            lng: 6.30713177,
            lat: 7.42358589
        },
        {
            lng: 6.31372118,
            lat: 7.41701317
        },
        {
            lng: 6.31501818,
            lat: 7.4114542
        },
        {
            lng: 6.32380581,
            lat: 7.4124589
        },
        {
            lng: 6.33267498,
            lat: 7.4040041
        },
        {
            lng: 6.33983612,
            lat: 7.38980913
        },
        {
            lng: 6.34638596,
            lat: 7.38092995
        },
        {
            lng: 6.3555479,
            lat: 7.37638998
        },
        {
            lng: 6.36062622,
            lat: 7.3763032
        },
        {
            lng: 6.36898088,
            lat: 7.37893105
        },
        {
            lng: 6.38285923,
            lat: 7.38053989
        },
        {
            lng: 6.38878918,
            lat: 7.37984514
        },
        {
            lng: 6.39207602,
            lat: 7.37945986
        },
        {
            lng: 6.3963418,
            lat: 7.38630581
        },
        {
            lng: 6.40398598,
            lat: 7.38779211
        },
        {
            lng: 6.41017723,
            lat: 7.38514805
        },
        {
            lng: 6.42310619,
            lat: 7.38515997
        },
        {
            lng: 6.43001223,
            lat: 7.38388777
        },
        {
            lng: 6.43533611,
            lat: 7.38471985
        },
        {
            lng: 6.44527102,
            lat: 7.38524199
        },
        {
            lng: 6.44705105,
            lat: 7.38106012
        },
        {
            lng: 6.44946384,
            lat: 7.37317419
        },
        {
            lng: 6.45422792,
            lat: 7.36801815
        },
        {
            lng: 6.45636988,
            lat: 7.35760021
        },
        {
            lng: 6.46252823,
            lat: 7.35288
        },
        {
            lng: 6.46359682,
            lat: 7.33325195
        },
        {
            lng: 6.46720695,
            lat: 7.32811499
        },
        {
            lng: 6.47084904,
            lat: 7.32482386
        },
        {
            lng: 6.47154188,
            lat: 7.32481289
        },
        {
            lng: 6.47667503,
            lat: 7.32348919
        },
        {
            lng: 6.47729015,
            lat: 7.32333088
        },
        {
            lng: 6.48038816,
            lat: 7.31497383
        },
        {
            lng: 6.48054504,
            lat: 7.31035614
        },
        {
            lng: 6.48514891,
            lat: 7.29528379
        },
        {
            lng: 6.49084902,
            lat: 7.29080105
        },
        {
            lng: 6.49701214,
            lat: 7.28631401
        },
        {
            lng: 6.49785709,
            lat: 7.28145504
        },
        {
            lng: 6.49318504,
            lat: 7.27807188
        },
        {
            lng: 6.49870491,
            lat: 7.26252222
        },
        {
            lng: 6.50384378,
            lat: 7.2518239
        },
        {
            lng: 6.51121521,
            lat: 7.25077486
        },
        {
            lng: 6.51978922,
            lat: 7.25270319
        },
        {
            lng: 6.53067684,
            lat: 7.25505495
        },
        {
            lng: 6.53832817,
            lat: 7.25700378
        },
        {
            lng: 6.55132818,
            lat: 7.26139402
        },
        {
            lng: 6.56432104,
            lat: 7.26532698
        },
        {
            lng: 6.57138014,
            lat: 7.2592082
        },
        {
            lng: 6.58005619,
            lat: 7.26759481
        },
        {
            lng: 6.58886099,
            lat: 7.26952124
        },
        {
            lng: 6.59495497,
            lat: 7.27518511
        },
        {
            lng: 6.60490084,
            lat: 7.27639914
        },
        {
            lng: 6.61809492,
            lat: 7.27848291
        },
        {
            lng: 6.62507582,
            lat: 7.28182411
        },
        {
            lng: 6.63806486,
            lat: 7.27122116
        },
        {
            lng: 6.64419985,
            lat: 7.26511812
        },
        {
            lng: 6.653337,
            lat: 7.25896406
        },
        {
            lng: 6.655931,
            lat: 7.24807692
        },
        {
            lng: 6.65642595,
            lat: 7.23584223
        },
        {
            lng: 6.6622529,
            lat: 7.22489977
        },
        {
            lng: 6.6702919,
            lat: 7.21875095
        },
        {
            lng: 6.67799091,
            lat: 7.21286583
        },
        {
            lng: 6.68575096,
            lat: 7.20742607
        },
        {
            lng: 6.68720818,
            lat: 7.19748211
        },
        {
            lng: 6.69070816,
            lat: 7.1854248
        },
        {
            lng: 6.69637394,
            lat: 7.17886782
        },
        {
            lng: 6.69838905,
            lat: 7.17491388
        },
        {
            lng: 6.69744778,
            lat: 7.16213989
        },
        {
            lng: 6.69691181,
            lat: 7.15486717
        },
        {
            lng: 6.69702911,
            lat: 7.14771509
        },
        {
            lng: 6.69890022,
            lat: 7.13499308
        },
        {
            lng: 6.70096684,
            lat: 7.11996222
        },
        {
            lng: 6.69630384,
            lat: 7.1027379
        },
        {
            lng: 6.69624186,
            lat: 7.10092402
        },
        {
            lng: 6.6958499,
            lat: 7.08890581
        },
        {
            lng: 6.69080591,
            lat: 7.07676315
        },
        {
            lng: 6.68437004,
            lat: 7.06418514
        },
        {
            lng: 6.68136597,
            lat: 7.04970312
        },
        {
            lng: 6.67707014,
            lat: 7.0410099
        },
        {
            lng: 6.67342615,
            lat: 7.0333519
        },
        {
            lng: 6.66966295,
            lat: 7.02544785
        },
        {
            lng: 6.67152023,
            lat: 7.01180506
        },
        {
            lng: 6.67255497,
            lat: 7.00440502
        },
        {
            lng: 6.67329979,
            lat: 6.99331903
        },
        {
            lng: 6.67264891,
            lat: 6.98156404
        },
        {
            lng: 6.67149305,
            lat: 6.96705008
        },
        {
            lng: 6.670784,
            lat: 6.95961618
        },
        {
            lng: 6.67065907,
            lat: 6.95829821
        },
        {
            lng: 6.66632223,
            lat: 6.94706678
        },
        {
            lng: 6.659307,
            lat: 6.92711687
        },
        {
            lng: 6.65659904,
            lat: 6.91678095
        },
        {
            lng: 6.65074778,
            lat: 6.9118042
        },
        {
            lng: 6.64678717,
            lat: 6.89526176
        },
        {
            lng: 6.63985205,
            lat: 6.88038397
        },
        {
            lng: 6.62734413,
            lat: 6.86352682
        },
        {
            lng: 6.62105989,
            lat: 6.84610081
        },
        {
            lng: 6.60499001,
            lat: 6.82307291
        },
        {
            lng: 6.61253595,
            lat: 6.80425787
        },
        {
            lng: 6.62076902,
            lat: 6.79927397
        },
        {
            lng: 6.62567091,
            lat: 6.78834486
        },
        {
            lng: 6.62375021,
            lat: 6.77810097
        },
        {
            lng: 6.62213612,
            lat: 6.76948977
        },
        {
            lng: 6.62196112,
            lat: 6.75864983
        },
        {
            lng: 6.61690998,
            lat: 6.74604702
        },
        {
            lng: 6.61314011,
            lat: 6.726964
        },
        {
            lng: 6.61061716,
            lat: 6.69932222
        },
        {
            lng: 6.61584997,
            lat: 6.68031693
        },
        {
            lng: 6.62328291,
            lat: 6.66865396
        },
        {
            lng: 6.62494898,
            lat: 6.65732193
        },
        {
            lng: 6.63417006,
            lat: 6.64216805
        },
        {
            lng: 6.64096022,
            lat: 6.6337471
        },
        {
            lng: 6.64774418,
            lat: 6.62486506
        },
        {
            lng: 6.65035391,
            lat: 6.61490107
        },
        {
            lng: 6.65455008,
            lat: 6.60306311
        },
        {
            lng: 6.65563011,
            lat: 6.5841279
        },
        {
            lng: 6.65927505,
            lat: 6.56676292
        },
        {
            lng: 6.66400194,
            lat: 6.5447669
        },
        {
            lng: 6.66289806,
            lat: 6.53371096
        },
        {
            lng: 6.66439104,
            lat: 6.52745008
        },
        {
            lng: 6.66550398,
            lat: 6.52277994
        },
        {
            lng: 6.66171789,
            lat: 6.51781416
        },
        {
            lng: 6.652987,
            lat: 6.50596523
        },
        {
            lng: 6.64614677,
            lat: 6.49708509
        },
        {
            lng: 6.63518,
            lat: 6.4898901
        },
        {
            lng: 6.62196684,
            lat: 6.4866538
        },
        {
            lng: 6.60380507,
            lat: 6.49134684
        },
        {
            lng: 6.58840704,
            lat: 6.49576378
        },
        {
            lng: 6.57123613,
            lat: 6.49605513
        },
        {
            lng: 6.56901884,
            lat: 6.49609184
        },
        {
            lng: 6.55259323,
            lat: 6.49406624
        },
        {
            lng: 6.53941298,
            lat: 6.49267483
        },
        {
            lng: 6.52666807,
            lat: 6.48989296
        },
        {
            lng: 6.51317978,
            lat: 6.48389578
        },
        {
            lng: 6.50878096,
            lat: 6.48304796
        },
        {
            lng: 6.49759293,
            lat: 6.47654819
        },
        {
            lng: 6.48267603,
            lat: 6.46780491
        },
        {
            lng: 6.47541714,
            lat: 6.46146679
        },
        {
            lng: 6.46347618,
            lat: 6.45106077
        },
        {
            lng: 6.44667482,
            lat: 6.44004107
        },
        {
            lng: 6.43296099,
            lat: 6.43427706
        },
        {
            lng: 6.41739178,
            lat: 6.42808199
        },
        {
            lng: 6.39987278,
            lat: 6.41546297
        },
        {
            lng: 6.37654305,
            lat: 6.40040493
        },
        {
            lng: 6.35938406,
            lat: 6.39254808
        },
        {
            lng: 6.35468006,
            lat: 6.39039516
        },
        {
            lng: 6.34611177,
            lat: 6.38892508
        },
        {
            lng: 6.31447315,
            lat: 6.37400818
        },
        {
            lng: 6.29820299,
            lat: 6.36713409
        },
        {
            lng: 6.27101707,
            lat: 6.35652399
        },
        {
            lng: 6.2690568,
            lat: 6.35560083
        },
        {
            lng: 6.25775719,
            lat: 6.35029078
        },
        {
            lng: 6.24945402,
            lat: 6.34415483
        },
        {
            lng: 6.24280405,
            lat: 6.33924198
        },
        {
            lng: 6.23552608,
            lat: 6.33175182
        },
        {
            lng: 6.23173618,
            lat: 6.32581806
        },
        {
            lng: 6.23044395,
            lat: 6.32364082
        },
        {
            lng: 6.22414207,
            lat: 6.31302881
        },
        {
            lng: 6.21029902,
            lat: 6.29919004
        },
        {
            lng: 6.20133018,
            lat: 6.28711796
        },
        {
            lng: 6.192626,
            lat: 6.27711678
        },
        {
            lng: 6.1822238,
            lat: 6.27636909
        },
        {
            lng: 6.17453718,
            lat: 6.28641987
        },
        {
            lng: 6.16441011,
            lat: 6.30274391
        },
        {
            lng: 6.15291691,
            lat: 6.32024002
        },
        {
            lng: 6.14053917,
            lat: 6.34006023
        },
        {
            lng: 6.12425423,
            lat: 6.3467989
        },
        {
            lng: 6.10634899,
            lat: 6.35333395
        },
        {
            lng: 6.09732199,
            lat: 6.35187292
        },
        {
            lng: 6.08309507,
            lat: 6.34288597
        },
        {
            lng: 6.06851387,
            lat: 6.32629395
        },
        {
            lng: 6.06353617,
            lat: 6.31830311
        },
        {
            lng: 6.05801582,
            lat: 6.30524778
        },
        {
            lng: 6.05413389,
            lat: 6.29354715
        },
        {
            lng: 6.05145407,
            lat: 6.27052307
        },
        {
            lng: 6.05167103,
            lat: 6.25529385
        },
        {
            lng: 6.0581708,
            lat: 6.24318695
        },
        {
            lng: 6.07051897,
            lat: 6.23582411
        },
        {
            lng: 6.08173895,
            lat: 6.23009777
        },
        {
            lng: 6.08740997,
            lat: 6.20946789
        },
        {
            lng: 6.08896017,
            lat: 6.19098616
        },
        {
            lng: 6.08799982,
            lat: 6.17439222
        },
        {
            lng: 6.09041405,
            lat: 6.15220213
        },
        {
            lng: 6.10363483,
            lat: 6.1272912
        },
        {
            lng: 6.11397982,
            lat: 6.11027479
        },
        {
            lng: 6.12318802,
            lat: 6.09420013
        },
        {
            lng: 6.13518095,
            lat: 6.07900095
        },
        {
            lng: 6.14561415,
            lat: 6.06751585
        },
        {
            lng: 6.154881,
            lat: 6.05513382
        },
        {
            lng: 6.16682911,
            lat: 6.05146885
        },
        {
            lng: 6.17547607,
            lat: 6.04370785
        },
        {
            lng: 6.18520784,
            lat: 6.03154707
        },
        {
            lng: 6.19575977,
            lat: 6.02721214
        },
        {
            lng: 6.20591402,
            lat: 6.02704096
        },
        {
            lng: 6.22124481,
            lat: 6.0184741
        },
        {
            lng: 6.22366095,
            lat: 6.00866222
        },
        {
            lng: 6.22449493,
            lat: 6.00526905
        },
        {
            lng: 6.22587013,
            lat: 5.99024916
        },
        {
            lng: 6.22264004,
            lat: 5.97600222
        },
        {
            lng: 6.22029305,
            lat: 5.96755314
        },
        {
            lng: 6.2179842,
            lat: 5.95923901
        },
        {
            lng: 6.21286297,
            lat: 5.94225502
        },
        {
            lng: 6.21069479,
            lat: 5.93675518
        },
        {
            lng: 6.21463823,
            lat: 5.93783998
        },
        {
            lng: 6.191607,
            lat: 5.9128561
        },
        {
            lng: 6.18966579,
            lat: 5.90689182
        },
        {
            lng: 6.17165613,
            lat: 5.89243317
        },
        {
            lng: 6.16337585,
            lat: 5.88011694
        },
        {
            lng: 6.15313578,
            lat: 5.87498617
        },
        {
            lng: 6.13449192,
            lat: 5.86423111
        },
        {
            lng: 6.12584114,
            lat: 5.85745478
        },
        {
            lng: 6.11366987,
            lat: 5.84728289
        },
        {
            lng: 6.10973883,
            lat: 5.84213018
        },
        {
            lng: 6.10706711,
            lat: 5.83862686
        },
        {
            lng: 6.09674692,
            lat: 5.82865381
        },
        {
            lng: 6.08609295,
            lat: 5.81222582
        },
        {
            lng: 6.07662296,
            lat: 5.79762077
        },
        {
            lng: 6.0663352,
            lat: 5.78949118
        },
        {
            lng: 6.05396414,
            lat: 5.78116798
        },
        {
            lng: 6.03398895,
            lat: 5.7669692
        },
        {
            lng: 6.03177595,
            lat: 5.76539612
        },
        {
            lng: 6.0025301,
            lat: 5.75574398
        },
        {
            lng: 5.98916817,
            lat: 5.75268984
        },
        {
            lng: 5.96869898,
            lat: 5.74801588
        },
        {
            lng: 5.93792582,
            lat: 5.74392509
        },
        {
            lng: 5.91799021,
            lat: 5.73895788
        },
        {
            lng: 5.893332,
            lat: 5.74191713
        },
        {
            lng: 5.88350487,
            lat: 5.74808407
        },
        {
            lng: 5.8874321,
            lat: 5.76255178
        },
        {
            lng: 5.89045191,
            lat: 5.76852798
        },
        {
            lng: 5.89482498,
            lat: 5.77718878
        },
        {
            lng: 5.88308096,
            lat: 5.78829479
        }
    ];

})(jQuery)