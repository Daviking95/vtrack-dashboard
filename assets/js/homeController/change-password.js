// (function ($) {

// 'use strict';

var tempPass = localStorage.getItem("tempPass");
var getToken = localStorage.getItem("x-auth-token");

console.log(tempPass);

$('#reset_oldPassword').val(tempPass);

function ChangePassword() {

    $('#loader').show();
    $('#reset_change_btn_div').hide();
  
    var reset_resetPassword = $('#reset_resetPassword').val();
    var reset_resetConfirmPassword = $('#reset_resetConfirmPassword').val();
  
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("x-auth-token", getToken);
  
    var raw = JSON.stringify({
      "oldPassword": tempPass,
      "password": reset_resetPassword,
      "passwordConfirmation": reset_resetConfirmPassword
    });
  
    var requestOptions = {
      method: 'PATCH',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };
  
    fetch("https://peaceful-atoll-23751.herokuapp.com/" + "just-clock-285910.uc.r.appspot.com/api/admin/resetPassword", requestOptions)
      .then(function (response) {
        if (response.status === 403) {
          alert("Token expired. Login");
          $('#loader').hide();
          $('#reset_change_btn_div').show();
          window.location.href = "index.php";
        }
        if (!response.ok) {
          alert("Cannot login. Ensure your data is correct and you have internet connection");
          $('#loader').hide();
          $('#reset_change_btn_div').show();
          throw new Error("HTTP status " + response.status);
        }
  
        return response.json();
      })
      .then(function (result) {
  
        // alert(result.details);
        alert(result.message);
        window.location.href = "dashboard/home.php";
        $('#loader').hide();
        $('#reset_change_btn_div').show();
  
  
      })
      .catch(error => console.log('error', error));
  
  }
  
  
  function init() {
    loginUser();
    Waves.init();
  }
  
  // init();
  
  // })(jQuery)