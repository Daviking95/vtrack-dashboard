(function ($) {
    var getToken = localStorage.getItem("x-auth-token");

    initiatorArrayItem1 = [];
    initiatorArrayItem2 = [];
    initiatorArrayItem3 = [];
    initiatorArrayItem4 = [];
    initiatorArrayItem5 = [];
    initiatorArrayItem6 = [];
    initiatorArrayItem7 = [];
    initiatorArrayItem8 = [];
    initiatorArrayItem9 = [];

    "use strict";

    async function getAllFormsForViolenceInitiatorPeopleAndGenderInvolved() {


        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("x-auth-token", getToken);

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        fetch("https://peaceful-atoll-23751.herokuapp.com/" + "just-clock-285910.uc.r.appspot.com/api/form/all", requestOptions)
            .then(function (response) {
                if (response.status === 403) {
                    alert("Token expired. Login");
                    window.location.href = "index.php";
                }
                if (!response.ok) {
                    alert("Cannot login. Ensure your data is correct and you have internet connection");
                    throw new Error("HTTP status " + response.status);
                }

                return response.json();
            })
            .then(function (result) {

                var detailsArray = result.details;
                var incidentWeaponArray = [];

                for (let index = 0; index < detailsArray.length; index++) {
                    const element = detailsArray[index];

                    if (element.firstApproval === true && element.secondApproval === true) {
                    incidentWeaponArray.push(element.incidentWeapon);
                    }

                }

                fillInitiatorArrayWithResult(incidentWeaponArray);

                showCharts();

            })
            .catch(error => console.log('error', error));

    }

    function fillInitiatorArrayWithResult(data) {

        initiatorArrayItem1 = [];
        initiatorArrayItem2 = [];
        initiatorArrayItem3 = [];
        initiatorArrayItem4 = [];
        initiatorArrayItem5 = [];
        initiatorArrayItem6 = [];
        initiatorArrayItem7 = [];
        initiatorArrayItem8 = [];
        initiatorArrayItem9 = [];

        data.reduce(function (a, b) {
                return a.concat(b);
            })
            .filter(function (element) {
                switch (element.weaponType) {
                    case 'Fists/physical means':
                        initiatorArrayItem1.push(element);
                        
                        break;
                    case 'Gun/firearm':
                        initiatorArrayItem2.push(element);
                        break;
                    case 'Knives/stabbing':
                        initiatorArrayItem3.push(element);
                        break;
                    case "Stones/throwing objects":
                        initiatorArrayItem4.push(element);
                        break;
                    case "Arson":
                        initiatorArrayItem5.push(element);
                        break;
                    case "Bomb/explosives":
                        initiatorArrayItem6.push(element);
                        break;
                    case "No weapon":
                        initiatorArrayItem7.push(element);
                        break;
                    case "Unable to determine":
                        initiatorArrayItem8.push(element);
                        break;
                    case "Others":
                        initiatorArrayItem9.push(element);
                        break;

                    default:
                        break;
                }
            });


    }

    function showCharts() {

        var MorrisCharts = function () {};

        //creates Bar chart
        MorrisCharts.prototype.createBarChart = function (element, data, xkey, ykeys, labels, lineColors) {
                Morris.Bar({
                    element: element,
                    data: data,
                    xkey: 'y',
                    ykeys: ['a'],
                    labels: ['Number(s)'],
                    gridLineColor: '#dbe5ec',
                    barSizeRatio: 0.4,
                    resize: true,
                    hideHover: 'auto',
                    barColors: lineColors
                });
            },

            MorrisCharts.prototype.init = function () {

                //creating bar chart
                var $barData = [{
                        y: 'Fists/physical means',
                        a: initiatorArrayItem1.length
                    },
                    {
                        y: 'Gun/firearm',
                        a: initiatorArrayItem2.length
                    },
                    {
                        y: 'Knives/stabbing',
                        a: initiatorArrayItem3.length
                    },
                    {
                        y: 'Stones/throwing objects',
                        a: initiatorArrayItem4.length
                    },
                    {
                        y: 'Arson',
                        a: initiatorArrayItem5.length
                    },
                    {
                        y: 'Bomb/explosives',
                        a: initiatorArrayItem6.length
                    },
                    {
                        y: 'No weapon',
                        a: initiatorArrayItem7.length
                    },
                    {
                        y: 'Unable to determine',
                        a: initiatorArrayItem8.length
                    },
                    {
                        y: 'Others',
                        a: initiatorArrayItem9.length
                    }
                ];
                this.createBarChart('morris-bar-example', $barData, 'y', ['a'], ['Number(s)'], ['#4d79f6']);

            },
            //init
            $.MorrisCharts = new MorrisCharts, $.MorrisCharts.Constructor = MorrisCharts
        $.MorrisCharts.init();

    }


    //initializing
    async function init() {
        "use strict";
        await getAllFormsForViolenceInitiatorPeopleAndGenderInvolved();
        // $.ChartJs.init()
    }

    init();

})(jQuery)