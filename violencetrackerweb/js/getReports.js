// (function ($) {

let statesWithViolenceArray = [];

let sBox = "";
let filter_state = "";
let filter_df = "";
let filter_dt = "";
let filter_vt = "";
let filter_year = "";

function getAllForms(isSearch) {

    if (isSearch) statesWithViolenceArray = [];

    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    // myHeaders.append("x-auth-token", getToken);

    var requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };

    fetch("https://peaceful-atoll-23751.herokuapp.com/just-clock-285910.uc.r.appspot.com/api/form/all", requestOptions)
        .then(function (response) {
            if (response.status === 403) {
                alert("Token expired. Login");
                window.location.href = "index.php";
            }
            if (!response.ok) {
                // alert("Cannot login. Ensure your data is correct and you have internet connection");

                response.json().then(function (data) {
                    alert(data.message);
                });
                throw new Error("HTTP status " + response.status);
            }

            return response.json();
        })
        .then(function (result) {

            // console.log(`What's left ${JSON.stringify(result.details)}`);

            let isComplete = "Complete";

            statesWithViolenceArray = [];

            for (let index = 0; index < result.details.length; index++) {
                const element = result.details[index];
                if (element.firstApproval === true && element.secondApproval === true) {

                    if (isSearch) {

                        console.log(dateFormat(element.dateOfIncidence));
                        console.log(dateFormat(filter_df));

                            console.log((dateFormat(filter_df) !== "" && dateFormat(filter_df) === dateFormat(element.dateOfIncidence)));

                        if ((sBox !== null && sBox.toLowerCase() === element.incidentTitle.toLowerCase()) || (filter_vt !== null && filter_vt.toLowerCase() === element.electionType.toLowerCase()) && (filter_state !== null && filter_state.toLowerCase() === element.state.toLowerCase()) || (dateFormat(filter_df) !== "" && dateFormat(filter_df) === dateFormat(element.dateOfIncidence))) {
                            statesWithViolenceArray.push(element);
                        }

                        // if ((sBox !== null && sBox.toLowerCase() === element.incidentTitle.toLowerCase())) {
                        //     statesWithViolenceArray.push(element);
                        // }

                        // if ((filter_vt !== null && filter_vt.toLowerCase() === element.electionType.toLowerCase())) {
                        //     statesWithViolenceArray.push(element);

                        // }

                        // if ((filter_state !== null && filter_state.toLowerCase() === element.state.toLowerCase())) {
                        //     statesWithViolenceArray.push(element);

                        // }

                        // if (dateFormat(filter_df) !== "" && dateFormat(filter_df) === dateFormat(element.dateOfIncidence)) {
                        //     statesWithViolenceArray.push(element);

                        // }

                        // if (dateFormat(filter_year) !== "") {
                        //     current_date = new Date(dateFormat(element.dateOfIncidence)).getFullYear();

                        //     console.log((current_date == filter_year));

                        //     if (filter_year == current_date) {
                        //         statesWithViolenceArray.push(element);
                        //     }

                        // }

                    } else {
                        statesWithViolenceArray.push(element);
                        
                    }

                }

            }

            console.log(`What's left ${statesWithViolenceArray.length}`);

            if(isSearch){

            console.log(`What's left again ${statesWithViolenceArray.length}`);


                for (let index = 0; index < statesWithViolenceArray.length; index++) {
                    const element = statesWithViolenceArray[index];
                    if(index === 0){
                        // statesWithViolenceArray.forEach(element => {
                            $('#getReports').html(
                                '<div class="report-card open-button" popup-open="popup-1" href="javascript:void(0)" hidden><span class="report-card__status report-card__status--ok">' + isComplete + '</span><h5 class="report-card__heading">' + jsUcfirst(element.incidentTitle) + '</h5><br/><p  style="color: black;font-size: 12px;font-weight: 300;">' + jsUcfirst(element.incidentDesc) + '</p><div class="report-card__date"><span class="report-card__date--time">' + element.timeOfReport + '</span><spanclass="report-card__date--day">' + element.dateOfReport + '</span></div></div>'
                            );
                        // });
                    }else{
                        $('#getReports').append(
                            '<div class="report-card open-button" popup-open="popup-1" href="javascript:void(0)" hidden><span class="report-card__status report-card__status--ok">' + isComplete + '</span><h5 class="report-card__heading">' + jsUcfirst(element.incidentTitle) + '</h5><br/><p  style="color: black;font-size: 12px;font-weight: 300;">' + jsUcfirst(element.incidentDesc) + '</p><div class="report-card__date"><span class="report-card__date--time">' + element.timeOfReport + '</span><spanclass="report-card__date--day">' + element.dateOfReport + '</span></div></div>'
                        );
                    }
                }

            }else{
                statesWithViolenceArray.forEach(element => {
                    $('#getReports').append(
                        '<div class="report-card open-button" popup-open="popup-1" href="javascript:void(0)" hidden><span class="report-card__status report-card__status--ok">' + isComplete + '</span><h5 class="report-card__heading">' + jsUcfirst(element.incidentTitle) + '</h5><br/><p  style="color: black;font-size: 12px;font-weight: 300;">' + jsUcfirst(element.incidentDesc) + '</p><div class="report-card__date"><span class="report-card__date--time">' + element.timeOfReport + '</span><spanclass="report-card__date--day">' + element.dateOfReport + '</span></div></div>'
                    );
                });
            }

            $("#getReports > div").slice(0, 6).show();

            $(".centerAlign").show();

            $("#getReports > div").click(function () {
                openPopup(statesWithViolenceArray[$(this).index()]);
            });

        })
        .catch(error => console.log('error', error));

}

$(function () {
    $("#loadMoreReport").on("click", function (e) {
        e.preventDefault();
        $("#getReports > div:hidden").slice(0, 6).fadeIn();
    });

});

function openPopup(element) {

    const reportPopup = document.querySelector(".report-popup");
    const reports = document.querySelectorAll(".report-cards .report-card");
    reports.forEach((report) => {
        report.addEventListener("click", (e) => {
            reportPopup.classList.add("active");
        });
    });
    try {
        reportPopup.addEventListener("click", (e) => {
            if (e.target !== e.currentTarget) return;
            reportPopup.classList.remove("active");
        });
    } catch (error) {}

    // $("#getReports > div").on("click", function () {
    // $(".report-popup").show();
    // });

    console.log(element);

    let incidentViolenceDiv = document.createElement("div");
    let violenceVictimDiv = document.createElement("div");
    let violencePerpertratorDiv = document.createElement("div");

    element.incidentViolence.forEach(element => {
        let incidentViolenceBody = document.createElement("div");
        incidentViolenceBody.innerHTML = '<br/><div>Type Of Violence</div><p class="popup-p">' + element.violenceType + '</p>';

        incidentViolenceDiv.append(incidentViolenceBody);
        // $('<li>'+element.violenceType+'</li>').appendTo(incidentViolenceDiv);
    });

    element.incidentViolenceVictims.forEach(element => {
        let incidentViolenceVictimsBody = document.createElement("div");
        incidentViolenceVictimsBody.innerHTML = '<br/><div>Violence Victim</div><p class="popup-p">' + element.violenceVictimsType + ": " + element.violenceVictimsDesc + '</p>';

        violenceVictimDiv.append(incidentViolenceVictimsBody);
        // $('<li>'+element.violenceType+'</li>').appendTo(incidentViolenceDiv);
    });

    element.violenceInitiator.forEach(element => {
        let incidentViolencePerpertratorBody = document.createElement("div");
        incidentViolencePerpertratorBody.innerHTML = '<br/><div>Perpertrator</div><p class="popup-p">' + element.violenceInitiatorType + ": " + element.violenceInitiatorDesc + '</p>';

        violencePerpertratorDiv.append(incidentViolencePerpertratorBody);
        // $('<li>'+element.violenceType+'</li>').appendTo(incidentViolenceDiv);
    });

    $(".report-popup__content ").html('<div ><div><i id="close-report" class="far fa-window-close" style="float: right;width:50px;height: 50px;font-size: 25px;color: red;" aria-hidden="true"></i></div><div>Title</div><h5 class="report-card__heading">' + jsUcfirst(element.incidentTitle) + '</h5><br/><div>Incident Description</div><p class="popup-p">' + jsUcfirst(element.incidentDesc) + '</p><br/><div>Election Type</div><p class="popup-p">' + jsUcfirst(element.electionType) + '</p><br/><div>Where The Incident Happened</div><p class="popup-p">' + jsUcfirst(element.geoPoliticalZone) + " | " + jsUcfirst(element.state) + " | " + jsUcfirst(element.city) + '</p><br/><div>Location Of Report</div><p class="popup-p">' + jsUcfirst(element.locationOfReport) + '</p></div>');

    $(".report-popup__content ").append(incidentViolenceDiv);

    $(".report-popup__content ").append(violenceVictimDiv);

    $(".report-popup__content ").append(violencePerpertratorDiv);

    $(".report-popup__content ").append('<br/><div class="report-card__date"><span class="report-card__date--time">' + element.timeOfReport + '</span><spanclass="report-card__date--day">' + element.dateOfReport + '</span></div>');

    $(".report-popup").show();

    $("#close-report").on('click', function (e) {
        $('.report-popup').fadeOut('slow');
    });
}

function jsUcfirst(string) {
    if (string === null) return "";
    return string.charAt(0).toUpperCase() + string.slice(1);
}

$(".search_filter").click(function () {
    sBox = $(".filter_search_box").val();
    filter_state = $(".filter_state").val();
    filter_df = $(".filter_df").val();
    filter_dt = $(".filter_dt").val();
    filter_vt = $(".filter_vt").val();

    console.log(`"I am listening" ${sBox} ${filter_state} ${dateFormat(filter_df)} ${dateFormat(filter_dt)} ${filter_vt}`);

    getAllForms(true);

});

const dateFormat = (s) => {

    var newdate = s.includes("/") ? s.split("/").reverse().join("-") : s;

    var date = new Date(newdate);

    return date.toDateString();
}

function init() {
    getAllForms(false);

}

init();

// })(jQuery);