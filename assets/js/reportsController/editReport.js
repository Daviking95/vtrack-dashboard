//     var files = [];
//     document.getElementById("files").addEventListener("change", function (e) {
//         files = e.target.files;

//     uploadingMediaAsset(files);

//     });


// function uploadingMediaAsset(files) {
//     let mediaPath =
//         `violenceTrackMediaForm/monitorCode-${$('#monitorCodeSpinner').val().replace("/", "")}/formCode-${$('#txtFormCode').val().replace("/", "")}`;

//     var storagePath;

//     alert(mediaPath);


//     for (let index = 0; index < files.length; index++) {
//         const element = files[index];
//         storagePath = `${mediaPath}/${files[index].split("/")}`;

//         alert(`StoragePath ${storagePath}`);

//     }

//     document.getElementById("send").addEventListener("click", function () {
//         //checks if files are selected
//         if (files.length != 0) {

//             //Loops through all the selected files
//             for (let i = 0; i < files.length; i++) {

//                 //create a storage reference
//                 var storage = firebase.storage().ref(files[i].name);

//                 //upload file
//                 var upload = storage.put(files[i]);

//                 //update progress bar
//                 upload.on(
//                     "state_changed",
//                     function progress(snapshot) {
//                         var percentage =
//                             (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
//                         document.getElementById("progress").value = percentage;
//                     },

//                     function error() {
//                         alert("error uploading file");
//                     },

//                     function complete() {
//                         document.getElementById(
//                             "uploading"
//                         ).innerHTML += `${files[i].name} upoaded <br />`;
//                     }
//                 );
//             }
//         } else {
//             alert("No file chosen");
//         }
//     });
// }


(function ($) {
    var getToken = localStorage.getItem("x-auth-token");
    var urlParams = new URLSearchParams(window.location.search);
    var getQueryString = urlParams.get('form_id');

    var getRole = localStorage.getItem("role");
    // console.log(getRole);

    'use strict';

    $.urlParam = function (name) {
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        return results[1] || 0;
    }

    function jsUcfirst(string) {
        if (string === null) return "";
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    function getEditedReport() {

        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("x-auth-token", getToken);

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        fetch("https://peaceful-atoll-23751.herokuapp.com/" + `just-clock-285910.uc.r.appspot.com/api/form/one/${getQueryString}`, requestOptions)
            .then(function (response) {
                if (response.status === 403) {
                    alert("Token expired. Login");
                    window.location.href = "../index.php";
                }
                if (!response.ok) {
                    // alert("Cannot login. Ensure your data is correct and you have internet connection");
                    throw new Error("HTTP status " + response.status);
                }

                return response.json();
            })
            .then(function (result) {

                monitorList = result.details;

                console.log(result.details);

                populateFormField(result.details);

                $("#form-horizontal").show();
                $("#loader").hide();
                if (getRole === "approver") $("#delete_form").show();

            })
            .catch(error => console.log('error', error));

    }

    function populateFormField(details) {

        console.log(details.incidentSource);

        displayImageList(details.mediaAsset);

        localStorage.setItem("formFirstApproval", details.firstApproval);
        localStorage.setItem("formSecondApproval", details.secondApproval);

        console.log(details.monitorCode);
        $('#txtFormCode').val(details.formCode);
        $('#timeOfReport').val(details.timeOfReport);
        $('#firstApproval').val(details.firstApproval);
        $('#secondApproval').val(details.secondApproval);
        $('#formSubmissionPoint').val(details.formSubmissionPoint);
        $('#locationOfReport').val(details.locationOfReport);

        $('#txtIncidenceTitle').val(jsUcfirst(details.incidentTitle));
        $('#txtIncidenceDesc').val(jsUcfirst(details.incidentDesc));
        // $('#monitorCodeSpinner').val(details.monitorCode);
        if (details.monitorCode !== "") {
            $('#monitorCodeSpinner').replaceWith('<input id="monitorCodeSpinner" type="text" class="form-control"  value="' + jsUcfirst(details.monitorCode) + '" disabled>');
        }

        // $('#monitorCodeSpinner option[value="' + details.monitorCode + '"]').attr('selected', 'selected');

        $('#txtMonitorName').val(jsUcfirst(details.monitorName));
        $('#txtReportDate').val(details.dateOfReport);
        $('#txtDateOfIncidence').val(dateFormat(details.dateOfIncidence)); // .val('2030-12-31'); // .val(details.dateOfIncidence);
        $('#electionSpinner option[value="' + details.electionType + '"]').attr('selected', 'selected');
        $('#timeOfIncidenceSpinner option[value="' + details.timeOfIncidence + '"]').attr('selected', 'selected');
        $('#geoPoliticalZoneSpinner option[value="' + details.geoPoliticalZone + '"]').attr('selected', 'selected');
        // $('#stateSpinner option[value="' + details.state + '"]').attr('selected', 'selected');
        if (details.state !== "") {
            $('#stateSpinnerEdit').replaceWith('<input id="stateSpinnerEdit" type="text" class="form-control" placeholder="Which newspaper" value="' + jsUcfirst(details.state) + '">');
            $("#stateSpinner").hide();
        }

        $('#lgaSpinner').val(jsUcfirst(details.city));

        $("#sourceWitnessCheck01").attr('checked', fillIncidenceSource(details.incidentSource, 1));
        $("#sourceWitnessCheck02").attr('checked', fillIncidenceSource(details.incidentSource, 2));
        $("#sourceWitnessCheck03").attr('checked', fillIncidenceSource(details.incidentSource, 3));
        $("#sourceWitnessCheck04").attr('checked', fillIncidenceSource(details.incidentSource, 4));
        $("#sourceWitnessCheck05").attr('checked', fillIncidenceSource(details.incidentSource, 5));
        $("#sourceWitnessCheck06").attr('checked', fillIncidenceSource(details.incidentSource, 6));
        $("#sourceWitnessCheck07").attr('checked', fillIncidenceSource(details.incidentSource, 7));
        $("#sourceWitnessCheck08").attr('checked', fillIncidenceSource(details.incidentSource, 8));
        $("#sourceWitnessCheck09").attr('checked', fillIncidenceSource(details.incidentSource, 9));
        $("#sourceWitnessCheck10").attr('checked', fillIncidenceSource(details.incidentSource, 10));

        $("#mediaSourceCheck01").attr('checked', fillmediaSource(details.mediaSource, 1));
        $("#mediaSourceCheck02").attr('checked', fillmediaSource(details.mediaSource, 2));
        $("#mediaSourceCheck03").attr('checked', fillmediaSource(details.mediaSource, 3));
        $("#mediaSourceCheck04").attr('checked', fillmediaSource(details.mediaSource, 4));
        $("#mediaSourceCheck05").attr('checked', fillmediaSource(details.mediaSource, 5));
        $("#mediaSourceCheck06").attr('checked', fillmediaSource(details.mediaSource, 6));
        $("#mediaSourceCheck07").attr('checked', fillmediaSource(details.mediaSource, 7));

        whereIncidentHappened(details.incidentLocation);

        violenceInitiator(details.violenceInitiatorPeopleAndGenderInvolved);

        $("#violenceInitiatorCheck01").attr('checked', fillViolenceInitiator(details.violenceInitiator, 1));
        $("#violenceInitiatorCheck02").attr('checked', fillViolenceInitiator(details.violenceInitiator, 2));
        $("#violenceInitiatorCheck03").attr('checked', fillViolenceInitiator(details.violenceInitiator, 3));
        $("#violenceInitiatorCheck04").attr('checked', fillViolenceInitiator(details.violenceInitiator, 4));
        $("#violenceInitiatorCheck05").attr('checked', fillViolenceInitiator(details.violenceInitiator, 5));
        $("#violenceInitiatorCheck06").attr('checked', fillViolenceInitiator(details.violenceInitiator, 6));
        $("#violenceInitiatorCheck07").attr('checked', fillViolenceInitiator(details.violenceInitiator, 7));
        $("#violenceInitiatorCheck08").attr('checked', fillViolenceInitiator(details.violenceInitiator, 8));
        $("#violenceInitiatorCheck09").attr('checked', fillViolenceInitiator(details.violenceInitiator, 9));
        $("#violenceInitiatorCheck10").attr('checked', fillViolenceInitiator(details.violenceInitiator, 10));
        $("#violenceInitiatorCheck11").attr('checked', fillViolenceInitiator(details.violenceInitiator, 11));
        $("#violenceInitiatorCheck12").attr('checked', fillViolenceInitiator(details.violenceInitiator, 12));

        violenceVictim(details.violenceVictimPeopleAndGenderInvolved);

        $("#violenceVictimCheck01").attr('checked', fillViolenceVictim(details.incidentViolenceVictims, 1));
        $("#violenceVictimCheck02").attr('checked', fillViolenceVictim(details.incidentViolenceVictims, 2));
        $("#violenceVictimCheck03").attr('checked', fillViolenceVictim(details.incidentViolenceVictims, 3));
        $("#violenceVictimCheck04").attr('checked', fillViolenceVictim(details.incidentViolenceVictims, 4));
        $("#violenceVictimCheck05").attr('checked', fillViolenceVictim(details.incidentViolenceVictims, 5));
        $("#violenceVictimCheck06").attr('checked', fillViolenceVictim(details.incidentViolenceVictims, 6));
        $("#violenceVictimCheck07").attr('checked', fillViolenceVictim(details.incidentViolenceVictims, 7));
        $("#violenceVictimCheck08").attr('checked', fillViolenceVictim(details.incidentViolenceVictims, 8));
        $("#violenceVictimCheck09").attr('checked', fillViolenceVictim(details.incidentViolenceVictims, 9));
        $("#violenceVictimCheck10").attr('checked', fillViolenceVictim(details.incidentViolenceVictims, 10));
        $("#violenceVictimCheck11").attr('checked', fillViolenceVictim(details.incidentViolenceVictims, 11));
        $("#violenceVictimCheck12").attr('checked', fillViolenceVictim(details.incidentViolenceVictims, 12));

        $("#violenceKindCheck01").attr('checked', fillViolenceKind(details.incidentViolence, 1));
        $("#violenceKindCheck02").attr('checked', fillViolenceKind(details.incidentViolence, 2));
        $("#violenceKindCheck03").attr('checked', fillViolenceKind(details.incidentViolence, 3));
        $("#violenceKindCheck04").attr('checked', fillViolenceKind(details.incidentViolence, 4));
        $("#violenceKindCheck05").attr('checked', fillViolenceKind(details.incidentViolence, 5));
        $("#violenceKindCheck06").attr('checked', fillViolenceKind(details.incidentViolence, 6));
        $("#violenceKindCheck07").attr('checked', fillViolenceKind(details.incidentViolence, 7));
        $("#violenceKindCheck08").attr('checked', fillViolenceKind(details.incidentViolence, 8));
        $("#violenceKindCheck09").attr('checked', fillViolenceKind(details.incidentViolence, 9));
        $("#violenceKindCheck10").attr('checked', fillViolenceKind(details.incidentViolence, 10));
        $("#violenceKindCheck11").attr('checked', fillViolenceKind(details.incidentViolence, 11));

        $("#violenceWeaponCheck01").attr('checked', fillViolenceWeapon(details.incidentWeapon, 1));
        $("#violenceWeaponCheck02").attr('checked', fillViolenceWeapon(details.incidentWeapon, 2));
        $("#violenceWeaponCheck03").attr('checked', fillViolenceWeapon(details.incidentWeapon, 3));
        $("#violenceWeaponCheck04").attr('checked', fillViolenceWeapon(details.incidentWeapon, 4));
        $("#violenceWeaponCheck05").attr('checked', fillViolenceWeapon(details.incidentWeapon, 5));
        $("#violenceWeaponCheck06").attr('checked', fillViolenceWeapon(details.incidentWeapon, 6));
        $("#violenceWeaponCheck07").attr('checked', fillViolenceWeapon(details.incidentWeapon, 7));
        $("#violenceWeaponCheck08").attr('checked', fillViolenceWeapon(details.incidentWeapon, 8));
        $("#violenceWeaponCheck09").attr('checked', fillViolenceWeapon(details.incidentWeapon, 9));

        $("#violenceImpactCheck01").attr('checked', fillViolenceImpact(details.incidentImpact, 1));
        $("#violenceImpactCheck02").attr('checked', fillViolenceImpact(details.incidentImpact, 2));
        $("#violenceImpactCheck03").attr('checked', fillViolenceImpact(details.incidentImpact, 3));
        $("#violenceImpactCheck04").attr('checked', fillViolenceImpact(details.incidentImpact, 4));
        $("#violenceImpactCheck05").attr('checked', fillViolenceImpact(details.incidentImpact, 5));
        $("#violenceImpactCheck06").attr('checked', fillViolenceImpact(details.incidentImpact, 6));
        $("#violenceImpactCheck07").attr('checked', fillViolenceImpact(details.incidentImpact, 7));
        $("#violenceImpactCheck08").attr('checked', fillViolenceImpact(details.incidentImpact, 8));
        $("#violenceImpactCheck09").attr('checked', fillViolenceImpact(details.incidentImpact, 9));
        $("#violenceImpactCheck10").attr('checked', fillViolenceImpact(details.incidentImpact, 10));
        $("#violenceImpactCheck11").attr('checked', fillViolenceImpact(details.incidentImpact, 11));
        $("#violenceImpactCheck12").attr('checked', fillViolenceImpact(details.incidentImpact, 12));

        afterIncident(details.afterIncident);

        $('#txtMoreReport').val(details.moreReportDetails.moreReportDetailsOption);

    }

    function displayImageList(mediaAsset) {

        if (mediaAsset != null)
            mediaAsset.forEach(element => {
                var _agentImageRef = firebase.storage().refFromURL(element);
                _agentImageRef.getDownloadURL().then(function (url) {
                    console.log(`log1 ${url}`);

                    $('#gallery_div').append("<a data-fancybox='gallery' href=" + url + "><img style='width: 150px; height: 150px; margin: 10px;' src=" + url + "></img></a>");

                });
            });

    }

    function fillIncidenceSource(incidentSource, position) {
        let isCheck = false;
        let didSourceSeeIncident = "";
        incidentSource.forEach(element => {

            if (element.incidentSourceOptionPosition == position) {
                isCheck = true;

                if (position == 10) {
                    $("#witness" + position + "Spinner").html('<input id="sourceWitness' + position + 'Spinner" name="sourceWitness' + position + 'Spinner" type="text" class="form-control" placeholder="Specify">');
                    $("#witness" + position + "Spinner").toggle();
                    return;
                }

                $("#witness" + position + "Spinner").html('<select id="sourceWitness' + position + 'Spinner"name="sourceWitness' + position + 'Spinner"class="select2 form-control mb-3 custom-select" style="width: 100%; height:36px;" data-placeholder="Choose"> <option value="" selected="true" disabled="disabled">Did the source see the incident happen ?</option> <option value="Yes">Yes</option> <option value="No">No</option> </select>');
                $("#witness" + position + "Spinner").toggle();

                // console.log(element);

                didSourceSeeIncident = element.didSourceSeeIncident == "" ? "No" : element.didSourceSeeIncident;

                $("#witness" + position + "Spinner option[value=" + didSourceSeeIncident + "]").attr('selected', 'selected');

            }
        });

        return isCheck;
    }

    function fillmediaSource(mediaSource, id) {
        let isCheck = false;
        let nameOne = "";
        mediaSource.forEach(element => {

            if (element.mediaSourceOptionPosition == id) {
                isCheck = true;

                if (id == 1) {
                    $("#media" + id + "Spinner").html('<input id="mediaSource' + id + 'Spinner" name="mediaSource' + id + 'Spinner" type="text" class="form-control" placeholder="Which newspaper" value="' + element.nameOne + '"><br> <input id="mediaSource' + id + 'Spinner2" name="mediaSource' + id + 'Spinner2" type="date" class="form-control" placeholder="Date" value="' + dateFormat(element.nameTwo) + '">');
                    $("#media" + id + "Spinner").toggle();
                    return;
                }

                if (id == 2) {
                    $("#media" + id + "Spinner").html('<input id="mediaSource' + id + 'Spinner" name="mediaSource' + id + 'Spinner" type="text" class="form-control" placeholder="Which Media house" value="' + element.nameOne + '"><br> <input id="mediaSource' + id + 'Spinner2" name="mediaSource' + id + 'Spinner2" type="text" class="form-control" placeholder="Media Link" value="' + element.nameTwo + '">');
                    $("#media" + id + "Spinner").toggle();
                    return;
                }

                if (id == 3) {
                    element.nameOne = element.nameOne == "" ? "facebook" : element.nameOne;

                    $("#media" + id + "Spinner").html('<select id="mediaSource' + id + 'Spinner"name="mediaSource' + id + 'Spinner"class="select2 form-control mb-3 custom-select" style="width: 100%; height:36px;" data-placeholder="Choose"> <option value="" selected="true" disabled="disabled">Select Platform</option> <option value="facebook">Facebook</option> <option value="whatsapp">WhatsApp</option> <option value="twitter">Twitter</option> <option value="instagram">Instagram</option> <option value="skype">Skype</option> <option value="telegram">Telegram</option> <option value="wechat">WeChat</option> </select><br> <input id="mediaSource' + id + 'Spinner2" name="mediaSource' + id + 'Spinner2" type="text" class="form-control" placeholder="Media Link" value="' + element.nameTwo + '">');
                    $("#media" + id + "Spinner").toggle();
                    $("#media" + id + "Spinner option[value=" + element.nameOne + "]").attr('selected', 'selected');

                    return;
                }

                if (id == 4 || id == 5) {
                    $("#media" + id + "Spinner").html('<input id="mediaSource' + id + 'Spinner" name="mediaSource' + id + 'Spinner" type="text" class="form-control" placeholder="Which Channel" value="' + element.nameOne + '"><br> <input id="mediaSource' + id + 'Spinner2" name="mediaSource' + id + 'Spinner2" type="text" class="form-control" placeholder="Which Program" value="' + dateFormat(element.nameTwo) + '"><br> <input id="mediaSource' + id + 'Spinner3" name="mediaSource' + id + 'Spinner3" type="date" class="form-control" placeholder="Date" value="' + element.nameThree + '">');
                    $("#media" + id + "Spinner").toggle();
                    return;
                }

                if (id == 6) {
                    $("#media" + id + "Spinner").html('<input id="mediaSource' + id + 'Spinner" name="mediaSource' + id + 'Spinner" type="text" class="form-control" placeholder="Specify Others" value="' + element.nameOne + '">');
                    $("#media" + id + "Spinner").toggle();
                    return;
                }

            }
        });

        return isCheck;
    }

    function whereIncidentHappened(incidentLocation) {
        $('#whereIncidentHappenedSpinner option[value="' + incidentLocation.locationOfIncident + '"]').attr('selected', 'selected');
        $("#incidentLocationSpecify").html('<br><input id="incidentLocationSpec" name="incidentLocationSpec" type="text" class="form-control" placeholder="Specify" value="' + incidentLocation.specifylocationOfIncident + '">');
        $("#incidentLocationSpecify").show();
    }

    function violenceInitiator(violenceInitiatorPeopleAndGenderInvolved) {
        $('#genderOfIncidentSpinners option[value="' + violenceInitiatorPeopleAndGenderInvolved.violenceInitiatorGenderOfIncident + '"]').attr('selected', 'selected');

        $('#peopleOfIncidentSpinners option[value="' + violenceInitiatorPeopleAndGenderInvolved.violenceInitiatorPeopleOfIncident + '"]').attr('selected', 'selected');
    }

    function fillViolenceInitiator(violenceInitiator, position) {
        let isCheck = false;
        violenceInitiator.forEach(element => {

            if (element.violenceInitiatorPosition == position) {
                isCheck = true;

                $("#violenceInitiator" + position + "Spinner").html('<input id="violenceInitiatorDesc' + position + 'Spinner" name="violenceInitiatorDesc' + position + 'Spinner" type="text" class="form-control" placeholder="Specify" value="' + element.violenceInitiatorDesc + '">');
                $("#violenceInitiator" + position + "Spinner").toggle();
                return;

            }
        });

        return isCheck;
    }

    function violenceVictim(violenceVictimPeopleAndGenderInvolved) {
        $('#victimTypeSpinners option[value="' + violenceVictimPeopleAndGenderInvolved.violenceVictimTypeOfIncident + '"]').attr('selected', 'selected');

        $('#victimPeopleOfIncidentSpinners option[value="' + violenceVictimPeopleAndGenderInvolved.violenceVictimsPeopleOfIncident + '"]').attr('selected', 'selected');

        $('#victimGenderOfIncidentSpinners option[value="' + violenceVictimPeopleAndGenderInvolved.violenceVictimsGenderOfIncident + '"]').attr('selected', 'selected');
    }

    function fillViolenceVictim(incidentViolenceVictims, position) {
        let isCheck = false;
        incidentViolenceVictims.forEach(element => {

            if (element.incidentViolenceVictimsPosition == position) {
                isCheck = true;

                $("#violenceVictim" + position + "Spinner").html('<input id="violenceVictimDesc' + position + 'Spinner" name="violenceVictimDesc' + position + 'Spinner" type="text" class="form-control" placeholder="Specify" value="' + element.violenceVictimsDesc + '">');
                $("#violenceVictim" + position + "Spinner").toggle();
                return;

            }
        });

        return isCheck;
    }

    function fillViolenceKind(incidentViolence, position) {
        let isCheck = false;
        incidentViolence.forEach(element => {

            if (element.incidentViolencePosition == position) {
                isCheck = true;

                if (position == 1 || position == 2 || position == 3 || position == 6 || position == 11) {

                    $("#violenceKind" + position + "Spinner").html('<input id="violenceKindDesc' + position + 'Spinner" name="violenceKindDesc' + position + 'Spinner" type="text" class="form-control" placeholder="Specify" value="' + element.violenceDesc + '">');
                    $("#violenceKind" + position + "Spinner").toggle();
                    return;
                }

            }
        });

        return isCheck;
    }

    function fillViolenceWeapon(incidentWeapon, position) {
        let isCheck = false;
        incidentWeapon.forEach(element => {

            if (element.incidentWeaponPosition == position) {
                isCheck = true;

                if (position == 9) {
                    $("#violenceWeapon" + position + "Spinner").html('<input id="violenceWeaponDesc' + position + 'Spinner" name="violenceWeaponDesc' + position + 'Spinner" type="text" class="form-control" placeholder="Specify" value="' + element.weaponDesc + '">');
                    $("#violenceWeapon" + position + "Spinner").toggle();
                    return;
                }

            }
        });

        return isCheck;
    }

    function fillViolenceImpact(incidentImpact, id) {
        let isCheck = false;
        incidentImpact.forEach(element => {

            if (element.incidentImpactPosition == id) {
                isCheck = true;

                if (id == 11) {
                    $("#violenceImpact" + id + "Spinner").html('<input id="violenceImpactSource' + id + 'Spinner" name="violenceImpactSource' + id + 'Spinner" type="text" class="form-control" placeholder="Specify" value="' + element.impactDesc + '">');
                    $("#violenceImpact" + id + "Spinner").toggle();
                    return;
                }

                if (id == 8 || id == 9 || id == 10) {
                    $("#violenceImpact" + id + "Spinner").html('<select id="violenceImpactSource' + id + 'Spinner" name="violenceImpactSource' + id + 'Spinner"class="select2 form-control mb-3 custom-select" style="width: 100%; height:36px;" data-placeholder="Choose"> <option value="" selected="true" disabled="disabled">Locally or nationally ?</option> <option value="Locally">Locally</option> <option value="Nationally">Nationally</option> </select>');
                    $("#violenceImpact" + id + "Spinner").toggle();
                    $("#violenceImpact" + id + "Spinner option[value=" + element.impactDesc + "]").attr('selected', 'selected');
                }

                // ----------------------------------------------------

                // $("#violenceImpact" + position + "Spinner").html('<input id="violenceImpact' + position + 'Spinner" name="violenceImpact' + position + 'Spinner" type="text" class="form-control" placeholder="Specify" value="' + element.impactDesc + '">');
                // $("#violenceImpact" + position + "Spinner").toggle();
                // return;

            }
        });

        return isCheck;
    }

    function afterIncident(afterIncident) {
        $('#txtAfterIncidenceDesc').val(afterIncident.afterIncidentDetails);

        $('#afterIncidenceSpinners option[value="' + afterIncident.afterIncidentOption + '"]').attr('selected', 'selected');

    }

    const capitalize = (s) => {
        if (typeof s !== 'string') return ''
        return s.charAt(0).toUpperCase() + s.slice(1)
    }

    function titleCase(str) {
        var splitStr = str.toLowerCase().split(' ');
        for (var i = 0; i < splitStr.length; i++) {
            splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
        }
        return splitStr.join(' ');
    }

    // todo 
    const dateFormat = (s) => {

        var now = new Date(s);

        var newdate = s.split("/").reverse().join("-");

        return newdate;
    }

    function init() {
        getEditedReport();
        deleteButtonOnClick();
        Waves.init();
    }

    function deleteButtonOnClick() {
        $("#delete_form").click(function () {
            var SweetAlert = function () {};

            SweetAlert.prototype.init = function () {

                swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    reverseButtons: true
                }).then((result) => {
                    if (result.value) {

                        deleteReport();
                        // swal.fire(
                        //     'Deleted!',
                        //     'The report has been deleted.',
                        //     'success'
                        //   )
                        // else {
                        // swal.fire(
                        //     'Error!',
                        //     'Report cannot be deleted. Please try again.',
                        //     'success'
                        //   )
                        // }

                    } else if (
                        // Read more about handling dismissals
                        result.dismiss === Swal.DismissReason.cancel
                    ) {
                        swal.fire(
                            'Cancelled',
                            'The report is safe :)',
                            'error'
                        )
                    }
                })
            }

            $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            $.SweetAlert.init()
        });
    }

    function deleteReport() {

        $("#form-horizontal").hide();
        $("#loader").show();

        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("x-auth-token", getToken);

        var requestOptions = {
            method: 'DELETE',
            headers: myHeaders,
            redirect: 'follow'
        };

        fetch("https://peaceful-atoll-23751.herokuapp.com/" + `just-clock-285910.uc.r.appspot.com/api/form/one/${getQueryString}`, requestOptions)
            .then(function (response) {
                if (response.status === 403) {
                    alert("Token expired. Login");
                    window.location.href = "../index.php";
                }
                if (!response.ok) {
                    // alert("Cannot login. Ensure your data is correct and you have internet connection");
                    throw new Error("HTTP status " + response.status);
                }

                return response.json();
            })
            .then(function (result) {

                alert(result.message);

                $("#form-horizontal").show();
                $("#loader").hide();

                window.location.href = "all.php";

            })
            .catch(error => console.log('error', error));
    }

    if ($.urlParam('form_id') !== 0) {

        $("#media_asset").hide();
        $("#form-horizontal").hide();
        $("#loader").show();

        init();
    }


})(jQuery)