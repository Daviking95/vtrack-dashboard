<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="float-right">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0);">VTrack</a></li>
                    <li class="breadcrumb-item active"><?php echo $page_title ?></li>
                </ol>
            </div>
            <h4 class="page-title"><?php echo $page_title ?></h4>
        </div>
        <!--end page-title-box-->
    </div>
    <!--end col-->
</div>