
function readURL(input) {
    total_file = input.files.length;
    for (var i = 0; i < total_file; i++) {
        $('#image_preview').append("<a data-fancybox='gallery' href='" + URL.createObjectURL(event.target.files[i]) + "'><img class='media-picture__img' style='width: 100px;float: left;margin: 10px;' src='" + URL.createObjectURL(event.target.files[i]) + "'></a>");
    }

    $('#image_preview').show();
}

(function ($) {
    var getToken = localStorage.getItem("x-auth-token");

    'use strict';

    $("#form-horizontal").show();

    var monitorList = [];

    var filteredStates = [];

    var state1Spinners = [
        "Benue",
        "Kogi",
        "Kwara",
        "Nasarawa",
        "Niger",
        "Plateau",
        "Federal Capital Territory"
    ];

    var state2Spinners = [
        "Adamawa",
        "Bauchi",
        "Borno",
        "Gombe",
        "Taraba",
        "Yobe"
    ];

    var state3Spinners = [
        "Jigawa",
        "Kaduna",
        "Kano",
        "Katsina",
        "Kebbi",
        "Sokoto",
        "Zamfara"
    ];

    var state4Spinners = [
        "Abia",
        "Anambra",
        "Ebonyi",
        "Enugu",
        "Imo"
    ];

    var state5Spinners = [
        "Akwa Ibom",
        "Bayelsa",
        "Cross River",
        "Rivers",
        "Delta",
        "Edo"
    ];

    var state6Spinners = [
        "Ekiti",
        "Lagos",
        "Ogun",
        "Ondo",
        "Osun",
        "Oyo"
    ];

    function getReports() {

        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("x-auth-token", getToken);

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        // https://vtrack-api.herokuapp.com
        fetch("https://peaceful-atoll-23751.herokuapp.com/" + "just-clock-285910.uc.r.appspot.com/api/monitor/all", requestOptions)
            .then(function (response) {
                if (response.status === 403) {
                    alert("Token expired. Login");
                    window.location.href = "../index.php";
                }
                if (!response.ok) {
                    alert("Cannot login. Ensure your data is correct and you have internet connection");
                    throw new Error("HTTP status " + response.status);
                }

                return response.json();
            })
            .then(function (result) {

                monitorList = result.details;

                populateMonitorCodeSpinner(result.details);

                displayMonitorName();

                populateStateOnGeoChange();

                displayReportDate();

                sourceWitnessCheckDisplayMoreOptions();

                mediaSourceCheckDisplayMoreOptions();

                showOptionOnIncidentLocationChange();

                violenceInitiatorCheckDisplayMoreOptions();

                violenceVictimCheckDisplayMoreOptions();

                violenceKindCheckDisplayMoreOptions();

                violenceWeaponCheckDisplayMoreOptions();

                violenceImpactCheckDisplayMoreOptions();

            })
            .catch(error => console.log('error', error));

    }

    function populateMonitorCodeSpinner(result) {
        $("#monitorCodeSpinner").append($("<option />").val("-1").text("Select monitor code"));

        $.each(result, function () {
            $("#monitorCodeSpinner").append($("<option />").val(this.user_monitor_code).text(this.user_monitor_code));
        });
    }

    function displayMonitorName() {
        $("#monitorCodeSpinner").change(function () {
            fetchMonitorName();
        });
    }

    function displayReportDate() {
        var fullDate = new Date();
        var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '0' + (fullDate.getMonth() + 1);

        var currentDate = fullDate.getDate() + "-" + twoDigitMonth + "-" + fullDate.getFullYear();

        document.getElementById("txtReportDate").innerHTML = currentDate;
        // $("#txtreportDate").val("Hi");
    }

    function fetchMonitorName() {
        var selectedMonitorName = $("#monitorCodeSpinner").val();
        $("#txtMonitorName").val("");

        monitorList.forEach(element => {
            if (element.user_monitor_code == selectedMonitorName) {
                $("#txtMonitorName").val(element.user_first_name + " " + element.user_last_name);
                return;
            }
        });
    }

    function populateStateOnGeoChange() {

        $("#geoPoliticalZoneSpinner").change(function () {

            filteredStates = [];
            $("#stateSpinner").empty();
            $("#stateSpinnerEdit").hide();

            switch ($("#geoPoliticalZoneSpinner").prop('selectedIndex')) {
                case 1:
                    filteredStates = state1Spinners;
                    break;
                case 2:
                    filteredStates = state2Spinners;
                    break;
                case 3:
                    filteredStates = state3Spinners;
                    break;
                case 4:
                    filteredStates = state4Spinners;
                    break;
                case 5:
                    filteredStates = state5Spinners;
                    break;
                case 6:
                    filteredStates = state6Spinners;
                    break;

                default:
                    break;
            }

            $.each(filteredStates, function () {
                $("#stateSpinner").append($("<option />").val(this).text(this));
            });

            $("#stateSpinner").show();

        });
    }

    function sourceWitnessCheckDisplayMoreOptions() {
        $("#sourceWitnessCheck01").change(function () {
            whereToShowSpinner(1);
        });

        $("#sourceWitnessCheck02").change(function () {
            whereToShowSpinner(2);
        });

        $("#sourceWitnessCheck03").change(function () {
            whereToShowSpinner(3);
        });

        $("#sourceWitnessCheck04").change(function () {
            whereToShowSpinner(4);
        });

        $("#sourceWitnessCheck05").change(function () {
            whereToShowSpinner(5);
        });

        $("#sourceWitnessCheck06").change(function () {
            whereToShowSpinner(6);
        });

        $("#sourceWitnessCheck07").change(function () {
            whereToShowSpinner(7);
        });

        $("#sourceWitnessCheck08").change(function () {
            whereToShowSpinner(8);
        });

        $("#sourceWitnessCheck09").change(function () {
            whereToShowSpinner(9);
        });

        $("#sourceWitnessCheck10").change(function () {
            whereToShowSpinner(10);
        });

    }

    function whereToShowSpinner(id) {

        if (id == 10) {
            $("#witness" + id + "Spinner").html('<input id="sourceWitness' + id + 'Spinner" name="sourceWitness' + id + 'Spinner" type="text" class="form-control" placeholder="Specify">');
            $("#witness" + id + "Spinner").toggle();
            return;
        }

        $("#witness" + id + "Spinner").html('<select id="sourceWitness' + id + 'Spinner"name="sourceWitness' + id + 'Spinner"class="select2 form-control mb-3 custom-select" style="width: 100%; height:36px;" data-placeholder="Choose"> <option value="" selected="true" disabled="disabled">Did the source see the incident happen ?</option> <option value="Yes">Yes</option> <option value="No">No</option> </select>');
        $("#witness" + id + "Spinner").toggle();
    }

    function mediaSourceCheckDisplayMoreOptions() {
        $("#mediaSourceCheck01").change(function () {
            whereToShowSpinnerForMediaSource(1);
        });

        $("#mediaSourceCheck02").change(function () {
            whereToShowSpinnerForMediaSource(2);
        });

        $("#mediaSourceCheck03").change(function () {
            whereToShowSpinnerForMediaSource(3);
        });

        $("#mediaSourceCheck04").change(function () {
            whereToShowSpinnerForMediaSource(4);
        });

        $("#mediaSourceCheck05").change(function () {
            whereToShowSpinnerForMediaSource(5);
        });

        $("#mediaSourceCheck06").change(function () {
            whereToShowSpinnerForMediaSource(6);
        });

        $("#mediaSourceCheck07").change(function () {
            whereToShowSpinnerForMediaSource(7);
        });
    }

    function whereToShowSpinnerForMediaSource(id) {

        if (id == 1) {
            $("#media" + id + "Spinner").html('<input id="mediaSource' + id + 'Spinner" name="mediaSource' + id + 'Spinner" type="text" class="form-control" placeholder="Which newspaper"><br> <input id="mediaSource' + id + 'Spinner2" name="mediaSource' + id + 'Spinner2" type="date" class="form-control" placeholder="Date">');
            $("#media" + id + "Spinner").toggle();
            return;
        }

        if (id == 2) {
            $("#media" + id + "Spinner").html('<input id="mediaSource' + id + 'Spinner" name="mediaSource' + id + 'Spinner" type="text" class="form-control" placeholder="Which Media house"><br> <input id="mediaSource' + id + 'Spinner2" name="mediaSource' + id + 'Spinner2" type="text" class="form-control" placeholder="Media Link">');
            $("#media" + id + "Spinner").toggle();
            return;
        }

        if (id == 3) {
            $("#media" + id + "Spinner").html('<select id="mediaSource' + id + 'Spinner"name="mediaSource' + id + 'Spinner"class="select2 form-control mb-3 custom-select" style="width: 100%; height:36px;" data-placeholder="Choose"> <option value="" selected="true" disabled="disabled">Select Platform</option> <option value="Facebook">Facebook</option> <option value="WhatsApp">WhatsApp</option> <option value="Twitter">Twitter</option> <option value="Instagram">Instagram</option> <option value="Skype">Skype</option> <option value="Telegram">Telegram</option> <option value="WeChat">WeChat</option> </select><br> <input id="mediaSource' + id + 'Spinner2" name="mediaSource' + id + 'Spinner2" type="text" class="form-control" placeholder="Platform Link">');
            $("#media" + id + "Spinner").toggle();
            return;
        }

        if (id == 4 || id == 5) {
            $("#media" + id + "Spinner").html('<input id="mediaSource' + id + 'Spinner" name="mediaSource' + id + 'Spinner" type="text" class="form-control" placeholder="Which Channel"><br> <input id="mediaSource' + id + 'Spinner2" name="mediaSource' + id + 'Spinner2" type="text" class="form-control" placeholder="Which Program"><br> <input id="mediaSource' + id + 'Spinner3" name="mediaSource' + id + 'Spinner3" type="date" class="form-control" placeholder="Date">');
            $("#media" + id + "Spinner").toggle();
            return;
        }

        if (id == 6) {
            $("#media" + id + "Spinner").html('<input id="mediaSource' + id + 'Spinner" name="mediaSource' + id + 'Spinner" type="text" class="form-control" placeholder="Specify Others">');
            $("#media" + id + "Spinner").toggle();
            return;
        }

    }

    function showOptionOnIncidentLocationChange() {
        $("#whereIncidentHappenedSpinner").change(function () {
            $("#incidentLocationSpecify").html('<br><input id="incidentLocationSpec" name="incidentLocationSpec" type="text" class="form-control" placeholder="Specify">');
            $("#incidentLocationSpecify").show();
        });
    }

    function violenceInitiatorCheckDisplayMoreOptions() {
        $("#violenceInitiatorCheck01").change(function () {
            whereToShowSpinnerViolenceInitiator(1);
        });

        $("#violenceInitiatorCheck02").change(function () {
            whereToShowSpinnerViolenceInitiator(2);
        });

        $("#violenceInitiatorCheck03").change(function () {
            whereToShowSpinnerViolenceInitiator(3);
        });

        $("#violenceInitiatorCheck04").change(function () {
            whereToShowSpinnerViolenceInitiator(4);
        });

        $("#violenceInitiatorCheck05").change(function () {
            whereToShowSpinnerViolenceInitiator(5);
        });

        $("#violenceInitiatorCheck06").change(function () {
            whereToShowSpinnerViolenceInitiator(6);
        });

        $("#violenceInitiatorCheck07").change(function () {
            whereToShowSpinnerViolenceInitiator(7);
        });

        $("#violenceInitiatorCheck08").change(function () {
            whereToShowSpinnerViolenceInitiator(8);
        });

        $("#violenceInitiatorCheck09").change(function () {
            whereToShowSpinnerViolenceInitiator(9);
        });

        $("#violenceInitiatorCheck10").change(function () {
            whereToShowSpinnerViolenceInitiator(10);
        });

        $("#violenceInitiatorCheck11").change(function () {
            whereToShowSpinnerViolenceInitiator(11);
        });

        $("#violenceInitiatorCheck12").change(function () {
            whereToShowSpinnerViolenceInitiator(12);
        });

    }

    function whereToShowSpinnerViolenceInitiator(id) {

        $("#violenceInitiator" + id + "Spinner").html('<input id="violenceInitiatorDesc' + id + 'Spinner" name="violenceInitiatorDesc' + id + 'Spinner" type="text" class="form-control" placeholder="Specify">');
        $("#violenceInitiator" + id + "Spinner").toggle();
        return;
    }

    function violenceVictimCheckDisplayMoreOptions() {
        $("#violenceVictimCheck01").change(function () {
            whereToShowSpinnerViolenceVictim(1);
        });

        $("#violenceVictimCheck02").change(function () {
            whereToShowSpinnerViolenceVictim(2);
        });

        $("#violenceVictimCheck03").change(function () {
            whereToShowSpinnerViolenceVictim(3);
        });

        $("#violenceVictimCheck04").change(function () {
            whereToShowSpinnerViolenceVictim(4);
        });

        $("#violenceVictimCheck05").change(function () {
            whereToShowSpinnerViolenceVictim(5);
        });

        $("#violenceVictimCheck06").change(function () {
            whereToShowSpinnerViolenceVictim(6);
        });

        $("#violenceVictimCheck07").change(function () {
            whereToShowSpinnerViolenceVictim(7);
        });

        $("#violenceVictimCheck08").change(function () {
            whereToShowSpinnerViolenceVictim(8);
        });

        $("#violenceVictimCheck09").change(function () {
            whereToShowSpinnerViolenceVictim(9);
        });

        $("#violenceVictimCheck10").change(function () {
            whereToShowSpinnerViolenceVictim(10);
        });

        $("#violenceVictimCheck11").change(function () {
            whereToShowSpinnerViolenceVictim(11);
        });

        $("#violenceVictimCheck12").change(function () {
            whereToShowSpinnerViolenceVictim(12);
        });

    }

    function whereToShowSpinnerViolenceVictim(id) {

        $("#violenceVictim" + id + "Spinner").html('<input id="violenceVictimDesc' + id + 'Spinner" name="violenceVictimDesc' + id + 'Spinner" type="text" class="form-control" placeholder="Specify">');
        $("#violenceVictim" + id + "Spinner").toggle();
        return;
    }

    function violenceKindCheckDisplayMoreOptions() {
        $("#violenceKindCheck01").change(function () {
            whereToShowSpinnerViolenceKind(1, "How many people killed?");
        });

        $("#violenceKindCheck02").change(function () {
            whereToShowSpinnerViolenceKind(2, "How many people wounded?");
        });

        $("#violenceKindCheck03").change(function () {
            whereToShowSpinnerViolenceKind(3, "How many people wounded?");
        });

        $("#violenceKindCheck06").change(function () {
            whereToShowSpinnerViolenceKind(6, "How many people kidnapped?");
        });

        $("#violenceKindCheck11").change(function () {
            whereToShowSpinnerViolenceKind(11, "(specify)");
        });

    }

    function whereToShowSpinnerViolenceKind(id, desc) {

        $("#violenceKind" + id + "Spinner").html('<input id="violenceKindDesc' + id + 'Spinner" name="violenceKindDesc' + id + 'Spinner" type="text" class="form-control" placeholder="' + desc + '">');
        $("#violenceKind" + id + "Spinner").toggle();
        return;
    }

    function violenceWeaponCheckDisplayMoreOptions() {

        $("#violenceWeaponCheck09").change(function () {
            whereToShowSpinnerViolenceWeapon(09, "(specify)");
        });

    }

    function whereToShowSpinnerViolenceWeapon(id, desc) {

        $("#violenceWeapon" + id + "Spinner").html('<input id="violenceWeaponDesc' + id + 'Spinner" name="violenceWeaponDesc' + id + 'Spinner" type="text" class="form-control" placeholder="' + desc + '">');
        $("#violenceWeapon" + id + "Spinner").toggle();
        return;
    }

    function violenceImpactCheckDisplayMoreOptions() {
        $("#violenceImpactCheck08").change(function () {
            whereToShowSpinnerViolenceImpact(8, "");
        });

        $("#violenceImpactCheck09").change(function () {
            whereToShowSpinnerViolenceImpact(9, "");
        });

        $("#violenceImpactCheck10").change(function () {
            whereToShowSpinnerViolenceImpact(10, "");
        });

        $("#violenceImpactCheck11").change(function () {
            whereToShowSpinnerViolenceImpact(11, "(specify)");
        });

    }

    function whereToShowSpinnerViolenceImpact(id, desc) {

        if (id == 11) {
            $("#violenceImpact" + id + "Spinner").html('<input id="violenceImpactSource' + id + 'Spinner" name="violenceImpactSource' + id + 'Spinner" type="text" class="form-control" placeholder="' + desc + '">');
            $("#violenceImpact" + id + "Spinner").toggle();
            return;
        }

        $("#violenceImpact" + id + "Spinner").html('<select id="violenceImpactSource' + id + 'Spinner"name="violenceImpactSource' + id + 'Spinner"class="select2 form-control mb-3 custom-select" style="width: 100%; height:36px;" data-placeholder="Choose"> <option value="" selected="true" disabled="disabled">Locally or nationally ?</option> <option value="Locally">Locally</option> <option value="Nationally">Nationally</option> </select>');
        $("#violenceImpact" + id + "Spinner").toggle();
    }

    function init() {
        getReports();
        // getImages();
        Waves.init();
    }

    init();

})(jQuery)