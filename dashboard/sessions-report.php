<!DOCTYPE html>
<html lang="en">
<?php $header_title = 'Report Overview | VTrack Reporting Dasboard - A concept of KDI';
include_once '../customs/app_head.php'
?>

<body>

    <!-- Top Bar Start -->
    <?php include_once '../customs/app_topbar.php'?>
    <!-- Top Bar End -->

    <div class="page-wrapper">
        <!-- Left Sidenav -->
        <?php include_once '../customs/app_sidenav.php'?>
        <!-- end left-sidenav-->

                <!-- Page Content-->
                <div class="page-content">

<div class="container-fluid">
    <!-- Page-Title -->
    <?php $page_title = 'Sessions Report'; 
        include '../customs/app_page_title.php' ?>
    <!-- end page title end breadcrumb -->
    
    <div class="row">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title mt-0">Audience Overview</h4>
                    <div class="">
                        <div id="ana_dash_1" class="apex-charts"></div>
                    </div>
                </div>
                <!--end card-body-->
            </div>
            <!--end card-->
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <h4 class="header-title mt-0 mb-3">Live Visits Our New Site</h4>
                            <div id="circlechart" class="apex-charts"></div>
                        </div>
                        <!--end col-->
                        <div class="col-lg-6">
                            <h4 class="header-title mt-0 mb-3">Traffic Sources</h4>
                            <div class="traffic-card">
                                <h3>80</h3>
                                <h5>Right Now</h5>
                            </div>
                            <div class="progress mb-4">
                                <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary"
                                    role="progressbar" style="width: 55%" aria-valuenow="55"
                                    aria-valuemin="0" aria-valuemax="100">55%</div>
                                <div class="progress-bar bg-secondary" role="progressbar" style="width: 28%"
                                    aria-valuenow="28" aria-valuemin="0" aria-valuemax="100">28%</div>
                                <div class="progress-bar bg-warning" role="progressbar" style="width: 17%"
                                    aria-valuenow="17" aria-valuemin="0" aria-valuemax="100">17%</div>
                            </div>
                            <ul class="list-unstyled url-list mb-0">
                                <li>
                                    <i class="mdi mdi-circle-medium text-primary"></i>
                                    <span>Organic</span>
                                </li>
                                <li>
                                    <i class="mdi mdi-circle-medium text-secondary"></i>
                                    <span>Direct</span>
                                </li>
                                <li>
                                    <i class="mdi mdi-circle-medium text-warning"></i>
                                    <span>Campaign</span>
                                </li>
                            </ul>
                        </div>
                        <!--end col-->
                    </div>
                    <!--end row-->
                </div>
                <!--end card-body-->
            </div>
            <!--end card-->
        </div>
        <!--end col-->
        <div class="col-lg-4">
            <div class="card">
                <div class="card-body mb-0">
                    <div class="row">
                        <div class="col-8 align-self-center">
                            <div class="impressions-data">
                                <h4 class="mt-0 header-title">Impressions</h4>
                                <h2 class="mt-0">140k <span
                                        class="text-success  font-12 font-weight-normal"><i
                                            class="mdi mdi-arrow-up mr-1"></i>21%</span></h2>
                            </div>
                        </div>
                        <!--end col-->
                    </div>
                    <!--end row-->
                </div>
                <!--end card-body-->
                <div class="card-body overflow-hidden p-0">
                    <div class="d-flex mb-0 h-100">
                        <div class="w-100">
                            <div class="apexchart-wrapper">
                                <div id="dash_spark_1" class="chart-gutters"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end card-body-->
            </div>
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title mt-0">Sessions Device</h4>
                    <div id="ana_device" class="apex-charts"></div>
                    <div class="table-responsive mt-4">
                        <table class="table mb-0">
                            <thead class="thead-light">
                                <tr>
                                    <th>Device</th>
                                    <th>Sassions</th>
                                    <th>Day</th>
                                    <th>Week</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">Dasktops</th>
                                    <td>1843</td>
                                    <td>-3</td>
                                    <td>-12</td>
                                </tr>
                                <tr>
                                    <th scope="row">Tablets</th>
                                    <td>2543</td>
                                    <td>-5</td>
                                    <td>-2</td>
                                </tr>
                                <tr>
                                    <th scope="row">Mobiles</th>
                                    <td>3654</td>
                                    <td>-5</td>
                                    <td>-6</td>
                                </tr>

                            </tbody>
                        </table>
                        <!--end /table-->
                    </div>
                </div>
                <!--end card-body-->
            </div>
            <!--end card-->
        </div>
        <!--end col-->
    </div>
    <!--end row-->
    <div class="row justify-content-center">
        <div class="col-md-3">
            <div class="card report-card">
                <div class="card-body">
                    <div class="float-right">
                        <i class="dripicons-user-group report-main-icon"></i>
                    </div>
                    <span class="badge badge-danger">Sessions</span>
                    <h3 class="my-3">24k</h3>
                    <p class="mb-0 text-muted text-truncate"><span class="text-success"><i
                                class="mdi mdi-trending-up"></i>8.5%</span>New Sessions Today</p>
                </div>
                <!--end card-body-->
            </div>
            <!--end card-->
        </div>
        <!--end col-->
        <div class="col-md-3">
            <div class="card report-card">
                <div class="card-body">
                    <div class="float-right">
                        <i class="dripicons-clock report-main-icon"></i>
                    </div>
                    <span class="badge badge-secondary">Avg.Sessions</span>
                    <h3 class="my-3">00:18</h3>
                    <p class="mb-0 text-muted text-truncate"><span class="text-danger"><i
                                class="mdi mdi-trending-down"></i>1.5%</span> Weekly Avg.Sessions</p>
                </div>
                <!--end card-body-->
            </div>
            <!--end card-->
        </div>
        <!--end col-->
        <div class="col-md-3">
            <div class="card report-card">
                <div class="card-body">
                    <div class="float-right">
                        <i class="dripicons-meter report-main-icon"></i>
                    </div>
                    <span class="badge badge-warning">Bounce Rate</span>
                    <h3 class="my-3">$2400</h3>
                    <p class="mb-0 text-muted text-truncate"><span class="text-danger"><i
                                class="mdi mdi-trending-down"></i>35%</span> Bounce Rate Weekly</p>
                </div>
                <!--end card-body-->
            </div>
            <!--end card-->
        </div>
        <!--end col-->
        <div class="col-md-3">
            <div class="card report-card">
                <div class="card-body">
                    <div class="float-right">
                        <i class="dripicons-wallet report-main-icon"></i>
                    </div>
                    <span class="badge badge-success">Goal Completions</span>
                    <h3 class="my-3">85000</h3>
                    <p class="mb-0 text-muted text-truncate"><span class="text-success"><i
                                class="mdi mdi-trending-up"></i>10.5%</span> Completions Weekly</p>
                </div>
                <!--end card-body-->
            </div>
            <!--end card-->
        </div>
        <!--end col-->
    </div>
    <!--end row-->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title mt-0">Sessions By Country</h4>
                    <div class="row">
                        <div class="col-lg-8">
                            <div id="world-map-markers" class="crm-dash-map"></div>
                        </div>
                        <!--end col-->
                        <div class="col-lg-4 align-self-center">
                            <div class="">
                                <span class="text-secondary">USA</span>
                                <small class="float-right text-muted ml-3 font-13">81%</small>
                                <div class="progress mt-2" style="height:3px;">
                                    <div class="progress-bar bg-pink" role="progressbar"
                                        style="width: 81%; border-radius:5px;" aria-valuenow="81"
                                        aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>

                            <div class="mt-3">
                                <span class="text-secondary">Greenland</span>
                                <small class="float-right text-muted ml-3 font-13">68%</small>
                                <div class="progress mt-2" style="height:3px;">
                                    <div class="progress-bar bg-secondary" role="progressbar"
                                        style="width: 68%; border-radius:5px;" aria-valuenow="68"
                                        aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                            <div class="mt-3">
                                <span class="text-secondary">Australia</span>
                                <small class="float-right text-muted ml-3 font-13">48%</small>
                                <div class="progress mt-2" style="height:3px;">
                                    <div class="progress-bar bg-purple" role="progressbar"
                                        style="width: 48%; border-radius:5px;" aria-valuenow="48"
                                        aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>

                            <div class="mt-3">
                                <span class="text-secondary">Brazil</span>
                                <small class="float-right text-muted ml-3 font-13">32%</small>
                                <div class="progress mt-2" style="height:3px;">
                                    <div class="progress-bar bg-warning" role="progressbar"
                                        style="width: 32%; border-radius:5px;" aria-valuenow="32"
                                        aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>

                            <div class="mt-3">
                                <span class="text-secondary">Nigeria</span>
                                <small class="float-right text-muted ml-3 font-13">32%</small>
                                <div class="progress mt-2" style="height:3px;">
                                    <div class="progress-bar bg-warning" role="progressbar"
                                        style="width: 32%; border-radius:5px;" aria-valuenow="32"
                                        aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                        <!--end col-->
                    </div>
                    <!--end row-->
                </div>
                <!--end card-body-->
            </div>
            <!--end card-->
        </div>
        <!--end col-->
    </div>
    <!--end row-->

</div><!-- container -->

<?php include '../customs/app_footer.php' ?>
<!--end footer-->
</div>
<!-- end page content -->
    </div>
    <!-- end page-wrapper -->

    <!-- jQuery  -->
    <?php include_once '../customs/app_js_files.php'?>

    <!-- Dashboard Functions js -->
    <script src="../assets/js/homeController/dashboard.js"></script>


</body>

</html>