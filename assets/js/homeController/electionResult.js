// (function ($) {

'use strict';

var firebaseDb = firebase.firestore();
var docRef = firebaseDb.collection("vtrack_media_center").doc("election_result_data");
var docRef2 = firebaseDb.collection("vtrack_media_center").doc("election_result_data");

var data;
let testTable;

let prevDataList = [];

let isEdit = false;

$("#submit_result").click(function () {

    $("#submit_result").hide();
    $("#loader3").show();

    let txtTitle = $("#txtTitle").val();
    let txtAccrVoters = $("#txtAccrVoters").val();
    let txtNoOfPolParty = $("#txtNoOfPolParty").val();
    let txtNoOfPollingUnits = $("#txtNoOfPollingUnits").val();
    let txtPopulation = $("#txtPopulation").val();
    let txtPvcCollected = $("#txtPvcCollected").val();
    let txtRegVoters = $("#txtRegVoters").val();
    let txtState = $("#txtState").val();
    let txtUncollectedPvc = $("#txtUncollectedPvc").val();
    let txtVoterPerPolParty = $("#txtVoterPerPolParty").val();
    let txtVotersTurnout = $("#txtVotersTurnout").val();
    let txtYear = $("#txtYear").val();

    if (txtTitle === null || txtTitle === "") {
        alert("You need to add title");

        $("#submit_result").show();
        $("#loader3").hide();
        return;
    }

    let dataObject = {
        "uncollected_pvc": parseInt(txtUncollectedPvc),
        "registered_voters": parseInt(txtRegVoters),
        "accredited_voters": parseInt(txtAccrVoters),
        "number_of_parties": parseInt(txtNoOfPolParty),
        "pvc_collected": parseInt(txtPvcCollected),
        "voters_per_pol_parties": parseInt(txtVoterPerPolParty),
        "polling_units": parseInt(txtNoOfPollingUnits),
        "voters_turnout": parseInt(txtVotersTurnout),
        "title": txtTitle,
        "population": parseInt(txtPopulation),
        "state": txtState,
        "year": txtYear
    };

    saveLinksToDb("results", dataObject);

});


$("#add_result").click(function () {

    popupFunction(null);
});

function getAllForms() {

    docRef.get().then(function (doc) {
        if (doc.exists) {
            data = doc.data();
            // console.log(data['results']);
            // console.log("Document data:", doc.data());

            var elementArray = data['results'];

            testTable = JSON.stringify(elementArray);

            DatatableDataForms();
            $("#csv_div").css("display", "block");

        } else {
            // doc.data() will be undefined in this case
            console.log("No such document!");
        }
    }).catch(function (error) {
        console.log("Error getting document:", error);
    });
}

function DatatableDataForms() {
    var e, a, i;
    (e = JSON.parse(testTable)), (a = $('.m_datatable_election_result').mDatatable({
        data: {
            type: 'local',
            source: e,
            pageSize: 10
        },
        layout: {
            theme: 'default',
            class: '',
            scroll: !1,
            footer: !1
        },
        sortable: !0,
        pagination: !0,
        search: {
            input: $('#allElectionReportSearch')
        },
        columns: [{
                field: 'title',
                title: 'Title'
            }, {
                field: 'year',
                title: 'Year'
            }, {
                field: 'state',
                title: 'State'
            }, {
                field: 'registered_voters',
                title: 'Registered Voters'
            }, {
                field: 'uncollected_pvc',
                title: 'Uncollected PVC'
            },
            {
                field: 'Actions',
                width: 110,
                title: 'Actions',
                sortable: !1,
                overflow: 'visible',
                template: function (e, a, i) {
                    // console.log(a[i]);
                    // console.log(i);
                    return (
                        "\t\t\t\t\t\t<div class='dropdown " +
                        (i.getPageSize() - a <= 4 ? "dropup" : "") +
                        "' style='display:none;'>\t\t\t\t\t\t\t<a href='#' class='btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill' data-toggle='dropdown' >                                <i class='la la-ellipsis-h'></i>                            </a>\t\t\t\t\t\t  \t<div class='dropdown-menu dropdown-menu-right'>\t\t\t\t\t\t    \t<a class='dropdown-item' href='#'><i class='la la-eye'></i> View Form</a>\t\t\t\t\t\t    \t<a class='dropdown-item' href='#'><i class='la la-trash-o'></i> Delete Form</a>\t\t\t\t\t\t  \t</div>\t\t\t\t\t\t</div>\t\t\t\t\t\t<a href='javascript:popupFunction(" + JSON.stringify(e) + ")' class='m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill' title='Edit '>                            <i class='la la-edit'></i>                        </a>\t\t\t\t\t<a href='javascript:deleteFunction(" + JSON.stringify(e) + ")' class='m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill' title='Delete '>                            <i class='la la-trash-o'></i>                        </a>\t\t\t\t\t"
                    )
                }
            }
        ]
    })), i = a.getDataSourceQuery(), $("#m_form_status").on("change", function () {
        a.search($(this).val(), "formCode")
    }).val(void 0 !== i.firstApproval ? i.firstApproval : ""), $("#m_form_type").on("change", function () {
        a.search($(this).val(), "Type")
    }).val(void 0 !== i.Type ? i.Type : ""), $("#m_form_status, #m_form_type").selectpicker()
}

function popupFunction(data) {

    $("#txtTitle").val("");
    $("#txtAccrVoters").val(0);
    $("#txtNoOfPolParty").val(0);
    $("#txtNoOfPollingUnits").val(0);
    $("#txtPopulation").val(0);
    $("#txtPvcCollected").val(0);
    $("#txtRegVoters").val(0);
    $("#txtState").val("");
    $("#txtUncollectedPvc").val(0);
    $("#txtVoterPerPolParty").val(0);
    $("#txtVotersTurnout").val(0);
    $("#txtYear").val("");

    isEdit = false;

    if (data !== null) {
        console.log(data);

        isEdit = true;

        $("#txtTitle").val(data.title.toString());
        $("#txtAccrVoters").val(data.accredited_voters == null ? 0 : data.accredited_voters.toString());
        $("#txtNoOfPolParty").val(data.number_of_parties == null ? 0 : data.number_of_parties.toString());
        $("#txtNoOfPollingUnits").val(data.polling_units == null ? 0 : data.polling_units.toString());
        $("#txtPopulation").val(data.population == null ? 0 : data.population.toString());
        $("#txtPvcCollected").val(data.pvc_collected == null ? 0 : data.pvc_collected.toString());
        $("#txtRegVoters").val(data.registered_voters == null ? 0 : data.registered_voters.toString());
        $("#txtState").val(data.state == null ? 0 : data.state.toString());
        $("#txtUncollectedPvc").val(data.uncollected_pvc == null ? 0 : data.uncollected_pvc.toString());
        $("#txtVoterPerPolParty").val(data.voters_per_pol_parties == null ? 0 : data.voters_per_pol_parties.toString());
        $("#txtVotersTurnout").val(data.voters_turnout == null ? 0 : data.voters_turnout.toString());
        $("#txtYear").val(data.year.toString());

    }

    const reportPopup = document.querySelector(".report-popup");
    reportPopup.classList.add("active");

    $(".report-popup").show();

    $("#close-report").on('click', function (e) {
        $('.report-popup').fadeOut('slow');
    });
}

function deleteFunction(data) {
    var SweetAlert = function () {};

    SweetAlert.prototype.init = function () {

        swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then(async (result) => {
            if (result.value) {

                var objArray = [];

                prevDataList = await getPreviousData();

                if (prevDataList["results"] !== undefined) {
                    prevDataList["results"].forEach(element => {
                        objArray.push(element);
                    });
                }

                // console.log(data);
                // console.log(objArray);

                var filtered = objArray.filter(function (value, index, arr) {
                    return value.title != data.title;
                });

                console.log(filtered);

                var obj = {
                    "results": filtered
                };

                docRef2.set(obj, {
                        merge: true
                    })
                    .then(function () {
                        console.log("Document successfully written!");

                        alert("Election Result removed successfully");

                        location.replace("election_result.php");

                    })
                    .catch(function (error) {
                        console.error("Error writing document: ", error);

                        alert("Error Occured while trying to save to DB");

                        location.replace("election_result.php");
                    });


            } else if (
                // Read more about handling dismissals
                result.dismiss === Swal.DismissReason.cancel
            ) {
                swal.fire(
                    'Cancelled',
                    'The data is safe :)',
                    'error'
                )
            }
        })
    }

    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
    $.SweetAlert.init()
}

async function getPreviousData() {

    const promise = new Promise((resolve, reject) => {

        docRef2.get().then(function (doc) {
            if (doc.exists) {

                resolve(doc.data());
            } else {
                console.log("No such document!");
            }
        }).catch(function (error) {
            console.log("Error getting document:", error);
        });
    });

    let result = await promise;

    prevDataList = result;

    return result;


}

async function updateData(data) {

    isEdit = false;

    var objArray = [];

    prevDataList = await getPreviousData();

    if (prevDataList["results"] !== undefined) {
        prevDataList["results"].forEach(element => {
            objArray.push(element);
        });
    }

    console.log(data);
    console.log(objArray);

    var filtered = objArray.filter(function (value, index, arr) {
        return value.title != data.title;
    });

    filtered.push(data);

    // objArray.forEach(element => {

    //     if (element.title == data.title) {
    //         objArray.pop(element);
    //         objArray.push(data);
    //         // element = data;
    //     }
    // });

    // console.log(objArray);

    var obj = {
        "results": filtered
    };

    docRef2.set(obj, {
            merge: true
        })
        .then(function () {
            console.log("Document successfully written!");

            alert("Election Result edited successfully");

            location.replace("election_result.php");

        })
        .catch(function (error) {
            console.error("Error writing document: ", error);

            alert("Error Occured while trying to save to DB");

            location.replace("election_result.php");
        });
}

async function saveLinksToDb(objKey, dataObject) {

    if (isEdit) {
        updateData(dataObject);
        return;
    }

    var objArray = [];

    prevDataList = await getPreviousData();

    console.log(prevDataList[objKey]);

    if (prevDataList[objKey] !== undefined) {
        prevDataList[objKey].forEach(element => {
            objArray.push(element);
        });

        objArray.push(dataObject);
    }

    console.log(objArray);

    var obj = {};

    obj[objKey] = objArray;

    docRef2.set(obj, {
            merge: true
        })
        .then(function () {
            console.log("Document successfully written!");

            alert("Election Result added successfully");

            location.replace("election_result.php");

        })
        .catch(function (error) {
            console.error("Error writing document: ", error);

            alert("Error Occured while trying to save to DB");

            location.replace("election_result.php");
        });
}


function init() {
    getAllForms();
    Waves.init();
}

init();

// })(jQuery)