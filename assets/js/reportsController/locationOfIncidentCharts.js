(function ($) {
    var getToken = localStorage.getItem("x-auth-token");

    var arrayItem1 = [];
    arrayItem2 = [];
    arrayItem3 = [];
    arrayItem4 = [];
    arrayItem5 = [];
    arrayItem6 = [];
    arrayItem7 = [];
    arrayItem8 = [];

    async function getAllFormsForWhereIncidentHappen() {


        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("x-auth-token", getToken);

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        fetch("https://peaceful-atoll-23751.herokuapp.com/" + "just-clock-285910.uc.r.appspot.com/api/form/all", requestOptions)
            .then(function (response) {
                if (response.status === 403) {
                    alert("Token expired. Login");
                    window.location.href = "index.php";
                }
                if (!response.ok) {
                    alert("Cannot login. Ensure your data is correct and you have internet connection");
                    throw new Error("HTTP status " + response.status);
                }

                // console.log(`Result ${response}`);
                return response.json();
            })
            .then(function (result) {
                // localStorage.setItem("x-auth-token", result.details.token);
                // localStorage.setItem("role", result.details.role);

                for (const incidentLocation in result.details) {
                    if (result.details.hasOwnProperty(incidentLocation)) {
                        const element = result.details[incidentLocation];

                        if (element.firstApproval === true && element.secondApproval === true) {
                            fillArrayWithResult(element);
                        }


                    }
                }

                showChart();


            })
            .catch(error => console.log('error', error));

    }

    // apex-bar-1

    function showChart() {

        var options = {
            chart: {
                height: 380,
                type: 'bar',
                toolbar: {
                    show: false
                }
            },
            plotOptions: {
                bar: {
                    horizontal: true,
                }
            },
            dataLabels: {
                enabled: false
            },
            series: [{
                data: [arrayItem1.length, arrayItem2.length, arrayItem3.length, arrayItem4.length, arrayItem5.length, arrayItem6.length, arrayItem7.length, arrayItem8.length]
            }],
            colors: ["#0000FF"],
            yaxis: {
                axisBorder: {
                    show: true,
                    color: '#bec7e0',
                },
                axisTicks: {
                    show: true,
                    color: '#bec7e0',
                },
            },
            xaxis: {
                categories: ['In/near polling station', 'Election office / Facility', 'Political party office/ Facility', "State (Gov't) office/ Property", 'Private property', 'Media office', "Community / Street / Public area", 'Others'],
            },
            states: {
                hover: {
                    filter: 'none'
                }
            },
            grid: {
                borderColor: '#f1f3fa'
            }
        }

        var chart = new ApexCharts(
            document.querySelector("#where_inci_happen_chart"),
            options
        );

        chart.render();
    }

    function fillArrayWithResult(element) {

        // console.log(element.incidentLocation.locationOfIncident);

        switch (element.incidentLocation.locationOfIncident) {
            case 'in/near polling station':
                arrayItem1.push(element);
                break;
            case 'election office / facility':
                arrayItem2.push(element);
                break;
            case 'political party office/ facility':
                arrayItem3.push(element);
                break;
            case "state (gov't) office/ property":
                arrayItem4.push(element);
                break;
            case 'private property':
                arrayItem5.push(element);
                break;
            case 'media office':
                arrayItem6.push(element);
                break;
            case "community / street / public area":
                arrayItem7.push(element);
                break;
            case 'others':
                arrayItem8.push(element);
                break;

            default:
                break;
        }

        // console.log(arrayItem7.length);

    }

    async function init() {
        await getAllFormsForWhereIncidentHappen();

        Waves.init();
    }

    init();

})(jQuery)