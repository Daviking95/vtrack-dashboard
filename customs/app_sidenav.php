<div class="left-sidenav">
    <div class="main-icon-menu">
        <nav class="nav">
            <a href="#MetricaDashboard" class="nav-link" data-toggle="tooltip-custom" data-placement="top" title=""
                data-original-title="Dashboard">
                <svg class="nav-svg" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                    xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512"
                    style="enable-background:new 0 0 512 512;" xml:space="preserve">
                    <g>
                        <path
                            d="M184,448h48c4.4,0,8-3.6,8-8V72c0-4.4-3.6-8-8-8h-48c-4.4,0-8,3.6-8,8v368C176,444.4,179.6,448,184,448z" />
                        <path class="svg-primary"
                            d="M88,448H136c4.4,0,8-3.6,8-8V296c0-4.4-3.6-8-8-8H88c-4.4,0-8,3.6-8,8V440C80,444.4,83.6,448,88,448z" />
                        <path class="svg-primary" d="M280.1,448h47.8c4.5,0,8.1-3.6,8.1-8.1V232.1c0-4.5-3.6-8.1-8.1-8.1h-47.8c-4.5,0-8.1,3.6-8.1,8.1v207.8
                                        C272,444.4,275.6,448,280.1,448z" />
                        <path d="M368,136.1v303.8c0,4.5,3.6,8.1,8.1,8.1h47.8c4.5,0,8.1-3.6,8.1-8.1V136.1c0-4.5-3.6-8.1-8.1-8.1h-47.8
                                        C371.6,128,368,131.6,368,136.1z" />
                    </g>
                </svg>
            </a>
            <!--end MetricaDashboard-->

            <a href="#MetricaReport" class="nav-link" data-toggle="tooltip-custom" data-placement="top" title=""
                data-original-title="Reports">
                <svg class="nav-svg" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                    xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512"
                    style="enable-background:new 0 0 512 512;" xml:space="preserve">
                    <path class="svg-primary"
                        d="M464 64H192c-8.8 0-16 7.7-16 16.5V112H74c-23.1 0-42 18.9-42 42v207.5c0 47.6 39 86.5 86 86.5h279.7c45.1 0 82.3-36.9 82.3-82V80c0-8.8-7.2-16-16-16zm-288 80v192h-42V163.2c0-6.8-.8-13.3-3.3-19.2H176zm-17 255.4C148 410 133.2 416 118.5 416c-14.5 0-28.1-5.7-38.5-16-10.3-10.3-16-24-16-38.5V163.2c0-10.6 8.4-19.2 19-19.2s19 8.6 19 19.2V352c0 8.8 7.2 16 16 16h57.5c-1.5 11.6-7.2 22.6-16.5 31.4zM448 366c0 13.3-5.4 25.8-14.9 35.3-9.5 9.5-22.2 14.7-35.4 14.7H187.3c12.8-14.9 20.7-33.9 20.7-54.5V97h240v269z">
                    </path>
                    <path
                        d="M248 136h160v56H248zM248 224h160v32H248zM248 288h160v32H248zM408 352H248s0 32-8 32h148.7c19.3 0 19.3-21 19.3-32z">
                    </path>
                </svg>
            </a>
            <!--end MetricaReport-->

            <a href="#MetricaMonitor" class="nav-link" data-toggle="tooltip-custom" data-placement="top" title=""
                data-original-title="Monitor">
                <svg class="nav-svg" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                    xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512"
                    style="enable-background:new 0 0 512 512;" xml:space="preserve">
                    <path class="svg-primary"
                        d="M256 256c52.805 0 96-43.201 96-96s-43.195-96-96-96-96 43.201-96 96 43.195 96 96 96zm0 48c-63.598 0-192 32.402-192 96v48h384v-48c0-63.598-128.402-96-192-96z">
                    </path>
                </svg>
            </a>
            <!--end MetricaMonitor-->

            <a href="#MetricaUsers" class="nav-link" data-toggle="tooltip-custom" data-placement="top" title=""
                data-original-title="Users" hidden>
                <svg class="nav-svg" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                    xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512"
                    style="enable-background:new 0 0 512 512;" xml:space="preserve">
                    <path class="svg-primary"
                        d="M239.208 343.937c-17.78 10.103-38.342 15.876-60.255 15.876-21.909 0-42.467-5.771-60.246-15.87C71.544 358.331 42.643 406 32 448h293.912c-10.639-42-39.537-89.683-86.704-104.063zM178.953 120.035c-58.479 0-105.886 47.394-105.886 105.858 0 58.464 47.407 105.857 105.886 105.857s105.886-47.394 105.886-105.857c0-58.464-47.408-105.858-105.886-105.858zm0 186.488c-33.671 0-62.445-22.513-73.997-50.523H252.95c-11.554 28.011-40.326 50.523-73.997 50.523z">
                    </path>
                    <g>
                        <path
                            d="M322.602 384H480c-10.638-42-39.537-81.691-86.703-96.072-17.781 10.104-38.343 15.873-60.256 15.873-14.823 0-29.024-2.654-42.168-7.49-7.445 12.47-16.927 25.592-27.974 34.906C289.245 341.354 309.146 364 322.602 384zM306.545 200h100.493c-11.554 28-40.327 50.293-73.997 50.293-8.875 0-17.404-1.692-25.375-4.51a128.411 128.411 0 0 1-6.52 25.118c10.066 3.174 20.779 4.862 31.895 4.862 58.479 0 105.886-47.41 105.886-105.872 0-58.465-47.407-105.866-105.886-105.866-37.49 0-70.427 19.703-89.243 49.09C275.607 131.383 298.961 163 306.545 200z">
                        </path>
                    </g>
                </svg>
            </a>
            <!--end MetricaUsers-->

            <a href="#MetricaAdmin" class="nav-link only_for_approver" data-toggle="tooltip-custom" data-placement="top"
                title="" data-original-title="Admin">
                <svg class="nav-svg" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                    xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512"
                    style="enable-background:new 0 0 512 512;" xml:space="preserve">
                    <path
                        d="M70.7 164.5l169.2 81.7c4.4 2.1 10.3 3.2 16.1 3.2s11.7-1.1 16.1-3.2l169.2-81.7c8.9-4.3 8.9-11.3 0-15.6L272.1 67.2c-4.4-2.1-10.3-3.2-16.1-3.2s-11.7 1.1-16.1 3.2L70.7 148.9c-8.9 4.3-8.9 11.3 0 15.6z">
                    </path>
                    <path class="svg-primary"
                        d="M441.3 248.2s-30.9-14.9-35-16.9-5.2-1.9-9.5.1S272 291.6 272 291.6c-4.5 2.1-10.3 3.2-16.1 3.2s-11.7-1.1-16.1-3.2c0 0-117.3-56.6-122.8-59.3-6-2.9-7.7-2.9-13.1-.3l-33.4 16.1c-8.9 4.3-8.9 11.3 0 15.6l169.2 81.7c4.4 2.1 10.3 3.2 16.1 3.2s11.7-1.1 16.1-3.2l169.2-81.7c9.1-4.2 9.1-11.2.2-15.5z">
                    </path>
                    <path
                        d="M441.3 347.5s-30.9-14.9-35-16.9-5.2-1.9-9.5.1S272.1 391 272.1 391c-4.5 2.1-10.3 3.2-16.1 3.2s-11.7-1.1-16.1-3.2c0 0-117.3-56.6-122.8-59.3-6-2.9-7.7-2.9-13.1-.3l-33.4 16.1c-8.9 4.3-8.9 11.3 0 15.6l169.2 81.7c4.4 2.2 10.3 3.2 16.1 3.2s11.7-1.1 16.1-3.2l169.2-81.7c9-4.3 9-11.3.1-15.6z">
                    </path>
                </svg>
            </a>
            <!--end MetricaAdmin-->

            <a href="#MetricaAuthentication" class="nav-link" data-toggle="tooltip-custom" data-placement="top" title=""
                data-original-title="Authentication">
                <svg class="nav-svg" version="1.1" id="Layer_5" xmlns="http://www.w3.org/2000/svg"
                    xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512"
                    style="enable-background:new 0 0 512 512;" xml:space="preserve">
                    <g>
                        <path class="svg-primary"
                            d="M376,192h-24v-46.7c0-52.7-42-96.5-94.7-97.3c-53.4-0.7-97.3,42.8-97.3,96v48h-24c-22,0-40,18-40,40v192c0,22,18,40,40,40
                                        h240c22,0,40-18,40-40V232C416,210,398,192,376,192z M270,316.8v68.8c0,7.5-5.8,14-13.3,14.4c-8,0.4-14.7-6-14.7-14v-69.2
                                        c-11.5-5.6-19.1-17.8-17.9-31.7c1.4-15.5,14.1-27.9,29.6-29c18.7-1.3,34.3,13.5,34.3,31.9C288,300.7,280.7,311.6,270,316.8z
                                            M324,192H188v-48c0-18.1,7.1-35.1,20-48s29.9-20,48-20s35.1,7.1,48,20s20,29.9,20,48V192z" />
                    </g>
                </svg>
            </a>
            <!--end MetricaAuthentication-->

        </nav>
        <!--end nav-->
    </div>
    <!--end main-icon-menu-->

    <div class="main-menu-inner">
        <div class="menu-body slimscroll">
            <div id="MetricaDashboard" class="main-icon-menu-pane">
                <div class="title-box">
                    <h6 class="menu-title">Dashboard</h6>
                </div>
                <ul class="nav">
                    <li class="nav-item"><a class="nav-link" href="../dashboard/home.php"><i
                                class="dripicons-meter"></i>Home</a></li>
                    <li class="nav-item"><a class="nav-link" href="../dashboard/logs.php"><i
                                class="dripicons-user-group"></i>Log Activity</a></li>
                    <li class="nav-item"><a class="nav-link" href="../dashboard/election_result.php"><i
                                class="dripicons-user-group"></i>Election Result</a></li>
                    <li class="nav-item"><a class="nav-link" href="../dashboard/sliders_data.php"><i
                                class="dripicons-user-group"></i>Slider Data</a></li>
                    <li class="nav-item only_for_approver"><a class="nav-link" href="../dashboard/media_photos.php"><i
                                class="dripicons-user-group"></i>Add Media Resources</a></li>
                    <li class="nav-item" hidden><a class="nav-link" href="../dashboard/sessions-report.php"><i
                                class="dripicons-document"></i>Sessions Report</a></li>
                </ul>
            </div><!-- end Analytic -->
            <div id="MetricaReport" class="main-icon-menu-pane">
                <div class="title-box">
                    <h6 class="menu-title">Reports</h6>
                </div>
                <ul class="nav">
                    <li class="nav-item"><a class="nav-link" href="../reports/add.php"><i
                                class="mdi mdi-book-multiple-plus"></i>Add Report</a></li>
                    <li class="nav-item"><a class="nav-link" href="../reports/all.php"><i
                                class="dripicons-device-desktop"></i>All Reports</a></li>
                    <li class="nav-item"><a class="nav-link" href="../reports/verification.php"><i
                                class="dripicons-swap"></i>Awaiting Verification</a></li>
                    <li class="nav-item only_for_approver"><a class="nav-link" href="../reports/approval.php"><i
                                class="dripicons-wallet"></i>Awaiting Approval</a></li>
                    <li class="nav-item"><a class="nav-link" href="../reports/web.php"><i
                                class="dripicons-wallet"></i>Web Reports</a></li>
                    <li class="nav-item"><a class="nav-link" href="../reports/app.php"><i
                                class="dripicons-wallet"></i>Mobile Reports</a></li>
                    <li class="nav-item"><a class="nav-link" href="../reports/sms.php"><i
                                class="dripicons-wallet"></i>SMS Reports</a></li>
                </ul>
            </div><!-- end Crypto -->
            <div id="MetricaMonitor" class="main-icon-menu-pane">
                <div class="title-box">
                    <h6 class="menu-title">Monitors</h6>
                </div>
                <ul class="nav">
                    <li class="nav-item"><a class="nav-link" href="../monitor/add.php"><i
                                class="dripicons-view-thumb"></i>Add Monitor</a></li>
                    <li class="nav-item"><a class="nav-link" href="../monitor/all.php"><i
                                class="dripicons-user-id"></i>View Monitors</a></li>
                </ul>
            </div><!-- end  Project-->
            <div id="MetricaUsers" class="main-icon-menu-pane">
                <div class="title-box">
                    <h6 class="menu-title">Users</h6>
                </div>
                <ul class="nav">
                    <li class="nav-item"><a class="nav-link" href="../user/all.php"><i
                                class="dripicons-device-desktop"></i>View All Users</a></li>
                </ul>
            </div><!-- end Ecommerce -->
            <div id="MetricaAdmin" class="main-icon-menu-pane">
                <div class="title-box">
                    <h6 class="menu-title">Admin</h6>
                </div>
                <ul class="nav">
                    <li class="nav-item only_for_approver"><a class="nav-link" href="../admin/add.php"><i
                                class="dripicons-monitor "></i>Add Admin</a></li>
                    <li class="nav-item"><a class="nav-link" href="../admin/all.php"><i
                                class="dripicons-user-id"></i>View All Admins</a></li>
                    <li class="nav-item only_for_approver" hidden><a class="nav-link"
                            href="../admin/set-permission.php"><i class="dripicons-user-id "></i>Set Permission</a></li>
                </ul>
            </div><!-- end CRM -->
            <div id="MetricaAuthentication" class="main-icon-menu-pane">
                <div class="title-box">
                    <h6 class="menu-title">Authentication</h6>
                </div>
                <ul class="nav">
                    <li class="nav-item"><a class="nav-link" href="../auth/profile.php"><i
                                class="dripicons-enter"></i>Profile</a></li>
                    <li class="nav-item"><a class="nav-link" href="../auth/change-password.php"><i
                                class="dripicons-pencil"></i>Change Password</a></li>
                    <li class="nav-item" id="logout"><a class="nav-link" href="../index.php"><i
                                class="dripicons-exit"></i>Log
                            out</a></li>
                </ul>
            </div><!-- end Authentication-->
        </div>
        <!--end menu-body-->
    </div><!-- end main-menu-inner-->
</div>