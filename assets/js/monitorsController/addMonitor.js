(function ($) {
    var getToken = localStorage.getItem("x-auth-token");

    'use strict';

    $("#form-horizontal-monitor").show();
    $("#loader").hide();

    let AddMonitorsConstants = {};

    function addMonitor() {

        AddMonitorsConstants["user_monitor_code"] = $('#txtMonitorCode').val();

        AddMonitorsConstants["user_name"] = $('#txtUserName').val();
    
        AddMonitorsConstants["user_first_name"] = $('#txtFirstName').val();
    
        AddMonitorsConstants["user_last_name"] = $('#txtLastName').val();
    
        AddMonitorsConstants["user_email"] = $('#txtEmail').val();
    
        AddMonitorsConstants["user_phone_number"] = $('#txtPhoneNumber').val();
    
        AddMonitorsConstants["user_password"] = $('#txtPassword').val();
    
        AddMonitorsConstants["user_password_confirm"] = $('#txtConfirmPassword').val();
    
        AddMonitorsConstants["user_gender"] = $('#monitorGenderSpinner').val();
    
        AddMonitorsConstants["user_age"] = $('#txtAge').val();
    
        AddMonitorsConstants["state"] = $('#monitorStateSpinner').val();
    
        AddMonitorsConstants["country"] = "Nigeria";

        // console.log(AddMonitorsConstants);

        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("x-auth-token", getToken);

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: JSON.stringify(AddMonitorsConstants),
            redirect: 'follow'
        };

        $("#form-horizontal-monitor").hide();
        $("#loader").show();

        fetch("https://peaceful-atoll-23751.herokuapp.com/" + "just-clock-285910.uc.r.appspot.com/api/monitor/register", requestOptions)
            .then(function (response) {
                if (response.status === 403) {
                    alert("Token expired. Login");
                    window.location.href = "../index.php";
                }
                if (!response.ok) {
                    // console.log(response.json());
                    // alert(response.statusText);

                    response.json().then(function(data) {
                        alert(data.message);
                    });
                    
                    $("#form-horizontal-monitor").show();
                    $("#loader").hide();
                    throw new Error("HTTP status " + response.status);
                }

                $("#form-horizontal-monitor").show();
                $("#loader").hide();

                return response.json();
            })
            .then(function (result) {

                console.log(result);

                if(result.statusCode !== 200){
                    alert(result.message);

                    $("#form-horizontal-monitor").show();
                    $("#loader").hide();
                    return;
                }

                $("#form-horizontal-monitor").show();
                $("#loader").hide();
                alert(result.message);
                window.location.href = "all.php";

            })
            .catch(error => console.log('error', error));

    }

    $("#addMonitor").click(function() {
        addMonitor();

    });

    // function init() {
    //     Waves.init();
    // }

    // init();

})(jQuery)