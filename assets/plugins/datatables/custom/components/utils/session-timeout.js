var SessionTimeoutDemo = { init: function () { $.sessionTimeout({ 
    title: "Session Timeout Notification",
    message: "Your session is about to expire.", 
    keepAliveUrl:"http://localhost/igrpay_web_final/dashboard.php",
    redirUrl:"?p=page_user_lock_1",
    logoutUrl:"?p=page_user_login_1",warnAfter:4e3,redirAfter:40e3,ignoreUserActivity:!0,countdownMessage:"Redirecting in {timer} seconds.",countdownBar:!0})}};jQuery(document).ready(function(){SessionTimeoutDemo.init()});