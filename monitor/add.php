<!DOCTYPE html>
<html lang="en">

<?php $header_title = 'Add Monitor | VTrack Reporting Dasboard - A concept of KDI';
include '../customs/app_head.php'
?>

<body>

    <?php include '../customs/app_topbar.php'?>

    <div class="page-wrapper">
        <!-- Left Sidenav -->
        <?php include '../customs/app_sidenav.php'?>
        <!-- end left-sidenav-->

        <!-- Page Content-->
        <div class="page-content">

            <div class="container-fluid">
                <!-- Page-Title -->
                <?php $page_title = 'Add Monitor';
include_once '../customs/app_page_title.php'?>
                <!-- end page title end breadcrumb -->

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">

                                <div class="cssload-thecube" id="loader" style="display: none">
                                    <div class="cssload-cube cssload-c1"></div>
                                    <div class="cssload-cube cssload-c2"></div>
                                    <div class="cssload-cube cssload-c4"></div>
                                    <div class="cssload-cube cssload-c3"></div>
                                </div>
                                <form id="form-horizontal-monitor" class="form-horizontal form-wizard-wrapper">
                                    <fieldset>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button type="button"
                                                    class="btn btn-primary waves-effect waves-light float-right mb-3"
                                                    data-toggle="modal" data-animation="bounce"
                                                    data-target=".bs-example-modal-lg" id="delete_monitor"
                                                    style="display: none">Delete Monitor</button>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4"></div>
                                            <div class="col-md-4">
                                                <a class="user-avatar mr-2" href="#">
                                                    <img id="user_image" src="../assets/images/favicon.png" style="margin-left: auto;margin-right: auto;width: 8em; height: 150px;" alt="user" class="thumb-xl rounded-circle">
                                                </a>
                                            </div>
                                            <div class="col-md-4"></div>

                                        </div>
                                        <br/>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="txtUserName" class="col-lg-3 col-form-label">User
                                                        Name</label>
                                                    <div class="col-lg-9">
                                                        <input id="txtUserName" name="txtUserName" type="text"
                                                            class="form-control">
                                                    </div>
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-->
                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="txtFirstName" class="col-lg-3 col-form-label">First
                                                        Name</label>
                                                    <div class="col-lg-9">
                                                        <input id="txtFirstName" name="txtFirstName" type="text"
                                                            class="form-control">
                                                    </div>
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-->
                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="txtLastName" class="col-lg-3 col-form-label">Last
                                                        Name</label>
                                                    <div class="col-lg-9">
                                                        <input id="txtLastName" name="txtLastName" type="text"
                                                            class="form-control">
                                                    </div>
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-->

                                        </div>
                                        <!--end row-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label for="txtMonitorCode" class="col-lg-3 col-form-label">Monitor
                                                        Code</label>
                                                    <div class="col-lg-9">
                                                        <input id="txtMonitorCode" name="txtMonitorCode" type="text"
                                                            class="form-control">
                                                    </div>
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-->
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label for="txtPhoneNumber" class="col-lg-3 col-form-label">Phone
                                                        Number</label>
                                                    <div class="col-lg-9">
                                                        <input id="txtPhoneNumber" name="txtPhoneNumber" type="text"
                                                            class="form-control">
                                                    </div>
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-->

                                        </div>
                                        <!--end row-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label for="txtEmail" class="col-lg-3 col-form-label">Email</label>
                                                    <div class="col-lg-9">
                                                        <input id="txtEmail" name="txtEmail" type="email"
                                                            class="form-control">
                                                    </div>
                                                </div>

                                                <!--end form-group-->
                                            </div>
                                            <!--end col-->
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label for="monitorGenderSpinner"
                                                        class="col-lg-3 col-form-label">Select Gender</label>
                                                    <div class="col-lg-9">
                                                        <select id="monitorGenderSpinner" name="monitorGenderSpinner"
                                                            class="select2 form-control mb-3 custom-select"
                                                            style="width: 100%; height:36px;" data-placeholder="Choose">
                                                            <option value="" selected="true" disabled="disabled">
                                                                --Please Select--</option>
                                                            <option value="male">Male</option>
                                                            <option value="female">Female</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-->

                                        </div>
                                        <!--end row-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label for="txtAge" class="col-lg-3 col-form-label">Age</label>
                                                    <div class="col-lg-9">
                                                        <input id="txtAge" name="txtAge" type="text"
                                                            class="form-control">
                                                    </div>
                                                </div>

                                                <!--end form-group-->
                                            </div>
                                            <!--end col-->
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label for="monitorStateSpinner"
                                                        class="col-lg-3 col-form-label">Select State</label>
                                                    <div class="col-lg-9">
                                                        <select id="monitorStateSpinner" name="monitorStateSpinner"
                                                            class="select2 form-control mb-3 custom-select"
                                                            style="width: 100%; height:36px;" data-placeholder="Choose">
                                                            <option disabled selected>--Select State--</option>
                                                            <option value="abia">Abia</option>
                                                            <option value="adamawa">Adamawa</option>
                                                            <option value="akwa Ibom">Akwa Ibom</option>
                                                            <option value="anambra">Anambra</option>
                                                            <option value="bauchi">Bauchi</option>
                                                            <option value="bayelsa">Bayelsa</option>
                                                            <option value="benue">Benue</option>
                                                            <option value="borno">Borno</option>
                                                            <option value="cross River">Cross River</option>
                                                            <option value="delta">Delta</option>
                                                            <option value="ebonyi">Ebonyi</option>
                                                            <option value="edo">Edo</option>
                                                            <option value="ekiti">Ekiti</option>
                                                            <option value="enugu">Enugu</option>
                                                            <option value="abuja">Federal Capital Territory</option>
                                                            <option value="gombe">Gombe</option>
                                                            <option value="imo">Imo</option>
                                                            <option value="jigawa">Jigawa</option>
                                                            <option value="kaduna">Kaduna</option>
                                                            <option value="kano">Kano</option>
                                                            <option value="katsina">Katsina</option>
                                                            <option value="kebbi">Kebbi</option>
                                                            <option value="kogi">Kogi</option>
                                                            <option value="kwara">Kwara</option>
                                                            <option value="lagos">Lagos</option>
                                                            <option value="nasarawa">Nasarawa</option>
                                                            <option value="niger">Niger</option>
                                                            <option value="ogun">Ogun</option>
                                                            <option value="ondo">Ondo</option>
                                                            <option value="osun">Osun</option>
                                                            <option value="oyo">Oyo</option>
                                                            <option value="plateau">Plateau</option>
                                                            <option value="rivers">Rivers</option>
                                                            <option value="sokoto">Sokoto</option>
                                                            <option value="taraba">Taraba</option>
                                                            <option value="yobe">Yobe</option>
                                                            <option value="zamfara">Zamfara</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-->

                                        </div>
                                        <!--end row-->
                                        <div class="row passwordRow">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label for="txtPassword"
                                                        class="col-lg-3 col-form-label">Password</label>
                                                    <div class="col-lg-9">
                                                        <input id="txtPassword" name="txtPassword" type="password"
                                                            class="form-control">
                                                    </div>
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-->
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label for="txtConfirmPassword"
                                                        class="col-lg-3 col-form-label">Confirm Password</label>
                                                    <div class="col-lg-9">
                                                        <input id="txtConfirmPassword" name="txtConfirmPassword"
                                                            type="password" class="form-control">
                                                    </div>
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-->

                                        </div>
                                        <!--end row-->
                                        <div class="row">
                                            <div class="col-md-4">
                                            </div>
                                            <div class="col-md-4">
                                                <button
                                                    class="btn btn-primary btn-round btn-block waves-effect waves-light"
                                                    type="button" id="addMonitor">Add Monitor<i
                                                        class="fas fa-sign-in-alt ml-1"></i></button>

                                                <button
                                                    class="btn btn-primary btn-round btn-block waves-effect waves-light"
                                                    type="button" id="updateMonitor" style="display: none;">Update
                                                    Monitor<i class="fas fa-sign-in-alt ml-1"></i></button>
                                            </div>
                                            <!--end col-->
                                            <div class="col-md-4">
                                            </div>
                                            <!--end col-->

                                        </div>
                                        <!--end row-->

                                    </fieldset>
                                    <!--end fieldset-->
                                </form>
                                <!--end form-->
                            </div>
                            <!--end card-body-->
                        </div>
                        <!--end card-->
                    </div>
                    <!--end col-->
                </div>
                <!--end row-->

            </div><!-- container -->

            <?php include_once '../customs/app_footer.php'?>
            <!--end footer-->
        </div>
        <!-- end page content -->
    </div>
    <!-- end page-wrapper -->

    <!-- jQuery  -->
    <?php include '../customs/app_js_files.php'?>

    <!-- Custom Charts Functions js -->
    <script src="../assets/js/monitorsController/addMonitor.js"></script>

    <!-- Custom Charts Functions js -->
    <script src="../assets/js/monitorsController/editMonitor.js"></script>

</body>

</html>