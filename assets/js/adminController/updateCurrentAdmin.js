(function ($) {
    var getToken = localStorage.getItem("x-auth-token");

    var getRole = localStorage.getItem("role");

    let AddAdminsConstants = {};

    'use strict';

    function getEditedReport() {

        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("x-auth-token", getToken);

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        fetch("https://peaceful-atoll-23751.herokuapp.com/" + `just-clock-285910.uc.r.appspot.com/api/admin/getCurrentAdmin`, requestOptions)
            .then(function (response) {
                if (response.status === 403) {
                    alert("Token expired. Login");
                    window.location.href = "../index.php";
                }
                if (!response.ok) {

                    response.json().then(function (data) {
                        alert(data.message);
                    });
                    // alert("Cannot login. Ensure your data is correct and you have internet connection");
                    throw new Error("HTTP status " + response.status);
                }

                return response.json();
            })
            .then(function (result) {

                populateFormField(result.details);

                $("#form-horizontal-admin").show();
                $("#loader").hide();

            })
            .catch(error => console.log('error', error));

    }

    function populateFormField(details) {

        $('#txtFirstName').val(details.firstName);
        $('#txtLastName').val(details.lastName);
        $('#txtEmail').val(details.email);
        $('#txtPhoneNumber').val(details.phoneNumber);

    }

    function updateAdmin() {

        AddAdminsConstants["firstName"] = $('#txtFirstName').val();

        AddAdminsConstants["lastName"] = $('#txtLastName').val();

        AddAdminsConstants["email"] = $('#txtEmail').val();

        AddAdminsConstants["phoneNumber"] = $('#txtPhoneNumber').val();

        AddAdminsConstants["picture"] = "";

        console.log(AddAdminsConstants);

        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("x-auth-token", getToken);

        var requestOptions = {
            method: 'PATCH',
            headers: myHeaders,
            body: JSON.stringify(AddAdminsConstants),
            redirect: 'follow'
        };

        $("#form-horizontal-admin").hide();
        $("#loader").show();

        fetch("https://peaceful-atoll-23751.herokuapp.com/" + `just-clock-285910.uc.r.appspot.com/api/admin/update`, requestOptions)
            .then(function (response) {
                if (response.status === 403) {
                    alert("Token expired. Login");
                    window.location.href = "../index.php";
                }
                if (!response.ok) {
                    // console.log(response.json());
                    // alert(response.statusText);

                    response.json().then(function (data) {
                        alert(data.message);
                    });

                    $("#form-horizontal-admin").show();
                    $("#loader").hide();
                    throw new Error("HTTP status " + response.status);
                }

                $("#form-horizontal-admin").show();
                $("#loader").hide();

                return response.clone().json();
            })
            .then(function (result) {

                console.log(result);

                if (result.statusCode !== 200) {
                    alert(result.message);

                    $("#form-horizontal-admin").show();
                    $("#loader").hide();
                    return;
                }

                $("#form-horizontal-admin").show();
                $("#loader").hide();
                alert(result.message);
                window.location.href = "profile.php";

            })
            .catch(error => console.log('error', error));

    }

    function init() {
        getEditedReport();
        Waves.init();
    }


    $("#form-horizontal-admin").hide();
    $("#loader").show();

    init();

    $("#updateAdmin").click(function () {
        updateAdmin();

    });


})(jQuery)