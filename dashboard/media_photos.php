<!DOCTYPE html>
<html lang="en">

<?php $header_title = 'Add Media Photo | VTrack Reporting Dasboard - A concept of KDI';
include '../customs/app_head.php'
?>

<body>

    <?php include '../customs/app_topbar.php'?>

    <div class="page-wrapper">
        <!-- Left Sidenav -->
        <?php include '../customs/app_sidenav.php'?>
        <!-- end left-sidenav-->

        <!-- Page Content-->
        <div class="page-content">

            <div class="container-fluid">
                <!-- Page-Title -->
                <?php $page_title = 'Add Media Photo';
include_once '../customs/app_page_title.php'?>
                <!-- end page title end breadcrumb -->

                <div class="row" id="photos_row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">

                                <div class="col-md-12">

                                    <div class="form-group row">
                                        <h3>Add Media Photos</h3>

                                        <div class="col-lg-12">

                                            <input type="file" class="input__input" multiple id="media_asset"
                                                accept="image/gif, image/jpeg, image/png" onchange="readURL(this);" />
                                            <div id="image_preview"></div>

                                        </div>
                                        <div class="col-lg-12" style="margin-top: 20px;margin-bottom:20px;"> <button
                                                type="button" class="btn btn-primary waves-effect waves-light"
                                                id="upload_images">Upload Images</button>
                                        </div>

                                        <div class="cssload-thecube" id="loader" style="display: none">
                                            <div class="cssload-cube cssload-c1"></div>
                                            <div class="cssload-cube cssload-c2"></div>
                                            <div class="cssload-cube cssload-c4"></div>
                                            <div class="cssload-cube cssload-c3"></div>
                                        </div>

                                    </div>

                                </div>
                                <!--end row-->
                                <!--end form-->
                            </div>
                            <!--end card-body-->
                        </div>
                        <!--end card-->
                    </div>
                    <!--end col-->
                </div>
                <!--end row-->



                <div class="row" id="ylinks_row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">

                                <div class="col-md-12">

                                    <div class="form-group row">
                                        <h3>Add Youtube Links</h3>
                                        <p>Add Youtube (embed) links (e.g. https://www.youtube.com/embed/WUlYvKhHUo0)
                                            and separate each link with a comma ',' (VERY IMPORTANT)</p>

                                        <div class="col-lg-12">

                                            <textarea id="txtYLinks" name="txtYLinks" rows="7" class="form-control"
                                                placeholder="Add Youtube (embed) links (e.g. https://www.youtube.com/embed/WUlYvKhHUo0) and separate each link with a comma ',' (VERY IMPORTANT)"></textarea>

                                        </div>
                                        <div class="col-lg-12" style="margin-top: 20px;margin-bottom:20px;"> <button
                                                type="button" class="btn btn-primary waves-effect waves-light"
                                                id="upload_ylinks">Add Youtube Video Links</button>
                                        </div>

                                        <div class="cssload-thecube" id="loader2" style="display: none">
                                            <div class="cssload-cube cssload-c1"></div>
                                            <div class="cssload-cube cssload-c2"></div>
                                            <div class="cssload-cube cssload-c4"></div>
                                            <div class="cssload-cube cssload-c3"></div>
                                        </div>

                                    </div>

                                </div>
                                <!--end row-->
                                <!--end form-->
                            </div>
                            <!--end card-body-->
                        </div>
                        <!--end card-->
                    </div>
                    <!--end col-->
                </div>
                <!--end row-->



                <div class="row" id="pubs_row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">

                                <div class="col-md-12">

                                    <div class="form-group row">
                                        <h3>Add Publications</h3>

                                        <label style="color:red">Ensure image cover and publication file have the same
                                            name, else it will not show on the website</label>


                                        <div class="col-lg-12">

                                            <label for="media_pub_cover">Publication Title</label>

                                            <input id="media_pub_title" name="media_pub_title" type="text" class="form-control">

                                        </div>

                                        <div class="col-lg-12">

                                            <label for="media_pub_cover">Choose image cover and the publication file
                                                together for publication</label>

                                            <input type="file" class="input__input" id="media_pub_cover"
                                                accept="image/gif, image/jpeg, image/png"
                                                onchange="readPubCover(this);" />
                                            <div id="image_pub_cover"></div>

                                        </div>

                                        <div class="col-lg-12">

                                            <label for="media_pub">Click to choose publication. Ensure your publication
                                                name is unique.</label>
                                            <input type="file" class="input__input" id="media_pub"
                                                accept="application/pdf,application/vnd.ms-excel" />
                                            <div id="image_pub"></div>

                                        </div>
                                        <div class="col-lg-12" style="margin-top: 20px;margin-bottom:20px;"> <button
                                                type="button" class="btn btn-primary waves-effect waves-light"
                                                id="upload_pubs">Add Publication</button>
                                        </div>

                                        <div class="cssload-thecube" id="loader3" style="display: none">
                                            <div class="cssload-cube cssload-c1"></div>
                                            <div class="cssload-cube cssload-c2"></div>
                                            <div class="cssload-cube cssload-c4"></div>
                                            <div class="cssload-cube cssload-c3"></div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div><!-- container -->

            <?php include_once '../customs/app_footer.php'?>
            <!--end footer-->
        </div>
        <!-- end page content -->
    </div>
    <!-- end page-wrapper -->

    <!-- jQuery  -->
    <?php include '../customs/app_js_files.php'?>

    <!-- Custom Charts Functions js -->
    <script src="../assets/js/homeController/media_upload.js"></script>

</body>

</html>