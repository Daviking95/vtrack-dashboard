<!DOCTYPE html>
<html lang="en">

<?php $header_title = 'Dashboard | VTrack Reporting Dasboard - A concept of KDI';
include '../customs/app_head.php'
?>

<body>

    <?php include '../customs/app_topbar.php' ?>

    <div class="page-wrapper">
        <!-- Left Sidenav -->
        <?php include '../customs/app_sidenav.php' ?>
        <!-- end left-sidenav-->

        <!-- Page Content-->
        <div class="page-content">

            <div class="container-fluid">
                <!-- Page-Title -->
                <?php $page_title = 'Dashboard';
include_once '../customs/app_page_title.php'?>
                <!-- end page title end breadcrumb -->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-3 align-self-center">
                                                <div class="icon-info">
                                                    <i class="mdi mdi-beaker text-info"></i>
                                                </div>
                                            </div>
                                            <div class="col-9 align-self-center text-right">
                                                <div class="ml-2">
                                                    <p class="mb-1 text-muted">Total Reports On VTrack</p>
                                                    <h4 class="mt-0 mb-1" id="total_forms">0</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end card-body-->
                                </div>
                                <!--end card-->
                            </div>
                            <div class="col-lg-4">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-3 align-self-center">
                                                <div class="icon-info">
                                                    <i class="mdi mdi-clipboard-outline text-warning"></i>
                                                </div>
                                            </div>
                                            <div class="col-9 align-self-center text-right">
                                                <div class="ml-2">
                                                    <p class="mb-1 text-muted">Awaiting Verification</p>
                                                    <h4 class="mt-0 mb-1" id="total_forms_verification">0</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end card-body-->
                                </div>
                                <!--end card-->
                            </div>
                            <!--end col-->

                            <div class="col-lg-4">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-3 align-self-center">
                                                <div class="icon-info">
                                                    <i class="mdi mdi-check-box-multiple-outline text-purple"></i>
                                                </div>
                                            </div>
                                            <div class="col-9 align-self-center text-right">
                                                <div class="ml-2">
                                                    <div class="ml-2">
                                                        <p class="mb-0 text-muted">Awaiting Approval</p>
                                                        <h4 class="mt-0 mb-1 d-inline-block" id="total_forms_approval">0
                                                        </h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end card-body-->
                                </div>
                                <!--end card-->
                            </div>
                            <!--end col-->

                        </div>
                        <!--end row-->
                    </div>
                    <!--end col-->

                </div>
                <!--end row-->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="header-title  mt-0 mb-4">Reports On Map</h4>
                                <div id="vmap" style="height: 400px"></div>
                                <div class="cssload-thecube" id="loader" style="display: none">
                                    <div class="cssload-cube cssload-c1"></div>
                                    <div class="cssload-cube cssload-c2"></div>
                                    <div class="cssload-cube cssload-c4"></div>
                                    <div class="cssload-cube cssload-c3"></div>
                                </div>
                            </div>
                            <!--end card-body-->
                        </div>
                        <!--end card-->
                    </div> <!-- end col -->
                </div> <!-- end row -->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="header-title mt-0 mb-4">Where Did The Incident Happen ?</h4>
                                <div class="chart-demo">
                                    <div id="where_inci_happen_chart" class="apex-charts"></div>
                                </div>
                            </div>
                            <!--end card-body-->
                        </div>
                        <!--end card-->
                    </div>
                    <!--end col-->
                </div>
                <!--end row-->

                <div class="row">
                    <div class="col-lg-12 col-xl-8">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="mt-0 header-title">Perpetrators - Who did the violence</h4>
                                <canvas id="bar" height="300"></canvas>
                            </div>
                            <!--end card-body-->
                        </div>
                        <!--end card-->
                    </div> <!-- end col -->

                    <div class="col-lg-12 col-xl-4">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="mt-0 header-title">Perpetrators - Gender</h4>
                                <canvas id="polarArea" height="300"> </canvas>
                            </div>
                            <!--end card-body-->
                        </div>
                        <!--end card-->
                    </div> <!-- end col -->
                </div> <!-- end row -->

                <div class="row">
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="mt-0 header-title">Gender of victims</h4>
                                <div id="donut-example" style="height: 300px"></div>
                                <!-- <div id="legend" class="donut-legend"></div> -->
                            </div>
                            <!--end card-body-->
                        </div>
                        <!--end card-->
                    </div> <!-- end col -->

                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="mt-0 header-title">Were the victims people or property</h4>
                                <div id="animating-donut" class="ct-chart ct-golden-section"></div>
                                <!-- <div id="animating-legend" class="donut-legend"></div> -->

                            </div>
                            <!--end card-body-->
                        </div>
                        <!--end-card-->
                    </div> <!-- end col -->
                </div> <!-- end row -->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="header-title mt-0 mb-4">Who were the victims</h4>
                                <div class="chart-demo">
                                    <div id="apex_line1" class="apex-charts"></div>
                                </div>
                            </div>
                            <!--end card-body-->
                        </div>
                        <!--end card-->
                    </div>
                    <!--end col-->
                </div>
                <!--end row-->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="mt-0 header-title">What kind of violence happened</h4>
                                <div class="chart-demo">
                                        <div id="apex_kind" class="apex-charts"></div>
                                    </div>     
                                <!-- <div id="chart-with-area" class="ct-chart ct-golden-section"></div> -->
                            </div>
                            <!--end card-body-->
                        </div>
                        <!--end-card-->
                    </div> <!-- end col -->
                </div> <!-- end row -->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="mt-0 header-title">What kind of weapon used</h4>
                                <div id="morris-bar-example" style="height: 300px"></div>
                            </div>
                            <!--end card-body-->
                        </div>
                        <!--end-card-->
                    </div> <!-- end col -->
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="mt-0 header-title">What was the impact of the violence</h4>
                                <div id="overlapping-bars" style="height: 300px"></div>
                            </div>
                            <!--end card-body-->
                        </div>
                        <!--end-card-->
                    </div> <!-- end col -->
                </div><!-- end row -->

            </div><!-- container -->

            <?php include_once '../customs/app_footer.php'?>
            <!--end footer-->
        </div>
        <!-- end page content -->
    </div>
    <!-- end page-wrapper -->

    <!-- jQuery  -->
    <?php include '../customs/app_js_files.php' ?>

    <!-- Dashboard Functions js -->
    <script src="../assets/js/homeController/dashboard.js"></script>

    <!-- Custom Charts Functions js -->
    <script src="../assets/js/reportsController/mapCharts.js"></script>

    <script src="../assets/js/reportsController/locationOfIncidentCharts.js"></script>
    <script src="../assets/js/reportsController/perpetratorsCharts.js"></script>
    <script src="../assets/js/reportsController/genderVictimsCharts.js"></script>
    <script src="../assets/js/reportsController/typeVictimsCharts.js"></script>
    <script src="../assets/js/reportsController/peopleVictimsCharts.js"></script>
    <script src="../assets/js/reportsController/kindOfViolenceCharts.js"></script>
    <script src="../assets/js/reportsController/kindOfWeaponCharts.js"></script>
    <script src="../assets/js/reportsController/impactOfViolenceCharts.js"></script>

</body>

</html>