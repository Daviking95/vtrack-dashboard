<!DOCTYPE html>
<html lang="en">

<?php $header_title = 'Monitors | VTrack Reporting Dasboard - A concept of KDI';
include '../customs/app_head.php'
?>

<body>

    <?php include '../customs/app_topbar.php' ?>

    <div class="page-wrapper">
        <!-- Left Sidenav -->
        <?php include '../customs/app_sidenav.php' ?>
        <!-- end left-sidenav-->

        <!-- Page Content-->
        <div class="page-content">

            <div class="container-fluid">
                <!-- Page-Title -->
                <?php $page_title = 'All Monitors';
include_once '../customs/app_page_title.php'?>
                <!-- end page title end breadcrumb -->

                <div class="row">
                    <div class="col-lg-12">
                        <!--begin::Portlet-->
                        <div
                            class="m-portlet m-portlet--mobile m-portlet--creative m-portlet--first m-portlet--bordered-semi">

                            <div class="m-portlet__body">
                                <!--begin: Datatable -->
                                
                                <!-- Table -->
                                <div class="m_datatable_2" id="local_data"></div>
                                <!-- End Table -->
                            </div>
                        </div>
                        <!--end::Portlet-->
                    </div>
                </div>

            </div><!-- container -->

            <?php include_once '../customs/app_footer.php'?>
            <!--end footer-->
        </div>
        <!-- end page content -->
    </div>
    <!-- end page-wrapper -->

    <!-- jQuery  -->
    <?php include '../customs/app_js_files.php' ?>

    <!-- Custom Charts Functions js -->
    <script src="../assets/js/monitorsController/monitorReportsTable.js"></script>

</body>

</html>