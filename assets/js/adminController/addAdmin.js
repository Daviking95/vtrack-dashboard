(function ($) {
    var getToken = localStorage.getItem("x-auth-token");

    'use strict';

    $("#form-horizontal-admin").show();
    $("#loader").hide();

    let AddAdminsConstants = {};

    function addAdmin() {

        AddAdminsConstants["firstName"] = $('#txtFirstName').val();
    
        AddAdminsConstants["lastName"] = $('#txtLastName').val();
    
        AddAdminsConstants["email"] = $('#txtEmail').val();
    
        AddAdminsConstants["phoneNumber"] = $('#txtPhoneNumber').val();
    
        AddAdminsConstants["password"] = $('#txtPassword').val();
    
        AddAdminsConstants["passwordConfirmation"] = $('#txtConfirmPassword').val();
    
        AddAdminsConstants["role"] = $('#adminRoleSpinner').val();

        // console.log(AddAdminsConstants);

        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("x-auth-token", getToken);

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: JSON.stringify(AddAdminsConstants),
            redirect: 'follow'
        };

        $("#form-horizontal-admin").hide();
        $("#loader").show();

        fetch("https://peaceful-atoll-23751.herokuapp.com/" + "just-clock-285910.uc.r.appspot.com/api/admin/create", requestOptions)
            .then(function (response) {
                if (response.status === 403) {
                    alert("Token expired. Login");
                    window.location.href = "../index.php";
                }
                if (!response.ok) {
                    // console.log(response.json());
                    // alert(response.statusText);

                    response.json().then(function(data) {
                        alert(data.message);
                    });
                    
                    $("#form-horizontal-admin").show();
                    $("#loader").hide();
                    throw new Error("HTTP status " + response.status);
                }

                $("#form-horizontal-admin").show();
                $("#loader").hide();

                return response.json();
            })
            .then(function (result) {

                console.log(result);

                if(result.statusCode !== 200){
                    alert(result.message);

                    $("#form-horizontal-admin").show();
                    $("#loader").hide();
                    return;
                }

                $("#form-horizontal-admin").show();
                $("#loader").hide();
                alert(result.message);
                window.location.href = "all.php";

            })
            .catch(error => console.log('error', error));

    }

    $("#addAdmin").click(function() {
        addAdmin();

    });

    // function init() {
    //     Waves.init();
    // }

    // init();

})(jQuery)