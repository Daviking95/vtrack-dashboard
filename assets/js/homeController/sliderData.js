// (function ($) {

'use strict';

var firebaseDb = firebase.firestore();
var docRef = firebaseDb.collection("vtrack_media_center").doc("index_sliders");
var docRef2 = firebaseDb.collection("vtrack_media_center").doc("index_sliders");

var data;
let testTable;

let prevDataList = [];

let isEdit = false;

$("#submit_result").click(function () {

    $("#submit_result").hide();
    $("#loader3").show();

    let txtTitle = $("#txtTitle").val();
    let txtNumbers = $("#txtNumbers").val();
    let txtHashtag = $("#txtHashtag").val();

    if (txtTitle === null || txtTitle === "") {
        alert("You need to add title");

        $("#submit_result").show();
        $("#loader3").hide();
        return;
    }

    let dataObject = {
        "title": txtTitle,
        "hashtag": txtHashtag,
        "numbers": txtNumbers
    };

    saveLinksToDb("data_array", dataObject);

});


$("#add_result").click(function () {

    popupFunction(null);
});

function getAllForms() {

    docRef.get().then(function (doc) {
        if (doc.exists) {
            data = doc.data();
            // console.log(data['data_array']);
            // console.log("Document data:", doc.data());

            var elementArray = data['data_array'];

            testTable = JSON.stringify(elementArray);

            DatatableDataForms();
            $("#csv_div").css("display", "block");

        } else {
            // doc.data() will be undefined in this case
            console.log("No such document!");
        }
    }).catch(function (error) {
        console.log("Error getting document:", error);
    });
}

function DatatableDataForms() {
    var e, a, i;
    (e = JSON.parse(testTable)), (a = $('.m_datatable_election_result').mDatatable({
        data: {
            type: 'local',
            source: e,
            pageSize: 10
        },
        layout: {
            theme: 'default',
            class: '',
            scroll: !1,
            footer: !1
        },
        sortable: !0,
        pagination: !0,
        search: {
            input: $('#allElectionReportSearch')
        },
        columns: [{
                field: 'title',
                title: 'Title'
            }, {
                field: 'numbers',
                title: 'Number'
            }, {
                field: 'hashtag',
                title: 'Hashtag'
            },
            {
                field: 'Actions',
                width: 110,
                title: 'Actions',
                sortable: !1,
                overflow: 'visible',
                template: function (e, a, i) {
                    // console.log(a[i]);
                    // console.log(i);
                    return (
                        "\t\t\t\t\t\t<div class='dropdown " +
                        (i.getPageSize() - a <= 4 ? "dropup" : "") +
                        "' style='display:none;'>\t\t\t\t\t\t\t<a href='#' class='btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill' data-toggle='dropdown' >                                <i class='la la-ellipsis-h'></i>                            </a>\t\t\t\t\t\t  \t<div class='dropdown-menu dropdown-menu-right'>\t\t\t\t\t\t    \t<a class='dropdown-item' href='#'><i class='la la-eye'></i> View Form</a>\t\t\t\t\t\t    \t<a class='dropdown-item' href='#'><i class='la la-trash-o'></i> Delete Form</a>\t\t\t\t\t\t  \t</div>\t\t\t\t\t\t</div>\t\t\t\t\t\t<a href='javascript:popupFunction(" + JSON.stringify(e) + ")' class='m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill' title='Edit '>                            <i class='la la-edit'></i>                        </a>\t\t\t\t\t<a href='javascript:deleteFunction(" + JSON.stringify(e) + ")' class='m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill' title='Delete '>                            <i class='la la-trash-o'></i>                        </a>\t\t\t\t\t"
                    )
                }
            }
        ]
    })), i = a.getDataSourceQuery(), $("#m_form_status").on("change", function () {
        a.search($(this).val(), "formCode")
    }).val(void 0 !== i.firstApproval ? i.firstApproval : ""), $("#m_form_type").on("change", function () {
        a.search($(this).val(), "Type")
    }).val(void 0 !== i.Type ? i.Type : ""), $("#m_form_status, #m_form_type").selectpicker()
}

function popupFunction(data) {

    $("#txtTitle").val("");
    $("#txtHashtag").val("");
    $("#txtNumbers").val("");

    isEdit = false;

    if (data !== null) {
        console.log(data);

        isEdit = true;

        $("#txtTitle").val(data.title.toString());
        $("#txtHashtag").val(data.hashtag.toString());
        $("#txtNumbers").val(data.numbers.toString());

    }

    const reportPopup = document.querySelector(".report-popup");
    reportPopup.classList.add("active");

    $(".report-popup").show();

    $("#close-report").on('click', function (e) {
        $('.report-popup').fadeOut('slow');
    });
}

function deleteFunction(data) {
    var SweetAlert = function () {};

    SweetAlert.prototype.init = function () {

        swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then(async (result) => {
            if (result.value) {

                var objArray = [];

                prevDataList = await getPreviousData();

                if (prevDataList["data_array"] !== undefined) {
                    prevDataList["data_array"].forEach(element => {
                        objArray.push(element);
                    });
                }

                // console.log(data);
                // console.log(objArray);

                var filtered = objArray.filter(function (value, index, arr) {
                    return value.title != data.title;
                });

                console.log(filtered);

                var obj = {
                    "data_array": filtered
                };

                docRef2.set(obj, {
                        merge: true
                    })
                    .then(function () {
                        console.log("Document successfully written!");

                        alert("Slider data removed successfully");

                        location.replace("sliders_data.php");

                    })
                    .catch(function (error) {
                        console.error("Error writing document: ", error);

                        alert("Error Occured while trying to save to DB");

                        location.replace("sliders_data.php");
                    });


            } else if (
                // Read more about handling dismissals
                result.dismiss === Swal.DismissReason.cancel
            ) {
                swal.fire(
                    'Cancelled',
                    'The data is safe :)',
                    'error'
                )
            }
        })
    }

    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
    $.SweetAlert.init()
}

async function getPreviousData() {

    const promise = new Promise((resolve, reject) => {

        docRef2.get().then(function (doc) {
            if (doc.exists) {

                resolve(doc.data());
            } else {
                console.log("No such document!");
            }
        }).catch(function (error) {
            console.log("Error getting document:", error);
        });
    });

    let result = await promise;

    prevDataList = result;

    return result;


}

async function updateData(data) {

    isEdit = false;

    var objArray = [];

    prevDataList = await getPreviousData();

    if (prevDataList["data_array"] !== undefined) {
        prevDataList["data_array"].forEach(element => {
            objArray.push(element);
        });
    }

    console.log(data);
    console.log(objArray);

    var filtered = objArray.filter(function (value, index, arr) {
        return value.title != data.title;
    });

    filtered.push(data);

    // objArray.forEach(element => {

    //     if (element.title == data.title) {
    //         objArray.pop(element);
    //         objArray.push(data);
    //         // element = data;
    //     }
    // });

    // console.log(objArray);

    var obj = {
        "data_array": filtered
    };

    docRef2.set(obj, {
            merge: true
        })
        .then(function () {
            console.log("Document successfully written!");

            alert("Slider data edited successfully");

            location.replace("sliders_data.php");

        })
        .catch(function (error) {
            console.error("Error writing document: ", error);

            alert("Error Occured while trying to save to DB");

            location.replace("sliders_data.php");
        });
}

async function saveLinksToDb(objKey, dataObject) {

    if (isEdit) {
        updateData(dataObject);
        return;
    }

    var objArray = [];

    prevDataList = await getPreviousData();

    console.log(prevDataList[objKey]);

    if (prevDataList[objKey] !== undefined) {
        prevDataList[objKey].forEach(element => {
            objArray.push(element);
        });

        objArray.push(dataObject);
    }

    console.log(objArray);

    var obj = {};

    obj[objKey] = objArray;

    docRef2.set(obj, {
            merge: true
        })
        .then(function () {
            console.log("Document successfully written!");

            alert("Slider data added successfully");

            location.replace("sliders_data.php");

        })
        .catch(function (error) {
            console.error("Error writing document: ", error);

            alert("Error Occured while trying to save to DB");

            location.replace("sliders_data.php");
        });
}


function init() {
    getAllForms();
    Waves.init();
}

init();

// })(jQuery)