<!DOCTYPE html>
<html lang="en">

<?php $header_title = 'Reports | VTrack Reporting Dasboard - A concept of KDI';
include '../customs/app_head.php'
?>

<body>

    <?php include '../customs/app_topbar.php'?>

    <div class="page-wrapper">
        <!-- Left Sidenav -->
        <?php include '../customs/app_sidenav.php'?>
        <!-- end left-sidenav-->

        <!-- Page Content-->
        <div class="page-content">

            <div class="container-fluid">
                <!-- Page-Title -->
                <?php $page_title = 'Add Report';
include_once '../customs/app_page_title.php'?>
                <!-- end page title end breadcrumb -->

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">

                                <button type="button" class="btn btn-primary waves-effect waves-light float-right mb-3"
                                    data-toggle="modal" data-animation="bounce" data-target=".bs-example-modal-lg"
                                    id="delete_form" style="display: none">Delete</button>
                                <div class="cssload-thecube" id="loader" style="display: none">
                                    <div class="cssload-cube cssload-c1"></div>
                                    <div class="cssload-cube cssload-c2"></div>
                                    <div class="cssload-cube cssload-c4"></div>
                                    <div class="cssload-cube cssload-c3"></div>
                                </div>
                                <form id="form-horizontal" class="form-horizontal form-wizard-wrapper">
                                    <h3>Page 1</h3>
                                    <fieldset>

                                        <div class="row">
                                            <!-- <div class="col-md-12">

                                                <div class="form-group row">

                                                    <div class="col-lg-6">
                                                        <label for="gallery_div" class="col-form-label">Media
                                                            Asset</label>
                                                        <input type="file" class="input__input" multiple
                                                            id="media_asset" accept="image/gif, image/jpeg, image/png"
                                                            onchange="readURL(this);" />
                                                        <div id="image_preview"></div>

                                                    </div>
                                                    <div class="col-lg-6" style="margin-top: 20px;margin-bottom:20px;"
                                                        id="gallery_div">
                                                    </div>
                                                </div>

                                            </div> -->
                                            <div class="col-md-6" hidden>
                                                <div class="form-group row">
                                                    <label for="txtFormCode" class="col-lg-3 col-form-label">Other
                                                        Data</label>
                                                    <div class="col-lg-9">
                                                        <input id="txtFormCode" name="txtFormCode" type="text"
                                                            class="form-control">
                                                        <input id="timeOfReport" name="timeOfReport" type="text"
                                                            class="form-control">
                                                        <input id="firstApproval" name="firstApproval" type="text"
                                                            class="form-control">
                                                        <input id="secondApproval" name="secondApproval" type="text"
                                                            class="form-control">
                                                        <input id="formSubmissionPoint" name="formSubmissionPoint"
                                                            type="text" class="form-control">
                                                        <input id="locationOfReport" name="locationOfReport" type="text"
                                                            class="form-control">
                                                    </div>
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-->
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label for="txtIncidenceTitle"
                                                        class="col-lg-3 col-form-label">Incidence Title</label>
                                                    <div class="col-lg-9">
                                                        <input id="txtIncidenceTitle" name="txtIncidenceTitle"
                                                            type="text" class="form-control">
                                                    </div>
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-->
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label for="txtIncidenceDesc"
                                                        class="col-lg-3 col-form-label">Incidence Description</label>
                                                    <div class="col-lg-9">
                                                        <textarea id="txtIncidenceDesc" name="txtIncidenceDesc" rows="4"
                                                            class="form-control"></textarea>
                                                    </div>
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-->

                                        </div>
                                        <!--end row-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label for="monitorCodeSpinner"
                                                        class="col-lg-3 col-form-label">Select Monitor Code</label>
                                                    <div class="col-lg-9">
                                                        <select id="monitorCodeSpinner" name="monitorCodeSpinner"
                                                            class="select2 form-control mb-3 custom-select"
                                                            style="width: 100%; height:36px;" data-placeholder="Choose"
                                                            onchange="displayMonitorName()">
                                                            <option value="" selected="true" disabled="disabled">
                                                                --Please Select--</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-->
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label for="txtMonitorName" class="col-lg-3 col-form-label">Monitor
                                                        Name</label>
                                                    <div class="col-lg-9">
                                                        <input id="txtMonitorName" name="txtMonitorName" type="text"
                                                            class="form-control" disabled>
                                                    </div>
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-->

                                        </div>
                                        <!--end row-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label for="txtReportDate" class="col-lg-3 col-form-label">Date of
                                                        report</label>
                                                    <div class="col-lg-9">
                                                        <?php echo
'<input id="txtReportDate" name="txtReportDate" type="text"
                                                            class="form-control" value="' . date('d/m/y') . '" disabled>'
?>
                                                    </div>
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-->
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label for="txtDateOfIncidence" class="col-lg-3 col-form-label">Date
                                                        of incidence</label>
                                                    <div class="col-lg-9">
                                                        <input type="date" class="form-control" placeholder="2017-06-04"
                                                            id="txtDateOfIncidence" min="1997-01-01" max="2030-12-31">
                                                    </div>
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-->

                                        </div>
                                        <!--end row-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label for="electionSpinner"
                                                        class="col-lg-3 col-form-label">Election</label>
                                                    <div class="col-lg-9">
                                                        <select id="electionSpinner" name="electionSpinner"
                                                            class="select2 form-control mb-3 custom-select"
                                                            style="width: 100%; height:36px;" data-placeholder="Choose">
                                                            <option value="" selected="true" disabled="disabled">
                                                                --Please Select--</option>
                                                            <option value="presidential">Presidential</option>
                                                            <option value="governorship">Governorship</option>
                                                            <option
                                                                value="legislative (national assembly - senate or house of rep)">
                                                                Legislative (National Assembly - Senate or House of Rep)
                                                            </option>
                                                            <option value="legislative (state house of assembly)">
                                                                Legislative (State House of Assembly)</option>
                                                                <option value="local government">
                                                                Local Government</option>

                                                        </select>
                                                    </div>
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-->
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label for="timeOfIncidenceSpinner"
                                                        class="col-lg-3 col-form-label">Time of incidence</label>
                                                    <div class="col-lg-9">
                                                        <select id="timeOfIncidenceSpinner"
                                                            name="timeOfIncidenceSpinner"
                                                            class="select2 form-control mb-3 custom-select"
                                                            style="width: 100%; height:36px;" data-placeholder="Choose">
                                                            <option value="" selected="true" disabled="disabled">
                                                                --Please Select--</option>
                                                            <option value="morning (6am - 12pm)">Morning (6am - 12pm)
                                                            </option>
                                                            <option value="afternoon (12pm - 6pm)">Afternoon (12pm -
                                                                6pm)</option>
                                                            <option value="evening (6pm - 12am)">Evening (6pm - 12am)
                                                            </option>
                                                            <option value="night (12am - 6am)">Night (12am - 6am)
                                                            </option>
                                                            <option value="unable to Determine">Unable to determine
                                                            </option>

                                                        </select>
                                                    </div>
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-->
                                        </div>
                                        <!--end row-->
                                        <p class="text-muted mb-3">Where the incidence happened</p>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label for="geoPoliticalZoneSpinner"
                                                        class="col-lg-3 col-form-label">Geo-Political Zone</label>
                                                    <div class="col-lg-9">
                                                        <select id="geoPoliticalZoneSpinner"
                                                            name="geoPoliticalZoneSpinner"
                                                            class="select2 form-control mb-3 custom-select"
                                                            style="width: 100%; height:36px;" data-placeholder="Choose">

                                                            <option value="" selected="true" disabled="disabled">Select
                                                                Geopolitical zone</option>

                                                            <option value="north central">North Central</option>
                                                            <option value="north east">North East</option>
                                                            <option value="north west">North West</option>
                                                            <option value="south east">South East</option>
                                                            <option value="south south">South South</option>
                                                            <option value="south west">South West</option>

                                                        </select>
                                                    </div>
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-->
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label for="stateSpinner"
                                                        class="col-lg-3 col-form-label">State</label>
                                                    <div class="col-lg-9">
                                                        <select id="stateSpinner" name="stateSpinner"
                                                            class="select2 form-control mb-3 custom-select"
                                                            style="width: 100%; height:36px;" data-placeholder="Choose">

                                                            <option value="" selected="true" disabled="disabled">
                                                                --Please Select--</option>

                                                        </select>
                                                        <div id="stateSpinnerEdit" style="display: none;"></div>
                                                    </div>
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-->
                                        </div>
                                        <!--end row-->
                                        <div class="row">
                                            <div class="col-md-9">
                                                <div class="form-group row">
                                                    <label for="lgaSpinner" class="col-lg-4 col-form-label">Local
                                                        Government</label>
                                                    <div class="col-lg-8">
                                                        <input id="lgaSpinner" name="lgaSpinner" type="text"
                                                            class="form-control">
                                                        <!-- <select id="lgaSpinner" name="lgaSpinner"
                                                            class="select2 form-control mb-3 custom-select"
                                                            style="width: 100%; height:36px;" data-placeholder="Choose">

                                                        </select> -->
                                                    </div>
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-->

                                        </div>
                                        <!--end row-->

                                    </fieldset>
                                    <!--end fieldset-->

                                    <h3>Page 2</h3>
                                    <fieldset>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-md-3 my-2 control-label">Who are your sources
                                                        ?</label>
                                                    <div class="col-md-9">
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="sourceWitnessCheck01"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="sourceWitnessCheck01">You (The Monitor) saw the
                                                                    incident</label>
                                                                <div id="witness1Spinner" style="display: none;"></div>
                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="sourceWitnessCheck02"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="sourceWitnessCheck02">Police personnel or
                                                                    report</label>
                                                                <div id="witness2Spinner" style="display: none;"></div>

                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="sourceWitnessCheck03"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="sourceWitnessCheck03">Hospital personnel or
                                                                    report</label>
                                                                <div id="witness3Spinner" style="display: none;"></div>

                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="sourceWitnessCheck04"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="sourceWitnessCheck04">Election official</label>
                                                                <div id="witness4Spinner" style="display: none;"></div>

                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="sourceWitnessCheck05"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="sourceWitnessCheck05">Election observer</label>
                                                                <div id="witness5Spinner" style="display: none;"></div>
                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="sourceWitnessCheck06"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="sourceWitnessCheck06">Government
                                                                    official</label>
                                                                <div id="witness6Spinner" style="display: none;"></div>
                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="sourceWitnessCheck07"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="sourceWitnessCheck07">NGO
                                                                    representative</label>
                                                                <div id="witness7Spinner" style="display: none;"></div>
                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="sourceWitnessCheck08"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="sourceWitnessCheck08">People in the
                                                                    community</label>
                                                                <div id="witness8Spinner" style="display: none;"></div>

                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="sourceWitnessCheck09"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="sourceWitnessCheck09">Media personnel</label>
                                                                <div id="witness9Spinner" style="display: none;"></div>
                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="sourceWitnessCheck10"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="sourceWitnessCheck10">Others</label>
                                                                <div id="witness10Spinner" style="display: none;"></div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--end row-->
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-->

                                        </div>
                                        <!--end row-->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-md-3 my-2 control-label">Did you read or hear
                                                        about it in the media ?</label>
                                                    <div class="col-md-9">
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="mediaSourceCheck01"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="mediaSourceCheck01">Newspaper(s)</label>
                                                                <div id="media1Spinner" style="display: none;"></div>
                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="mediaSourceCheck02"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="mediaSourceCheck02">Online Media</label>
                                                                <div id="media2Spinner" style="display: none;"></div>

                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="mediaSourceCheck03"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="mediaSourceCheck03">Social media</label>
                                                                <div id="media3Spinner" style="display: none;"></div>

                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="mediaSourceCheck04"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="mediaSourceCheck04">Television program</label>
                                                                <div id="media4Spinner" style="display: none;"></div>

                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="mediaSourceCheck05"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="mediaSourceCheck05">Radio program</label>
                                                                <div id="media5Spinner" style="display: none;"></div>
                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="mediaSourceCheck06"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="mediaSourceCheck06">Others</label>
                                                                <div id="media6Spinner" style="display: none;"></div>
                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="mediaSourceCheck07"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="mediaSourceCheck07">No media</label>
                                                                <div id="media7Spinner" style="display: none;"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--end row-->
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-->

                                        </div>
                                        <!--end row-->
                                    </fieldset>
                                    <!--end fieldset-->

                                    <h3>Page 3</h3>
                                    <fieldset>
                                        <p>Where did the incident happen ?</p>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label for="whereIncidentHappenedSpinner"
                                                        class="col-lg-3 col-form-label">Where did the incident happen
                                                        ?</label>
                                                    <div class="col-lg-9">
                                                        <select id="whereIncidentHappenedSpinner"
                                                            name="whereIncidentHappenedSpinner" class="form-control">
                                                            <option value="" selected="true" disabled="disabled">
                                                                --Please Select--</option>
                                                            <option value="in/near polling station">In/near polling
                                                                station</option>
                                                            <option value="election office / facility">Election office /
                                                                Facility</option>
                                                            <option value="political party office/ facility">Political
                                                                party office/ Facility</option>
                                                            <option value="state (gov't) office/ property">State (Gov't)
                                                                office/ Property</option>
                                                            <option value="private property">Private property</option>
                                                            <option value="media office">Media office</option>
                                                            <option value="community / street / public area">Community /
                                                                Street / Public area</option>
                                                            <option value="others">Others</option>
                                                        </select>
                                                        <div id="incidentLocationSpecify" style="display: none;"></div>
                                                    </div>
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-->
                                        </div>
                                        <!--end row-->
                                        <p>Who did the violence, people and gender ?</p>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label for="peopleOfIncidentSpinners"
                                                        class="col-lg-3 col-form-label">How many people involved in the
                                                        violence
                                                        ?</label>
                                                    <div class="col-lg-9">
                                                        <select id="peopleOfIncidentSpinners"
                                                            name="peopleOfIncidentSpinners" class="form-control">
                                                            <option value="" selected="true" disabled="disabled">
                                                                --Please Select--</option>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                            <option value="13">13</option>
                                                            <option value="14">14</option>
                                                            <option value="15">15</option>
                                                            <option value="16">16</option>
                                                            <option value="17">17</option>
                                                            <option value="18">18</option>
                                                            <option value="19">19</option>
                                                            <option value="20">20</option>
                                                            <option value="21">21</option>
                                                            <option value="22">22</option>
                                                            <option value="23">23</option>
                                                            <option value="24">24</option>
                                                            <option value="25">25</option>
                                                            <option value="26">26</option>
                                                            <option value="27">27</option>
                                                            <option value="28">28</option>
                                                            <option value="29">29</option>
                                                            <option value="30">30</option>
                                                            <option value="unable to determine">Unable to Determine
                                                            </option>

                                                        </select>
                                                    </div>
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-->
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label for="genderOfIncidentSpinners"
                                                        class="col-lg-3 col-form-label">Gender involved in the violence
                                                        ?</label>
                                                    <div class="col-lg-9">
                                                        <select id="genderOfIncidentSpinners"
                                                            name="genderOfIncidentSpinners" class="form-control">
                                                            <option value="" selected="true" disabled="disabled">
                                                                --Please Select--</option>
                                                            <option value="only men">Only Men</option>
                                                            <option value="only women">Only Women</option>
                                                            <option value="both men and women">Both men and women
                                                            </option>
                                                            <option value="unable to determine">Unable to Determine
                                                            </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-->
                                        </div>
                                        <!--end row-->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-md-3 my-2 control-label">Who did the
                                                        violence?</label>
                                                    <div class="col-md-9">
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceInitiatorCheck01"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceInitiatorCheck01">Political party
                                                                    leader or supporter, Candidate or candidate
                                                                    supporter</label>
                                                                <div id="violenceInitiator1Spinner"
                                                                    style="display: none;"></div>
                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceInitiatorCheck02"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceInitiatorCheck02">Government / State
                                                                    actor</label>
                                                                <div id="violenceInitiator2Spinner"
                                                                    style="display: none;"></div>

                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceInitiatorCheck03"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceInitiatorCheck03">Party agent</label>
                                                                <div id="violenceInitiator3Spinner"
                                                                    style="display: none;"></div>

                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceInitiatorCheck04"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceInitiatorCheck04">Election observer /
                                                                    Monitor</label>
                                                                <div id="violenceInitiator4Spinner"
                                                                    style="display: none;"></div>

                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceInitiatorCheck05"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceInitiatorCheck05">Election Worker (POs,
                                                                    APOs, Returning Officers)</label>
                                                                <div id="violenceInitiator5Spinner"
                                                                    style="display: none;"></div>
                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceInitiatorCheck06"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceInitiatorCheck06">Voter(s)/
                                                                    Public</label>
                                                                <div id="violenceInitiator6Spinner"
                                                                    style="display: none;"></div>
                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceInitiatorCheck07"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceInitiatorCheck07">Campaign Material
                                                                    (Posters, Billboards)</label>
                                                                <div id="violenceInitiator7Spinner"
                                                                    style="display: none;"></div>
                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceInitiatorCheck08"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceInitiatorCheck08">Election office,
                                                                    property, material (like ballots, trucks, warehouse,
                                                                    etc.)</label>
                                                                <div id="violenceInitiator8Spinner"
                                                                    style="display: none;"></div>

                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceInitiatorCheck09"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceInitiatorCheck09">Political party
                                                                    office or property (specify party)</label>
                                                                <div id="violenceInitiator9Spinner"
                                                                    style="display: none;"></div>
                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceInitiatorCheck10"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceInitiatorCheck10">State (Gov't) office
                                                                    or property </label>
                                                                <div id="violenceInitiator10Spinner"
                                                                    style="display: none;"></div>

                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceInitiatorCheck11"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceInitiatorCheck11">Private
                                                                    property/building</label>
                                                                <div id="violenceInitiator11Spinner"
                                                                    style="display: none;"></div>

                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceInitiatorCheck12"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceInitiatorCheck12">Others</label>
                                                                <div id="violenceInitiator12Spinner"
                                                                    style="display: none;"></div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--end row-->
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-->
                                        </div>
                                        <!--end row-->
                                        <!-- ####################################################### -->

                                        <p>Victims of the violence, people and gender ?</p>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label for="victimTypeSpinners" class="col-lg-3 col-form-label">Were
                                                        victims People, Property or both ?</label>
                                                    <div class="col-lg-9">
                                                        <select id="victimTypeSpinners" name="victimTypeSpinners"
                                                            class="form-control">
                                                            <option value="" selected="true" disabled="disabled">
                                                                --Please Select--</option>
                                                            <option value="people">People</option>
                                                            <option value="property">Property</option>
                                                            <option value="both">Both
                                                            </option>

                                                        </select>
                                                    </div>
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-->
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label for="victimPeopleOfIncidentSpinners"
                                                        class="col-lg-3 col-form-label">How many people involved in the
                                                        violence
                                                        ?</label>
                                                    <div class="col-lg-9">
                                                        <select id="victimPeopleOfIncidentSpinners"
                                                            name="victimPeopleOfIncidentSpinners" class="form-control">
                                                            <option value="" selected="true" disabled="disabled">
                                                                --Please Select--</option>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                            <option value="13">13</option>
                                                            <option value="14">14</option>
                                                            <option value="15">15</option>
                                                            <option value="16">16</option>
                                                            <option value="17">17</option>
                                                            <option value="18">18</option>
                                                            <option value="19">19</option>
                                                            <option value="20">20</option>
                                                            <option value="21">21</option>
                                                            <option value="22">22</option>
                                                            <option value="23">23</option>
                                                            <option value="24">24</option>
                                                            <option value="25">25</option>
                                                            <option value="26">26</option>
                                                            <option value="27">27</option>
                                                            <option value="28">28</option>
                                                            <option value="29">29</option>
                                                            <option value="30">30</option>
                                                            <option value="unable to determine">Unable to Determine
                                                            </option>

                                                        </select>
                                                    </div>
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-->
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label for="victimGenderOfIncidentSpinners"
                                                        class="col-lg-3 col-form-label">Gender involved in the violence
                                                        ?</label>
                                                    <div class="col-lg-9">
                                                        <select id="victimGenderOfIncidentSpinners"
                                                            name="victimGenderOfIncidentSpinners" class="form-control">
                                                            <option value="" selected="true" disabled="disabled">
                                                                --Please Select--</option>
                                                            <option value="only men">Only Men</option>
                                                            <option value="only women">Only Women</option>
                                                            <option value="both men and women">Both men and women
                                                            </option>
                                                            <option value="unable to determine">Unable to Determine
                                                            </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-->
                                        </div>
                                        <!--end row-->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-md-3 my-2 control-label">Victims of the
                                                        violence?</label>
                                                    <div class="col-md-9">
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceVictimCheck01"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceVictimCheck01">Political party
                                                                    leader or supporter, Candidate or candidate
                                                                    supporter</label>
                                                                <div id="violenceVictim1Spinner" style="display: none;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceVictimCheck02"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceVictimCheck02">Government / State
                                                                    actor</label>
                                                                <div id="violenceVictim2Spinner" style="display: none;">
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceVictimCheck03"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceVictimCheck03">Party agent</label>
                                                                <div id="violenceVictim3Spinner" style="display: none;">
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceVictimCheck04"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceVictimCheck04">Election observer /
                                                                    Monitor</label>
                                                                <div id="violenceVictim4Spinner" style="display: none;">
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceVictimCheck05"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceVictimCheck05">Election Worker (POs,
                                                                    APOs, Returning Officers)</label>
                                                                <div id="violenceVictim5Spinner" style="display: none;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceVictimCheck06"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceVictimCheck06">Voter(s)/
                                                                    Public</label>
                                                                <div id="violenceVictim6Spinner" style="display: none;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceVictimCheck07"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceVictimCheck07">Campaign Material
                                                                    (Posters, Billboards)</label>
                                                                <div id="violenceVictim7Spinner" style="display: none;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceVictimCheck08"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceVictimCheck08">Election office,
                                                                    property, material (like ballots, trucks, warehouse,
                                                                    etc.)</label>
                                                                <div id="violenceVictim8Spinner" style="display: none;">
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceVictimCheck09"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceVictimCheck09">Political party
                                                                    office or property (specify party)</label>
                                                                <div id="violenceVictim9Spinner" style="display: none;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceVictimCheck10"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceVictimCheck10">State (Gov't) office
                                                                    or property </label>
                                                                <div id="violenceVictim10Spinner"
                                                                    style="display: none;"></div>

                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceVictimCheck11"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceVictimCheck11">Private
                                                                    property/building</label>
                                                                <div id="violenceVictim11Spinner"
                                                                    style="display: none;"></div>

                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceVictimCheck12"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceVictimCheck12">Others</label>
                                                                <div id="violenceVictim12Spinner"
                                                                    style="display: none;"></div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--end row-->
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-->
                                        </div>
                                        <!--end row-->
                                    </fieldset>
                                    <!--end fieldset-->

                                    <h3>Page 4</h3>
                                    <fieldset>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-md-3 my-2 control-label">What kind of violence
                                                        happened ?</label>
                                                    <div class="col-md-9">
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceKindCheck01"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceKindCheck01">Murder</label>
                                                                <div id="violenceKind1Spinner" style="display: none;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceKindCheck02"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceKindCheck02">Attempted Murder</label>
                                                                <div id="violenceKind2Spinner" style="display: none;">
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceKindCheck03"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceKindCheck03">Physical harm /
                                                                    torture</label>
                                                                <div id="violenceKind3Spinner" style="display: none;">
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceKindCheck04"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceKindCheck04">Sexual assault</label>
                                                                <div id="violenceKind4Spinner" style="display: none;">
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceKindCheck05"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceKindCheck05">Intimidation /
                                                                    psychological abuse</label>
                                                                <div id="violenceKind5Spinner" style="display: none;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceKindCheck06"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceKindCheck06">Kidnapping</label>
                                                                <div id="violenceKind6Spinner" style="display: none;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceKindCheck07"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceKindCheck07">Group Clash (group fight,
                                                                    also check physical harm)</label>
                                                                <div id="violenceKind7Spinner" style="display: none;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceKindCheck08"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceKindCheck08">Politically motivated
                                                                    arrest or detention</label>
                                                                <div id="violenceKind8Spinner" style="display: none;">
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceKindCheck09"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceKindCheck09">Destruction of
                                                                    property</label>
                                                                <div id="violenceKind9Spinner" style="display: none;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceKindCheck10"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceKindCheck10">Theft</label>
                                                                <div id="violenceKind10Spinner" style="display: none;">
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceKindCheck11"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceKindCheck11">Other</label>
                                                                <div id="violenceKind11Spinner" style="display: none;">
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--end row-->
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-->
                                        </div>
                                        <!--end row-->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-md-3 my-2 control-label">What kind of weapon was
                                                        used ?</label>
                                                    <div class="col-md-9">
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceWeaponCheck01"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceWeaponCheck01">Fists/physical
                                                                    means</label>
                                                                <div id="violenceWeapon1Spinner" style="display: none;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceWeaponCheck02"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceWeaponCheck02">Gun/firearm</label>
                                                                <div id="violenceWeapon2Spinner" style="display: none;">
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceWeaponCheck03"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceWeaponCheck03">Knives/stabbing</label>
                                                                <div id="violenceWeapon3Spinner" style="display: none;">
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceWeaponCheck04"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceWeaponCheck04">Stones/throwing
                                                                    objects</label>
                                                                <div id="violenceWeapon4Spinner" style="display: none;">
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceWeaponCheck05"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceWeaponCheck05">Arson</label>
                                                                <div id="violenceWeapon5Spinner" style="display: none;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceWeaponCheck06"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceWeaponCheck06">Bomb/explosives</label>
                                                                <div id="violenceWeapon6Spinner" style="display: none;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceWeaponCheck07"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceWeaponCheck07">No weapon</label>
                                                                <div id="violenceWeapon7Spinner" style="display: none;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceWeaponCheck08"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceWeaponCheck08">Unable to
                                                                    determine</label>
                                                                <div id="violenceWeapon8Spinner" style="display: none;">
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceWeaponCheck09"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceWeaponCheck09">Others</label>
                                                                <div id="violenceWeapon9Spinner" style="display: none;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--end row-->
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-->
                                        </div>
                                        <!--end row-->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-md-3 my-2 control-label">What was the impact of
                                                        the violence ?</label>
                                                    <div class="col-md-9">
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceImpactCheck01"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceImpactCheck01">Complaint filed with
                                                                    Electoral Commission</label>
                                                                <div id="violenceImpact1Spinner" style="display: none;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceImpactCheck02"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceImpactCheck02">Civic education event
                                                                    disrupted</label>
                                                                <div id="violenceImpact2Spinner" style="display: none;">
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceImpactCheck03"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceImpactCheck03">Campaign activity
                                                                    disrupted</label>
                                                                <div id="violenceImpact3Spinner" style="display: none;">
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceImpactCheck04"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceImpactCheck04">Voting disrupted/voters
                                                                    dispersed or left area</label>
                                                                <div id="violenceImpact4Spinner" style="display: none;">
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceImpactCheck05"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceImpactCheck05">Disrupted vote
                                                                    count</label>
                                                                <div id="violenceImpact5Spinner" style="display: none;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceImpactCheck06"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceImpactCheck06">Transportation
                                                                    disrupted</label>
                                                                <div id="violenceImpact6Spinner" style="display: none;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceImpactCheck07"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceImpactCheck07">Economic/financial
                                                                    loss</label>
                                                                <div id="violenceImpact7Spinner" style="display: none;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceImpactCheck08"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceImpactCheck08">Cancelled
                                                                    election</label>
                                                                <div id="violenceImpact8Spinner" style="display: none;">
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceImpactCheck09"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceImpactCheck09">Postponed
                                                                    election</label>
                                                                <div id="violenceImpact9Spinner" style="display: none;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceImpactCheck10"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceImpactCheck10">Re-run election</label>
                                                                <div id="violenceImpact10Spinner"
                                                                    style="display: none;">
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceImpactCheck11"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceImpactCheck11">Other</label>
                                                                <div id="violenceImpact11Spinner"
                                                                    style="display: none;">
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="checkbox my-2">
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input"
                                                                    id="violenceImpactCheck12"
                                                                    data-parsley-multiple="groups"
                                                                    data-parsley-mincheck="2">
                                                                <label class="custom-control-label"
                                                                    for="violenceImpactCheck12">Unable to
                                                                    determine</label>
                                                                <div id="violenceImpact12Spinner"
                                                                    style="display: none;">
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--end row-->
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-->
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label for="afterIncidenceSpinners"
                                                        class="col-lg-3 col-form-label">Did the government, security,
                                                        parties, or community do anything after this incident ?</label>
                                                    <div class="col-lg-9">
                                                        <select id="afterIncidenceSpinners"
                                                            name="afterIncidenceSpinners" class="form-control">
                                                            <option value="" selected="true" disabled="disabled">
                                                                --Please Select--</option>
                                                            <option value="yes">Yes</option>
                                                            <option value="no">No</option>

                                                        </select>
                                                    </div>
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-->
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label for="txtAfterIncidenceDesc"
                                                        class="col-lg-3 col-form-label">What did they do? For example,
                                                        did the police come?</label>
                                                    <div class="col-lg-9">
                                                        <textarea id="txtAfterIncidenceDesc"
                                                            name="txtAfterIncidenceDesc" rows="5"
                                                            class="form-control"></textarea>
                                                    </div>
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-->
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label for="txtMoreReport" class="col-lg-3 col-form-label">Anything
                                                        else you need to add or have questions about how to
                                                        report?</label>
                                                    <div class="col-lg-9">
                                                        <textarea id="txtMoreReport" name="txtMoreReport" rows="5"
                                                            class="form-control"></textarea>
                                                    </div>
                                                </div>
                                                <!--end form-group-->
                                            </div>
                                            <!--end col-->
                                            <div class="col-md-12">

                                                <div class="form-group row">

                                                    <div class="col-lg-6">
                                                        <label for="gallery_div" class="col-form-label">Media
                                                            Asset</label>
                                                        <input type="file" class="input__input" multiple
                                                            id="media_asset"
                                                            accept="image/gif, image/jpeg, image/png" onchange="readURL(this);" />
                                                        <div id="image_preview"></div>

                                                    </div>
                                                    <div class="col-lg-6" style="margin-top: 20px;margin-bottom:20px;"
                                                        id="gallery_div">
                                                    </div>
                                                </div>

                                            </div>
                                            <!--end row-->
                                    </fieldset>
                                    <!--end fieldset-->
                                </form>
                                <!--end form-->
                            </div>
                            <!--end card-body-->
                        </div>
                        <!--end card-->
                    </div>
                    <!--end col-->
                </div>
                <!--end row-->

            </div><!-- container -->

            <?php include_once '../customs/app_footer.php'?>
            <!--end footer-->
        </div>
        <!-- end page content -->
    </div>
    <!-- end page-wrapper -->

    <!-- jQuery  -->
    <?php include '../customs/app_js_files.php'?>

    <!-- Custom Charts Functions js -->
    <script src="../assets/js/reportsController/addReport.js"></script>

    <!-- Custom Charts Functions js -->
    <script src="../assets/js/reportsController/editReport.js"></script>

</body>

</html>