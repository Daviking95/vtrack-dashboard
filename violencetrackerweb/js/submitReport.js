// (function($){

// function initialize() {
//     var input = document.getElementById('location_autosuggest');
//     new google.maps.places.Autocomplete(input);
//   }

//   google.maps.event.addDomListener(window, 'load', initialize);
let form_type = "";
let report_title = "";
let description = "";
let verified_by = "";
let date_of_report = "";
let time_of_report = "";
let location_autosuggest = "";
let news_source = "";
let video_link = "";
var total_file = "";

var storageRef = "";

let sourceWitnessList = [];
let mediaSourceList = [];
let violenceInitiatorList = [];
let violenceVictimList = [];
let violenceKindList = [];
let violenceWeaponList = [];
let violenceImpactList = [];
let mediaAsset = [];

let incidentLocationList, violenceInitiatorPeopleAndGenderList, violenceVictimPeopleAndGenderInvolvedList, afterIncidentList;
let StepOneReportConstants = {};

function readURL(input) {
    total_file = input.files.length;
    for (var i = 0; i < total_file; i++) {
        $('#image_preview').append("<a data-fancybox='gallery' href='" + URL.createObjectURL(event.target.files[i]) + "'><img class='media-picture__img' style='width: 100px;float: left;margin: 10px;' src='" + URL.createObjectURL(event.target.files[i]) + "'></a>");
    }

    $('#image_preview').show();
}

$("#preview").click(function () {

    form_type = $("#form_type").val();
    report_title = $("#report_title").val();
    description = document.getElementById("incident_desc").value; // $("#incident_desc").val();
    verified_by = $("#verified_by").val();
    date_of_report = $("#date_of_report").val();
    time_of_report = $("#time_of_report").val();
    location_autosuggest = $("#location_autosuggest").val();
    news_source = $("#news_source").val();
    video_link = $("#video_link").val();

    // sendPhotosToFirestore();

    // console.log(description);

    confirmSubmission();

});

function clearAllFormerEntries() {
    sourceWitnessList = [];
    mediaSourceList = [];
    violenceInitiatorList = [];
    violenceVictimList = [];
    violenceKindList = [];
    violenceWeaponList = [];
    violenceImpactList = [];

    incidentLocationList = {};
    violenceInitiatorPeopleAndGenderList = {};
    violenceVictimPeopleAndGenderInvolvedList = {};
    afterIncidentList = {};
    StepOneReportConstants = {};
}

function sendPhotosToFirestore() {
    if (report_title === "") return alert("Report Title cannot be empty");

    document.getElementById("preview").style.display = "none";
    document.getElementById("loader").style.display = "block";

    var mediaAssetList = [];

    storageRef = firebase.storage().ref().child(`violenceTrackMediaForm/mediaFromWeb/formTitle-${report_title}${getCurrentDateTime()}`);
    var file = document.getElementById("media_asset").files;

    for (let index = 0; index < file.length; index++) {
        const element = file[index];

        mediaAssetList.push(getAssetUrl(element));

    }

    Promise.all(mediaAssetList).then(function (values) {

        mediaAsset = values;

        console.log(mediaAsset);

        finalStage();

    });

}

async function getAssetUrl(element) {
    let promise = new Promise((resolve, reject) => {

        var thisRef = storageRef.child(element.name);

        thisRef.put(element).then(snapshot => {
                return snapshot.ref.getDownloadURL();
            })

            .then(downloadURL => {
                console.log(`Successfully uploaded file and got download link - ${downloadURL}`);

                resolve(downloadURL);
                // return downloadURL;
            })

            .catch(error => {
                console.log(`Failed to upload file and get link - ${error}`);
            });
    });

    let result = await promise;

    return result;
}

function finalStage() {

    clearAllFormerEntries();

    var d = new Date(); // for now
    var timeOfReport = d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();

    StepOneReportConstants = {
        "monitorName": ""
    };
    StepOneReportConstants["monitorCode"] = "";
    StepOneReportConstants["dateOfReport"] = getCurrentDate();
    StepOneReportConstants["electionType"] = "";
    StepOneReportConstants["dateOfIncidence"] = date_of_report;
    StepOneReportConstants["timeOfIncidence"] = time_of_report;
    StepOneReportConstants["geoPoliticalZone"] = "";
    StepOneReportConstants["city"] = "";
    StepOneReportConstants["state"] = "";
    StepOneReportConstants["incidentTitle"] = report_title;
    StepOneReportConstants["incidentDesc"] = description;
    StepOneReportConstants["formCode"] = "";
    StepOneReportConstants["timeOfReport"] = timeOfReport;
    StepOneReportConstants["firstApproval"] = false;
    StepOneReportConstants["secondApproval"] = false;
    StepOneReportConstants["formSubmissionPoint"] = "web";
    StepOneReportConstants["locationOfReport"] = location_autosuggest;
    StepOneReportConstants["formType"] = form_type;

    isSourceWitnessChecked("news_source", 10);

    isMediaSourceChecked("video_link", 6);

    incidentLocationList = {
        "locationOfIncident": location_autosuggest,
        "specifylocationOfIncident": ""
    };

    violenceInitiatorPeopleAndGenderList = {
        "violenceInitiatorPeopleOfIncident": "",
        "violenceInitiatorGenderOfIncident": "",
    };

    isViolenceInitiatorChecked("perpertrator_type_boxes_1", 1);
    isViolenceInitiatorChecked("perpertrator_type_boxes_2", 2);
    isViolenceInitiatorChecked("perpertrator_type_boxes_3", 3);
    isViolenceInitiatorChecked("perpertrator_type_boxes_4", 4);
    isViolenceInitiatorChecked("perpertrator_type_boxes_5", 5);
    isViolenceInitiatorChecked("perpertrator_type_boxes_6", 6);
    isViolenceInitiatorChecked("perpertrator_type_boxes_7", 7);
    isViolenceInitiatorChecked("perpertrator_type_boxes_8", 8);
    isViolenceInitiatorChecked("perpertrator_type_boxes_9", 9);
    isViolenceInitiatorChecked("perpertrator_type_boxes_10", 10);
    isViolenceInitiatorChecked("perpertrator_type_boxes_11", 11);
    isViolenceInitiatorChecked("perpertrator_type_boxes_12", 12);

    violenceVictimPeopleAndGenderInvolvedList = {
        "violenceVictimTypeOfIncident": "",
        "violenceVictimsPeopleOfIncident": "",
        "violenceVictimsGenderOfIncident": "",
    };

    // One submitting
    isViolenceVictimChecked("victim_types_boxes_1", 1);
    isViolenceVictimChecked("victim_types_boxes_2", 2);
    isViolenceVictimChecked("victim_types_boxes_3", 3);
    isViolenceVictimChecked("victim_types_boxes_4", 4);
    isViolenceVictimChecked("victim_types_boxes_5", 5);
    isViolenceVictimChecked("victim_types_boxes_6", 6);
    isViolenceVictimChecked("victim_types_boxes_7", 7);
    isViolenceVictimChecked("victim_types_boxes_8", 8);
    isViolenceVictimChecked("victim_types_boxes_9", 9);
    isViolenceVictimChecked("victim_types_boxes_10", 10);
    isViolenceVictimChecked("victim_types_boxes_11", 11);
    isViolenceVictimChecked("victim_types_boxes_12", 12);

    // One submitting
    isViolenceKindChecked("violence_type_boxes_1", 1);
    isViolenceKindChecked("violence_type_boxes_2", 2);
    isViolenceKindChecked("violence_type_boxes_3", 3);
    isViolenceKindChecked("violence_type_boxes_4", 4);
    isViolenceKindChecked("violence_type_boxes_5", 5);
    isViolenceKindChecked("violence_type_boxes_6", 6);
    isViolenceKindChecked("violence_type_boxes_7", 7);
    isViolenceKindChecked("violence_type_boxes_8", 8);
    isViolenceKindChecked("violence_type_boxes_9", 9);
    isViolenceKindChecked("violence_type_boxes_10", 10);
    isViolenceKindChecked("violence_type_boxes_11", 11);

    isViolenceWeaponChecked("nonviolent_election_box_1", 1);
    isViolenceWeaponChecked("nonviolent_election_box_2", 2);
    isViolenceWeaponChecked("nonviolent_election_box_3", 3);
    isViolenceWeaponChecked("nonviolent_election_box_4", 4);
    isViolenceWeaponChecked("nonviolent_election_box_5", 5);
    isViolenceWeaponChecked("nonviolent_election_box_6", 6);
    isViolenceWeaponChecked("nonviolent_election_box_7", 7);
    isViolenceWeaponChecked("nonviolent_election_box_8", 8);
    isViolenceWeaponChecked("nonviolent_election_box_9", 9);

    // One submitting
    isViolenceImpactChecked("violence_impact_boxes_1", 1);
    isViolenceImpactChecked("violence_impact_boxes_2", 2);
    isViolenceImpactChecked("violence_impact_boxes_3", 3);
    isViolenceImpactChecked("violence_impact_boxes_4", 4);
    isViolenceImpactChecked("violence_impact_boxes_5", 5);
    isViolenceImpactChecked("violence_impact_boxes_6", 6);
    isViolenceImpactChecked("violence_impact_boxes_7", 7);
    isViolenceImpactChecked("violence_impact_boxes_8", 8);
    isViolenceImpactChecked("violence_impact_boxes_9", 9);
    isViolenceImpactChecked("violence_impact_boxes_10", 10);
    isViolenceImpactChecked("violence_impact_boxes_11", 11);
    isViolenceImpactChecked("violence_impact_boxes_12", 12);

    afterIncidentList = {
        "afterIncidentOption": "",
        "afterIncidentDetails": ""
    }

    moreReportDetails = {
        "moreReportDetailsOption": ""
    }

    StepOneReportConstants["incidentSource"] = sourceWitnessList;

    StepOneReportConstants["mediaSource"] = mediaSourceList;

    StepOneReportConstants["incidentLocation"] = incidentLocationList;

    StepOneReportConstants["violenceInitiatorPeopleAndGenderInvolved"] = violenceInitiatorPeopleAndGenderList;

    StepOneReportConstants["violenceInitiator"] = violenceInitiatorList;

    StepOneReportConstants["violenceVictimPeopleAndGenderInvolved"] = violenceVictimPeopleAndGenderInvolvedList;

    StepOneReportConstants["incidentViolenceVictims"] = violenceVictimList;

    StepOneReportConstants["incidentViolence"] = violenceKindList;

    StepOneReportConstants["incidentWeapon"] = violenceWeaponList;

    StepOneReportConstants["incidentImpact"] = violenceImpactList;

    StepOneReportConstants["afterIncident"] = afterIncidentList;

    StepOneReportConstants["moreReportDetails"] = moreReportDetails;

    StepOneReportConstants["mediaAsset"] = mediaAsset;

    // console.log(StepOneReportConstants);

    console.log(JSON.stringify(StepOneReportConstants));

    saveDataToDb(StepOneReportConstants);

}

function confirmSubmission() {
    $.confirm({
        title: 'Confirm!',
        content: 'Are you sure about the information provided ?',
        buttons: {
            confirm: function () {
                // saveDataToDb(StepOneReportConstants);

                sendPhotosToFirestore();

                // $.alert('Confirmed!');
            },
            cancel: function () {
                $.alert('Canceled!');
            },
        }
    });
}

function saveDataToDb(StepOneReportConstants) {

    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    // myHeaders.append("x-auth-token", getToken);

    var raw = JSON.stringify(StepOneReportConstants);

    var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: raw,
        redirect: 'follow'
    };

    fetch("https://peaceful-atoll-23751.herokuapp.com/" + "just-clock-285910.uc.r.appspot.com/api/form/add", requestOptions)
        .then(function (response) {
            if (response.status === 403) {
                alert("Token expired. Login");
                document.getElementById('loader').style.display = "none";
                document.getElementById('preview').style.display = "block";
                window.location = "../index.php";
            }

            if (!response.ok) {

                response.json().then(function (data) {
                    alert(data.message);
                    document.getElementById('loader').style.display = "none";
                    document.getElementById('preview').style.display = "block";
                });
                throw new Error("HTTP status " + response.status);
            }

            return response.json();
        })
        .then(function (result) {

            if (result.statusCode !== 200) {
                alert(result.message);
                document.getElementById('loader').style.display = "none";
                document.getElementById('preview').style.display = "block";
                return;

            }

            alert(result.message);
            location.replace("submitReport.html");
            document.getElementById('loader').style.display = "none";
            document.getElementById('preview').style.display = "block";


        })
        .catch(error => console.log('error', error));
}


function isSourceWitnessChecked(id, position) {

    let e = document.getElementById(id);

    if (news_source !== "") {

        var data = {
            "incidentSourceOption": "Others",
            "incidentSourceOptionPosition": position,
        };

        data["didSourceSeeIncident"] = news_source;

        sourceWitnessList.push(data);
    }
}

function isMediaSourceChecked(id, position) {

    // let e = document.getElementById(id);

    if (video_link !== "") {
        var data = {
            "mediaSourceOption": "Others",
            "mediaSourceOptionPosition": position,

        };

        data["nameOne"] = video_link;
        data["nameTwo"] = "";
        data["nameThree"] = "";

        mediaSourceList.push(data);
    }
}

function isViolenceInitiatorChecked(id, position) {

    if (document.getElementById(id).checked) {
        var data = {
            "violenceInitiatorType": document.getElementById(id).nextElementSibling.innerText,
            "violenceInitiatorPosition": position
        };
        data["violenceInitiatorDesc"] = "";

        violenceInitiatorList.push(data);
    }
}

function isViolenceVictimChecked(id, position) {
    if (document.getElementById(id).checked) {
        var data = {
            "violenceVictimsType": document.getElementById(id).nextElementSibling.innerText,
            "incidentViolenceVictimsPosition": position,
        };
        data["violenceVictimsDesc"] = "";

        violenceVictimList.push(data);
    }
}

function isViolenceKindChecked(id, position) {

    if (document.getElementById(id).checked) {
        var data = {
            "violenceType": document.getElementById(id).nextElementSibling.innerText,
            "incidentViolencePosition": position,
        };
        data["violenceDesc"] = "";

        violenceKindList.push(data);
    }
}

function isViolenceWeaponChecked(id, position) {

    if (document.getElementById(id).checked) {
        var data = {
            "weaponType": document.getElementById(id).nextElementSibling.innerText,
            "incidentWeaponPosition": position,

        };
        data["weaponDesc"] = ""

        violenceWeaponList.push(data);
    }
}

function isViolenceImpactChecked(id, position) {

    if (document.getElementById(id).checked) {
        var data = {
            "impactType": document.getElementById(id).nextElementSibling.innerText,
            "incidentImpactPosition": position,
        };
        data["impactDesc"] = "";

        violenceImpactList.push(data);
    }
}

function getCurrentDateTime() {
    var today = new Date();
    var date = today.getFullYear() + (today.getMonth() + 1) + today.getDate();
    var time = today.getHours() + today.getMinutes() + today.getSeconds() + today.getMilliseconds();
    var dateTime = date + time;
    return dateTime;
}

function getCurrentDate() {
    var today = new Date();
    var dd = today.getDate();

    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }

    if (mm < 10) {
        mm = '0' + mm;
    }
    today = dd + '-' + mm + '-' + yyyy;
    return today;
}

function initAutocomplete() {
    const map = new google.maps.Map(document.getElementById("map"), {
        center: {
            lat: 9.072264,
            lng: 7.491302
        },
        zoom: 13,
        mapTypeId: "roadmap"
    });
    // Create the search box and link it to the UI element.
    const input = document.getElementById("location_autosuggest");
    const searchBox = new google.maps.places.SearchBox(input);
    // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    // Bias the SearchBox results towards current map's viewport.
    map.addListener("bounds_changed", () => {
        searchBox.setBounds(map.getBounds());
    });
    let markers = [];
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener("places_changed", () => {
        const places = searchBox.getPlaces();

        if (places.length == 0) {
            return;
        }
        // Clear out the old markers.
        markers.forEach(marker => {
            marker.setMap(null);
        });
        markers = [];
        // For each place, get the icon, name and location.
        const bounds = new google.maps.LatLngBounds();
        places.forEach(place => {
            if (!place.geometry) {
                console.log("Returned place contains no geometry");
                return;
            }
            const icon = {
                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(25, 25)
            };
            // Create a marker for each place.
            markers.push(
                new google.maps.Marker({
                    map,
                    icon,
                    title: place.name,
                    position: place.geometry.location
                })
            );

            if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }
        });
        map.fitBounds(bounds);
    });
}


// })(jQuery);