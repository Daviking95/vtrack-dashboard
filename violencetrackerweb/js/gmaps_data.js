(function ($) {

    let statesWithViolenceArray = [];

    let sBox = "";
    let filter_state = "";
    let filter_df = "";
    let filter_dt = "";
    let filter_vt = "";
    let filter_year = "";

    function getAllForms(isSearch) {

        if (isSearch) statesWithViolenceArray = [];

        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        // myHeaders.append("x-auth-token", getToken);

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        fetch("https://peaceful-atoll-23751.herokuapp.com/just-clock-285910.uc.r.appspot.com/api/form/all", requestOptions)
            .then(function (response) {
                if (response.status === 403) {
                    alert("Token expired. Login");
                    window.location.href = "index.php";
                }
                if (!response.ok) {
                    alert("Cannot login. Ensure your data is correct and you have internet connection");
                    throw new Error("HTTP status " + response.status);
                }

                return response.json();
            })
            .then(function (result) {

                statesWithViolenceArray = [];

                result.details.forEach(element => {

                    // console.log(element);

                    // console.log(`${dateFormat(element.dateOfIncidence)}`);
                    if (element.firstApproval === true && element.secondApproval === true) {

                        if (isSearch) {

                            // console.log((dateFormat(filter_df) !== "" && dateFormat(filter_df) === dateFormat(element.dateOfIncidence)));

                            // console.log((sBox !== null && sBox.toLowerCase() === element.incidentTitle.toLowerCase()) || (filter_vt !== null && filter_vt.toLowerCase() === element.electionType.toLowerCase()) && (filter_state !== null && filter_state.toLowerCase() === element.state.toLowerCase()));

                            if ((sBox !== null && sBox.toLowerCase() === element.incidentTitle.toLowerCase()) || (filter_vt !== null && filter_vt.toLowerCase() === element.electionType.toLowerCase()) && (filter_state !== null && filter_state.toLowerCase() === element.state.toLowerCase()) || (dateFormat(filter_df) !== "" && dateFormat(filter_df) === dateFormat(element.dateOfIncidence))) {
                                statesWithViolenceArray.push(element);
                            }

                            // if (dateFormat(filter_df) !== "" && dateFormat(filter_df) === dateFormat(element.dateOfIncidence)) {
                            //     statesWithViolenceArray.push(element);

                            // }

                            // if (dateFormat(filter_year) !== "") {
                            //     current_date = new Date(dateFormat(element.dateOfIncidence)).getFullYear();

                            //     if (filter_year == current_date) {
                            //         statesWithViolenceArray.push(element);
                            //     }

                            // }

                            // console.log(`What's left ${statesWithViolenceArray.length}`);

                        } else {
                            statesWithViolenceArray.push(element);
                        }
                    }
                });

                // console.log(JSON.stringify(statesWithViolenceArray));
                // console.log(`What's left ${statesWithViolenceArray.length}`);

                showMap(statesWithViolenceArray);

            })
            .catch(error => console.log('error', error));

    }

    async function getLatLong(state) {

        // console.log(state);
        var stateObject = {};

        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        return new Promise((resolve, reject) => {
            fetch(`https://peaceful-atoll-23751.herokuapp.com/https://maps.googleapis.com/maps/api/geocode/json?address=${state}&key=AIzaSyDyE7hN2pWfF64NDqyXKz3Sm_lql5obcOs`, requestOptions)
                .then(function (response) {

                    // response.json().then(function(data) {
                    //     console.log(`State ${state}`);
                    //     console.log(data);
                    // });
                    return response.json();
                })
                .then(function (result) {

                    // console.log(`${result.results[0].geometry.location.lat} ${result.results[0].geometry.location.lng}`);

                    var latitude = result.results[0].geometry.location.lat;
                    var longitude = result.results[0].geometry.location.lng;

                    stateObject = {
                        state: state,
                        lat: latitude,
                        long: longitude
                    };

                    // console.log(stateObject);
                    resolve(stateObject);

                })
                .catch(error => console.log('error', error));
        })

    }

    async function showMap(statesWithViolence) {

        // console.log(statesWithViolence);

        var map_parameters = {
            center: {
                lat: 9.072264,
                lng: 7.491302
            },
            zoom: 6,
            mapTypeId: google.maps.MapTypeId.HYBRID

            // mapTypeId: 'satellite'
        };

        var map = new google.maps.Map(document.getElementById('map1'), map_parameters);

        // map.setMapTypeId('hybrid');

        var position, marker, info;

        let contentString = "";

        for (let index = 0; index < statesWithViolence.length; index++) {

            const element = statesWithViolence[index];

            const markerColor = setColorForMarker(element.incidentViolence);

            // console.log(element);

            var circle = {
                path: google.maps.SymbolPath.CIRCLE,
                fillColor: markerColor,
                fillOpacity: .9,
                scale: 5.5,
                strokeColor: 'white',
                strokeWeight: 1
            };

            // console.log(`Fill color ${circle['fillColor']}`);

            getLatLong(element.city).then(data => {

                circle['fillColor'] = setColorForMarker(element.incidentViolence);

                // console.log(`Circle ${JSON.stringify(circle)}`);
                position = {
                    position: {
                        lat: data.lat,
                        lng: data.long
                    },
                    map: map,
                    icon: circle,
                    title: data.state
                };

                marker = new google.maps.Marker(position);

                info = new google.maps.InfoWindow();
                marker.addListener('click', function () {
                    // alert(element.incidentTitle);
                    contentString =
                        '<div id="content" style="height: 550px; width: 350px; background-color: #333333; color: #fff; padding: 20px;">' +
                        '<div id="siteNotice">' +
                        "</div>" +
                        '<h1 id="firstHeading" >' + jsUcfirst(element.state) + ' STATE</h1><br /><hr style="width: 80%; padding-left: 50px;"><b style="line-height: 2;">' + element.incidentTitle + '</b><hr style="width: 80%; padding-left: 50px;"><br />' +
                        '<div id="bodyContent">' + "<br /> <b>" + element.incidentDesc + "</b><br /><br /><br /> <i class='fa fa-map-marker' aria-hidden='true'></i> " + jsUcfirst(element.locationOfReport) + "<div style='line-height: 3;'><i class='fa fa-clock-o' aria-hidden='true'></i> " + element.timeOfReport + " " + dateFormat(element.dateAdded) + '</div></div>'
                    "</div>";

                    info.setContent(contentString);
                    info.open(map, this);

                });
            });

        }

        const dateFormat = (s) => {
            // console.log(s);

            var now = new Date(s);

            var day = ("0" + now.getDate()).slice(-2);
            var month = ("0" + (now.getMonth() + 1)).slice(-2);

            var newDate = now.getFullYear() + "-" + (month) + "-" + (day);

            // console.log(newDate);

            return newDate;
        }

        function marker_clicked(contentString) {
            info.setContent(contentString);
            info.open(map, this);
        }

        function setColorForMarker(incidentViolence) {
            let setColor = "white";

            // console.log(`Inci ${JSON.stringify(incidentViolence)}`);

            for (const key in incidentViolence) {
                if (incidentViolence.hasOwnProperty(key)) {
                    const element = incidentViolence[key];
                    // console.log(`${element.incidentViolencePosition}  ${element.violenceType}`);
                    // console.log(Object.values(element).indexOf("violenceType"));

                    switch (element.incidentViolencePosition) {
                        case 1:
                            setColor = "#EB1212";
                            break;
                        case 2:
                            setColor = "#A8413E";
                            break;
                        case 3:
                            setColor = "#8F1A07";
                            break;
                        case 4:
                            setColor = "#B0593A";
                            break;
                        case 5:
                            setColor = "#DB6810";
                            break;
                        case 6:
                            setColor = "#E6C90B";
                            break;
                        case 7:
                            setColor = "#3605FA";
                            break;
                        case 8:
                            setColor = "#B0593A";
                            break;
                        case 9:
                            setColor = "#88AFD1";
                            break;
                        case 10:
                            setColor = "#B0593A";
                            break;
                        case 11:
                            setColor = "#756F6F";
                            break;

                        default:
                            setColor = "white";
                            break;
                    }

                }
            }

            // console.log(`SetColor ${setColor}`);
            return setColor;
        }

    }

    function jsUcfirst(string) {
        if(string === null) return "";
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    function init() {
        getAllForms(false);

        $(".search_filter").click(function () {
            sBox = $(".filter_search_box").val();
            filter_state = $(".filter_state").val();
            filter_df = $(".filter_df").val();
            filter_dt = $(".filter_dt").val();
            filter_vt = $(".filter_vt").val();

            // console.log(`"I am listening" ${sBox} ${filter_state} ${dateFormat(filter_df)} ${dateFormat(filter_dt)} ${filter_vt}`);

            getAllForms(true);

        });

        // Active Year maker toggle ===================================================================
        var $yearMakerLabel = $(".year-marker__label");
        var $yearMakerActive = $(".year-marker__active");

        // main_date = new Date().getFullYear();

        // console.log((document.querySelector(".year-marker__label").innerHTML === main_date));

        // $yearMakerLabel.innerHTML = main_date;
        // console.log($yearMakerLabel.innerHTML);
        // document.querySelector(".year-marker__label").append($yearMakerActive);


        $yearMakerLabel.click(function () {

            var par = $(this).parent();
            $(this).siblings(".year-marker__active").remove();
            par.append($yearMakerActive);

            // console.log(`Year : ${document.querySelector(".year-marker__label").innerHTML}`);
            filter_year = document.querySelector(".year-marker__active").previousSibling;

            filter_year = filter_year.previousSibling.innerHTML;

            if (filter_year == undefined) filter_year = "2020";
            // console.log(filter_year);
            getAllForms(true);

        });
    }

    const dateFormat = (s) => {

        var newdate = s.includes("/") ? s.split("/").reverse().join("-") : s;

        var date = new Date(newdate);

        return date.toDateString();
    }

    init();

})(jQuery)