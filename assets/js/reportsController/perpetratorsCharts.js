/**
 * Theme: VTrack Reporting Dasboard - A concept of KDI
 * Author: VTrack
 * Chart Js
 */


(function ($) {

    var getToken = localStorage.getItem("x-auth-token");

    pepGenderArray1 = [];
    pepGenderArray2 = [];
    pepGenderArray3 = [];
    pepGenderArray4 = [];

    perpetratorArrayItem1 = [];
    perpetratorArrayItem2 = [];
    perpetratorArrayItem3 = [];
    perpetratorArrayItem4 = [];
    perpetratorArrayItem5 = [];
    perpetratorArrayItem6 = [];
    perpetratorArrayItem7 = [];
    perpetratorArrayItem8 = [];
    perpetratorArrayItem9 = [];
    perpetratorArrayItem10 = [];
    perpetratorArrayItem11 = [];
    perpetratorArrayItem12 = [];

    "use strict";

    var ChartJs = function () {};

    ChartJs.prototype.respChart = function (selector, type, data, options) {
            // get selector by context
            var ctx = selector.get(0).getContext("2d");
            // pointing parent container to make chart js inherit its width
            var container = $(selector).parent();

            // enable resizing matter
            $(window).resize(generateChart);

            // this function produce the responsive Chart JS
            function generateChart() {
                // make chart width fit with its container
                var ww = selector.attr('width', $(container).width());
                switch (type) {
                    case 'Bar':
                        new Chart(ctx, {
                            type: 'bar',
                            data: data,
                            options: options
                        });
                        break;
                    case 'PolarArea':
                        new Chart(ctx, {
                            data: data,
                            type: 'polarArea',
                            options: options
                        });
                        break;
                }
                // Initiate new chart or Redraw

            };
            // run function - render chart at first load
            generateChart();
        },

        //init
        ChartJs.prototype.init = function () {

            // "Political party leader or supporter, Candidate or candidate supporter", "Government / State actor", "Party agent", "Election observer / Monitor", "Election Worker (POs, APOs, Returning Officers)", "Voter(s)/ Public", "Campaign Material (Posters, Billboards)", "Election office, property, material (like ballots, trucks, warehouse, etc.)", "Political party office or property (specify party)", "State (Gov't) office or property", "Private property/building", "Other"

            //barchart
            var barChart = {
                labels: ["Political party leader or supporter, Candidate or candidate supporter", "Government / State actor", "Party agent", "Election observer / Monitor", "Election Worker (POs, APOs, Returning Officers)", "Voter(s)/ Public", "Campaign Material (Posters, Billboards)", "Election office, property, material (like ballots, trucks, warehouse, etc.)", "Political party office or property (specify party)", "State (Gov't) office or property", "Private property/building", "Other"],
                // labels: ["Political party leader or supporter, Candidate or candidate supporter", "Government / State actor"],
                datasets: [{
                    label: "Who Did The Violence",
                    backgroundColor: "FFA500",
                    borderColor: "#FFA500",
                    borderWidth: 2,
                    barPercentage: 0.3,
                    categoryPercentage: 0.5,
                    hoverBackgroundColor: "rgba(74, 199, 236, 0.7)",
                    hoverBorderColor: "#FFA500",
                    // data: [1, 2]
                    data: [perpetratorArrayItem1.length, perpetratorArrayItem2.length, perpetratorArrayItem3.length, perpetratorArrayItem4.length, perpetratorArrayItem5.length, perpetratorArrayItem6.length, perpetratorArrayItem7.length, perpetratorArrayItem8.length, perpetratorArrayItem9.length, perpetratorArrayItem10.length, perpetratorArrayItem11.length, perpetratorArrayItem12.length]
                }]
            };
            var barOpts = {
                responsive: true,
                legend: {
                    labels: {
                        fontColor: '#FFA500'
                    }
                },
                scales: {
                    xAxes: [{
                        barPercentage: 0.8,
                        categoryPercentage: 0.4,
                        display: false,
                        gridLines: {
                            color: "#FFA500"
                        },
                        ticks: {
                            fontColor: '#a4abc5'
                        }
                    }],
                    yAxes: [{
                        gridLines: {
                            color: "#eaf0f7",
                        },
                        ticks: {
                            fontColor: '#a4abc5'
                        }
                    }]
                }

            };
            this.respChart($("#bar"), 'Bar', barChart, barOpts);

            // console.log(pepGenderArray1.length);

            //Polar area chart
            var polarChart = {
                datasets: [{
                    data: [
                        pepGenderArray1.length,
                        pepGenderArray2.length,
                        pepGenderArray3.length,
                        pepGenderArray4.length
                    ],
                    backgroundColor: [
                        "#4d79f6",
                        "#ff5da0",
                        "#4ac7ec",
                        "#e0e7fd",
                    ],
                    borderColor: "transparent",
                    label: 'My dataset', // for legend
                    hoverBorderColor: "#ffffff"
                }],
                labels: [
                    "Only Men",
                    "Only Women",
                    "Both men and women",
                    "Unable to Determine"
                ]
            };
            var polarAreaOpts = {
                legend: {
                    labels: {
                        fontColor: '#50649c'
                    }
                },
                scale: {
                    gridLines: {
                        color: '#eaf0f7'
                    },
                    angleLines: {
                        color: '#eaf0f7' // lines radiating from the center
                    }
                }
            };
            this.respChart($("#polarArea"), 'PolarArea', polarChart, polarAreaOpts);
        },
        $.ChartJs = new ChartJs, $.ChartJs.Constructor = ChartJs

    async function getAllFormsForViolenceInitiatorPeopleAndGenderInvolved() {


        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("x-auth-token", getToken);

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        fetch("https://peaceful-atoll-23751.herokuapp.com/" + "just-clock-285910.uc.r.appspot.com/api/form/all", requestOptions)
            .then(function (response) {
                if (response.status === 403) {
                    alert("Token expired. Login");
                    window.location.href = "index.php";
                }
                if (!response.ok) {
                    alert("Cannot login. Ensure your data is correct and you have internet connection");
                    throw new Error("HTTP status " + response.status);
                }

                return response.json();
            })
            .then(function (result) {

                pepGenderArray1 = [];
                pepGenderArray2 = [];
                pepGenderArray3 = [];
                pepGenderArray4 = [];

                for (const violenceInitiatorPeopleAndGenderInvolved in result.details) {
                    if (result.details.hasOwnProperty(violenceInitiatorPeopleAndGenderInvolved)) {
                        const element = result.details[violenceInitiatorPeopleAndGenderInvolved];

                        if (element.firstApproval === true && element.secondApproval === true) {
                            fillArrayWithResult(element);
                            // fillperpetratorArrayWithResult(element);
                        }

                    }
                }


                var detailsArray = result.details;
                var violenceInitiatorArray = [];

                for (let index = 0; index < detailsArray.length; index++) {
                    const element = detailsArray[index];

                    if (element.firstApproval === true && element.secondApproval === true) {
                        violenceInitiatorArray.push(element.violenceInitiator);
                    }

                }

                fillperpetratorArrayWithResult(violenceInitiatorArray);


                $.ChartJs.init()

            })
            .catch(error => console.log('error', error));

    }

    function fillArrayWithResult(element) {

        // console.log(element.violenceInitiatorPeopleAndGenderInvolved.violenceInitiatorGenderOfIncident.toLowerCase());

        let elementFinal = element.violenceInitiatorPeopleAndGenderInvolved.violenceInitiatorGenderOfIncident.toLowerCase();

        switch (elementFinal) {
            case 'only men':
                pepGenderArray1.push(element);

                // console.log(pepGenderArray1.length);
                break;
            case 'only women':
                pepGenderArray2.push(element);
                break;
            case 'both men and women':
                pepGenderArray3.push(element);
                break;
            case "unable to determine":
                pepGenderArray4.push(element);
                break;

            default:
                break;
        }

    }

    function fillperpetratorArrayWithResult(data) {

        perpetratorArrayItem1 = [];
        perpetratorArrayItem2 = [];
        perpetratorArrayItem3 = [];
        perpetratorArrayItem4 = [];
        perpetratorArrayItem5 = [];
        perpetratorArrayItem6 = [];
        perpetratorArrayItem7 = [];
        perpetratorArrayItem8 = [];
        perpetratorArrayItem9 = [];
        perpetratorArrayItem10 = [];
        perpetratorArrayItem11 = [];
        perpetratorArrayItem12 = [];

        data.reduce(function (a, b) {
                return a.concat(b);
            })
            .filter(function (element) {
                // console.log(element.violenceInitiatorType.toLowerCase());

                // console.log((element.violenceInitiatorType.toLowerCase()) === 'political party \nleader or supporter, candidate or candidate supporter');

                let elementFinal  = element.violenceInitiatorType.toLowerCase();

                switch (elementFinal) {
                    case 'political party leader or supporter, candidate or candidate supporter':
                        perpetratorArrayItem1.push(element);
                        break;
                    case 'government / state actor':
                        perpetratorArrayItem2.push(element);
                        break;
                    case 'party agent':
                        perpetratorArrayItem3.push(element);
                        break;
                    case "election observer / monitor":
                        perpetratorArrayItem4.push(element);
                        break;

                    case "election worker (pos, apos, returning officers)":
                        perpetratorArrayItem5.push(element);
                        break;
                    case "voter(s)/ public":
                        perpetratorArrayItem6.push(element);
                        break;
                    case "campaign material (posters, billboards)":
                        perpetratorArrayItem7.push(element);
                        break;
                    case "election office, property, material (like ballots, trucks, warehouse, etc.)":
                        perpetratorArrayItem8.push(element);
                        break;
                    case "political party office or property (specify party)":
                        perpetratorArrayItem9.push(element);
                        break;
                    case "state (gov't) office or property":
                        perpetratorArrayItem10.push(element);
                        break;
                    case "private property/building":
                        perpetratorArrayItem11.push(element);
                        break;
                    case "others":
                        perpetratorArrayItem12.push(element);
                        break;

                    default:
                        break;
                }
            });

            console.log(perpetratorArrayItem1.length);

    }

    //initializing
    async function init() {
        "use strict";
        await getAllFormsForViolenceInitiatorPeopleAndGenderInvolved();
        // $.ChartJs.init()
    }

    init();

})(jQuery)