(function ($) {
    var getToken = localStorage.getItem("x-auth-token");
    var urlParams = new URLSearchParams(window.location.search);
    var getQueryString = urlParams.get('admin_id');

    var getRole = localStorage.getItem("role");

    let AddAdminsConstants = {};

    'use strict';

    $.urlParam = function (name) {
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        return results[1] || 0;
    }

    function getEditedReport() {

        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("x-auth-token", getToken);

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        fetch("https://peaceful-atoll-23751.herokuapp.com/" + `just-clock-285910.uc.r.appspot.com/api/admin/one/${getQueryString}`, requestOptions)
            .then(function (response) {
                if (response.status === 403) {
                    alert("Token expired. Login");
                    window.location.href = "../index.php";
                }
                if (!response.ok) {

                    response.json().then(function (data) {
                        alert(data.message);
                    });
                    // alert("Cannot login. Ensure your data is correct and you have internet connection");
                    throw new Error("HTTP status " + response.status);
                }

                return response.json();
            })
            .then(function (result) {

                populateFormField(result.details);

                $("#form-horizontal-admin").show();
                $("#loader").hide();
                if (getRole === "approver") $("#delete_admin").show();

            })
            .catch(error => console.log('error', error));

    }

    function populateFormField(details) {

        $('#txtFirstName').val(details.firstName);
        $('#txtLastName').val(details.lastName);
        $('#txtEmail').val(details.email);
        $('#txtPhoneNumber').val(details.phoneNumber);
        $('#adminRoleSpinner').val(details.role);

        $('.passwordRow').hide();

    }

    function deleteButtonOnClick() {
        $("#delete_admin").click(function () {
            var SweetAlert = function () {};

            SweetAlert.prototype.init = function () {

                swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    reverseButtons: true
                }).then((result) => {
                    if (result.value) {

                        deleteReport();

                    } else if (
                        // Read more about handling dismissals
                        result.dismiss === Swal.DismissReason.cancel
                    ) {
                        swal.fire(
                            'Cancelled',
                            'The report is safe :)',
                            'error'
                        )
                    }
                })
            }

            $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            $.SweetAlert.init()
        });
    }

    function deleteReport() {

        $("#form-horizontal-admin").hide();
        $("#loader").show();

        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("x-auth-token", getToken);

        var requestOptions = {
            method: 'DELETE',
            headers: myHeaders,
            redirect: 'follow'
        };

        fetch("https://peaceful-atoll-23751.herokuapp.com/" + `just-clock-285910.uc.r.appspot.com/api/admin/one/${getQueryString}`, requestOptions)
            .then(function (response) {
                if (response.status === 403) {
                    alert("Token expired. Login");
                    window.location.href = "../index.php";
                }
                if (!response.ok) {

                    response.json().then(function (data) {
                        alert(data.message);
                    });
                    // alert("Cannot login. Ensure your data is correct and you have internet connection");
                    throw new Error("HTTP status " + response.status);
                }

                return response.json();
            })
            .then(function (result) {

                alert(result.message);

                $("#form-horizontal-admin").show();
                $("#loader").hide();

                window.location.href = "all.php";

            })
            .catch(error => console.log('error', error));
    }

    function updateAdmin() {

        AddAdminsConstants["firstName"] = $('#txtFirstName').val();

        AddAdminsConstants["lastName"] = $('#txtLastName').val();

        AddAdminsConstants["email"] = $('#txtEmail').val();

        AddAdminsConstants["phoneNumber"] = $('#txtPhoneNumber').val();

        AddAdminsConstants["role"] = $('#adminRoleSpinner').val();

        AddAdminsConstants["picture"] = "";

        console.log(AddAdminsConstants);

        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("x-auth-token", getToken);

        var requestOptions = {
            method: 'PATCH',
            headers: myHeaders,
            body: JSON.stringify(AddAdminsConstants),
            redirect: 'follow'
        };

        $("#form-horizontal-admin").hide();
        $("#loader").show();

        fetch("https://peaceful-atoll-23751.herokuapp.com/" + `just-clock-285910.uc.r.appspot.com/api/admin/one/update/${getQueryString}`, requestOptions)
            .then(function (response) {
                if (response.status === 403) {
                    alert("Token expired. Login");
                    window.location.href = "../index.php";
                }
                if (!response.ok) {
                    // console.log(response.json());
                    // alert(response.statusText);

                    response.json().then(function (data) {
                        alert(data.message);
                    });

                    $("#form-horizontal-admin").show();
                    $("#loader").hide();
                    throw new Error("HTTP status " + response.status);
                }

                $("#form-horizontal-admin").show();
                $("#loader").hide();

                return response.clone().json();
            })
            .then(function (result) {

                console.log(result);

                if (result.statusCode !== 200) {
                    alert(result.message);

                    $("#form-horizontal-admin").show();
                    $("#loader").hide();
                    return;
                }

                $("#form-horizontal-admin").show();
                $("#loader").hide();
                alert(result.message);
                window.location.href = "all.php";

            })
            .catch(error => console.log('error', error));

    }

    function init() {
        getEditedReport();
        deleteButtonOnClick();
        Waves.init();
    }

    if ($.urlParam('admin_id') !== 0) {

        $("#form-horizontal-admin").hide();
        $("#loader").show();

        $("#addAdmin").hide();
        $("#updateAdmin").show();

        init();
    }

    $("#updateAdmin").click(function () {
        updateAdmin();

    });


})(jQuery)