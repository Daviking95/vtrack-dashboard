//Overlapping bars on mobile


(function ($) {

    var getToken = localStorage.getItem("x-auth-token");

    initiatorArrayItem1 = [];
    initiatorArrayItem2 = [];
    initiatorArrayItem3 = [];
    initiatorArrayItem4 = [];
    initiatorArrayItem5 = [];
    initiatorArrayItem6 = [];
    initiatorArrayItem7 = [];
    initiatorArrayItem8 = [];
    initiatorArrayItem9 = [];
    initiatorArrayItem10 = [];
    initiatorArrayItem11 = [];
    initiatorArrayItem12 = [];

    "use strict";

    async function getAllFormsForViolenceInitiatorPeopleAndGenderInvolved() {


        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("x-auth-token", getToken);

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        fetch("https://peaceful-atoll-23751.herokuapp.com/" + "just-clock-285910.uc.r.appspot.com/api/form/all", requestOptions)
            .then(function (response) {
                if (response.status === 403) {
                    alert("Token expired. Login");
                    window.location.href = "index.php";
                }
                if (!response.ok) {
                    alert("Cannot login. Ensure your data is correct and you have internet connection");
                    throw new Error("HTTP status " + response.status);
                }

                return response.json();
            })
            .then(function (result) {

                var detailsArray = result.details;
                var incidentImpactArray = [];

                for (let index = 0; index < detailsArray.length; index++) {
                    const element = detailsArray[index];

                    if (element.firstApproval === true && element.secondApproval === true) {
                        incidentImpactArray.push(element.incidentImpact);
                    }

                }

                fillInitiatorArrayWithResult(incidentImpactArray);

                showCharts();

            })
            .catch(error => console.log('error', error));

    }

    function fillInitiatorArrayWithResult(data) {

        initiatorArrayItem1 = [];
        initiatorArrayItem2 = [];
        initiatorArrayItem3 = [];
        initiatorArrayItem4 = [];
        initiatorArrayItem5 = [];
        initiatorArrayItem6 = [];
        initiatorArrayItem7 = [];
        initiatorArrayItem8 = [];
        initiatorArrayItem9 = [];
        initiatorArrayItem10 = [];
        initiatorArrayItem11 = [];
        initiatorArrayItem12 = [];

        data.reduce(function (a, b) {
                return a.concat(b);
            })
            .filter(function (element) {
                switch (element.impactType) {
                    case 'Complaint filed with Electoral Commission':
                        initiatorArrayItem1.push(element);

                        break;
                    case 'Civic education event disrupted':
                        initiatorArrayItem2.push(element);
                        break;
                    case 'Campaign activity disrupted':
                        initiatorArrayItem3.push(element);

                        break;
                    case "Voting disrupted/voters dispersed or left area":
                        initiatorArrayItem4.push(element);
                        break;

                    case "Disrupted vote count":
                        initiatorArrayItem5.push(element);
                        break;
                    case "Transportation disrupted":
                        initiatorArrayItem6.push(element);
                        break;
                    case "Economic/financial loss":
                        initiatorArrayItem7.push(element);
                        break;
                    case "Cancelled election":
                        initiatorArrayItem8.push(element);
                        break;
                    case "Postponed election":
                        initiatorArrayItem9.push(element);
                        break;
                    case "Re-run election":
                        initiatorArrayItem10.push(element);
                        break;
                    case "Other":
                        initiatorArrayItem11.push(element);
                        break;
                    case "Unable to determine":
                        initiatorArrayItem12.push(element);
                        break;

                    default:
                        break;
                }
            });


    }

    // function showCharts() {
    //     var data = {
    //         labels: ['Complaint filed with Electoral Commission', 'Civic education event disrupted', 'Campaign activity disrupted', 'Voting disrupted/voters dispersed or left area', 'Disrupted vote count', 'Transportation disrupted', 'Economic/financial loss', 'Cancelled election', 'Postponed election', 'Re-run election', 'Other', 'Unable to determine'],
    //         series: [
    //             [initiatorArrayItem1.length, initiatorArrayItem2.length, initiatorArrayItem3.length, initiatorArrayItem4.length, initiatorArrayItem5.length, initiatorArrayItem6.length, initiatorArrayItem7.length, initiatorArrayItem8.length, initiatorArrayItem9.length, initiatorArrayItem10.length, initiatorArrayItem11.length, initiatorArrayItem12.length],
    //             //   [3, 2, 9, 5, 4, 6, 4, 6, 7, 8, 7, 4]
    //         ]
    //     };

    //     var options = {
    //         seriesBarDistance: 10
    //     };

    //     var responsiveOptions = [
    //         ['screen and (max-width: 640px)', {
    //             seriesBarDistance: 5,
    //             axisX: {
    //                 labelInterpolationFnc: function (value) {
    //                     return value[0];
    //                 }
    //             }
    //         }]
    //     ];

    //     new Chartist.Bar('#overlapping-bars', data, options, responsiveOptions);
    // }

    function showCharts() {

        var MorrisCharts = function () {};

        //creates Bar chart
        MorrisCharts.prototype.createBarChart = function (element, data, xkey, ykeys, labels, lineColors) {
                Morris.Bar({
                    element: element,
                    data: data,
                    xkey: 'y',
                    ykeys: ['a'],
                    labels: ['Number(s)'],
                    gridLineColor: '#012F43',
                    barSizeRatio: 0.4,
                    resize: true,
                    hideHover: 'auto',
                    barColors: lineColors
                });
            },

            MorrisCharts.prototype.init = function () {

                //creating bar chart
                var $barData = [{
                        y: 'Complaint filed with Electoral Commission',
                        a: initiatorArrayItem1.length
                    },
                    {
                        y: 'Civic education event disrupted',
                        a: initiatorArrayItem2.length
                    },
                    {
                        y: 'Campaign activity disrupted',
                        a: initiatorArrayItem3.length
                    },
                    {
                        y: 'Voting disrupted/voters dispersed or left area',
                        a: initiatorArrayItem4.length
                    },
                    {
                        y: 'Disrupted vote count',
                        a: initiatorArrayItem5.length
                    },
                    {
                        y: 'Transportation disrupted',
                        a: initiatorArrayItem6.length
                    },
                    {
                        y: 'Economic/financial loss',
                        a: initiatorArrayItem7.length
                    },
                    {
                        y: 'Cancelled election',
                        a: initiatorArrayItem8.length
                    },
                    {
                        y: 'Postponed election',
                        a: initiatorArrayItem9.length
                    },
                    {
                        y: 'Re-run election',
                        a: initiatorArrayItem10.length
                    },
                    {
                        y: 'Other',
                        a: initiatorArrayItem11.length
                    },
                    {
                        y: 'Unable to determine',
                        a: initiatorArrayItem12.length
                    }
                ];
                this.createBarChart('overlapping-bars', $barData, 'y', ['a'], ['Number(s)'], ['#012F43']);

            },
            //init
            $.MorrisCharts = new MorrisCharts, $.MorrisCharts.Constructor = MorrisCharts
        $.MorrisCharts.init();

    }

    //initializing
    async function init() {
        "use strict";
        await getAllFormsForViolenceInitiatorPeopleAndGenderInvolved();
        // $.ChartJs.init()
    }

    init();

})(jQuery)