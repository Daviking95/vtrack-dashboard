(function ($) {
    var getToken = localStorage.getItem("x-auth-token");
    var urlParams = new URLSearchParams(window.location.search);
    var getQueryString = urlParams.get('monitor_code');

    var getRole = localStorage.getItem("role");

    let AddMonitorsConstants = {};

    'use strict';

    $.urlParam = function (name) {
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        return results[1] || 0;
    }

    function getEditedReport() {

        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("x-auth-token", getToken);

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        fetch("https://peaceful-atoll-23751.herokuapp.com/" + `just-clock-285910.uc.r.appspot.com/api/monitor/one/${getQueryString}`, requestOptions)
            .then(function (response) {
                if (response.status === 403) {
                    alert("Token expired. Login");
                    window.location.href = "../index.php";
                }
                if (!response.ok) {

                    response.json().then(function (data) {
                        alert(data.message);
                    });
                    // alert("Cannot login. Ensure your data is correct and you have internet connection");
                    throw new Error("HTTP status " + response.status);
                }

                return response.json();
            })
            .then(function (result) {
                // console.log(result.details.user_image);

                populateFormField(result.details);

                $("#form-horizontal-monitor").show();
                $("#loader").hide();
                if (getRole === "approver") $("#delete_monitor").show();

            })
            .catch(error => console.log('error', error));

    }

    function populateFormField(details) {

        // console.log(details.user_image);

        $('#txtMonitorCode').val(details.user_monitor_code);
        $('#txtUserName').val(details.user_name);
        $('#txtFirstName').val(details.user_first_name);
        $('#txtLastName').val(details.user_last_name);
        $('#txtEmail').val(details.user_email);
        $('#txtPhoneNumber').val(details.user_phone_number);

        $('#txtPassword').val(details.user_password);
        $('#txtConfirmPassword').val(details.user_password_confirm);
        $('#monitorGenderSpinner').val(details.user_gender);
        $('#txtAge').val(details.user_age);
        $('#monitorStateSpinner').val(details.state);
        // document.querySelector('user_image').src = details.user_image;
        // $('#user_image').src(details.user_image);
        retrieveImage(details.user_image);

        $('#txtMonitorCode').attr('disabled', 'diabled');
        $('#txtPassword').attr('disabled', 'diabled');
        $('#txtConfirmPassword').attr('disabled', 'diabled');

        $('.passwordRow').hide();

    }

    function retrieveImage(imageUri) {

        var _agentImageRef = firebase.storage().refFromURL(imageUri);
        _agentImageRef.getDownloadURL().then(function (url) {
            console.log(`log1 ${url}`);

            $('#user_image').attr("src", url);
        });

        // if (imageUri.startsWith('gs://')) {
        //     firebase.storage().refFromURL(imageUri).getMetadata().then(function(metadata) {
        //     $('#user_image').src = metadata.downloadURLs[0];
        //     console.log("URL is "+metadata.downloadURLs[0]);
        //   }).catch(function(error){
        //     console.log("error downloading "+error);
        //   });
        // }
    }

    function deleteButtonOnClick() {
        $("#delete_monitor").click(function () {
            var SweetAlert = function () {};

            SweetAlert.prototype.init = function () {

                swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    reverseButtons: true
                }).then((result) => {
                    if (result.value) {

                        deleteReport();

                    } else if (
                        // Read more about handling dismissals
                        result.dismiss === Swal.DismissReason.cancel
                    ) {
                        swal.fire(
                            'Cancelled',
                            'The report is safe :)',
                            'error'
                        )
                    }
                })
            }

            $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            $.SweetAlert.init()
        });
    }

    function deleteReport() {

        $("#form-horizontal-monitor").hide();
        $("#loader").show();

        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("x-auth-token", getToken);

        var requestOptions = {
            method: 'DELETE',
            headers: myHeaders,
            redirect: 'follow'
        };

        fetch("https://peaceful-atoll-23751.herokuapp.com/" + `just-clock-285910.uc.r.appspot.com/api/monitor/one/${getQueryString}`, requestOptions)
            .then(function (response) {
                if (response.status === 403) {
                    alert("Token expired. Login");
                    window.location.href = "../index.php";
                }
                if (!response.ok) {

                    response.json().then(function (data) {
                        alert(data.message);
                    });
                    // alert("Cannot login. Ensure your data is correct and you have internet connection");
                    throw new Error("HTTP status " + response.status);
                }

                return response.json();
            })
            .then(function (result) {

                alert(result.message);

                $("#form-horizontal-monitor").show();
                $("#loader").hide();

                window.location.href = "all.php";

            })
            .catch(error => console.log('error', error));
    }

    function updateMonitor() {

        AddMonitorsConstants["user_name"] = $('#txtUserName').val();

        AddMonitorsConstants["user_first_name"] = $('#txtFirstName').val();

        AddMonitorsConstants["user_last_name"] = $('#txtLastName').val();

        AddMonitorsConstants["user_email"] = $('#txtEmail').val();

        AddMonitorsConstants["user_phone_number"] = $('#txtPhoneNumber').val();

        AddMonitorsConstants["user_gender"] = $('#monitorGenderSpinner').val();

        AddMonitorsConstants["user_age"] = $('#txtAge').val();

        AddMonitorsConstants["state"] = $('#monitorStateSpinner').val();

        AddMonitorsConstants["country"] = "Nigeria";

        console.log(AddMonitorsConstants);

        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("x-auth-token", getToken);

        var requestOptions = {
            method: 'PUT',
            headers: myHeaders,
            body: JSON.stringify(AddMonitorsConstants),
            redirect: 'follow'
        };

        $("#form-horizontal-monitor").hide();
        $("#loader").show();

        fetch("https://peaceful-atoll-23751.herokuapp.com/" + `just-clock-285910.uc.r.appspot.com/api/monitor/one/update/${getQueryString}`, requestOptions)
            .then(function (response) {
                if (response.status === 403) {
                    alert("Token expired. Login");
                    window.location.href = "../index.php";
                }
                if (!response.ok) {
                    // console.log(response.json());
                    // alert(response.statusText);

                    response.json().then(function (data) {
                        alert(data.message);
                    });

                    $("#form-horizontal-monitor").show();
                    $("#loader").hide();
                    throw new Error("HTTP status " + response.status);
                }

                $("#form-horizontal-monitor").show();
                $("#loader").hide();

                return response.json();
            })
            .then(function (result) {

                console.log(result);

                if (result.statusCode !== 200) {
                    alert(result.message);

                    $("#form-horizontal-monitor").show();
                    $("#loader").hide();
                    return;
                }

                $("#form-horizontal-monitor").show();
                $("#loader").hide();
                alert(result.message);
                window.location.href = "all.php";

            })
            .catch(error => console.log('error', error));

    }

    function init() {
        getEditedReport();
        deleteButtonOnClick();
        Waves.init();
    }

    if ($.urlParam('monitor_code') !== 0) {

        $("#form-horizontal-monitor").hide();
        $("#loader").show();

        $("#addMonitor").hide();
        $("#updateMonitor").show();

        init();
    }

    $("#updateMonitor").click(function () {
        updateMonitor();

    });


})(jQuery)