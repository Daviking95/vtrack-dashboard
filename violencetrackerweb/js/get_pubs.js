window.$(document).ready(function () {

    var firebaseDb = firebase.firestore();

    var docRef = firebaseDb.collection("vtrack_media_center");

    gettingAllPubs("media_pubs");

    async function gettingAllPubs(docString) {

        var mediaAssetList;

        const promise = new Promise((resolve, reject) => {

            docRef.doc(docString).get().then(function (doc) {
                if (doc.exists) {
                    mediaAssetList = doc.data();

                    resolve(mediaAssetList)
                } else {
                    // doc.data() will be undefined in this case
                    console.log("No such document!");
                }
            }).catch(function (error) {
                console.log("Error getting document:", error);
            });

        });

        let result = await promise;

        displayResult(result);
        return result;
    }

    function displayResult(result) {

        result["mediaPubs"].forEach(element => {

            var _agentImageRef = firebase.storage().refFromURL(element.cover);

            console.log(_agentImageRef.name);

            _agentImageRef.getDownloadURL().then(function (url) {

                $('#get_all_pubs').append('<a data-fancybox href="' + element.pub + '"><div class="publication-content__each"><img width="250" height="200" src="' + url + '"alt="Report Cases Of All Forms Of Violence" /><h6 class="heading-9">'+ element.title +'</h6></div></a>');

                $(".media-video > a").slice(0, 6).show();

            });


        });

    }


});