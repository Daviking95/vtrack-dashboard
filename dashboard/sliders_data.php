<!DOCTYPE html>
<html lang="en">

<?php $header_title = 'Slider Data | VTrack Reporting Dasboard - A concept of KDI';
include '../customs/app_head.php'
?>

<body>

    <?php include '../customs/app_topbar.php'?>

    <div class="page-wrapper">
        <!-- Left Sidenav -->
        <?php include '../customs/app_sidenav.php'?>
        <!-- end left-sidenav-->

        <!-- Page Content-->
        <div class="page-content">

            <div class="container-fluid">
                <!-- Page-Title -->
                <?php $page_title = 'Home Slider Data';
include_once '../customs/app_page_title.php'?>
                <!-- end page title end breadcrumb -->

                <div class="row">
                    <div class="col-lg-12">
                        <!--begin::Portlet-->
                        <div
                            class="m-portlet m-portlet--mobile m-portlet--creative m-portlet--first m-portlet--bordered-semi">

                            <div class="m-portlet__body">
                                <!--begin: Datatable -->
                                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                                    <div class="row align-items-center">
                                        <div class="col-xl-8 order-2 order-xl-1">
                                            <div class="form-group m-form__group row align-items-center">

                                                <div class="col-md-6">
                                                    <div class="m-input-icon m-input-icon--left">
                                                        <input type="text" class="form-control m-input m-input--solid"
                                                            placeholder="Search..." id="allElectionReportSearch">
                                                        <span class="m-input-icon__icon m-input-icon__icon--left">
                                                            <span>
                                                                <i class="la la-search"></i>
                                                            </span>
                                                        </span>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                        <div class="col-xl-4 order-2 order-xl-1">
                                            <span class="input-group-append ml-2">
                                                <button class="btn btn-primary js--triggerAnimation" type="button"
                                                    id="add_result">Add Slider Data
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <!-- Table -->
                                <div class="m_datatable_election_result" id="local_data"></div>
                                <!-- End Table -->
                            </div>
                        </div>
                        <!--end::Portlet-->
                    </div>
                </div>

                <div class="report-popup">
                    <div class="report-popup__content">
                        <div class="row">
                            <div class="col-lg-12"><i id="close-report" class="far fa-window-close"
                                    style="float: right;width:50px;height: 50px;font-size: 25px;color: red;"
                                    aria-hidden="true"></i></div>
                        </div>

                        <div class="row">

                            <div class="form-group col-lg-12 row">
                                <label for="txtTitle" class="col-lg-3 col-form-label">Title.</label>
                                <div class="col-lg-9">
                                    <input id="txtTitle" name="txtTitle" type="text" class="form-control" required>
                                </div>
                            </div>

                        </div>

                        <div class="row">

                            <div class="form-group col-lg-6 row">
                                <label for="txtNumbers" class="col-lg-3 col-form-label">Numbers</label>
                                <div class="col-lg-9">
                                    <input id="txtNumbers" name="txtNumbers" type="number" class="form-control"
                                        required>
                                </div>
                            </div>

                            <div class="form-group col-lg-6 row">
                                <label for="txtHashtag" class="col-lg-3 col-form-label">Hashtag Title</label>
                                <div class="col-lg-9">
                                    <input id="txtHashtag" name="txtHashtag" type="text" class="form-control" required>
                                </div>
                            </div>

                        </div>

                        <div class="row">

                            <div class="form-group col-lg-6 row center">
                                <button class="btn btn-primary js--triggerAnimation" type="button"
                                    id="submit_result">SUBMIT
                                </button>
                            </div>


                            <div class="cssload-thecube" id="loader3" style="display: none">
                                <div class="cssload-cube cssload-c1"></div>
                                <div class="cssload-cube cssload-c2"></div>
                                <div class="cssload-cube cssload-c4"></div>
                                <div class="cssload-cube cssload-c3"></div>
                            </div>
                        </div>

                    </div>

                    <div></div>
                </div>

            </div><!-- container -->

            <?php include_once '../customs/app_footer.php'?>
            <!--end footer-->
        </div>
        <!-- end page content -->
    </div>
    <!-- end page-wrapper -->

    <!-- jQuery  -->
    <?php include '../customs/app_js_files.php'?>

    <!-- Custom Charts Functions js -->
    <script src="../assets/js/homeController/sliderData.js"></script>

</body>

</html>