(function ($) {

    var getToken = localStorage.getItem("x-auth-token");

    initiatorArrayItem1 = [];
    initiatorArrayItem2 = [];
    initiatorArrayItem3 = [];
    initiatorArrayItem4 = [];
    initiatorArrayItem5 = [];
    initiatorArrayItem6 = [];
    initiatorArrayItem7 = [];
    initiatorArrayItem8 = [];
    initiatorArrayItem9 = [];
    initiatorArrayItem10 = [];
    initiatorArrayItem11 = [];

    "use strict";

    async function getAllFormsForViolenceInitiatorPeopleAndGenderInvolved() {


        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("x-auth-token", getToken);

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        fetch("https://peaceful-atoll-23751.herokuapp.com/" + "just-clock-285910.uc.r.appspot.com/api/form/all", requestOptions)
            .then(function (response) {
                if (response.status === 403) {
                    alert("Token expired. Login");
                    window.location.href = "index.php";
                }
                if (!response.ok) {
                    alert("Cannot login. Ensure your data is correct and you have internet connection");
                    throw new Error("HTTP status " + response.status);
                }

                return response.json();
            })
            .then(function (result) {

                var detailsArray = result.details;
                var incidentViolenceArray = [];

                for (let index = 0; index < detailsArray.length; index++) {
                    const element = detailsArray[index];

                    if (element.firstApproval === true && element.secondApproval === true) {
                    incidentViolenceArray.push(element.incidentViolence);
                    }

                }

                fillInitiatorArrayWithResult(incidentViolenceArray);

                showNewChart();

            })
            .catch(error => console.log('error', error));

    }

    function fillInitiatorArrayWithResult(data) {

        initiatorArrayItem1 = [];
        initiatorArrayItem2 = [];
        initiatorArrayItem3 = [];
        initiatorArrayItem4 = [];
        initiatorArrayItem5 = [];
        initiatorArrayItem6 = [];
        initiatorArrayItem7 = [];
        initiatorArrayItem8 = [];
        initiatorArrayItem9 = [];
        initiatorArrayItem10 = [];
        initiatorArrayItem11 = [];

        data.reduce(function (a, b) {
                return a.concat(b);
            })
            .filter(function (element) {
                // console.log(element.violenceType);
                switch (element.violenceType) {
                    case 'Murder':
                        initiatorArrayItem1.push(element);

                        break;
                    case 'Attempted murder':
                        initiatorArrayItem2.push(element);
                        break;
                    case 'Physical harm / torture':
                        initiatorArrayItem3.push(element);
                        break;
                    case "Sexual assault":
                        initiatorArrayItem4.push(element);
                        break;

                    case "Intimidation / psychological abuse":
                        initiatorArrayItem5.push(element);
                        break;
                    case "Kidnapping":
                        initiatorArrayItem6.push(element);
                        break;
                    case "Group Clash (groups fight, also check physical harm)":
                        initiatorArrayItem7.push(element);
                        break;
                    case "Politically motivated arrest or detention":
                        initiatorArrayItem8.push(element);
                        break;
                    case "Destruction of property":
                        initiatorArrayItem9.push(element);
                        break;
                    case "Theft":
                        initiatorArrayItem10.push(element);
                        break;
                    case "Other":
                        initiatorArrayItem11.push(element);
                        break;

                    default:
                        break;
                }
            });

    }

    function showNewChart(){
        var options = {
            series: [
            // {
            //   name: "High - 2013",
            //   data: [28, 29, 33, 36, 32, 32, 33]
            // },
            {
              name: "Data",
              data: [initiatorArrayItem1.length, initiatorArrayItem2.length, initiatorArrayItem3.length, initiatorArrayItem4.length, initiatorArrayItem5.length, initiatorArrayItem6.length, initiatorArrayItem7.length, initiatorArrayItem8.length, initiatorArrayItem9.length, initiatorArrayItem10.length, initiatorArrayItem11.length]
            }
          ],
            chart: {
            height: 350,
            type: 'line',
            dropShadow: {
              enabled: true,
              color: '#000',
              top: 18,
              left: 7,
              blur: 10,
              opacity: 0.2
            },
            toolbar: {
              show: false
            }
          },
          colors: ['#77B6EA', '#545454'],
          dataLabels: {
            enabled: true,
          },
          stroke: {
            curve: 'smooth'
          },
          title: {
            text: '',
            align: 'left'
          },
          grid: {
            borderColor: '#e7e7e7',
            row: {
              colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
              opacity: 0.5
            },
          },
          markers: {
            size: 1
          },
          xaxis: {
            categories: ["Murder", "Attempted murder", "Physical harm / torture", "Sexual assault", "Intimidation / psychological abuse", "Kidnapping", "Group Clash (groups fight, also check physical harm)", "Politically motivated arrest or detention", "Destruction of property", "Theft", "Other"],
            title: {
              text: 'Month'
            }
          },
          yaxis: {
            title: {
              text: 'Temperature'
            },
            min: -1,
            max: 40
          },
          legend: {
            position: 'top',
            horizontalAlign: 'right',
            floating: true,
            offsetY: -25,
            offsetX: -5
          }
          };
  
          var chart = new ApexCharts(document.querySelector("#apex_kind"), options);
          chart.render();

          return chart;
    }

    function showCharts() {

        var options = {
            chart: {
                height: 374,
                type: 'line',
                shadow: {
                    enabled: false,
                    color: '#bbb',
                    top: 3,
                    left: 2,
                    blur: 3,
                    opacity: 1
                },
            },
            stroke: {
                width: 5,
                curve: 'smooth'
            },
            series: [{
                name: 'Numbers',
                data: [initiatorArrayItem1.length, initiatorArrayItem2.length, initiatorArrayItem3.length, initiatorArrayItem4.length, initiatorArrayItem5.length, initiatorArrayItem6.length, initiatorArrayItem7.length, initiatorArrayItem8.length, initiatorArrayItem9.length, initiatorArrayItem10.length, initiatorArrayItem11.length]
            }],
            xaxis: {
                type: 'text',
                categories: ["Murder", "Attempted murder", "Physical harm / torture", "Sexual assault", "Intimidation / psychological abuse", "Kidnapping", "Group Clash (groups fight, also check physical harm)", "Politically motivated arrest or detention", "Destruction of property", "Theft", "Other"],
                axisBorder: {
                    show: true,
                    color: '#bec7e0',
                },
                axisTicks: {
                    show: true,
                    color: '#bec7e0',
                },
            },
            title: {
                // text: 'Social Media',
                align: 'left',
                style: {
                    fontSize: "16px",
                    color: '#666'
                }
            },
            fill: {
                type: 'gradient',
                gradient: {
                    shade: 'dark',
                    gradientToColors: ['#43cea2'],
                    shadeIntensity: 1,
                    type: 'horizontal',
                    opacityFrom: 1,
                    opacityTo: 1,
                    stops: [0, 100, 100, 100]
                },
            },
            markers: {
                size: 4,
                opacity: 0.9,
                colors: ["#ffbc00"],
                strokeColor: "#fff",
                strokeWidth: 2,
                style: 'inverted', // full, hollow, inverted
                hover: {
                    size: 7,
                }
            },
            yaxis: {
                min: -10,
                max: 40,
                title: {
                    text: 'No.',
                },
            },
            grid: {
                row: {
                    colors: ['transparent', 'transparent'], // takes an array which will be repeated on columns
                    opacity: 0.2
                },
                borderColor: '#185a9d'
            },
            responsive: [{
                breakpoint: 600,
                options: {
                    chart: {
                        toolbar: {
                            show: false
                        }
                    },
                    legend: {
                        show: false
                    },
                }
            }]
        }

        var chart = new Chartist.Line('#chart-with-area',
            options
        );

        return chart;
    }


    // function showCharts() {
    //     new Chartist.Line('#chart-with-area', {
    //         labels: ["Murder", "Attempted murder", "Physical harm / torture", "Sexual assault", "Intimidation / psychological abuse", "Kidnapping", "Group Clash (groups fight, also check physical harm)", "Politically motivated arrest or detention", "Destruction of property", "Theft", "Other"],
    //         series: [
    //             [initiatorArrayItem1.length, initiatorArrayItem2.length, initiatorArrayItem3.length, initiatorArrayItem4.length, initiatorArrayItem5.length, initiatorArrayItem6.length, initiatorArrayItem7.length, initiatorArrayItem8.length, initiatorArrayItem9.length, initiatorArrayItem10.length, initiatorArrayItem11.length]
    //         ]
    //     }, {
    //         low: 0,
    //         showArea: true,
    //         plugins: [
    //             Chartist.plugins.tooltip()
    //         ]
    //     });
    // }

    //initializing
    async function init() {
        "use strict";
        await getAllFormsForViolenceInitiatorPeopleAndGenderInvolved();
        // $.ChartJs.init()
    }

    init();

})(jQuery)