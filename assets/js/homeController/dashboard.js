(function ($) {
    var getToken = localStorage.getItem("x-auth-token");

    'use strict';

    function getAllForms() {

        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("x-auth-token", getToken);

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        fetch("https://peaceful-atoll-23751.herokuapp.com/" + "just-clock-285910.uc.r.appspot.com/api/form/all", requestOptions)
            .then(function (response) {
                if (response.status === 403) {
                    alert("Token expired. Login");
                    window.location.href = "index.php";
                }
                if (!response.ok) {
                    alert("Cannot login. Ensure your data is correct and you have internet connection");
                    window.location.href = "index.php";

                    throw new Error("HTTP status " + response.status);
                }

                return response.json();
            })
            .then(function (result) {
                // localStorage.setItem("x-auth-token", result.details.token);
                // localStorage.setItem("role", result.details.role);

                $('#total_forms').text(result.details.length);
            })
            .catch(error => console.log('error', error));

    }

    function getAllAwaitingVerificationForms() {

        var firstApprovals = [];

        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("x-auth-token", getToken);

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        fetch("https://peaceful-atoll-23751.herokuapp.com/" + "just-clock-285910.uc.r.appspot.com/api/form/all", requestOptions)
            .then(function (response) {
                if (response.status === 403) {
                    alert("Token expired. Login");
                    window.location.href = "../index.php";
                }
                if (!response.ok) {
                    alert("Cannot login. Ensure your data is correct and you have internet connection");
                    throw new Error("HTTP status " + response.status);
                }

                return response.json();
            })
            .then(function (result) {
                // localStorage.setItem("x-auth-token", result.details.token);
                // localStorage.setItem("role", result.details.role);

                result.details.forEach(element => {
                    if(!element.firstApproval){
                        firstApprovals.push(element);
                    }
                    
                });

                $('#total_forms_verification').text(firstApprovals.length);
            })
            .catch(error => console.log('error', error));

    }

    function getAllAwaitingApprovalForms() {

        var secondApprovals = [];

        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("x-auth-token", getToken);

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        fetch("https://peaceful-atoll-23751.herokuapp.com/" + "just-clock-285910.uc.r.appspot.com/api/form/all", requestOptions)
            .then(function (response) {
                if (response.status === 403) {
                    alert("Token expired. Login");
                    window.location.href = "../index.php";
                }
                if (!response.ok) {
                    alert("Cannot login. Ensure your data is correct and you have internet connection");
                    throw new Error("HTTP status " + response.status);
                }

                return response.json();
            })
            .then(function (result) {
                // localStorage.setItem("x-auth-token", result.details.token);
                // localStorage.setItem("role", result.details.role);

                result.details.forEach(element => {
                    if(!element.secondApproval){
                        secondApprovals.push(element);
                    }
                    
                });

                $('#total_forms_approval').text(secondApprovals.length);
            })
            .catch(error => console.log('error', error));

    }

    function init() {
        getAllForms();
        getAllAwaitingVerificationForms();
        getAllAwaitingApprovalForms();
        Waves.init();
    }

    init();

})(jQuery)