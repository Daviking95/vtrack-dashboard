<head>
    <meta charset="utf-8" />
    <title><?php echo $header_title ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Admin dashboard for reporting violence and election cases" name="description" />
    <meta content="VTrack" name="author" />

    <!-- App favicon -->
    <link rel="shortcut icon" href="../assets/images/favicon.png">


    <!--Form Wizard-->
    <link rel="stylesheet" href="../assets/plugins/jquery-steps/jquery.steps.css">
    <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
         rel = "stylesheet">

    <!-- <link href="../assets/plugins/daterangepicker/daterangepicker.css" rel="stylesheet" />
        <link href="../assets/plugins/select2/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/timepicker/bootstrap-material-datetimepicker.css" rel="stylesheet">
        <link href="../assets/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css" rel="stylesheet" /> -->
        
    <!-- DataTables -->
    <link href="../assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />

    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="../assets/plugins/morris/morris.css">

    <!--Chartist Chart CSS -->
    <link rel="stylesheet" href="../assets/plugins/chartist/css/chartist.min.css">

    <!-- jvectormap -->
    <link href="../assets/plugins/jvectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="../assets/plugins/vector-map/jqvmap.css" />
    <link href="../assets/css/map.css" rel="stylesheet" type="text/css" />

    <!-- Sweet Alert -->
    <link href="../assets/plugins/sweet-alert2/sweetalert2.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/plugins/animate/animate.css" rel="stylesheet" type="text/css">

    <link href="../assets/plugins/vendor/vendors.bundle.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/base/style.bundle.css" rel="stylesheet" type="text/css" />

    <!-- App css -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/metisMenu.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/spinkit.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />

</head>