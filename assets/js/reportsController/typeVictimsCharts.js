//Animating a Donut with Svg.animate

(function ($) {

  var getToken = localStorage.getItem("x-auth-token");

  VictimTypesArrayItem1 = [];
  VictimTypesArrayItem2 = [];
  VictimTypesArrayItem3 = [];

  "use strict";

  async function getAllFormsForViolenceVictimsPeopleAndGenderInvolved() {


    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("x-auth-token", getToken);

    var requestOptions = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };

    fetch("https://peaceful-atoll-23751.herokuapp.com/" + "just-clock-285910.uc.r.appspot.com/api/form/all", requestOptions)
      .then(function (response) {
        if (response.status === 403) {
          alert("Token expired. Login");
          window.location.href = "index.php";
        }
        if (!response.ok) {
          alert("Cannot login. Ensure your data is correct and you have internet connection");
          throw new Error("HTTP status " + response.status);
        }

        return response.json();
      })
      .then(function (result) {

        VictimTypesArrayItem1 = [];
        VictimTypesArrayItem2 = [];
        VictimTypesArrayItem3 = [];

        for (const violenceVictimPeopleAndGenderInvolved in result.details) {
          if (result.details.hasOwnProperty(violenceVictimPeopleAndGenderInvolved)) {
            const element = result.details[violenceVictimPeopleAndGenderInvolved];

            if (element.firstApproval === true && element.secondApproval === true) {
              fillInitiatorArrayWithResult(element);
            }

          }
        }

        showingChart();

      })
      .catch(error => console.log('error', error));

  }

  function fillInitiatorArrayWithResult(element) {

    let elementFinal = element.violenceVictimPeopleAndGenderInvolved.violenceVictimTypeOfIncident.toLowerCase();

    switch (elementFinal) {
      case 'people':
        VictimTypesArrayItem1.push(element);

        break;
      case 'property':
        VictimTypesArrayItem2.push(element);
        break;
      case 'both':
        VictimTypesArrayItem3.push(element);
        break;

      default:
        break;
    }

  }


  function showingChart() {

    var chart = new Chartist.Pie('#animating-donut', {
      series: [VictimTypesArrayItem1.length, VictimTypesArrayItem2.length, VictimTypesArrayItem3.length, ],
      labels: [`People : ${VictimTypesArrayItem1.length}`, `Property : ${VictimTypesArrayItem2.length}`, `Both : ${VictimTypesArrayItem3.length}`]
    }, {
      donut: true,
      showLabel: true,
      plugins: [
        Chartist.plugins.tooltip()
      ]
    });

    chart.on('draw', function (data) {
      if (data.type === 'slice') {
        // Get the total path length in order to use for dash array animation
        var pathLength = data.element._node.getTotalLength();

        // Set a dasharray that matches the path length as prerequisite to animate dashoffset
        data.element.attr({
          'stroke-dasharray': pathLength + 'px ' + pathLength + 'px'
        });

        // Create animation definition while also assigning an ID to the animation for later sync usage
        var animationDefinition = {
          'stroke-dashoffset': {
            id: 'anim' + data.index,
            dur: 1000,
            from: -pathLength + 'px',
            to: '0px',
            easing: Chartist.Svg.Easing.easeOutQuint,
            // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
            fill: 'freeze'
          }
        };

        // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
        if (data.index !== 0) {
          animationDefinition['stroke-dashoffset'].begin = 'anim' + (data.index - 1) + '.end';
        }

        // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us
        data.element.attr({
          'stroke-dashoffset': -pathLength + 'px'
        });

        // We can't use guided mode as the animations need to rely on setting begin manually
        // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
        data.element.animate(animationDefinition, false);
      }
    });

    chart.on('created', function () {
      if (window.__anim21278907124) {
        clearTimeout(window.__anim21278907124);
        window.__anim21278907124 = null;
      }
      window.__anim21278907124 = setTimeout(chart.update.bind(chart), 10000);
    });

  }

  //initializing
  async function init() {
    "use strict";
    await getAllFormsForViolenceVictimsPeopleAndGenderInvolved();
    // $.ChartJs.init()
  }

  init();

})(jQuery)