var firebaseDb = firebase.firestore();

var docRef = firebaseDb.collection("vtrack_media_center");

var prevDataList = [];

function readURL(input) {
    total_file = input.files.length;

    for (var i = 0; i < total_file; i++) {
        $('#image_preview').append("<a data-fancybox='gallery' href='" + URL.createObjectURL(event.target.files[i]) + "'><img class='media-picture__img' style='width: 100px;float: left;margin: 10px;' src='" + URL.createObjectURL(event.target.files[i]) + "'></a>");
    }

    $('#image_preview').show();
}

function readPubCover(input) {
    total_file = input.files.length;

    mediaExt = ['jpeg', 'jpg', 'png', 'gif'];

    for (var i = 0; i < total_file; i++) {
        console.log(event.target.files[i].name.split('.').pop());
        if (mediaExt.includes(event.target.files[i].name.split('.').pop())) {
            $('#image_pub_cover').append("<a data-fancybox='gallery' href='" + URL.createObjectURL(event.target.files[i]) + "'><img class='media-picture__img' style='width: 100px;float: left;margin: 10px;' src='" + URL.createObjectURL(event.target.files[i]) + "'></a>");
        } else {
            $('#image_pub_cover').append("<div>" + event.target.files[i].name + "</div>");
        }
    }

    $('#image_pub_cover').show();
}

$("#upload_images").click(function () {
    $("#upload_images").hide();
    $("#loader").show();
    $("#ylinks_row").hide();
    $("#pubs_row").hide();

    var mediaAssetList = [];

    storageRef = firebase.storage().ref().child(`violenceTrackMediaForm/mediaCenter`);
    var file = document.getElementById("media_asset").files;

    for (let index = 0; index < file.length; index++) {
        const element = file[index];

        mediaAssetList.push(getAssetUrl(element));

    }

    Promise.all(mediaAssetList).then(function (values) {

        mediaAsset = values;

        console.log(mediaAsset);

        if (mediaAsset.includes(undefined) || mediaAsset.includes("") || mediaAsset.length === 0) {
            alert("You have not added anything");

            $("#upload_images").show();
            $("#loader").hide();
            $("#ylinks_row").show();
            $("#pubs_row").show();

            return;
        }

        saveLinksToDb("media_photos", "mediaAsset", mediaAsset);

    });


});


$("#upload_pubs").click(async function () {
    $("#upload_pubs").hide();
    $("#loader3").show();
    $("#ylinks_row").hide();
    $("#photos_row").hide();

    var mediaAssetList = [];

    var valueObj;

    storageRef = firebase.storage().ref().child(`violenceTrackMediaForm/mediaCenter/pubs`);
    var file = document.getElementById("media_pub_cover").files;
    var filePub = document.getElementById("media_pub").files;
    var fileTitle = $("#media_pub_title").val();

    for (let index = 0; index < file.length; index++) {
        const element = file[index];

        coverFile = await getAssetUrl(element);
        pubFile = await getAssetUrl(filePub[index]);

        valueObj = {
            cover: coverFile,
            pub: pubFile,
            title: fileTitle
        };
        mediaAssetList.push(valueObj);

        console.log(mediaAssetList);

    }

    Promise.all(mediaAssetList).then(function (values) {

        mediaAsset = values;

        console.log(mediaAsset);

        if (mediaAsset.includes(undefined) || mediaAsset.includes("") || mediaAsset.length === 0) {
            alert("You have not added anything");

            $("#upload_pubs").show();
            $("#loader3").hide();
            $("#ylinks_row").show();
            $("#photos_row").show();

            return;
        }

        saveLinksToDb("media_pubs", "mediaPubs", mediaAsset);

    });


});

async function getPreviousData(docString) {

    const promise = new Promise((resolve, reject) => {

        docRef.doc(docString).get().then(function (doc) {
            if (doc.exists) {

                resolve(doc.data());
            } else {
                console.log("No such document!");
            }
        }).catch(function (error) {
            console.log("Error getting document:", error);
        });
    });

    let result = await promise;

    prevDataList = result;

    return result;


}

async function saveLinksToDb(docString, objKey, objArray) {

    if (objArray.includes(undefined) || objArray.includes("") || objArray.length === 0) return alert("You have not added anything");

    prevDataList = await getPreviousData(docString);

    if (prevDataList[objKey] !== undefined) {
        prevDataList[objKey].forEach(element => {
            console.log(element);
            objArray.push(element);
        });
    }

    console.log(objArray);

    var obj = {};

    obj[objKey] = objArray;

    docRef.doc(docString).set(obj, {
            merge: true
        })
        .then(function () {
            console.log("Document successfully written!");

            alert("Media uploaded successfully");

            location.replace("media_photos.php");

        })
        .catch(function (error) {
            console.error("Error writing document: ", error);

            alert("Error Occured while trying to save to DB");

            location.replace("media_photos.php");
        });
}

async function getAssetUrl(element) {
    let promise = new Promise((resolve, reject) => {

        var thisRef = storageRef.child(element.name);

        thisRef.put(element).then(snapshot => {
                return snapshot.ref.getDownloadURL();
            })

            .then(downloadURL => {
                console.log(`Successfully uploaded file and got download link - ${downloadURL}`);

                resolve(downloadURL);
                // return downloadURL;
            })

            .catch(error => {
                console.log(`Failed to upload file and get link - ${error}`);
            });
    });

    let result = await promise;

    return result;
}


$("#upload_ylinks").click(function () {
    $("#upload_ylinks").hide();
    $("#loader2").show();
    $("#photos_row").hide();
    $("#pubs_row").hide();

    var ylinksList = $("#txtYLinks").val().split(',');

    console.log(ylinksList);

    if (ylinksList.includes(undefined) || ylinksList.includes("") || ylinksList.length === 0) {
        alert("You have not added anything");

        $("#upload_ylinks").show();
        $("#loader2").hide();
        $("#photos_row").show();
        $("#pubs_row").show();

        return;

    }

    saveLinksToDb("youtube_links", "ylinks", ylinksList);
});