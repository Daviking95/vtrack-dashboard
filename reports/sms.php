<!DOCTYPE html>
<html lang="en">

<?php $header_title = 'Reports | VTrack Reporting Dasboard - A concept of KDI';
include '../customs/app_head.php'
?>

<body>

    <?php include '../customs/app_topbar.php' ?>

    <div class="page-wrapper">
        <!-- Left Sidenav -->
        <?php include '../customs/app_sidenav.php' ?>
        <!-- end left-sidenav-->

        <!-- Page Content-->
        <div class="page-content">

            <div class="container-fluid">
                <!-- Page-Title -->
                <?php $page_title = 'SMS Reports';
include_once '../customs/app_page_title.php'?>
                <!-- end page title end breadcrumb -->

                <div class="row">
                    <div class="col-lg-12">
                        <!--begin::Portlet-->
                        <div
                            class="m-portlet m-portlet--mobile m-portlet--creative m-portlet--first m-portlet--bordered-semi">

                            <div class="m-portlet__body">
                                <!--begin: Datatable -->
                                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                                    <div class="row align-items-center">
                                        <div class="col-xl-8 order-2 order-xl-1">
                                            <div class="form-group m-form__group row align-items-center">
                                                <div class="col-md-4" hidden>
                                                    <div class="m-form__group m-form__group--inline">
                                                        <div class="m-form__label">
                                                            <label>
                                                                Status:
                                                            </label>
                                                        </div>
                                                        <div class="m-form__control">
                                                            <select class="form-control m-bootstrap-select"
                                                                id="m_form_status">
                                                                <option value="">
                                                                    All
                                                                </option>
                                                                <option value="false">
                                                                    Pending
                                                                </option>
                                                                <option value="true">
                                                                    Verified
                                                                </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none m--margin-bottom-10"></div>
                                                </div>
                                                <div class="col-md-4" hidden>
                                                    <div class="m-form__group m-form__group--inline">
                                                        <div class="m-form__label">
                                                            <label class="m-label m-label--single">
                                                                Type:
                                                            </label>
                                                        </div>
                                                        <div class="m-form__control">
                                                            <select class="form-control m-bootstrap-select"
                                                                id="m_form_type">
                                                                <option value="">
                                                                    All
                                                                </option>
                                                                <option value="1">
                                                                    Web
                                                                </option>
                                                                <option value="2">
                                                                    Mobile
                                                                </option>
                                                                <option value="3">
                                                                    SMS
                                                                </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none m--margin-bottom-10"></div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="m-input-icon m-input-icon--left">
                                                        <input type="text" class="form-control m-input m-input--solid"
                                                            placeholder="Search..." id="generalSearch">
                                                        <span class="m-input-icon__icon m-input-icon__icon--left">
                                                            <span>
                                                                <i class="la la-search"></i>
                                                            </span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Table -->
                                <div class="m_datatable_2" id="local_data"></div>
                                <!-- End Table -->
                            </div>
                        </div>
                        <!--end::Portlet-->
                    </div>
                </div>

            </div><!-- container -->

            <?php include_once '../customs/app_footer.php'?>
            <!--end footer-->
        </div>
        <!-- end page content -->
    </div>
    <!-- end page-wrapper -->

    <!-- jQuery  -->
    <?php include '../customs/app_js_files.php' ?>

    <!-- Custom Charts Functions js -->
    <script src="../assets/js/reportsController/smsReportsTable.js"></script>

</body>

</html>