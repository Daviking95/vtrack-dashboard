(function ($) {

  var getToken = localStorage.getItem("x-auth-token");

  GenderVictimArrayItem1 = [];
  GenderVictimArrayItem2 = [];
  GenderVictimArrayItem3 = [];
  GenderVictimArrayItem4 = [];

  "use strict";

  async function getAllFormsForViolenceVictimsPeopleAndGenderInvolved() {


    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("x-auth-token", getToken);

    var requestOptions = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };

    fetch("https://peaceful-atoll-23751.herokuapp.com/" + "just-clock-285910.uc.r.appspot.com/api/form/all", requestOptions)
      .then(function (response) {
        if (response.status === 403) {
          alert("Token expired. Login");
          window.location.href = "index.php";
        }
        if (!response.ok) {
          alert("Cannot login. Ensure your data is correct and you have internet connection");
          throw new Error("HTTP status " + response.status);
        }

        return response.json();
      })
      .then(function (result) {

        GenderVictimArrayItem1 = [];
        GenderVictimArrayItem2 = [];
        GenderVictimArrayItem3 = [];
        GenderVictimArrayItem4 = [];

        for (const violenceVictimPeopleAndGenderInvolved in result.details) {
          if (result.details.hasOwnProperty(violenceVictimPeopleAndGenderInvolved)) {
            const element = result.details[violenceVictimPeopleAndGenderInvolved];

            if (element.firstApproval === true && element.secondApproval === true) {
            fillInitiatorArrayWithResult(element);
            }

          }
        }

        showingChart();

      })
      .catch(error => console.log('error', error));

  }

  function fillInitiatorArrayWithResult(element) {

    let elementFinal = element.violenceVictimPeopleAndGenderInvolved.violenceVictimsGenderOfIncident.toLowerCase();

    switch (elementFinal) {
      case 'only men':
        GenderVictimArrayItem1.push(element);
        break;
      case 'only women':
        GenderVictimArrayItem2.push(element);
        break;
      case 'both men and women':
        GenderVictimArrayItem3.push(element);
        break;
      case 'unable to determine':
        GenderVictimArrayItem4.push(element);
        break;

      default:
        break;
    }

  }

  // ------------

  function showingChart() {
    var Data = [{
        label: "Only Men",
        value: GenderVictimArrayItem1.length
      },
      {
        label: "Only Women",
        value: GenderVictimArrayItem2.length
      },
      {
        label: "Both men and women",
        value: GenderVictimArrayItem3.length
      },
      {
        label: "Unable to Determine",
        value: GenderVictimArrayItem4.length
      }
    ];
    var total = 62;
    var browsersChart = Morris.Donut({
      element: 'donut-example',
      data: Data,
      formatter: function (value, data) {
        return value;
        return Math.floor(value / total * 100) + '%';
      }
    });

    browsersChart.options.data.forEach(function (label, i) {
      var legendItem = $('<span></span>').text(label['label'] + " ( " + label['value'] + " )").prepend('<br><span>&nbsp;</span>');
      legendItem.find('span')
        .css('backgroundColor', browsersChart.options.colors[i])
        .css('width', '20px')
        .css('display', 'inline-block')
        .css('margin', '5px');
      $('#legend').append(legendItem)
    });
  }

  //initializing
  async function init() {
    "use strict";
    await getAllFormsForViolenceVictimsPeopleAndGenderInvolved();
    // $.ChartJs.init()
  }

  init();

})(jQuery)