
    <!-- jQuery  -->
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/metisMenu.min.js"></script>
    <script src="assets/js/waves.min.js"></script>
    <script src="assets/js/jquery.slimscroll.min.js"></script>

    <!-- Parsley js -->
    <script src="assets/plugins/parsleyjs/parsley.min.js"></script>
    <script src="assets/pages/jquery.validation.init.js"></script>

    <script src="assets/js/jquery.core.js"></script>

    <!-- App js -->
    <script src="assets/js/app.js"></script>